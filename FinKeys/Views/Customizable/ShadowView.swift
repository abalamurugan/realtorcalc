//
//  ShadowView.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

@IBDesignable open class ShadowView: UIView {

    @IBInspectable open var cornerRadius: CGFloat = 2{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var borderWidth: CGFloat = 0 {
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var borderColor: UIColor? = UIColor.black{
        didSet{
            self.layoutSubviews()
        }
    }
    
    @IBInspectable open var shadowOffsetWidth: CGFloat = 0{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var shadowOffsetHeight: CGFloat = 3{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var shadowColor: UIColor? = UIColor.black{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var shadowOpacity: CGFloat = 0.5{
        didSet{
            self.layoutSubviews()
        }
    }
    
    /// enable IXCardView shadow
    @IBInspectable var masksToBounds = false
    
    public override init(frame:CGRect) {
        super.init(frame:frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override open  func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor?.cgColor
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = Float(shadowOpacity)
    }
    
}
@IBDesignable open class ShadowButton: UIButton {
    
    @IBInspectable open var cornerRadius: CGFloat = 2{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var borderWidth: CGFloat = 0 {
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var borderColor: UIColor? = UIColor.black{
        didSet{
            self.layoutSubviews()
        }
    }
    
    @IBInspectable open var shadowOffsetWidth: CGFloat = 0{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var shadowOffsetHeight: CGFloat = 3{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var shadowColor: UIColor? = UIColor.black{
        didSet{
            self.layoutSubviews()
        }
    }
    @IBInspectable open var shadowOpacity: CGFloat = 0.5{
        didSet{
            self.layoutSubviews()
        }
    }
    
    /// enable IXCardView shadow
    @IBInspectable var masksToBounds = false
    
    public override init(frame:CGRect) {
        super.init(frame:frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override open  func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor?.cgColor
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = Float(shadowOpacity)
    }
    
}
