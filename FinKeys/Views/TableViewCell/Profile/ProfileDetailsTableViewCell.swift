//
//  ProfileDetailsTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ProfileDetailsTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "ProfileDetailsTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(item: OutputDetailsModel){
        self.titleLabel.text = item.key
        if let value = item.value as? String, value != ""{
            self.descriptionLabel.text = value
            self.descriptionLabel.font = UIFont.helveticaNeue(size: 14, weight: .bold)
            self.descriptionLabel.textColor = UIColor.black
        }else{
            self.descriptionLabel.text = "Update"
            self.descriptionLabel.font = UIFont.helveticaNeue(size: 12, weight: .regular)
            self.descriptionLabel.textColor = UIColor.lightGray
        }
    }
    static var nib: UINib{
        return UINib.init(nibName: ProfileDetailsTableViewCell.reuseIdentifier, bundle: Bundle.main)
    }
}
