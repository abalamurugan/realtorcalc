//
//  ProfileHeaderTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ProfileHeaderTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "ProfileHeaderTableViewCell"
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.profileImageView.clipsToBounds = true
        self.profileImageView.layer.cornerRadius = profileImageView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib.init(nibName: ProfileHeaderTableViewCell.reuseIdentifier, bundle: Bundle.main)
    }
    
}
