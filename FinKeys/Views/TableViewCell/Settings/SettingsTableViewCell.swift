//
//  SettingsTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 05/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "SettingsTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(item: MenuItem){
        self.titleLabel.text = item.title
        self.iconImageView.image = item.icon
    }
    static var nib: UINib{
        return UINib.init(nibName: "SettingsTableViewCell", bundle: Bundle.main)
    }
}
