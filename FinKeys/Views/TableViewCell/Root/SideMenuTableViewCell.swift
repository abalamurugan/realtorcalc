//
//  SideMenuTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 14/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "SideMenuTableViewCell"
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var iconImageView : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
