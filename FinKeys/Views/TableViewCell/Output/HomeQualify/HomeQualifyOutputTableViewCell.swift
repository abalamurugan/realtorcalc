//
//  HomeQualifyOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 28/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class HomeQualifyOutputTableViewCell: UITableViewCell, CellReusable {
    static var reuseIdentifier: String = "HomeQualifyOutputTableViewCell"
    
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var detailButton : UIButton!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var priceDescriptionLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func set(_ item : HomeQualifyModel){
        self.titleLabel.text = item.title?.uppercased()
        self.amountLabel.text = item.price?.readableFormat
        self.priceDescriptionLabel.text = item.priceDescription
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
