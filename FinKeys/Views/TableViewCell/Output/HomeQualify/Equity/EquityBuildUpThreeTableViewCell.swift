//
//  EquityBuildUpThreeTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 05/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class EquityBuildUpThreeTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "EquityBuildUpThreeTableViewCell"
    
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var equityLabel: UILabel!
    @IBOutlet weak var equityBackgroundView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(_ item: EquityModel){
        valueLabel.text = item.value?.clean.readableFormat
        balanceLabel.text = item.balance?.clean.readableFormat
        equityLabel.text = item.equity?.clean.readableFormat
    }
    func configUI(indexPath: IndexPath){
        valueLabel.textColor = indexPath.section == 0 ? UIColor.white : UIColor.black
        balanceLabel.textColor = indexPath.section == 0 ? UIColor.white : UIColor.black
        equityLabel.textColor = indexPath.section == 0 ? UIColor.white : UIColor.black
        countLabel.textColor = indexPath.section == 0 ? UIColor.white : UIColor.lightGray
        valueLabel.textAlignment = indexPath.section == 0 ? NSTextAlignment.center : NSTextAlignment.right
        balanceLabel.textAlignment = indexPath.section == 0 ? NSTextAlignment.center : NSTextAlignment.right
        countLabel.textAlignment = indexPath.section == 0 ? NSTextAlignment.center : NSTextAlignment.right
        equityLabel.textAlignment = indexPath.section == 0 ? NSTextAlignment.center : NSTextAlignment.right
    }
    func setTitle(){
        countLabel.text = "Year"
        valueLabel.text = "Value"
        balanceLabel.text = "Mortgage"
        equityLabel.text = "Equity"
    }
    static var nib : UINib{
        return UINib.init(nibName: "EquityBuildUpThreeTableViewCell", bundle: Bundle.main)
    }
}
