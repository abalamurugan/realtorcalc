//
//  EquityBuildupTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 28/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class EquityBuildupTableViewCell: UITableViewCell, CellReusable {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var valueLabel : UILabel!
    @IBOutlet weak var equityBackgroundView : UIView!

    static var reuseIdentifier: String = "EquityBuildupTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item : OutputDetailsModel){
        titleLabel.text = item.key
        if let _price = item.value as? Double{
            valueLabel.text = _price.readableFormat
        }else if let _price = item.value as? String{
            valueLabel.text = _price
        }
    }
    
    static var nib : UINib{
        return UINib.init(nibName: "EquityBuildupTableViewCell", bundle: Bundle.main)
    }
    
}
