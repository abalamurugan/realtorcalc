//
//  ActionQualifyTableViewCell.swift
//  FinKeys
//
//  Created by Bala on 16/10/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ActionQualifyTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "ActionQualifyTableViewCell"
    
    @IBOutlet weak var firstMonthlyIncomeLabel: UILabel!
    @IBOutlet weak var firstMonthlyPaymentLabel: UILabel!
    @IBOutlet weak var secondMonthlyIncomeLabel: UILabel!
    @IBOutlet weak var secondMonthlyPaymentLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func set(item: ActionQualifyModel?){
        self.firstMonthlyIncomeLabel.text = item?.firstRaiseMonthlyIncome
        self.firstMonthlyPaymentLabel.text = item?.firstLowerMonthlyPayment
        self.secondMonthlyIncomeLabel.text = item?.secondRaiseMonthlyIncome
        self.secondMonthlyPaymentLabel.text = item?.secondLowerMonthlyPayment
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
