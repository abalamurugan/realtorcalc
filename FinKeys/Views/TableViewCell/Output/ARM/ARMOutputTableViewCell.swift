//
//  ARMOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 14/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ARMOutputTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "ARMOutputTableViewCell"
    
    @IBOutlet weak var periodPrincipalLabel : UILabel!
    @IBOutlet weak var fixedRateDiffLabel : UILabel!
    @IBOutlet weak var adjPeriodMonthLabel : UILabel!
    @IBOutlet weak var savingCostLabel : UILabel!
    @IBOutlet weak var cummulativeLabel : UILabel!
    @IBOutlet weak var interestLabel : UILabel!
    @IBOutlet weak var periodPrinTitleLabel : UILabel!
    @IBOutlet weak var fixedRateTitleLabel : UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item : ARMFixedOutputModel){
        periodPrincipalLabel.text = item.periodPmt?.readableFormat
//        fixedRateDiffLabel.text = item.fixedPMTDifference?.readableFormat
        adjPeriodMonthLabel.text = item.adjustablePeriod?.toString
        savingCostLabel.text = item.savingsPeriod?.readableFormat
        cummulativeLabel.text = item.cumulative?.readableFormat
        interestLabel.text = item.interestRate?.toPercentage
    }
}
class ARMBreakEvenPointTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "ARMBreakEvenPointTableViewCell"
    
    @IBOutlet weak var breakEvenLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
}
