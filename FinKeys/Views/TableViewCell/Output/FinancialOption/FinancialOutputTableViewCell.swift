//
//  FinancialOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancialOutputTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "FinancialOutputTableViewCell"
    
    @IBOutlet weak var principalInterestLabel : UILabel!
    @IBOutlet weak var insuranceEscrowLabel : UILabel!
    @IBOutlet weak var mtgEscrowLabel : UILabel!
    @IBOutlet weak var taxEscrowLabel : UILabel!
    @IBOutlet weak var hoaDueLabel : UILabel!
    @IBOutlet weak var totalAmountLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item : FinancialModel){
        principalInterestLabel.text = item.pmtPrincipal?.readableFormat
        insuranceEscrowLabel.text = item.insuranceEscrow?.readableFormat
        mtgEscrowLabel.text = item.mtgInsuranceEscrow?.readableFormat
        taxEscrowLabel.text = item.taxesEscrow?.readableFormat
        hoaDueLabel.text = item.hoaComboFee?.readableFormat
        totalAmountLabel.text = item.totalAmount?.readableFormat
    }
}
class FinancialCashOutputTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "FinancialCashOutputTableViewCell"
    
    @IBOutlet weak var approximateClosingLabel : UILabel!
    @IBOutlet weak var downPaymentLabel: UILabel!
    @IBOutlet weak var totalPaymentLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func set(_ item : FinancialModel){
        approximateClosingLabel.text = item.aproxClosingCost?.readableFormat
        downPaymentLabel.text = item.downPayment?.readableFormat
        totalPaymentLabel.text = item.totalCashClose?.readableFormat
    }
}
