//
//  BuyDownOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 02/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BuyDownOutputTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "BuyDownOutputTableViewCell"
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var buyerRateLabel: UILabel!
    @IBOutlet weak var totalPaymentLabel: UILabel!
    @IBOutlet weak var monthlySubsidyLabel: UILabel!
    @IBOutlet weak var buyerPaymentLabel: UILabel!
    @IBOutlet weak var yearSubsidyLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(_ item: BuyDownModel){
        yearLabel.text = item.year?.toString
        buyerRateLabel.text = item.buyerRate?.toPercentage
        totalPaymentLabel.text = item.totalPayment?.readableFormat
        monthlySubsidyLabel.text = item.monthlySubsidy?.roundUp
        buyerPaymentLabel.text = item.buyerPayment?.readableFormat
        yearSubsidyLabel.text = item.yearlySubsidy?.readableFormat
    }

}
