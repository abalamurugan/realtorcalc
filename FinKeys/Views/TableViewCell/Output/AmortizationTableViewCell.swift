//
//  AmortizationTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 29/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class AmortizationTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "AmortizationTableViewCell"
    @IBOutlet weak var loanBalanceLabel : UILabel!
    @IBOutlet weak var interestRateLabel : UILabel!
    @IBOutlet weak var totalPaymentLabel : UILabel!
    @IBOutlet weak var principalLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var extraLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item : AmortizationLoanDetailsModel){
        loanBalanceLabel.text = item.loanBalance?.readableFormat
        interestRateLabel.text = item.interest?.readableFormat
        totalPaymentLabel.text = item.paymentAmount?.readableFormat
        principalLabel.text = item.principal?.readableFormat
        extraLabel.text = item.extra?.readableFormat
        dateLabel.text = item.paymentDate?.toString(format: "MMM, YYYY")
    }
}
