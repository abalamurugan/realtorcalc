//
//  LoanSummaryTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LoanSummaryTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "LoanSummaryTableViewCell"
    
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var aprLabel: UILabel!
    @IBOutlet weak var principalPaidLabel: UILabel!
    @IBOutlet weak var interestPaidLabel: UILabel!
    @IBOutlet weak var financeAmountLabel: UILabel!
    @IBOutlet weak var cumPrincipalPaidLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item: LoanItems){
        interestLabel.text = item.rate?.toPercentage
        principalPaidLabel.text = item.principalPaid?.readableFormat
        interestPaidLabel.text = item.interestPaid?.readableFormat
        financeAmountLabel.text = item.financeAmount?.readableFormat
        cumPrincipalPaidLabel.text = item.cummPrincipalPaid?.readableFormat
        aprLabel.text = item.apr?.toPercentage
    }
    static var nib : UINib{
        return UINib.init(nibName: "LoanSummaryTableViewCell", bundle: Bundle.main)
    }
}
