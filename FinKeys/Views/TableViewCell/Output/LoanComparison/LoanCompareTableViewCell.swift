//
//  LoanCompareTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 25/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LoanCompareTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "LoanCompareTableViewCell"

    @IBOutlet weak var endOfYearLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var principalInterestLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var principalPaidLabel: UILabel!
    @IBOutlet weak var interestPaidLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var cumPrincipalPaidLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item: LoanItems){
        endOfYearLabel.text = item.endYear?.toString
        rateLabel.text = item.rate?.toPercentage
        principalInterestLabel.text = item.princleInterest?.readableFormat
        balanceLabel.text = item.balance?.readableFormat
        principalPaidLabel.text = item.principalPaid?.readableFormat
        interestPaidLabel.text = item.interestPaid?.readableFormat
        totalLabel.text = item.total?.readableFormat
        cumPrincipalPaidLabel.text = item.cummPrincipalPaid?.readableFormat
    }
    static var nib : UINib{
        return UINib.init(nibName: "LoanCompareTableViewCell", bundle: Bundle.main)
    }
}
