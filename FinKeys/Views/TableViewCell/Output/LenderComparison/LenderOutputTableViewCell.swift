//
//  LenderOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderOutputTableViewCell: UITableViewCell, CellReusable {
    static var reuseIdentifier: String = "LenderOutputTableViewCell"
    
    @IBOutlet weak var monthlyPaymentLabel: UILabel!
    @IBOutlet weak var interestCostLabel: UILabel!
    @IBOutlet weak var loanCostLabel: UILabel!
    @IBOutlet weak var aprLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item: LenderModel){
        self.monthlyPaymentLabel.text = item.monthlyPayment?.readableFormat
        self.interestCostLabel.text = item.interestCost?.readableFormat
        self.loanCostLabel.text = item.loanCost?.readableFormat
        self.aprLabel.text = item.apr?.roundUp
    }

}
