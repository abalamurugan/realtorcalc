//
//  RefinanceOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 12/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RefinanceOutputTableViewCell: UITableViewCell,CellReusable {
    
    static var reuseIdentifier: String = "RefinanceOutputTableViewCell"
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var valueLabel : UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func set(_ item : OutputDetailsModel){
        titleLabel.text = item.key
        if let _price = item.value as? Double{
            valueLabel.text = _price.readableFormat
        }else if let _price = item.value as? String{
            valueLabel.text = _price
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib : UINib{
        return UINib.init(nibName: "RefinanceOutputTableViewCell", bundle: Bundle.main)
    }
}
