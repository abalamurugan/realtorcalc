//
//  RefinanceHeaderView.swift
//  RealtorCalc
//
//  Created by Bala on 12/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RefinanceHeaderView: UITableViewHeaderFooterView, CellReusable {
    static var reuseIdentifier: String = "RefinanceHeaderView"
    
    
    @IBOutlet weak var titleLabel : UILabel!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    static var nib : UINib{
        return UINib.init(nibName: RefinanceHeaderView.reuseIdentifier, bundle: Bundle.main)
    
    }
}
