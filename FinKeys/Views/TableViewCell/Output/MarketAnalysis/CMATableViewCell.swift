//
//  CMATableViewCell.swift
//  FinKeys
//
//  Created by Bala on 24/11/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class CMATableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "CMATableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputValueLabel: UILabel!
    @IBOutlet weak var adjustValueLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item: OutputDetailsModel){
        titleLabel.text = item.key
        if let _value = item.value as? Double{
            let value = (item.isCurrency ?? false) ? _value.readableFormat : _value.readableWithoutCurrency
            inputValueLabel.text = value
        }else if let _value = item.value as? String{
            inputValueLabel.text = _value
        }
        if let _adj = item.valueTwo as? Double{
             adjustValueLabel.text = _adj.readableFormat
        }else if let _adj = item.value as? String{
            adjustValueLabel.text = _adj
        }
    }
    
    static var nib: UINib{
        return UINib.init(nibName: CMATableViewCell.reuseIdentifier, bundle: Bundle.main)
    }
}
