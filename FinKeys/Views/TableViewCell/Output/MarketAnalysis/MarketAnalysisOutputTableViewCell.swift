//
//  MarketAnalysisOutputTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 24/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MarketAnalysisOutputTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "MarketAnalysisOutputTableViewCell"
    
    @IBOutlet weak var listedPriceLabel: UILabel!
    @IBOutlet weak var salesPriceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var adjustedSalesPriceLabel: UILabel!
    @IBOutlet weak var adjustValueLabel: UILabel!
    @IBOutlet weak var proximityLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(_ item: MarketOutputModel){
        let listedPrice = item.listedPrice ?? 0
        let salesPrice = item.salesPrice ?? 0
        let adjustValue = item.adjustValue ?? 0
        listedPriceLabel.text = listedPrice.readableFormat
        salesPriceLabel.text = salesPrice.readableFormat
        addressLabel.text = item.address == "Enter your address" ? "" : item.address
        adjustValueLabel.text = adjustValue.readableFormat
        adjustedSalesPriceLabel.text = item.adjSalesPrice?.readableFormat
        proximityLabel.text = item.proximity
        titleLabel.text = item.title
    }

}
