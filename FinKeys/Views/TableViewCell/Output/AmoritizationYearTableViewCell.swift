//
//  AmoritizationYearTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class AmoritizationYearTableViewCell: UITableViewCell, CellReusable {
    static var reuseIdentifier: String = "AmoritizationYearTableViewCell"
    
    
    @IBOutlet weak var totalLoanBalanceLabel : UILabel!
    @IBOutlet weak var totalPaymentLabel : UILabel!
    @IBOutlet weak var interestSumLabel : UILabel!
    @IBOutlet weak var extraSumLabel : UILabel!
    @IBOutlet weak var principalSumLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class AmoritizationOutputTableViewCell: UITableViewCell, CellReusable {
    static var reuseIdentifier: String = "AmoritizationOutputTableViewCell"
    
    
    @IBOutlet weak var regularPmtLabel : UILabel!
    @IBOutlet weak var balanceWithExtraPmtLabel : UILabel!
    @IBOutlet weak var balanceWithNoExtraPmtLabel : UILabel!
    @IBOutlet weak var savingsOverPeriodLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
