//
//  ContactListTableViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 02/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SDWebImage

class ContactListTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "ContactListTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var moreButton: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contactImageView.clipsToBounds = true
        self.contactImageView.layer.cornerRadius = contactImageView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(_ item: ContactInfoModel){
        nameLabel.text = item.fullName
        titleLabel.text = item.title
        contactImageView.sd_setImage(with: item.imageURL, placeholderImage: UIImage.init(named: "placeholder"))
    }

}
