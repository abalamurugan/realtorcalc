//
//  FinancialKeysCollectionViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancialKeysCollectionViewCell: UICollectionViewCell, CellReusable {
    static var reuseIdentifier: String = "FinancialKeysCollectionViewCell"
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var financeImageView : UIImageView!
    @IBOutlet weak var imageBackgroundView : UIView!

    
    func set(_ item : FinancialKeyItem){
        titleLabel.text = item.title
        financeImageView.image = item.icon
        imageBackgroundView.layer.backgroundColor = item.bgColor.cgColor
    }
    
}
