//
//  FinancialKeyCollectionReusableView.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 04/09/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancialKeyCollectionReusableView: UICollectionReusableView, CellReusable {
    
    static var reuseIdentifier: String = "FinancialKeyCollectionReusableView"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerBackgroundView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib.init(nibName: "FinancialKeyCollectionReusableView", bundle: Bundle.main)
    }
}
