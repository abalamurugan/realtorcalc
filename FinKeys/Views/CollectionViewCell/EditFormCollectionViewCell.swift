//
//  EditFormCollectionViewCell.swift
//  RealtorCalc
//
//  Created by balamurugan on 10/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class EditFormCollectionViewCell: UICollectionViewCell, CellReusable {
    
    static var reuseIdentifier: String = "EditFormCollectionViewCell"
    
    @IBOutlet weak var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var nib: UINib{
        return UINib.init(nibName: EditFormCollectionViewCell.reuseIdentifier, bundle: Bundle.main)
    }
    
}
