//
//  DatePickerViewController.swift
//  GaadiBazaar
//
//  Created by Naveen on 04/07/18.
//  Copyright © 2018 Alex Appadurai. All rights reserved.
//

import UIKit

class DatePicker: UIViewController {

    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var completion: ((Date)-> Void)?
    
    var maxYearValue : Int = -2//minus two year from current year
    var minYearValue : Int = -60 //minus 60 years from current year so starting will be 1959
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let calendar = Calendar.current
        let component = maxYearValue == -2 ? Calendar.Component.day : Calendar.Component.year
        let twoDaysAgo = calendar.date(byAdding: component, value: maxYearValue, to: Date())
        let sixtyYearsAgo = calendar.date(byAdding: .year, value: minYearValue, to: Date())
//        datePicker.maximumDate = twoDaysAgo
        datePicker.minimumDate = sixtyYearsAgo
        // Do any additional setup after loading the view.
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            self.completion?(self.datePicker.date)
        }
        
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    class func show(_ controller: UIViewController,_ maxYear : Int?=nil, completion: @escaping ((Date)-> Void)) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "DatePicker") as! DatePicker
        vc.completion = completion
        if let max = maxYear{
            vc.maxYearValue = max
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        controller.present(vc, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
