//
//  BottomPickerTableViewCell.swift
//  RealtorCalc
//
//  Created by Bala on 16/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BottomPickerTableViewCell: UITableViewCell, CellReusable {
    
    static var reuseIdentifier: String = "BottomPickerTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(_ item: Any){
        if let _value = item as? String{
            self.titleLabel.text = _value
        }else if let model = item as? ACTypeModel{
            self.titleLabel.text = model.type
        }else if let model = item as? BottomPickerModel{
            self.titleLabel.text = model.key
        }else if let model = item as? DropDownInfoModel{
            self.titleLabel.text = model.name
        }
    }
}
