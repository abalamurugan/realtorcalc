//
//  ImagePickerView.swift
//  RealtorCalc
//
//  Created by Bala on 14/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ImagePickerView: UIView {
    
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var proximityTextField: UITextField!
    @IBOutlet weak var addImageView: UIImageView!
    @IBOutlet weak var propertyImageButton: UIButton!
    @IBOutlet weak var contentView: UIView!

    private var imageActionCompletion: ((Bool)->Void)?
    
    let placeholderAddress = "Enter your address"

    var image: UIImage?{
        didSet{
            propertyImageButton.setImage(image, for: .normal)
        }
    }
    var imageURL: URL?{
        didSet{
            propertyImageButton.sd_setImage(with: self.imageURL, for: .normal, completed: nil)
        }
    }
    var address: String?{
        didSet{
            addressTextView.text = (self.address == nil || self.address == "") ? placeholderAddress : self.address
            addressTextView.textColor = addressTextView.text != placeholderAddress ? UIColor.black : UIColor.lightGray
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.addressTextView.delegate = self
    }
    private func setupUI(){
        addressTextView.text = placeholderAddress
        addressTextView.layer.cornerRadius = 5
        addressTextView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        addressTextView.textColor = UIColor.lightGray
        addressTextView.layer.borderWidth = 1
        propertyImageButton.clipsToBounds = true
        propertyImageButton.layer.cornerRadius = 5
        self.addImageView.tintColor = UIColor.lightGray
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame : CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }
    private func configureUI(){
        Bundle.main.loadNibNamed("ImagePickerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight] //Flexible to parent view
    }
    func imagePickerCallBack(_ sender: @escaping((Bool)->Void)){
        self.imageActionCompletion = sender
    }
    @IBAction private func propertyImageAction(_ sender: UIButton){
        self.imageActionCompletion?(true)
    }
}
extension ImagePickerView: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        addressTextView.text = ""
        addressTextView.textColor = UIColor.black
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        let text = textView.text.replacingOccurrences(of: " ", with: "")
        if text == ""{
            addressTextView.text = placeholderAddress
            addressTextView.textColor = UIColor.lightGray
        }
    }
}
