//
//  EditFormView.swift
//  RealtorCalc
//
//  Created by balamurugan on 10/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
protocol EditFormViewDelegate {
    func didFormSelected(_ index: Int)
}
class EditFormView: UIView {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var contentView: UIView!
    
    var delegate: EditFormViewDelegate?
    
    var items: Array<Any>?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(EditFormCollectionViewCell.nib, forCellWithReuseIdentifier: EditFormCollectionViewCell.reuseIdentifier)
    }
    override init(frame : CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    private func setupUI(){
        Bundle.main.loadNibNamed("EditFormView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight] //Flexible to parent view
    }

}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension EditFormView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EditFormCollectionViewCell.reuseIdentifier, for: indexPath) as! EditFormCollectionViewCell
        cell.numberLabel.text = "\(indexPath.row + 1)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 80, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didFormSelected(indexPath.row)
    }
}
