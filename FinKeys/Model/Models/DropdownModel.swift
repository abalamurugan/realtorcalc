//
//  DropdownModel.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 06/09/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class DropdownModel: BaseModel {
    var response: DropdownDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(DropdownDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class DropdownDetailModel: Codable{
    var data: Array<DropDownInfoModel>?
    private enum CodingKeys: String, CodingKey {
        case data
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decode(Array<DropDownInfoModel>.self, forKey: .data)
    }
}
class DropDownInfoModel: Codable, Equatable{
    init() {
        
    }
    init(value: String) {
        self.value = value
    }
    var name: String?
    var value: String?
    static func == (lhs: DropDownInfoModel, rhs: DropDownInfoModel)->Bool{
        return lhs.value == rhs.value
    }
}
