//
//  PrintPaymentModel.swift
//  FinKeys
//
//  Created by Bala on 14/12/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class PrintPaymentModel: BaseModel {
    var response: PaymentResponseModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(PaymentResponseModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class PaymentResponseModel: Codable{
    var userId: String?
    var paymentStatus: String?
    var isPrintAvailable: String?
}
