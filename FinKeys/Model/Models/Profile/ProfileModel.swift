//
//  ProfileModel.swift
//  RealtorCalc
//
//  Created by Bala on 27/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class ProfileModel: BaseModel{
    var response: UserDetailsModel?
    
    var fullName: String{
        return (response?.name ?? "") + " " + (response?.lastname ?? "")
    }
    var location: String{
        return (response?.city ?? "-") + ", " + (response?.state ?? "")
    }
    var imageURL: URL?{ 
        if let imgPath = response?.photo{
            return URL.init(string: Config.basePath + imgPath)
        }
        return nil
    }
    private enum CodingKeys: String, CodingKey {
        case response
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(UserDetailsModel.self, forKey: .response)
        try super.init(from: decoder)
    }
        
}

