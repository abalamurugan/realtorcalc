//
//  UserDetails.swift
//  RealtorCalc
//
//  Created by Bala on 16/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class UserDetailsModel: Codable {
    var userid: String?
    var name: String?
    var lastname: String?
    var email: String?
    
    var title: String?
    var user_pass: String?
    var company_name: String?
    var cellNo: String?
    var phone: String?
    var fax: String?
    var address: String?
    var city: String?
    var state: String?
    var zip: String?
    var photo: String?
    var paymentStatus: String?
    var mlsId: String?
    var verifyStatus : String?

    init() {
        
    }
    init(dict: Dictionary<String, Any>) {
        if let response = dict["response"] as? Dictionary<String, Any>{
            self.userid = response["userid"] as? String
            self.name = response["name"] as? String
            self.lastname = response["lastname"] as? String
            self.email = response["email"] as? String
            self.verifyStatus = response["verifyStatus"] as? String
            self.mlsId = response["mlsId"] as? String
            self.paymentStatus = response["paymentStatus"] as? String

        }
    }
}
class UserManager {
    
    static let instance = UserManager()
    
    var userModel: UserDetailsModel?
    
    var isLogged: Bool{
        return userModel != nil ? true : false
    }
    var userID: String{
        return userModel!.userid!
    }
}

