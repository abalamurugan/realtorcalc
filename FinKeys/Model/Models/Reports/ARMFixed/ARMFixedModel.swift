//
//  ARMFixedModel.swift
//  RealtorCalc
//
//  Created by Bala on 13/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class ARMFixedModel: BaseModel {
    var response: ARMFixedDetailsModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(ARMFixedDetailsModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class ARMFixedDetailsModel: Codable {
    var userId: String?
    var mortgageAmt: String?
    var periodicRate: String?
    var interestRate: String?
    var loanTermFix: String?
    var loanTermAdj: String?
    var payPerYrFix: String?
    var payPerYrAdj : String?
    var holdingPeriodFix: String?
    var holdingPeriodAdj: String?
    var periodicIntCap: String?
    var lifeTimeRateCap: String?
    var savingPerMonth: String?
    var pmtFix: String?
    var pmtAdj: String?
    var createdDate: String?

}
