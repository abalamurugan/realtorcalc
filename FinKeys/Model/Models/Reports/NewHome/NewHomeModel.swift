//
//  NewHomeModel.swift
//  RealtorCalc
//
//  Created by Bala on 16/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class NewHomeModel: BaseModel {
    var response: NewHomeDetailsModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(NewHomeDetailsModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class NewHomeDetailsModel: Codable {
    var userId: String?
    var contactId: String?
    var buyer1_income: String?
    var buyer2_income: String?
    var other_income: String?
    var non_taxable_income: String?
    var buyer_month_debt: String?
    var buyer_emp_status: String?
    var spouse_emp_status: String?
    var purchase_price: String?
    var down_pay: String?
    var nd_mort_amt: String?
    var ins_fund_fee: String?
    var real_estate_tax: String?
    var hazard_Ins: String?
    var mort_ins: String?
    var conda_hoa_fee: String?
    var nd_mort_pay: String?
    var loan_plan: String?
    var payment_type: String?
    var interest_rate: String?
    var tot_payment: String?
    var pay_per_year: String?
    var income_ratio: String?
    var debt_ratio: String?
    var discount_point: String?
    var pre_paids: String?
    var title_record_misc: String?
    var mort_ins_fund_fee: String?
    var downPay1: String?
    var assAppr: String?
    var createdDate: String?
    var loan_org_fee: String?
    

}
