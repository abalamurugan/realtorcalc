//
//  LoanComparisonModel.swift
//  RealtorCalc
//
//  Created by Bala on 12/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class LoanComparisonModel: BaseModel {
    var response: LoanComparisonDetails?
    private enum CodingKeys: String, CodingKey{
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(LoanComparisonDetails.self, forKey: .response)
        try super.init(from: decoder)
    }
    
}
class LoanComparisonDetails: Codable {
    var userId: String?
    var contactId: String?
    var subjectLoan_info: LoanComparisonSubInfoDetails?
    var comparsionLoan_info: LoanComparisonInfoDetails?
    var holdingTerm: String?
    var createdDate: String?
    
    private enum CodingKeys: String, CodingKey {
        case subjectLoan_info
        case comparsionLoan_info
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.subjectLoan_info = try container.decode(LoanComparisonSubInfoDetails.self, forKey: .subjectLoan_info)
        self.comparsionLoan_info = try container.decode(LoanComparisonInfoDetails.self, forKey: .comparsionLoan_info)
    }
}
class LoanComparisonInfoDetails: Codable{
    var loanPlanC: String?
    var paymentTypeC: String?
    var InterestRateC: String?
    var mtgAmountC: String?
    var mtgTermC: String?
    var payPerYrC: String?
    var extraPaymentC: String?
    var financeCostC: String?
}
class LoanComparisonSubInfoDetails: Codable{
    var loanPlanS: String?
    var paymentTypeS: String?
    var InterestRateS: String?
    var mtgAmountS: String?
    var mtgTermS: String?
    var payPerYrS: String?
    var extraPaymentS: String?
    var financeCostS: String?
}
