//
//  AmortizationViewModel.swift
//  RealtorCalc
//
//  Created by Bala on 04/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class AmortizationViewModel: BaseModel {
    var response: AmortizationDetailsModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(AmortizationDetailsModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class AmortizationDetailsModel: Codable{
    var userId: String?
    var contactId: String?
    var mtgAmount: String?
    var paymentType: String?
    var loanType: String?
    var interestRate: String?
    var lifeTimeRate: String?
    var loanTerm: String?
    var payPerYr: String?
    var paymentMade: String?
    var firstPay: String?
    var extraPrincipal: String?
    var createdDate: String?
    var periodicInterest: String?
}
