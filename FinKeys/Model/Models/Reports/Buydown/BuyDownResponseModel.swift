//
//  BuyDownResponseModel.swift
//  RealtorCalc
//
//  Created by Bala on 16/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class BuyDownResponseModel: BaseModel {
    var response: BuyDownDetailsModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(BuyDownDetailsModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class BuyDownDetailsModel: Codable{
    var userId: String?
    var salespricefix: String?
    var downpaymentfix: String?
    var loanamountfix: String?
    var baseinterestfix: String?
    var baseloantermfix: String?
    var noofpmtfix: String?
    var initialinterestfix: String?
    var subsidytermfix: String?
    var subsidypmtfix: String?
    var subsidypercentfix: String?
    var salespricegrt: String?
    var downpaymentgrt: String?
    var loanamountgrt: String?
    var baseinterestgrt: String?
    var baseloanTermgrt: String?
    var noofpmtgrt: String?
    var initialinterestgrt: String?
    var yearlyincreasegrt: String?
    var subsidytermgrt: String?
    var maxloanIncr: String?
    var subsidypercentadj: String?
    var createddate: String?
}
