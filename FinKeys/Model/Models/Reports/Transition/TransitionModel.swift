//
//  TransitionModel.swift
//  RealtorCalc
//
//  Created by Bala on 13/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class TransitionModel: BaseModel{
    var response: TransitionDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(TransitionDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class TransitionDetailModel: Codable {
    var userId: String?
    var contactId: String?
    var current_home_net: TransitionCurrentModel?
    var new_home_net: TransitionNewModel?
    var createdDate: String?
    private enum CodingKeys: String, CodingKey {
        case current_home_net
        case new_home_net
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.current_home_net = try container.decode(TransitionCurrentModel.self, forKey: .current_home_net)
        self.new_home_net = try container.decode(TransitionNewModel.self, forKey: .new_home_net)
    }

}
class TransitionCurrentModel: Codable {
    var salesPrice: String?
    var stMtgBalance: String?
    var ndMtgBalance: String?
    var realEstateComm: String?
    var titleRecordMisc: String?
    var ownerCarryBack: String?
    var commisionRate: String?
    var savingInterest: String?
    var yearlyApprec: String?
    var homeOwnerTax: String?
    var loanPlan: String?
    var paymentType: String?
    var interestRate: String?
    var loanTerm: String?
    var payPerYear: String?
    var monthlyTaxes: String?
    var monthlyIns: String?
    var monthlyMtgIns: String?
    var monthlyHoaDue: String?
    var monthlyNdMtgPmt: String?

}
class TransitionNewModel: Codable {
    var purchasePrice: String?
    var stMtgAmount: String?
    var ndMtgAmount: String?
    var loanFeesCharges: String?
    var prepaidCharges: String?
    var titleRecordMisc: String?
    var cashEquity: String?
    var yearlyApprec: String?
    var loanPlan: String?
    var paymentType: String?
    var interestRate: String?
    var loanTerm: String?
    var payPerYear: String?
    var monthlyTaxes: String?
    var monthlyIns: String?
    var monthlyMtgIns: String?
    var monthlyHoaDue: String?
    var monthlyNdMtgPmt: String?
    
}
