//
//  MaxQualificationModel.swift
//  RealtorCalc
//
//  Created by Bala on 18/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class MaxQualificationModel: BaseModel {
    var response: MaxQualificationDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(MaxQualificationDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class MaxQualificationDetailModel: Codable {
    var userId: String?
    var buyer1Income: String?
    var buyer2Income: String?
    var otherIncome: String?
    var nonTaxableIncome: String?
    var buyerMonthDebt: String?
    var buyerEmpStatus: String?
    var spouseEmpStatus: String?
    var stRatio: String?
    var ndRatio: String?
    var realEstateTaxes: String?
    var hazardInsurance: String?
    var mortInsurance: String?
    var hoaFees: String?
    var loanOriginFees: String?
    var discountPoints: String?
    var prePaids: String?
    var titleRecordMisc: String?
    var mtgInsFunding: String?
    var homeWarranty: String?
    var sellerPaid: String?
    var downPayment: String?
    var availableFunds: String?
    var varaiableClosing: String?
    var minDownPmt: String?
    var annualAppreciation: String?
    var loanPlan: String?
    var paymenttype: String?
    var interestRate: String?
    var loanTerm: String?
    var payPerYr: String?
    var createddate: String?
    

}
