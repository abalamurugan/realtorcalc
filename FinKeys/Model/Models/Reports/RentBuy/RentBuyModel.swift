//
//  RentBuyModel.swift
//  RealtorCalc
//
//  Created by Bala on 12/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class RentBuyModel: BaseModel {
    var response: RentBuyDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(RentBuyDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class RentBuyDetailModel: Codable{
    var userId: String?
    var contactId: String?
    var savingAccount: String?
    var otherLiquidAss: String?
    var familyAss: String?
    var debtsLiquidate: String?
    var saveCushionNeed: String?
    var investInterestRate: String?
    var homeOwnerTax: String?
    var purchasePrice: String?
    var loanPlan: String?
    var paymentType: String?
    var miniDownPay: String?
    var downPayAmount: String?
    var ndmtgAmt: String?
    var loanFeesCharge: String?
    var prepaids: String?
    var titleRecord: String?
    var yearlyApprec: String?
    var currentMonthlyRent: String?
    var hoaConda: String?
    var homeOwnerInsur: String?
    var interestRate: String?
    var loanTerm: String?
    var payPerYear: String?
    var monthlyTaxes: String?
    var monthlyInsur: String?
    var monthlyMtgInsur: String?
    var monthlyHoa: String?
    var monthlyNdMtgPmt: String?
    var createdDate: String?
 }
