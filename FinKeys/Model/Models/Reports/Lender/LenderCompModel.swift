//
//  LenderCompModel.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 26/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class LenderCompModel: BaseModel {
    var response: LenderInfoModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(LenderInfoModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class LenderInfoModel: Codable{
    var lender1Mtg1: LenderMtgModel?
    var lender1Mtg2: LenderMtgModel?
    var lender1Mtg3: LenderMtgModel?
    var lender2Mtg1: LenderMtgModel?
    var lender2Mtg2: LenderMtgModel?
    var lender2Mtg3: LenderMtgModel?
    var lender1: LenderDetailModel?
    var lender2: LenderDetailModel?
    var createdDate: String?
    private enum CodingKeys: String, CodingKey {
        case lender1Mtg1
        case lender1Mtg2
        case lender1Mtg3
        case lender2Mtg1
        case lender2Mtg2
        case lender2Mtg3
        case lender1
        case lender2
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lender1Mtg1 = try container.decode(LenderMtgModel.self, forKey: .lender1Mtg1)
        self.lender1Mtg2 = try container.decode(LenderMtgModel.self, forKey: .lender1Mtg2)
        self.lender1Mtg3 = try container.decode(LenderMtgModel.self, forKey: .lender1Mtg3)
        self.lender2Mtg1 = try container.decode(LenderMtgModel.self, forKey: .lender2Mtg1)
        self.lender2Mtg2 = try container.decode(LenderMtgModel.self, forKey: .lender2Mtg2)
        self.lender2Mtg3 = try container.decode(LenderMtgModel.self, forKey: .lender2Mtg3)
        self.lender1 = try container.decode(LenderDetailModel.self, forKey: .lender1)
        self.lender2 = try container.decode(LenderDetailModel.self, forKey: .lender2)

    }
}
class LenderMtgModel: Codable {
    var mtg: String?
    var loanType: String?
    var paymentType: String?
    var loanAmount: String?
    var interestRate: String?
    var amortTerm: String?
    var payPerYear: String?
    var maxLoanToValue: String?
    var discountPoints: String?
    var applicationFee: String?
    var ownerTitlePolicy: String?
    var lenderTitlePolicy: String?
    var originFee: String?
    var commitFee: String?
    var settleFee: String?
    var processFee: String?
    var underWriteFee: String?
    var apprFee: String?
    var miscelFee: String?
    var assumeWQualify: String?
    var assumeWOQualify: String?
    var mtgInsurance: String?
    var adjustPeriod: String?
    var periodicIntCap: String?
    var lifeTimeIntCap: String?
}
class LenderDetailModel: Codable {
    var lender: String?
    var phone: String?
    var officer: String?
}
