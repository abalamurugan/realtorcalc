//
//  PDFModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class PDFModel: BaseModel {
    
    var response: PDFInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case response
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(PDFInfoModel.self, forKey: .response)
        try super.init(from: decoder)
    }
    var pdfURL: String?{
        return response?.REPORTNAME
    }
}

class PDFInfoModel: Codable{
    var REPORTNAME: String?
    var contactId: String?
    var payment_status: String?
}

