//
//  MarketAnalysisModel.swift
//  RealtorCalc
//
//  Created by Bala on 18/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class MarketAnalysisModel: BaseModel {
    var response: MarketAnalysisDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(MarketAnalysisDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class MarketAnalysisDetailModel: Codable {
    private enum CodingKeys: String, CodingKey {
        case subject_info,comparable1_info,comparable2_info, comparable3_info
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.subject_info = try container.decode(SubjectInfoModel?.self, forKey: .subject_info)
        self.comparable1_info = try container.decode(ComparableOneModel?.self, forKey: .comparable1_info)
        self.comparable2_info = try container.decode(ComparableTwoModel?.self, forKey: .comparable2_info)
        self.comparable3_info = try container.decode(ComparableThreeModel?.self, forKey: .comparable3_info)

    }
    var subject_info: SubjectInfoModel?
    var comparable1_info: ComparableOneModel?
    var comparable2_info: ComparableTwoModel?
    var comparable3_info: ComparableThreeModel?
    
}
class SubjectInfoModel: Codable {
    var statusS: String?
    var styleS: String?
    var yearBuiltS: String?
    var acresS: String?
    var landscapeS: String?
    var lotValueS: String?
    var garagev: String?
    var carportS: String?
    var sqFtTotalS: String?
    var sqFtMainS: String?
    var sqFtUpS: String?
    var sqFtDnS: String?
    var finishDnS: String?
    var bedsTotalS: String?
    var bedsMainS: String?
    var bedsUpS: String?
    var bedsDnS: String?
    var bathsTotS: String?
    var bathsMainS: String?
    var bathsUpS: String?
    var bathsDnS: String?
    var firePlacesS: String?
    var acS: String?
    var secSystS: String?
    var conditionsS: String?
    var poolS: String?
    var hotTubS: String?
    var viewS: String?
    var addressS: String?
    var subjectPhoto: String?
    var MLS: String?
    var parcel: String?
    var stFullBathS: String?
    var stTHBathS: String?
    var stHFBathS: String?
    var ndFullBathS: String?
    var ndTHBathS: String?
    var ndHFBathS: String?
    var baseFullBathS: String?
    var baseTFBathS: String?
    var baseHFBathS: String?
    var imageURL: URL?{
        if let imgPath = self.subjectPhoto, imgPath != ""{
            return URL.init(string: Config.basePath + imgPath)
        }
        return nil
    }

}
class ComparableOneModel: Codable {
    var proximityC1: String?
    var statusC1: String?
    var soldC1: String?
    var dateSoldC1: String?
    var styleC1: String?
    var yearBuiltC1: String?
    var acresC1: String?
    var landscapeC1: String?
    var lotValueC1: String?
    var garageC1: String?
    var carportC1: String?
    var sqFtTotalC1: String?
    var sqFtMainC1: String?
    var sqFtUpC1: String?
    var sqFtDnC1: String?
    var finishDnC1: String?
    var bedsTotalC1: String?
    var bedsMainC1: String?
    var bedsUpC1: String?
    var bedsDnC1: String?
    var bathsTotC1: String?
    var bathsMainC1: String?
    var bathsUpC1: String?
    var bathsDnC1: String?
    var firePlacesC1: String?
    var acC1: String?
    var acAdjC1: String?
    var secSystC1: String?
    var secSystAdjC1: String?
    var conditionsC1: String?
    var conditionsAdjC1: String?
    var poolC1: String?
    var poolAdjC1: String?
    var hotTubAdjC1: String?
    var hotTubC1: String?
    var viewC1: String?
    var viewAdjC1: String?
    var concesC1: String?
    var addressC1: String?
    var photoC1: String?
    var listedC1: String?
    var MLSC1: String?
    var parcelC1: String?
    var stFullBathC1: String?
    var stTHBathC1: String?
    var stHFBathC1: String?
    var ndFullBathC1: String?
    var ndTHBathC1: String?
    var ndHFBathC1: String?
    var baseFullBathC1: String?
    var baseTFBathC1: String?
    var baseHFBathC1: String?
    var imageURL: URL?{
        if let imgPath = self.photoC1, imgPath != ""{
            return URL.init(string: Config.basePath + imgPath)
        }
        return nil
    }
}
class ComparableTwoModel: Codable {
    var proximityC2: String?
    var statusC2: String?
    var soldC2: String?
    var dateSoldC2: String?
    var styleC2: String?
    var yearBuiltC2: String?
    var acresC2: String?
    var landscapeC2: String?
    var lotValueC2: String?
    var garageC2: String?
    var carportC2: String?
    var sqFtTotalC2: String?
    var sqFtMainC2: String?
    var sqFtUpC2: String?
    var sqFtDnC2: String?
    var finishDnC2: String?
    var bedsTotalC2: String?
    var bedsMainC2: String?
    var bedsUpC2: String?
    var bedsDnC2: String?
    var bathsTotC2: String?
    var bathsMainC2: String?
    var bathsUpC2: String?
    var bathsDnC2: String?
    var firePlacesC2: String?
    var acC2: String?
    var acAdjC2: String?
    var secSystC2: String?
    var secSystAdjC2: String?
    var conditionsC2: String?
    var conditionsAdjC2: String?
    var poolC2: String?
    var poolAdjC2: String?
    var hotTubAdjC2: String?
    var hotTubC2: String?
    var viewC2: String?
    var viewAdjC2: String?
    var concesC2: String?
    var addressC2: String?
    var photoC2: String?
    var listedC2: String?
    var MLSC2: String?
    var parcelC2: String?
    var stFullBathC2: String?
    var stTHBathC2: String?
    var stHFBathC2: String?
    var ndFullBathC2: String?
    var ndTHBathC2: String?
    var ndHFBathC2: String?
    var baseFullBathC2: String?
    var baseTFBathC2: String?
    var baseHFBathC2: String?
    var imageURL: URL?{
        if let imgPath = self.photoC2, imgPath != ""{
            return URL.init(string: Config.basePath + imgPath)
        }
        return nil
    }
}
class ComparableThreeModel: Codable {
    var proximityC3: String?
    var statusC3: String?
    var soldC3: String?
    var dateSoldC3: String?
    var styleC3: String?
    var yearBuiltC3: String?
    var acresC3: String?
    var landscapeC3: String?
    var lotValueC3: String?
    var garageC3: String?
    var carportC3: String?
    var sqFtTotalC3: String?
    var sqFtMainC3: String?
    var sqFtUpC3: String?
    var sqFtDnC3: String?
    var finishDnC3: String?
    var bedsTotalC3: String?
    var bedsMainC3: String?
    var bedsUpC3: String?
    var bedsDnC3: String?
    var bathsTotC3: String?
    var bathsMainC3: String?
    var bathsUpC3: String?
    var bathsDnC3: String?
    var firePlacesC3: String?
    var acC3: String?
    var acAdjC3: String?
    var secSystC3: String?
    var secSystAdjC3: String?
    var conditionsC3: String?
    var conditionsAdjC3: String?
    var poolC3: String?
    var poolAdjC3: String?
    var hotTubAdjC3: String?
    var hotTubC3: String?
    var viewC3: String?
    var viewAdjC3: String?
    var concesC3: String?
    var addressC3: String?
    var photoC3: String?
    var listedC3: String?
    var MLSC3: String?
    var parcelC3: String?
    var stFullBathC3: String?
    var stTHBathC3: String?
    var stHFBathC3: String?
    var ndFullBathC3: String?
    var ndTHBathC3: String?
    var ndHFBathC3: String?
    var baseFullBathC3: String?
    var baseTFBathC3: String?
    var baseHFBathC3: String?
    var imageURL: URL?{
        if let imgPath = self.photoC3, imgPath != ""{
            return URL.init(string: Config.basePath + imgPath)
        }
        return nil
    }
}
