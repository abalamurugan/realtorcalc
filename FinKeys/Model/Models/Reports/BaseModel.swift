//
//  BaseModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class BaseModel: Codable{
    var success: Int?
    var error: Int?
    var success_message: String?
    var error_message: String?
}
