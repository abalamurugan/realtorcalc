//
//  FinancingOption.swift
//  RealtorCalc
//
//  Created by Bala on 12/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class FinancingOption: BaseModel {
    var response: FinancingDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(FinancingDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class FinancingDetailModel: Codable{
    var option1: FinancingOptionsDetails?
    var option2: FinancingOptionsDetails?
    var option3: FinancingOptionsDetails?
    var createdDate: String?
    
    private enum CodingKeys: String, CodingKey {
        case option1
        case option2
        case option3
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.option1 = try container.decode(FinancingOptionsDetails.self, forKey: .option1)
        self.option2 = try container.decode(FinancingOptionsDetails.self, forKey: .option2)
        self.option3 = try container.decode(FinancingOptionsDetails.self, forKey: .option3)
    }
}
class FinancingOptionsDetails: Codable{
    var loanPlan: String?
    var paymentType: String?
    var interestRate: String?
    var loanTerm: String?
    var payPerYr: String?
    var taxEscrow: String?
    var insEscrow: String?
    var mtgInsPmt: String?
    var hoaCondoFees: String?
    var purchasePrice: String?
    var stMtgAmt: String?
    var ndMtgAmt: String?
    var loanOrigDisc: String?
    var prePaids: String?
    var titleRecordMisc: String?
    var mortInsurance: String?
    var minimumDown: String?
    var downPayment: String?
}
