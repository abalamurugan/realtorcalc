//
//  InvestmentsModel.swift
//  RealtorCalc
//
//  Created by Bala on 18/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class InvestmentsModel: BaseModel {
    var response: InvestmentDetailsModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(InvestmentDetailsModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class InvestmentDetailsModel: Codable {
    var userId: String?
    var contactId: String?
    var salesPrice: String?
    var stMortgage: String?
    var ndMortgage: String?
    var stTerm: String?
    var ndTerm: String?
    var stPmtYr: String?
    var ndPmtYr: String?
    var stRate: String?
    var ndRate: String?
    var monthlyRents: String?
    var yearlyRents: String?
    var holdPerYrs: String?
    var purchaseCosts: String?
    var vacancy: String?
    var improvement: String?
    var invTaxBracket: String?
    var apprecRate: String?
    var rentalApprec: String?
    var expenseApprec: String?
    var projSaleCosts: String?
    var taxOnDep: String?
    var captialGainTax: String?
    var propertyTaxes: String?
    var insurance: String?
    var managementFee: String?
    var maintenance: String?
    var utilities: String?
    var advertising: String?
    var otherExpenses: String?
    var capitalImprovements: String?
    var safeRate: String?
    var befTaxInvRate: String?
    var createdDate: String?
}
