//
//  RefinanceModel.swift
//  RealtorCalc
//
//  Created by Bala on 13/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class RefinanceModel: BaseModel {
    var response: RefinanceDetailModel?
    private enum CodingKeys: String, CodingKey {
        case response
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.response = try container.decode(RefinanceDetailModel.self, forKey: .response)
        try super.init(from: decoder)
    }
}
class RefinanceDetailModel: Codable{
    var userId: String?
    var contactId: String?
    var originalBalance: String?
    var interestRateE: String?
    var loanTerm: String?
    var payPerYear: String?
    var monthsPaidMortgage: String?
    var holdingPeriod: String?
    var currentBalance: String?
    var interestArrears: String?
    var interestRateN: String?
    var mortgageTerm: String?
    var closingCostFinanced: String?
    var closingCostUpFront: String?
    var excessFunds: String?
    var interestAccumulated: String?
    var interestLost: String?
    var fundsInvested: String?
    var createdDate: String?
    var payPerYearN: String?
}
