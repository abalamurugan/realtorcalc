//
//  ContactModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 02/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class ContactModel: Codable{
    var success: Int?
    var error: Int?
    var success_message: String?
    var response: [ContactInfoModel]?
}
class ContactInfoModel: Codable{
    var userID: String?
    var contactId: String?
    var photo: String?
    var title: String?
    var firstname: String?
    var lastname: String?
    var companyname: String?
    var address: String?
    var city: String?
    var state: String?
    var notes: String?
    var cellNo: String?
    var phone: String?
    var fax: String?
    var emailid: String?
    
    var fullName: String{
        return (firstname ?? "") + " " + (lastname ?? "")
    }
    var imageURL: URL?{
        if let imgPath = photo{
            return URL.init(string: Config.basePath + imgPath)
        }
        return nil
    }
}
