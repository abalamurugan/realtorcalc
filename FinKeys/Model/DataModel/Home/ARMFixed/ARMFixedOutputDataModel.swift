//
//  ARMFixedOutputDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 20/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class ARMFixedOutputDataModel {
    
    var armFixedOutputList : Array<ARMFixedOutputModel>?
    
    var breakEvenPoint: String?
    
    var count : Int{
        return (armFixedOutputList?.count ?? 0) + 1//'1' for break even point
    }
    
    subscript(_ indexPath : IndexPath)->ARMFixedOutputModel{
        return armFixedOutputList![indexPath.row]
    }
}
