//
//  ARMFixedDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 20/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class ARMFixedDataModel {
    
    var armDetails: ARMFixedDetailsModel?
    //Interest Rate
    var adjustInterestRate : Double?
    var fixedInterestRate : Double?
    var lifeTimeInterestRate : Double?
    var periodicInterestGapRate : Double?
    //Loan Amount
    var loanMtgAmount : Double?
    var paymentPerYear : Double?
    var loanTerm : Double?
    
    var adjPaymentPerYear : Double?
    var adjLoanTerm : Double?
    var fixedPmt : Double?
    
    var cummulativeValue: Double?
    var adjPrincipalIntPmt: Double?
    var savingsPerMonth: Double?
    
    var armFixedOutputList : Array<ARMFixedOutputModel> = []
    var equityBuildUpList : Array<EquityBuildUp> = []
    var breakEvenDuration: String?

    var totalInterestCount : Int{
        var count = 1
        var tempAdjustInterestRate = self.adjustInterestRate
        while tempAdjustInterestRate! <= lifeTimeInterestRate! {
            if tempAdjustInterestRate! == lifeTimeInterestRate!{//Dont increament count if lifetime and adjInterestRate become same
//                tempAdjustInterestRate! += periodicInterestGapRate!
                return count
            }
            count += 1
            tempAdjustInterestRate! += periodicInterestGapRate!
        }
        return count
    }
    
    init() {
      
    }
    
    func calculateEqulityBuildUp(){
        let count = 12 * totalInterestCount
        var balance : Double = 100000
        for index in 1...count{
            let ipmt = -(Formula.calculateIPMT(fixedInterestRate!, paymentPerYear!, loanTerm!, balance))//Interest
            let pmt = Formula.calculatePMT(fixedInterestRate!, paymentPerYear!, loanTerm!, loanMtgAmount!)
            let ppmt = pmt - ipmt
            balance = balance - ppmt
            equityBuildUpList.append(.init(index, round(balance), round(ipmt), round(ppmt)))
        }
        calculateARMFixedOutput()
    }
    
    func calculateNewEqulityBuildUp(){
        let total = totalInterestCount + 1
        let count = 12 * total
        var pmt = adjPrincipalIntPmt ?? 0
        var loanBalance = loanMtgAmount!
        var pmtChangeCount: Double = 0
        var diffFixedRate = savingsPerMonth
        var breakEvenPoint: Double = 0
        var breakDays: Int = 0
         for index in 1...count{
            let _index = index - 1
            var updatedInterest: Double = (adjustInterestRate! + (periodicInterestGapRate! * pmtChangeCount))
            if _index % 12 == 0 && _index != 0{
                pmtChangeCount += 1//Increase when pmt change
                updatedInterest = (adjustInterestRate! + (periodicInterestGapRate! * pmtChangeCount))
                let term = (adjLoanTerm! - pmtChangeCount)
                pmt = Formula.calculatePMT(updatedInterest, adjPaymentPerYear!, term, loanBalance)
                diffFixedRate = fixedPmt! - pmt
            }
            breakEvenPoint = breakEvenPoint + diffFixedRate!
            if breakEvenPoint < 1 && breakDays == 0{
                breakDays = index * 30
                print("Break Count=====>\(breakDays)")
                let years = breakDays/365
                let month = (Double(breakDays).truncatingRemainder(dividingBy: 365)) / 30.5
                let days = (Double(breakDays).truncatingRemainder(dividingBy: 365)).truncatingRemainder(dividingBy: 30.5)
                breakEvenDuration = "\(years)years \(month.toInt)months \(days.toInt)days"
 
            }
            let interestAmount = (loanBalance * updatedInterest.toValue)/adjPaymentPerYear!
            let principalAmount = pmt - interestAmount
            loanBalance = loanBalance - principalAmount
//            print(pmt,interestAmount,principalAmount,loanBalance)
            equityBuildUpList.append(EquityBuildUp.init(totalPayment: pmt, interest: interestAmount, principal: principalAmount, balance: loanBalance, period: _index))
        }
        calculateNewARMFixedOutput()
//        calculateARMFixedOutput()
    }
    func calculateARMFixedOutput(){
        var armFixedInterest = adjustInterestRate
        let equityYearList = equityBuildUpList.filter({$0.period! % 12 == 0})//get yearly balance amount as list
        
        //Calculate first item in armOutput list remaining in for loop
        let adjPmt = Formula.calculatePMT(adjustInterestRate!, adjPaymentPerYear!, adjLoanTerm!, loanMtgAmount!)
        fixedPmt = Formula.calculatePMT(fixedInterestRate!, paymentPerYear!, loanTerm!, loanMtgAmount!)
        let diffFixedRate = fixedPmt! - adjPmt
        let armFixedModel = ARMFixedOutputModel()
        armFixedModel.fixedPMTDifference = diffFixedRate
        armFixedModel.adjustablePeriod = paymentPerYear
        armFixedModel.interestRate = armFixedInterest
        armFixedModel.cumulative = armFixedModel.savingsPeriod
        cummulativeValue = armFixedModel.savingsPeriod
        armFixedOutputList.append(armFixedModel)
        
        for (index,item) in equityYearList.enumerated(){
            if armFixedInterest! != lifeTimeInterestRate!{
                armFixedInterest! += periodicInterestGapRate!
            }
            let armFixedModel = ARMFixedOutputModel()
            if item.period! == equityYearList.last!.period!{
                let balance = equityYearList[index - 1].balance
                armFixedModel.periodPmt = pmtDifference(balance!, armFixedInterest!)
            }else{
                armFixedModel.periodPmt = pmtDifference(item.balance!, armFixedInterest!)
            }
            armFixedModel.adjustablePeriod = paymentPerYear
            armFixedModel.interestRate = armFixedInterest
//            if fixedPmt! > armFixedModel.periodPmt!{
                armFixedModel.fixedPMTDifference = fixedPmt! - armFixedModel.periodPmt!
//            }else{
//                armFixedModel.fixedPMTDifference = armFixedModel.periodPmt! - fixedPmt!
//            }
            let cum = (cummulativeValue ?? 0) + (armFixedModel.savingsPeriod ?? 0)
            armFixedModel.cumulative = cum.removeMinus
            cummulativeValue = cum
            armFixedOutputList.append(armFixedModel)
        }
    }
    func calculateNewARMFixedOutput(){
        var armFixedInterest = adjustInterestRate
        let equityYearList = equityBuildUpList.filter({$0.period! % 12 == 0 && $0.period! != 0})//get yearly balance amount as list
        
        //Calculate first item in armOutput list remaining in for loop
        
        let armFixedModel = ARMFixedOutputModel()
        armFixedModel.fixedPMTDifference = self.savingsPerMonth
        armFixedModel.adjustablePeriod = self.paymentPerYear!
        armFixedModel.periodPmt = self.savingsPerMonth
        armFixedModel.interestRate = armFixedInterest
        armFixedModel.cumulative = armFixedModel.savingsPeriod
        cummulativeValue = armFixedModel.savingsPeriod
        armFixedOutputList.append(armFixedModel)
        
        for (_,item) in equityYearList.enumerated(){
            if armFixedInterest! != lifeTimeInterestRate!{
                armFixedInterest! += periodicInterestGapRate!
            }
            let armModel = ARMFixedOutputModel()
            armModel.periodPmt = item.totalPayment
            armModel.fixedPMTDifference = self.fixedPmt! - item.totalPayment!
//            armFixedInterest = armFixedInterest! + periodicInterestGapRate!
            armModel.interestRate = armFixedInterest
            armModel.adjustablePeriod = adjPaymentPerYear!
            cummulativeValue = cummulativeValue! + armModel.savingsPeriod!
            armModel.cumulative = cummulativeValue
            armFixedOutputList.append(armModel)
        }
    }
    func pmtDifference(_ balanceAmount : Double, _ interest : Double) -> Double{
        let pmt = Formula.calculatePMT(interest, adjPaymentPerYear!, adjLoanTerm!, balanceAmount)
        return pmt
    }
}
//ARM vs Fixed Output model class
class ARMFixedOutputModel{
    var interestRate : Double?
    var periodPmt :Double?
    var adjustablePeriod : Double?
    var fixedPMTDifference : Double?
    var savingsPeriod : Double?{
        if fixedPMTDifference != nil && adjustablePeriod != nil{
            return fixedPMTDifference! * adjustablePeriod!
        }
        return nil
    }
    var cumulative: Double?
//    var cumulative : Double?{
//        if adjustablePeriod != nil && fixedPMTDifference != nil{
//            return adjustablePeriod! * fixedPMTDifference!
//        }
//        return nil
//    }
}
//Equity Build up model class
class EquityBuildUp{
    var period : Int?
    var balance : Double?
    var interest : Double?
    var principal : Double?
    var totalPayment: Double?
    init(_ period : Int, _ balance : Double, _ interest : Double, _ principal : Double) {
        self.period = period
        self.balance = balance
        self.interest = interest
        self.principal = principal
    }
    init(totalPayment: Double, interest: Double, principal: Double, balance: Double, period: Int) {
        self.period = period
        self.totalPayment = totalPayment
        self.interest = interest
        self.principal = principal
        self.balance = balance
    }
}
