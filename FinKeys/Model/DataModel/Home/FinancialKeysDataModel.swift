//
//  FinancialKeysDataModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit

enum FinancialKeyTitles : String{
 
    case financingOption = "Financing Options"
    case maximumQualification = "Maximum Qualification"
    case newHomeQualification = "New Home Qualification"
    case rentBuy = "Rent vs Buy"
    case rePurchase = "Transition Equity to New Home"
    case investments = "Investments"
    case amortization = "Amortization"
    case comparativeMarketAnalysis = "Comparative Market Analysis"
    case loanComparison = "Loan Comparison"
    case buyDown = "Buy Down Strategy"
    case armFixed = "ARM vs. Fixed Rate"
    case lenderComparison = "Lender Comparison"
    case refinance = "Refinance"
    case financialKey = "Financial Keys"
}

class FinancialKeysDataModel {
    
    var financialKeyList: Array<FinancialKeyItem>?
    var newFinancialKeyList: Array<FinancialKeyModel> = []
    
    init() {
        newFinancialKeyList.append(FinancialKeyModel.init(title: "Qualification & CMA", items: qualificationList, headerColor: UIColor.init(hexString: "e43394")))
        newFinancialKeyList.append(FinancialKeyModel.init(title: "Purchasing", items: purchasingList, headerColor: UIColor.init(hexString: "efb250")))
        newFinancialKeyList.append(FinancialKeyModel.init(title: "Financing Comparisons", items: financingComparisonList, headerColor: UIColor.init(hexString: "5aa690")))
        newFinancialKeyList.append(FinancialKeyModel.init(title: "Financial Analysis", items: financialAnalysisList, headerColor: UIColor.init(hexString: "8ea7f9")))

        var list = Array<FinancialKeyItem>()
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.financingOption.rawValue, icon: UIImage.init(named: "finOption")!, bgColor: UIColor.init(hexString: "F0FFF0"), segueId: FinancingContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.maximumQualification.rawValue,icon: UIImage.init(named: "max_qualification")!, bgColor: UIColor.init(hexString: "FF69B4"), segueId: MaximumQualifyContainerViewController.segueIdentifier))
            
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.newHomeQualification.rawValue,icon: UIImage.init(named: "homeQua")!, bgColor: UIColor.init(hexString: "E0FFFF"), segueId: HomeQualifyContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.rentBuy.rawValue,icon: UIImage.init(named: "rentBuy")!, bgColor: UIColor.init(hexString: "C0C0C0"), segueId: RentBuyContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.rePurchase.rawValue,icon: UIImage.init(named: "purchase")!, bgColor: UIColor.init(hexString: "00FF00"), segueId: RepurchaseContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.investments.rawValue,icon: UIImage.init(named: "investReport")!, bgColor: UIColor.init(hexString: "FFF0F5"), segueId: InvestmentContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.amortization.rawValue,icon: UIImage.init(named: "amor")!, bgColor: UIColor.init(hexString: "BDB76B"), segueId: AmortizationInputViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.comparativeMarketAnalysis.rawValue,icon: UIImage.init(named: "cma1")!, bgColor: UIColor.init(hexString: "FFD700"), segueId: ComparableContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.loanComparison.rawValue,icon: UIImage.init(named: "loanCom")!, bgColor: UIColor.init(hexString: "F08080"),segueId: LoanCompareContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.buyDown.rawValue,icon: UIImage.init(named: "buyDown")!, bgColor: UIColor.init(hexString: "DEB887"), segueId: BuyDownContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.armFixed.rawValue,icon: UIImage.init(named: "armFixed")!, bgColor: UIColor.init(hexString: "0000CD"), segueId: ARMFixedFormViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.lenderComparison.rawValue,icon: UIImage.init(named: "lender")!, bgColor: UIColor.init(hexString: "C0C0C0"), segueId: LenderCompContainerViewController.segueIdentifier))
        
        list.append(FinancialKeyItem.init(title: FinancialKeyTitles.refinance.rawValue,icon: UIImage.init(named: "refine")!, bgColor: UIColor.init(hexString: "008080"), segueId: RefinanceContainerViewController.segueIdentifier))
        
        financialKeyList = list
    }
    
    var count : Int{
        return newFinancialKeyList.count
//        return financialKeyList?.count ?? 0
    }
    subscript(_ indexPath: IndexPath)->FinancialKeyModel{
        return newFinancialKeyList[indexPath.row]
    }

//    subscript(_ indexPath : IndexPath)->FinancialKeyItem{
//        return financialKeyList![indexPath.row]
//    }
    private var qualificationList: Array<FinancialKeyItem>{
        var items: Array<FinancialKeyItem> = [FinancialKeyItem.init(title: FinancialKeyTitles.maximumQualification.rawValue,icon: UIImage.init(named: "max_qualification")!, bgColor: UIColor.init(hexString: "FF69B4"), segueId: MaximumQualifyContainerViewController.segueIdentifier)]
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.newHomeQualification.rawValue,icon: UIImage.init(named: "homeQua")!, bgColor: UIColor.init(hexString: "E0FFFF"), segueId: HomeQualifyContainerViewController.segueIdentifier))
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.comparativeMarketAnalysis.rawValue,icon: UIImage.init(named: "cma1")!, bgColor: UIColor.init(hexString: "FFD700"), segueId: ComparableContainerViewController.segueIdentifier))
        return items
    }
    private var purchasingList: Array<FinancialKeyItem>{
        var items: Array<FinancialKeyItem> = [FinancialKeyItem.init(title: FinancialKeyTitles.rePurchase.rawValue,icon: UIImage.init(named: "purchase")!, bgColor: UIColor.init(hexString: "00FF00"), segueId: RepurchaseContainerViewController.segueIdentifier)]
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.financingOption.rawValue, icon: UIImage.init(named: "finOption")!, bgColor: UIColor.init(hexString: "F0FFF0"), segueId: FinancingContainerViewController.segueIdentifier))
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.buyDown.rawValue,icon: UIImage.init(named: "buyDown")!, bgColor: UIColor.init(hexString: "DEB887"), segueId: BuyDownContainerViewController.segueIdentifier))
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.refinance.rawValue,icon: UIImage.init(named: "refine")!, bgColor: UIColor.init(hexString: "008080"), segueId: RefinanceContainerViewController.segueIdentifier))
        return items
    }
    private var financingComparisonList: Array<FinancialKeyItem>{
        var items: Array<FinancialKeyItem> = [FinancialKeyItem.init(title: FinancialKeyTitles.loanComparison.rawValue,icon: UIImage.init(named: "loanCom")!, bgColor: UIColor.init(hexString: "F08080"),segueId: LoanCompareContainerViewController.segueIdentifier)]
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.lenderComparison.rawValue,icon: UIImage.init(named: "lender")!, bgColor: UIColor.init(hexString: "C0C0C0"), segueId: LenderCompContainerViewController.segueIdentifier))
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.armFixed.rawValue,icon: UIImage.init(named: "armFixed")!, bgColor: UIColor.init(hexString: "0000CD"), segueId: ARMFixedFormViewController.segueIdentifier))
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.rentBuy.rawValue,icon: UIImage.init(named: "rentBuy")!, bgColor: UIColor.init(hexString: "C0C0C0"), segueId: RentBuyContainerViewController.segueIdentifier))
        return items
    }
    private var financialAnalysisList: Array<FinancialKeyItem>{
        var items: Array<FinancialKeyItem> = [FinancialKeyItem.init(title: FinancialKeyTitles.investments.rawValue,icon: UIImage.init(named: "investReport")!, bgColor: UIColor.init(hexString: "FFF0F5"), segueId: InvestmentContainerViewController.segueIdentifier)]
        items.append(FinancialKeyItem.init(title: FinancialKeyTitles.amortization.rawValue,icon: UIImage.init(named: "amor")!, bgColor: UIColor.init(hexString: "BDB76B"), segueId: AmortizationInputViewController.segueIdentifier))
        return items
    }
}

/// Financial key class model used to show list of items on home screen
class FinancialKeyItem {
    var title : String?
    var segueId : String?
    var id : Int?
    var isNavigation : Bool?
    var icon : UIImage!
    var bgColor : UIColor!
    
    init(title : String,icon : UIImage,bgColor : UIColor, segueId : String? = nil) {
        self.title = title
        self.segueId = segueId
        self.icon = icon
        self.bgColor = bgColor
    }
}
class FinancialKeyModel{
    var title: String?
    var items: Array<FinancialKeyItem>?
    var headerColor: UIColor?
    init(title: String, items: Array<FinancialKeyItem>, headerColor: UIColor) {
        self.title = title
        self.items = items
        self.headerColor = headerColor
    }
}
