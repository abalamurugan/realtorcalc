//
//  HomeQualifyOutputModel.swift
//  RealtorCalc
//
//  Created by Bala on 28/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class HomeQualifyOutputDataModel{
    
}
class HomeQualifyModel {
    var title : String?
    var price : Double?
    var priceDescription : String?
    var details : HomePriceDetailModel?
    var detailList = Array<OutputDetailsModel>()
    var actionToQualify: ActionQualifyModel?
}
class HomePriceDetailModel{}

class OutputDetailsModel{
    var key : String?
    var value : Any?
    var valueTwo : Any?
    var isHeading: Bool?
    var isCurrency: Bool?
    init(key : String, value : Any, isHeading: Bool = false) {
        self.key = key
        self.value = value
        self.isHeading = isHeading
    }
    init(key: String, value: Any?, valueTwo: Any?, isCurrency: Bool = true) {
        self.key = key
        self.value = value
        self.valueTwo = valueTwo
        self.isCurrency = isCurrency
    }
}
class ActionQualifyModel{
    var firstRaiseMonthlyIncome: String?
    var firstLowerMonthlyPayment: String?
    var secondRaiseMonthlyIncome: String?
    var secondLowerMonthlyPayment: String?
}
