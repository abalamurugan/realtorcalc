//
//  RefinanceDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 12/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class RefinanceOutputDataModel{
    var outputList : Array<RefinanceOutputModel>?
    subscript(_ indexPath : IndexPath)->OutputDetailsModel{
        return outputList![indexPath.section].list![indexPath.row]
    }
}

class RefinanceOutputModel{
    var title : String?
    var list : Array<OutputDetailsModel>?
    init(title : String, list : Array<OutputDetailsModel>) {
        self.title = title
        self.list = list
    }
}
