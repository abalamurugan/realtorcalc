//
//  AmortizationDataModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class AmortizationDataModel {
    var contactModel: ContactInfoModel?
    var loanPlanModel: BottomPickerModel?
    var paymentTypeModel: BottomPickerModel?
    var paymentTypeList: Array<DropDownInfoModel>?
    var loanList: Array<DropDownInfoModel>?
    
    var selectedLoan: DropDownInfoModel?
    var selectedPaymentType: DropDownInfoModel?

    var interestRate : Double?
    var periodicIntRate : Double?
    var paymentPerYear : Double?
    var loanTerm : Double?
    var loanMtgAmount : Double?
    var noPaymentMade : Int?
    var paymentDate : Date?
    var extraPrincipal: Double?
    var balanceWithNoExtraPmt : Double?
    var balanceWithExtraPmt : Double?
    var regularPmt : Double?
    var savingsOverPeriod: Double?
    var loanSummaryList = Array<AmortizationLoanDetailsModel>()
    var summaryList = [[AmortizationLoanDetailsModel]]()
    
    func calculatePMT()-> Double{
        let interest = interestRate! / 100
        let powA = (1 + interest/paymentPerYear!)
        let powB = -1 * loanTerm! * paymentPerYear!
        let exp3 = (1 - (pow(powA, Double(powB))))
        let expressionFirst = (loanMtgAmount! * (interest / paymentPerYear!))
        let expA = (expressionFirst / exp3)
        return expA
    }
    
    func getSummaryList(){
        var loanPayment : Double = loanMtgAmount!
        var pmt = calculatePMT()
        var intRate = interestRate
        balanceWithNoExtraPmt = Formula.FVCalc(rate: interestRate!.toValue/paymentPerYear!, nper: Double(noPaymentMade!), pmt: -pmt, fv: loanMtgAmount!).removeMinus
        balanceWithExtraPmt = Formula.FVCalc(rate: interestRate!.toValue/paymentPerYear!, nper: Double(noPaymentMade!), pmt: pmt + (extraPrincipal ?? 0), fv: -loanMtgAmount!).removeMinus
        savingsOverPeriod = balanceWithNoExtraPmt! - balanceWithExtraPmt!
        regularPmt = pmt
        
        for _index in 1...noPaymentMade!{//24 -> Number of payment made
            if _index == 13 && selectedPaymentType?.value != "1"{//dont change pmt for payment type "fixed"
                intRate = (interestRate ?? 0) + (periodicIntRate ?? 0)
                pmt = Formula.calculatePMT(intRate ?? 0, paymentPerYear ?? 0, (loanTerm ?? 0) - 1, balanceWithNoExtraPmt ?? 0)
            }
            let calInterest = (loanPayment * intRate!) / (paymentPerYear! * 100)
            let principal = pmt - calInterest
            loanPayment = loanPayment - (principal + (extraPrincipal ?? 0))
            if _index != 1{
                self.paymentDate = Calendar.current.date(byAdding: .month, value: 1, to: self.paymentDate!)
            }
            print("===========")
            print("Interest=====>\(calInterest)")
            print("principal=====>\(principal)")
            print("extraPrincipal=====>\(extraPrincipal ?? 0)")
            print("Loan balance=====>\(loanPayment)")
            print("===========\n")
            let item = AmortizationLoanDetailsModel.init(interest: calInterest, principal: principal, extra: (extraPrincipal ?? 0), loanBalance: loanPayment, paymentAmount: pmt)
            item.paymentDate = self.paymentDate
            loanSummaryList.append(item)
            
         }
        self.summaryList = loanSummaryList.chunked(into: 12)
        
    }
    func clearValues(){
        loanSummaryList = []
        summaryList = []
    }
    var count : Int{
        return self.loanSummaryList.count 
    }
    subscript(_ indexPath : IndexPath)->AmortizationLoanDetailsModel{
        return self.loanSummaryList[indexPath.row]
    }
}
class AmortizationLoanDetailsModel {
    var interest : Double?
    var principal : Double?
    var extra : Double?
    var loanBalance : Double?
    var paymentAmount : Double?
    var paymentDate : Date?
    init(interest : Double,principal : Double,extra : Double,loanBalance : Double, paymentAmount : Double) {
        self.interest = interest
        self.principal = principal
        self.extra = extra
        self.loanBalance = loanBalance
        self.paymentAmount = paymentAmount
    }
}
class AmortizationYearDataModel {
    var summaryList : [[AmortizationLoanDetailsModel]] = []
    
    var contactModel: ContactInfoModel?
    
    var count : Int{
        return summaryList.count 
    }
    var balanceWithNoExtraPmt : Double?
    var balanceWithExtraPmt : Double?
    var regularPmt : Double?
    var savingsOverPeriod: Double?
    func totalPayment(_ indexPath : IndexPath)->Double{
        let total = summaryList[indexPath.row].map({$0.paymentAmount!}).reduce(0, +)
        return total
    }
    func totalLoanBalance(_ indexPath : IndexPath)->Double{
        let total = summaryList[indexPath.row].map({$0.loanBalance!}).reduce(0, +)
        return total
    }
    func totalExtra(_ indexPath : IndexPath)->Double{
        let total = summaryList[indexPath.row].map({$0.extra!}).reduce(0, +)
        return total
    }
    func totalPrincipal(_ indexPath : IndexPath)->Double{
        let total = summaryList[indexPath.row].map({$0.principal!}).reduce(0, +)
        return total
    }
    func totalInterest(_ indexPath : IndexPath)->Double{
        let total = summaryList[indexPath.row].map({$0.interest!}).reduce(0, +)
        return total
    }
    func paymentDuration(_ indexPath : IndexPath)->String{
        let date1 = summaryList[indexPath.row].first?.paymentDate
        let formated1 = date1?.toString(format: "MMM,YYYY")
        let date2 = summaryList[indexPath.row].last?.paymentDate
        let formated2 = date2?.toString(format: "MMM,YYYY")

        return "\(formated1!) - \(formated2!)"
    }
}
extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
