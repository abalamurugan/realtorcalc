//
//  ContactFormDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 28/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

enum FormType {
    case profile
    case contact
}

class ContactFormDataModel {
    var type: FormType = .contact
    var profileDetails: ProfileModel?
}
