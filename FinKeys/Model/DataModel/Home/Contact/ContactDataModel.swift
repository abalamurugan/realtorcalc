//
//  ContactDataModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 02/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class ContactDataModel {
    
    var contactList: Array<ContactInfoModel>?
    
    var count: Int{
        if let searchValue = searchText, searchValue != ""{
            return searchContactList?.count ?? 0
        }
        return contactList?.count ?? 0
    }    
    subscript(_ indexPath: IndexPath)->ContactInfoModel{
        if let searchValue = searchText, searchValue != ""{
            return searchContactList![indexPath.row]
        }
        return contactList![indexPath.row]
    }
    
    var isPicker: Bool = false
    
    var searchContactList: Array<ContactInfoModel>?
    
    var searchText: String?{
        didSet{
            self.searchContact()
        }
    }
    
    private func searchContact(){
        if let text = searchText{
            searchContactList = contactList?.filter({$0.fullName.lowercased().contains(text.lowercased())})
        }
    }
    
    func fetchContactDetails(success: @escaping((Dictionary<String, Any>)->Void),failure: @escaping((Any?)->Void)){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.contact.key.tag: Constants.tagValue.contact, APIKey.contact.key.userId: UserManager.instance.userID]
        connection.sendRequest(urlPath: nil, method: .get, param: param, encoding: .queryString, { (json, data) in
            success(json)
        }) { (error) in
            failure(error)
        }
    }
    func fetchContacts(success: @escaping((ContactModel?)->Void),failure: @escaping((Any?)->Void)){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.contact.key.tag: Constants.tagValue.contact, APIKey.contact.key.userId: UserManager.instance.userID]
        connection.networkRequest(urlPath: nil, method: .get, param: param, encoding: .queryString, { (response: ResponseModel<ContactModel>) in
            self.contactList = response.item?.response
            success(response.item)
        }) { (error) in
            failure(error)
        }
        
//    func fetch(){
//        let connection = NetworkManager.init()
//        connection.networkRequest(urlPath: nil, method: .get, param: nil, encoding: .queryString, { (response) in
//
//        }, { (error) in
//
//        }) { (response:ResponseModel<ContactModel>) in
//            print("response model====>",response.item!.success_message)
//        }
//    }
    
}

}
