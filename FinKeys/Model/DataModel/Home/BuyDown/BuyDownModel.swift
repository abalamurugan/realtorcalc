//
//  BuyDownModel.swift
//  RealtorCalc
//
//  Created by Bala on 01/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class BuyDownModel {
    var buyerRate: Double?
    var year: Double?
    var totalPayment: Double?
    var monthlySubsidy: Double?
    var buyerPayment: Double?
    var yearlySubsidy: Double?
}
class BuyDownOutput{
    var title: String?
    var list: Array<BuyDownModel>?
    init(title: String, list: Array<BuyDownModel>) {
        self.title = title
        self.list = list
    }
}
