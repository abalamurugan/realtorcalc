//
//  SettingsDataModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 05/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit

class SettingsDataModel {
    var items = Array<MenuItem>()
    init() {
        items.append(MenuItem.init(title: "Change Password", icon: UIImage.init(named: "changepassword"), segueId: ChangePasswordViewController.segueIdentifier))
    }
    var count: Int{
        return items.count
    }
    subscript(_ indexPath: IndexPath)->MenuItem{
        return items[indexPath.row]
    }
}

