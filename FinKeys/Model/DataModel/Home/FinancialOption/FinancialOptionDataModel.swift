//
//  FinancialOptionDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
 
enum FinanceOptionType : Int {
    case payment
    case cashNeeded
    case loanPlan
    case paymentType
}

class FinancialOptionDataModel {
    
    var financialOutputList : Array<FinancialModel>?
    
    var formDetails : FinancialModel?

    var count : Int{
        return financialOutputList?.count ?? 0
    }
    
    subscript(_ indexPath : IndexPath)-> FinancialModel{
        return financialOutputList![indexPath.row]
    }
    var type : FinanceOptionType = .payment
    var contactModel: ContactInfoModel?
    var loanPlanModel: BottomPickerModel?
    var paymentTypeModel: BottomPickerModel?
    var selectedIndex: Int?
    

    func getDownPayment(_ purchasePayment : Double, _ mtgOne : Double, _ mtgTwo : Double)->Double{
        var downPayment = purchasePayment
        downPayment -= mtgOne
        downPayment -= mtgTwo
        return downPayment
    }
}

class FinancialModel {
    var pmtPrincipal : Double?
    //Extra
    var taxesEscrow : Double?
    var mtgInsuranceEscrow : Double?
    var hoaComboFee : Double?
    var insuranceEscrow : Double?
    var totalAmount : Double?{
        var total : Double = 0
        if let pmt = pmtPrincipal{
            total += pmt
        }
        if let tax = taxesEscrow{
            total += tax
        }
        if let mtg = mtgInsuranceEscrow{
            total += mtg
        }
        if let hoa = hoaComboFee{
            total += hoa
        }
        if let insurance = insuranceEscrow{
            total += insurance
        }
        return total
    }
    //Cash
    var downPayment : Double?
    var aproxClosingCost : Double?{
        var total : Double = 0
        if let _loanOrgi = loanOrgiExtra{
            total += _loanOrgi
        }
        if let _prepaid = prepaidExtra{
            total += _prepaid
        }
        if let _title = titleRecordExtra{
            total += _title
        }
        if let _mortage = mortageExtra{
            total += _mortage
        }
        return total
    }
    var totalCashClose : Double?{
        var total : Double = 0
        if let downPay = downPayment{
            total += downPay
        }
        if let _aproxClosingCost = aproxClosingCost{
            total += _aproxClosingCost
        }
        return total
    }
    //Cash Extra
    var loanOrgiExtra : Double?
    var prepaidExtra : Double?
    var titleRecordExtra : Double?
    var mortageExtra : Double?
    
    var loanPlan: String?
    var paymentType: String?
    var interestRate: Double?
    var loanTerm: Double?
    var paymentPerYear: Double?
    var mtgAmount: Double?
    var fullName: String?
    var contactModel: ContactInfoModel?
}
