//
//  ProfileDataModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation


class ProfileDataModel {
    
    var profileItem: ProfileModel?{
        didSet{
            self.profileList.removeAll()
            self.updateValues()
        }
    }
    var profileList: Array<OutputDetailsModel> = []
    
    func updateValues(){
        self.profileList.append(OutputDetailsModel.init(key: "", value: profileItem?.response?.email ?? ""))
        self.profileList.append(OutputDetailsModel.init(key: "Email ID", value: profileItem?.response?.email ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Title", value: profileItem?.response?.title ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Company Name", value: profileItem?.response?.company_name ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Mobile Number", value: profileItem?.response?.cellNo ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Phone Number", value: profileItem?.response?.phone ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Fax Number", value: profileItem?.response?.fax ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Address", value: profileItem?.response?.address ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "Zip code", value: profileItem?.response?.zip ?? "Update"))
        self.profileList.append(OutputDetailsModel.init(key: "MLS No.", value: profileItem?.response?.mlsId ?? "Update"))

    }
    
    var count: Int{
        return profileList.count
    }
    
    
}
