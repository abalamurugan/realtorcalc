//
//  InvestmentDataModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 26/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
enum OutputType{
    case financing
    case investment
    case rentBuy
    case maximumQualification
    case transitionEquity
    case lender
    case cma
}

class InvestmentDataModel {
    var eoyFirstMortgageList : Array<Double> = []
    var fiveYearIRRList: Array<Double> = []
    var eoySecondMortgageList : Array<Double> = []
    
    var outputList: Array<RefinanceOutputModel> = []

}
class InvestmentOutputDataModel{
    var type: OutputType = .investment
    var contactModel: ContactInfoModel?
    var outputList: Array<RefinanceOutputModel>?
    var count: Int{
        return outputList?.count ?? 0
    }
     
    subscript(_ indexPath: IndexPath)->OutputDetailsModel{
        return outputList![indexPath.section].list![indexPath.row]
    }
}
