//
//  InvestmentOutputModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class InvestmentOutputModel {
    var grossScheduleIncome: Double?
    var lessVacantCreditLoss: Double?
    var grossOperatingIncome: Double?
    var operatingExpenses: Double?
    var netOperatingExpenses: Double?
    var nonOperatingExpenses: Double?
    var firstMtgDebtServices: Double?
    var secondMtgDebtServices: Double?
    var cashFlowBeforeTaxes: Double?
    var interestExpenses: Double?
    var depreciation: Double?
    var taxableIncome: Double?
    var xMarginalTaxRate: Double?
    var incomeTaxes: Double?
    var cashFlowAfterTaxes: Double?
}

