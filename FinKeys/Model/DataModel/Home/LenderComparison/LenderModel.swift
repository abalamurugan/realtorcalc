//
//  LenderModel.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class LenderModel{
    var monthlyPayment: Double?
    var interestCost: Double?
    var loanCost: Double?
    var apr: Double?
    init(monthlyPayment: Double, interestCost: Double, loanCost: Double, apr: Double) {
        self.monthlyPayment = monthlyPayment
        self.interestCost = interestCost
        self.loanCost = loanCost
        self.apr = apr
    }
}
