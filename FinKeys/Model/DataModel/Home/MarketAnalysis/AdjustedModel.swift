//
//  AdjustedModel.swift
//  RealtorCalc
//
//  Created by Bala on 22/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class AdjustedModel {
    var garage: Double?
    var carport: Double?
    var fireplace: Double?
    var centralAir: Double?
    var perFullBath: Double?
    var perThreeFourBath: Double?
    var perBedRoom: Double?
    var bsmtFinish: Double?
    var bsmtUnfinish: Double?
    var mainFloor: Double?
    var upperFloor: Double?
    var fullLandscape: Double?
    var ageVarience: Double?
    var appreciation: Double?
    var perHalfBath: Double?
}
class ACTypeModel{
    var type: String?
    var price: Double?
    init(type: String, price: Double) {
        self.type = type
        self.price = price
    }
}
