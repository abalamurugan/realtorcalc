//
//  SubjectModel.swift
//  RealtorCalc
//
//  Created by Bala on 23/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class SubjectModel {
    var year: Double?
    var acres: Double?
    var landscape: Double?
    var lotValue: Double?
    var garage: Double?
    var carport: Double?
    var sqFtmain: Double?
    var sqFtUp: Double?
    var sqFtDown: Double?
    var finishDown: Double?
    var bedsMain: Double?
    var bedsUp: Double?
    var bedsDn: Double?
    var bathMain: Double?
    var bathUp: Double?
    var bathDn: Double?
    var fireplace: Double?
    var sold: Double?
    var listed: Double?
    var adjusted: Double?
    var f1FullBath: Double?
    var f1HalfBath: Double?
    var f1ThreeFourBath: Double?
    var f2FullBath: Double?
    var f2HalfBath: Double?
    var f2ThreeFourBath: Double?
    var bFullBath: Double?
    var bHalfBath: Double?
    var bThreeFourBath: Double?
    
    var bathTotal: Double{
        var total = bathMain ?? 0
        total += bathUp ?? 0
        total += bathDn ?? 0
        return total
    }
    var bedsTotal: Double{
        var total = bedsMain ?? 0
        total += bedsUp ?? 0
        total += bedsDn ?? 0
        return total
    }
    var sqFtTotal: Double{
        var total = sqFtmain ?? 0
        total += sqFtUp ?? 0
        total += sqFtDown ?? 0
        return total
    }
}
