//
//  MarketOutputModel.swift
//  RealtorCalc
//
//  Created by balamurugan on 24/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class MarketOutputModel {
    var address: String?
    var proximity: String?
    var listedPrice: Double?
    var salesPrice: Double?
    var adjSalesPrice: Double?
    var adjustValue: Double?
    var title: String?
}
