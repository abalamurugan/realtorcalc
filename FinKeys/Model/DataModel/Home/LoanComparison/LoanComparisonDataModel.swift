//
//  LoanComparison.swift
//  RealtorCalc
//
//  Created by Bala on 25/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class LoanComparisonDataModel{
    var subLoanList: Array<LoanItems> = []
    var comparisonList: Array<LoanItems> = []
    var list: Array<LoanComparisonItem> = []
    var summaryList: Array<LoanComparisonItem> = []
    
    var subInterest: Double?
    var subMtgAmount: Double?
    var subMtgTerm: Double?
    var subPaymentPerYear: Double?
    var subExtraPayment: Double?
    var subFinancingCost: Double?
    
    var comInterest: Double?
    var comMtgAmount: Double?
    var comMtgTerm: Double?
    var comPaymentPerYear: Double?
    var comExtraPayment: Double?
    var comFinancingCost: Double?
    
    var subCumlativePrincipalPaid: Double = 0
    var comCumlativePrincipalPaid: Double = 0

    var subPmt: Double{
        return Formula.calculatePMT(subInterest ?? 0, subPaymentPerYear ?? 0, subMtgTerm ?? 0, subMtgAmount ?? 0)
    }
    
    var comPmt: Double{
        return Formula.calculatePMT(comInterest ?? 0, comPaymentPerYear ?? 0, comMtgTerm ?? 0, comMtgAmount ?? 0)
    }
    func calc(){
        reset()
        calculateSubLoan()
        calculateComp()
        calcSubjectSummary()
        calcComparisonSummary()
        calcComparison()
    }
    private func reset(){
        subCumlativePrincipalPaid = 0
        comCumlativePrincipalPaid = 0
        self.list.removeAll()
        self.summaryList.removeAll()
        self.subLoanList.removeAll()
        self.comparisonList.removeAll()
    }
    private func calculateSubLoan(){
        for index in 0...5{//subject loan
            let pmt = Formula.calculatePMT(subInterest ?? 0, subPaymentPerYear ?? 0, subMtgTerm ?? 0, subMtgAmount ?? 0, Double(index + 1))
            let futureValue = Formula.FVCalc(rate: subInterest!.toValue/subPaymentPerYear!, nper: Double(index + 1) * subPaymentPerYear!, pmt: -pmt, fv: subMtgAmount!).removeMinus
            var principalPaid: Double = 0
            if index == 0{
                principalPaid = ((subMtgAmount ?? 0) - futureValue) + (subExtraPayment ?? 0) * (subPaymentPerYear ?? 0)

            }else{
                principalPaid = (subLoanList[index - 1].balance! - futureValue) + (subExtraPayment ?? 0) * (subPaymentPerYear ?? 0)
            }
            subCumlativePrincipalPaid += principalPaid
            let interestPaid = subPmt * 12 - principalPaid
            let total = principalPaid + interestPaid
            
            let subjectItem = LoanItems()
            subjectItem.interestPaid = interestPaid
            subjectItem.total = total
            subjectItem.cummPrincipalPaid = subCumlativePrincipalPaid
            subjectItem.principalPaid = principalPaid
            subjectItem.balance = futureValue
            subjectItem.princleInterest = pmt
            subjectItem.rate = subInterest
            subjectItem.endYear = Double(index + 1)
            subLoanList.append(subjectItem)
        }
        list.append(LoanComparisonItem.init(list: subLoanList, title: "Subject Loan Variance"))
    }
    private func calculateComp(){
        for index in 0...5{//subject loan
            let pmt = Formula.calculatePMT(comInterest ?? 0, comPaymentPerYear ?? 0, comMtgTerm ?? 0, comMtgAmount ?? 0, Double(index + 1))
            let futureValue = Formula.FVCalc(rate: comInterest!.toValue/comPaymentPerYear!, nper: Double(index + 1) * comPaymentPerYear!, pmt: -pmt, fv: comMtgAmount!).removeMinus
            var principalPaid: Double = 0
            if index == 0{
                principalPaid = (comMtgAmount ?? 0) - futureValue + (comExtraPayment ?? 0) * (comPaymentPerYear ?? 0)
            }else{
                principalPaid = (comparisonList[index - 1].balance! - futureValue) +  (comExtraPayment ?? 0) * (comPaymentPerYear ?? 0)
            }
            comCumlativePrincipalPaid += principalPaid
            let interestPaid = comPmt * 12 - principalPaid
            let total = principalPaid + interestPaid
            
            let compareItem = LoanItems()
            compareItem.interestPaid = interestPaid
            compareItem.total = total
            compareItem.cummPrincipalPaid = comCumlativePrincipalPaid
            compareItem.principalPaid = principalPaid
            compareItem.balance = futureValue
            compareItem.princleInterest = pmt
            compareItem.rate = comInterest
            compareItem.endYear = Double(index + 1)
            comparisonList.append(compareItem)
        }
        list.append(LoanComparisonItem.init(list: comparisonList, title: "Comparison Loan Variance"))
    }
    private func calcSubjectSummary(){
        var interestPaid: Double = 0
        var principalPaid: Double = 0
        var cumulative: Double = 0
        for item in subLoanList{
            interestPaid += item.interestPaid ?? 0
            principalPaid += item.principalPaid ?? 0
        }
        let lastItem = subLoanList.last//Subtract last item value as per report. Consider first 5
        interestPaid -= lastItem?.interestPaid ?? 0
        principalPaid -= lastItem?.principalPaid ?? 0
        cumulative += interestPaid
        cumulative += principalPaid
        cumulative += subFinancingCost ?? 0
        var subList: Array<LoanItems> = []
        let subItem = LoanItems()
        let mtgAmount = (subMtgAmount ?? 0) - (subFinancingCost ?? 0)
        let mortgApr = Formula.rateCalculation(term: subMtgTerm ?? 0 , paymentPerYear: subPaymentPerYear ?? 0, payment: subPmt, loanAmount: mtgAmount)
        subItem.apr = mortgApr
        subItem.interestPaid = interestPaid
        subItem.principalPaid = principalPaid
        subItem.cummPrincipalPaid = cumulative
        subItem.financeAmount = subFinancingCost
        subItem.rate = subInterest
        subList.append(subItem)
        summaryList.append(LoanComparisonItem.init(list: subList, title: "Subject summary"))
    }
    private func calcComparisonSummary(){
        var interestPaid: Double = 0
        var principalPaid: Double = 0
        var cumulative: Double = 0
        for item in comparisonList{
            interestPaid += item.interestPaid ?? 0
            principalPaid += item.principalPaid ?? 0
        }
        let lastItem = comparisonList.last//Subtract last item value as per report. Consider first 5
        interestPaid -= lastItem?.interestPaid ?? 0
        principalPaid -= lastItem?.principalPaid ?? 0
        cumulative += interestPaid
        cumulative += principalPaid
        cumulative += comFinancingCost ?? 0
        var compareList: Array<LoanItems> = []
        let compareItem = LoanItems()
        let mtgAmount = (comMtgAmount ?? 0) - (comFinancingCost ?? 0)
        let mortgApr = Formula.rateCalculation(term: comMtgTerm ?? 0 , paymentPerYear: comPaymentPerYear ?? 0, payment: comPmt, loanAmount: mtgAmount)
        compareItem.apr = mortgApr
        compareItem.interestPaid = interestPaid
        compareItem.principalPaid = principalPaid
        compareItem.cummPrincipalPaid = cumulative
        compareItem.financeAmount = comFinancingCost
        compareItem.rate = comInterest
        compareList.append(compareItem)
        summaryList.append(LoanComparisonItem.init(list: compareList, title: "Compare summary"))
    }
    private func calcComparison(){
        let subjectItem = summaryList.first?.list?.first
        let compareItem = summaryList.last?.list?.first
        var interestDifference = (subjectItem?.interestPaid ?? 0) - (compareItem?.interestPaid ?? 0)
//        if interestDifference < 0{
//            interestDifference = interestDifference.magnitude
//        }
        var principalPaidDiff = (subjectItem?.principalPaid ?? 0) - (compareItem?.principalPaid ?? 0)
        if principalPaidDiff < 0{
            principalPaidDiff = principalPaidDiff.magnitude
        }
        var financeDiff = (subjectItem?.financeAmount ?? 0) - (compareItem?.financeAmount ?? 0)
        if financeDiff < 0{
            financeDiff = financeDiff.magnitude
        }
        var cumlativeDiff = (subjectItem?.cummPrincipalPaid ?? 0) - (compareItem?.cummPrincipalPaid ?? 0)
//        if cumlativeDiff < 0{
//            cumlativeDiff = cumlativeDiff
//        }
        let aprDiff = (subjectItem?.apr ?? 0) - (compareItem?.apr ?? 0)
        
        let comparsionDifference = LoanItems()
        var finalDiffList: Array<LoanItems> = []

        comparsionDifference.apr = aprDiff
        comparsionDifference.cummPrincipalPaid = cumlativeDiff
        comparsionDifference.principalPaid = principalPaidDiff
        comparsionDifference.interestPaid = interestDifference
        comparsionDifference.financeAmount = financeDiff
        finalDiffList.append(comparsionDifference)
        summaryList.append(LoanComparisonItem.init(list: finalDiffList, title: "Comparison Loan Variance"))
    }
}

class LoanItems{
    var rate: Double?
    var princleInterest: Double?
    var balance: Double?
    var principalPaid: Double?
    var interestPaid: Double?
    var total: Double?
    var cummPrincipalPaid: Double?
    var endYear: Double?
    var apr: Double?
    var financeAmount: Double?
    
}
class LoanComparisonItem{
    var list: Array<LoanItems>?
    var title: String?
    init(list: Array<LoanItems>, title: String) {
        self.list = list
        self.title = title
    }
}
