//
//  LoanComparison.swift
//  RealtorCalc
//
//  Created by Bala on 25/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class LoanComparisonDataModel{
    var subLoanList: Array<LoanItems> = []
    var comparisonList: Array<LoanItems> = []
    
    var subInterest: Double?
    var subMtgAmount: Double?
    var subMtgTerm: Double?
    var subPaymentPerYear: Double?
    var subExtraPayment: Double?
    var subFinancingCost: Double?
    
    var comInterest: Double?
    var comMtgAmount: Double?
    var comMtgTerm: Double?
    var comPaymentPerYear: Double?
    var comExtraPayment: Double?
    var comFinancingCost: Double?
    
    var subCumlativePrincipalPaid: Double = 0
    var comCumlativePrincipalPaid: Double = 0

    var subPmt: Double{
        return Formula.calculatePMT(subInterest ?? 0, subPaymentPerYear ?? 0, subMtgTerm ?? 0, subMtgAmount ?? 0)
    }
    
    var comPmt: Double{
        return Formula.calculatePMT(comInterest ?? 0, comPaymentPerYear ?? 0, comMtgTerm ?? 0, comMtgAmount ?? 0)
    }
    
    func calculate(){
        for index in 0...5{//subject loan
            let pmt = Formula.calculatePMT(subInterest ?? 0, subPaymentPerYear ?? 0, subMtgTerm ?? 0, subMtgAmount ?? 0, Double(index + 1))
            let fv = Formula.FVCalculation(subInterest ?? 0, subPaymentPerYear ?? 0, subMtgTerm ?? 0, subMtgAmount ?? 0, Double(index + 1))
            let principalPaid = subMtgAmount ?? 0 - fv
            subCumlativePrincipalPaid += principalPaid
            let interestPaid = subPmt * 12 - principalPaid
            let total = principalPaid + interestPaid
            
            let subjectItem = LoanItems()
            subjectItem.interestPaid = interestPaid
            subjectItem.total = total
            subjectItem.cummPrincipalPaid = subCumlativePrincipalPaid
            subjectItem.principalPaid = principalPaid
            subjectItem.balance = fv
            subjectItem.princleInterest = pmt
            subjectItem.rate = subInterest
            subjectItem.endYear = Double(index + 1)
            subLoanList.append(subjectItem)
        }
    }
    func calculateComp(){
        for index in 0...5{//subject loan
            let pmt = Formula.calculatePMT(comInterest ?? 0, comPaymentPerYear ?? 0, comMtgTerm ?? 0, comMtgAmount ?? 0, Double(index + 1))
            let fv = Formula.FVCalculation(comInterest ?? 0, comPaymentPerYear ?? 0, comMtgTerm ?? 0, comMtgAmount ?? 0, Double(index + 1))
            let principalPaid = comMtgAmount ?? 0 - fv
            comCumlativePrincipalPaid += principalPaid
            let interestPaid = comPmt * 12 - principalPaid
            let total = principalPaid + interestPaid
            
            let compareItem = LoanItems()
            compareItem.interestPaid = interestPaid
            compareItem.total = total
            compareItem.cummPrincipalPaid = comCumlativePrincipalPaid
            compareItem.principalPaid = principalPaid
            compareItem.balance = fv
            compareItem.princleInterest = pmt
            compareItem.rate = comInterest
            compareItem.endYear = Double(index + 1)
            comparisonList.append(compareItem)
        }
    }
}

class LoanItems{
    var rate: Double?
    var princleInterest: Double?
    var balance: Double?
    var principalPaid: Double?
    var interestPaid: Double?
    var total: Double?
    var cummPrincipalPaid: Double?
    var endYear: Double?
}
