//
//  EquityDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 28/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class EquityDataModel{
    
    var list : Array<OutputDetailsModel>?
    
    var equityList : Array<EquityModel>?
    
    var totalAmount : Double?
    
    var count : Int{
        return list?.count ?? 0
    }
    
    var model : Any?
    
    var title : String?
    
    var isEquity: Bool = false
    
    var type: OutputType = .rentBuy

    subscript(_ indexPath : IndexPath) -> OutputDetailsModel{
        return list![indexPath.row]
    }
}

class EquityBuildUpModel{
    var balance: Double?
    var interest: Double?
    var principal: Double?
    init(balance : Double, interest: Double, principal: Double) {
        self.balance = balance
        self.interest = interest
        self.principal = principal
    }
}
class EquityModel{
    var value: Double?
    var balance: Double?
    var equity: Double?
    init(value : Double, balance: Double, equity: Double) {
        self.balance = balance
        self.value = value
        self.equity = equity
    }
}
