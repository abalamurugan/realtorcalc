//
//  BottomPickerDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 16/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class BottomPickerDataModel{
    
    var items: Array<Any>?
    
    var pickerTitle: String?
    
    var tag: Int?
    
    var count: Int{
        return items?.count ?? 0
    }
    subscript(_ indexPath: IndexPath)->Any{
        return items![indexPath.row]
    }
}
class BottomPickerModel {
    var key: String?
    var value: String?
    init(key: String, value: String) {
        self.key = key
        self.value = value
    }
}
