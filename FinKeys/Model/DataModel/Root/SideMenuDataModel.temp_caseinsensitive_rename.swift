//
//  SidemenuDataModel.swift
//  RealtorCalc
//
//  Created by Bala on 14/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit

enum MenuTitle : String{
    case financialKey = "Financial Keys"
    case contact = "Add Contacts"
    case account = "Accounts"
    case settings = "Settings"
    case logout = "Logout"
}
class SideMenuDataModel {
    var count : Int{
        return menuList.count
    }
    subscript(_ indexPath : IndexPath)->MenuItem{
        return menuList[indexPath.row]
    }
    var menuList : Array<MenuItem> = []
    init() {
        menuList.append(MenuItem.init(title: MenuTitle.financialKey.rawValue, icon: UIImage.init(named: "home")))
        menuList.append(MenuItem.init(title: MenuTitle.contact.rawValue, icon: UIImage.init(named: "phone-book")))
        menuList.append(MenuItem.init(title: MenuTitle.account.rawValue, icon: UIImage.init(named: "settings")))
        menuList.append(MenuItem.init(title: MenuTitle.settings.rawValue, icon: UIImage.init(named: "user")))
        menuList.append(MenuItem.init(title: MenuTitle.logout.rawValue, icon: UIImage.init(named: "logout")))
 
    }
}
class MenuItem {
    var title : String?
    var icon : UIImage?
    var segueID : String?
    
    var type : MenuTitle?{
        let _type = MenuTitle.init(rawValue: title ?? "Financial Keys")
        if let menu = _type{
            return menu
        }
        return nil
    }
    var index : Int{
        if type! == .financialKey{
            return 0
        }else if type! == .contact{
            return 1
        }else if type! == .account{
            return 2
        }else{
            return 3
        }
    }
    
    init(title : String, icon : UIImage?, segueId: String? = nil) {
        self.title = title
        self.icon = icon
        self.segueID = segueId
    }
}
