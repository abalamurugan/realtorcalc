//
//  ProtocolIdentifier.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

/// set reusable identifier to UITableViewCell and UICollectionViewCell
protocol CellReusable: class {
    static var reuseIdentifier: String { get }
}

/// set Stoaryboard Segue Identifier to UIViewController, UITableViewController , UICollectionViewController
protocol StoryboardSegueIdentifier: class {
    static var segueIdentifier: String { get }
}
