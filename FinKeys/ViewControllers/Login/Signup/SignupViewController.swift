//
//  SignupViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var mlsNoTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var officePhoneTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var faxTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var addressTextView: UITextView!
    
    private var mlsList: Array<DropDownInfoModel>?
    private var selectedMls: DropDownInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        // Do any additional setup after loading the view.
    }
    private func setup(){
        self.fetch()
        self.mlsNoTextField.delegate = self
        self.addressTextView.layer.borderColor = UIColor.lightGray.cgColor
        self.addressTextView.layer.borderWidth = 1
    }
    private func fetch(){
        CommonRequest.dropDownAPI(type: DropDownAPIType.mlsBox) { (list) in
            self.mlsList = list
        }
    }
 
    @IBAction func dismissAction(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func registerAction(_ sender: UIButton){
        if isValidationSuccess(){
            LoadingView.instance.showActivityIndicator(uiView: self.view)
            self.registerAPI()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension SignupViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.mlsNoTextField.text = model?.name
            self.selectedMls = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mlsNoTextField{
            self.showBottomPicker(delegate: self, items: mlsList ?? [], tag: 0, title: "Choose MLS Type")
            return false
        }
        return true
    }
    
}
extension SignupViewController{
    func isValidationSuccess()->Bool{
        if firstNameTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("title"))
            return false
        }else if firstNameTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("first name"))
            return false
        }else if lastNameTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("last name"))
            return false
        }else if emailTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("email id"))
            return false
        }else if mobileNumberTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("mobile number"))
            return false
        }else if companyNameTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("company name"))
            return false
        }else if passwordTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("password"))
            return false
        }else if confirmPasswordTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("confirm password"))
            return false
        }else if passwordTextField.text != confirmPasswordTextField.text{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.loginValidation.passwordNotMatch)
            return false
        }else if cityTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("city"))
            return false
        }else if stateTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("state"))
            return false
        }else if zipTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("zip"))
            return false
        }else if faxTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("fax"))
            return false
        }else if officePhoneTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("office phone"))
            return false
        }else if selectedMls == nil{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("mls no"))
            return false
        }else if addressTextView.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("address"))
            return false
        }

        
        return true
    }
    var param: Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict.updateValue(firstNameTextField.text!, forKey: APIKey.authenticate.key.firstname)
        dict.updateValue(passwordTextField.text!, forKey: APIKey.authenticate.key.password)
        dict.updateValue(emailTextField.text!, forKey: APIKey.authenticate.key.email)
        dict.updateValue(mobileNumberTextField.text!, forKey: APIKey.authenticate.key.cellNo)
        dict.updateValue(titleTextField.text!, forKey: APIKey.authenticate.key.txtTitle)
        dict.updateValue(officePhoneTextField.text!, forKey: APIKey.authenticate.key.telephone)
        dict.updateValue(faxTextField.text!, forKey: APIKey.authenticate.key.txtFax)
        dict.updateValue(addressTextView.text, forKey: APIKey.authenticate.key.txtAddress)
        dict.updateValue(cityTextField.text!, forKey: APIKey.authenticate.key.txtCity)
        dict.updateValue(stateTextField.text!, forKey: APIKey.authenticate.key.txtState)
        dict.updateValue(zipTextField.text!, forKey: APIKey.authenticate.key.txtZip)
        dict.updateValue(Constants.tagValue.register, forKey: APIKey.common.key.tag)
        return dict
    }
}
extension SignupViewController{
    func registerAPI(){
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: param, encoding: .queryString, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let message = json["success_message"] as? String{
                self.showAlert("Success", message){
                    self.dismiss(animated: true, completion: nil)
                }
            }
//            self.showAlert("Success", "Account created successfully.")
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
