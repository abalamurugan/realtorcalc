//
//  LoginViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    //Default login credential for testing
    private func defaultValues(){
        emailTextField.text = "dineshgovindaswamy@gmail.com"
        passwordTextField.text = "123456"
    }
    @IBAction func loginAction(_ sender : UIButton){
        if isValidationSuccess(){
            self.loginRequest()
        }
    }
    private func loginRequest(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        let param = [APIKey.authenticate.key.email: emailTextField.text!, APIKey.authenticate.key.password : passwordTextField.text!, APIKey.authenticate.key.tag : Constants.tagValue.login]
        connection.sendRequest(urlPath: nil,method: .post, param: param, encoding: .queryString, { (successResponse, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if successResponse.isSuccess{
                UserStorage.setValues(value: successResponse, key: StorageKey.userDetails.rawValue)
                let userDetails = UserDetailsModel.init(dict: successResponse)
                UserManager.instance.userModel = userDetails
                self.performSegue(withIdentifier: TabBarViewController.segueIdentifier, sender: self)
            }
           
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showAlert("Error", "Invalid email or password")
        }
    }
    private func isValidationSuccess()->Bool{
        if emailTextField.text?.isEmpty ?? false{
            showAlert(LocalizableKey.error.errorTitle, LocalizableKey.loginValidation.emailInvalid)
            return false
        }else if passwordTextField.text?.isEmpty ?? false{
            showAlert(LocalizableKey.error.errorTitle, LocalizableKey.loginValidation.passwordInvalid)
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
