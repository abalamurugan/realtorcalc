//
//  ForgotPasswordViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showForgotPassword"
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        self.setupLeftBackButton()
        self.title = LocalizableKey.navigationTitle.forgotPassword
    }
    
    func isValidationSuccess()->Bool{
        if emailTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Email"))
            return false
        }
        return true
    }
    @IBAction func submitAction(_ sender: UIButton){
        self.otpProcess()
    }
    private func otpProcess(){
        if isValidationSuccess(){
            self.emailTextField.resignFirstResponder()
            self.otpTextField.resignFirstResponder()
            if otpTextField.isHidden{
                self.generateOTP()
            }else{
                self.verifyOTP()
            }
        }
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == ChangePasswordViewController.segueIdentifier{
            let changePasswordController = segue.destination as! ChangePasswordViewController
            changePasswordController.email = emailTextField.text!
        }
    }
 
}
extension ForgotPasswordViewController{
    func generateOTP(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.authenticate.key.enterEmail: emailTextField.text!, APIKey.common.key.tag: Constants.tagValue.otpGenerate]
        connection.sendRequest(urlPath: nil, method: .post, param: param, imageData: nil, imageName: nil, encoding: .queryString, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let response = json["response"] as? Dictionary<String, Any>{
                if let otp = response["OTP"] as? Int{
                    self.otpTextField.isHidden = false
                    self.submitButton.setTitle("Submit OTP", for: .normal)
                    self.otpTextField.text = "\(otp)"
                }
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    func verifyOTP(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.authenticate.key.enterEmail: emailTextField.text!, APIKey.common.key.tag: Constants.tagValue.otpVerify, APIKey.authenticate.key.enterOtp: otpTextField.text!]
        connection.sendRequest(urlPath: nil, method: .post, param: param, imageData: nil, imageName: nil, encoding: .queryString, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.performSegue(withIdentifier: ChangePasswordViewController.segueIdentifier, sender: self)
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
