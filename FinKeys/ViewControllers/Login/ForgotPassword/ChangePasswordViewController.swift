//
//  ChangePasswordViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
enum ChangePasswordType{
    case forgot
    case change
}

class ChangePasswordViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showChangePassword"
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var email: String!
    var type: ChangePasswordType = .forgot


    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        self.setupLeftBackButton()
        self.title = LocalizableKey.navigationTitle.changePassword
        self.oldPasswordTextField.isHidden = type == .forgot ? true : false
    }
    
    private func isValidationSuccess()->Bool{
        if type == .change{
            if oldPasswordTextField.isEmpty{
                self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("old Password"))
                return false
            }
        }
        if newPasswordTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("New Password"))
            return false
        }else if confirmPasswordTextField.isEmpty{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Confirm Password"))
            return false
        }else if newPasswordTextField.text != confirmPasswordTextField.text{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.loginValidation.passwordNotMatch)
            return false
        }
        return true
    }
    @IBAction func saveAction(_ sender: UIButton){
        if isValidationSuccess(){
            self.changePassword()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChangePasswordViewController{
    func changePassword(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.authenticate.key.enterEmail: email!, APIKey.common.key.tag: Constants.tagValue.passwordChange, APIKey.authenticate.key.newPass: newPasswordTextField.text!, APIKey.authenticate.key.confirmPass: confirmPasswordTextField.text!]
        connection.sendRequest(urlPath: nil, method: .post, param: param, imageData: nil, imageName: nil, encoding: .queryString, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showAlert("Success", "Password changed successfully"){
                self.dismiss(animated: true, completion: nil)
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
