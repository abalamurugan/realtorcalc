//
//  BottomPickerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 16/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
protocol BottomPickerViewControllerDelegate {
    func didPickedItem(viewController: BottomPickerViewController, item: Any)
    func didPickerCancelled(viewController: BottomPickerViewController)
}

class BottomPickerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var heightConstraints: NSLayoutConstraint!

    
    lazy var dataModel: BottomPickerDataModel = {return BottomPickerDataModel()}()
    
    var delegate: BottomPickerViewControllerDelegate?
    
    class func showBottomPicker()->BottomPickerViewController{
        let bottomPicker = Storyboard.reusable.storyboard.instantiateViewController(withIdentifier: "BottomPickerViewController") as! BottomPickerViewController
        bottomPicker.modalPresentationStyle = .overCurrentContext
        bottomPicker.modalTransitionStyle = .crossDissolve
        return bottomPicker
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        self.titleLabel.text = self.dataModel.pickerTitle
    }
    @IBAction func dismissAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BottomPickerViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BottomPickerTableViewCell.reuseIdentifier, for: indexPath) as! BottomPickerTableViewCell
        cell.set(dataModel[indexPath])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate?.didPickedItem(viewController: self, item: self.dataModel[indexPath])
        }
    }
}
