//
//  TabBarViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showTabBar"
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        // Do any additional setup after loading the view.
    }
    private func configUI(){
        self.tabBar.items![2].image = UIImage.init(named: "search")
        self.tabBar.items![2].selectedImage = UIImage.init(named: "search")

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
