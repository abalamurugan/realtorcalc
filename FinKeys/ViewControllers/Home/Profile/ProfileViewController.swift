//
//  ProfileViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 08/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showProfile"
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var dataModel: ProfileDataModel = {return ProfileDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.fetchProfileDetails()
        // Do any additional setup after loading the view.
    }
    
    private func configureUI(){
        self.tableView.estimatedRowHeight = 100
        self.registerCell()
        self.title = LocalizableKey.navigationTitle.account
        self.rightBarButtonWithIcon(image: UIImage.init(named: "edit")!)
    }
    private func registerCell(){
        self.tableView.register(ProfileHeaderTableViewCell.nib, forCellReuseIdentifier: ProfileHeaderTableViewCell.reuseIdentifier)
        self.tableView.register(ProfileDetailsTableViewCell.nib, forCellReuseIdentifier: ProfileDetailsTableViewCell.reuseIdentifier)

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == ContactFormViewController.segueIdentifier{
            let profileEditController = segue.destination as! ContactFormViewController
            profileEditController.dataModel.type = .profile
            profileEditController.dataModel.profileDetails = self.dataModel.profileItem
            profileEditController.didContactCreatedCallback { (bool) in
                self.fetchProfileDetails()
            }
        }
    }
    

}
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderTableViewCell.reuseIdentifier, for: indexPath) as! ProfileHeaderTableViewCell
            cell.locationLabel.text = dataModel.profileItem?.location
            cell.profileImageView.sd_setImage(with: dataModel.profileItem?.imageURL, placeholderImage: UIImage.init(named: "placeholder"))

//            cell.editButton.tag = indexPath.row
//            cell.editButton.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
            cell.nameLabel.text = dataModel.profileItem?.fullName
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileDetailsTableViewCell.reuseIdentifier, for: indexPath) as! ProfileDetailsTableViewCell
        cell.set(item: dataModel.profileList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 220
        }
        return UITableView.automaticDimension
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: ContactFormViewController.segueIdentifier, sender: self)

    }
//    @objc func editAction(_ sender: UIButton){
//        self.performSegue(withIdentifier: ContactFormViewController.segueIdentifier, sender: self)
//    }
}

// MARK: - API Call
extension ProfileViewController{
     func fetchProfileDetails(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.userId: UserManager.instance.userID, APIKey.common.key.tag: Constants.tagValue.getProfile]
        connection.networkRequest(urlPath: nil, method: .get, param: param, encoding: .queryString, { (response:ResponseModel<ProfileModel>) in
            self.dataModel.profileItem = response.item
            self.tableView.reloadData()
        }) { (error) in
            self.showError(error)
        }
    }
}
