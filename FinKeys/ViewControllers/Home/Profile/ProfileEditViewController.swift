//
//  ProfileEditViewController.swift
//  RealtorCalc
//
//  Created by Bala on 11/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SideMenu

class ProfileEditViewController: BaseViewController {
    
    @IBOutlet weak var editSaveButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.profile
        self.setupLeftMenu()
        self.sideMenuSetup()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- SideMenu Delegate
extension ProfileEditViewController: UISideMenuNavigationControllerDelegate, SideMenuViewControllerDelegate {
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        if let sideMenu = menu.topViewController as? SideMenuViewController{
            sideMenu.delegate = self
        }
    }
    func menuItemDidSelected(_ item: MenuItem) {
        self.tabBarController?.selectedIndex = item.index
    }
}
