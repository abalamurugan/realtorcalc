//
//  AdjustableFormTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 17/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
protocol AdjustableFormTableViewControllerDelegate {
    func didChangeAdjustableValues(viewController:AdjustableFormTableViewController, model: AdjustedModel)
}
class AdjustableFormTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showAdjusted"
    
    @IBOutlet weak var garageTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var carportTextField: UITextField!
    @IBOutlet weak var firePlaceTextField: UITextField!
    @IBOutlet weak var centralAirTextField: UITextField!
    @IBOutlet weak var perFullPathTextField: UITextField!
    @IBOutlet weak var perThreeByFourPathTextField: UITextField!
    @IBOutlet weak var perBedRoomPathTextField: UITextField!
    @IBOutlet weak var bsmtFinishTextField: UITextField!
    @IBOutlet weak var bsmtUnFinishTextField: UITextField!
    @IBOutlet weak var mainFloorTextField: UITextField!
    @IBOutlet weak var upperFloorTextField: UITextField!
    @IBOutlet weak var fullLandscapeTextField: UITextField!
    @IBOutlet weak var ageVarienceTextField: UITextField!
    @IBOutlet weak var appreciationTextField: UITextField!
    @IBOutlet weak var perHalfBathTextField: UITextField!


    var delegate: AdjustableFormTableViewControllerDelegate?
    var adjustModel = AdjustedModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        self.mapValues()
        self.dateTextField.delegate = self
        self.garageTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    private func mapValues(){
        garageTextField.text = adjustModel.garage?.toString
        carportTextField.text = adjustModel.carport?.toString
        firePlaceTextField.text = adjustModel.fireplace?.toString
        centralAirTextField.text = adjustModel.centralAir?.toString
        perFullPathTextField.text = adjustModel.perFullBath?.toString
        perThreeByFourPathTextField.text = adjustModel.perThreeFourBath?.toString
        perBedRoomPathTextField.text = adjustModel.perBedRoom?.toString
        bsmtFinishTextField.text = adjustModel.bsmtFinish?.toString
        bsmtUnFinishTextField.text = adjustModel.bsmtUnfinish?.toString
        mainFloorTextField.text = adjustModel.mainFloor?.toString
        upperFloorTextField.text = adjustModel.upperFloor?.toString
        fullLandscapeTextField.text = adjustModel.fullLandscape?.toString
        ageVarienceTextField.text = adjustModel.ageVarience?.toString
        appreciationTextField.text = adjustModel.appreciation?.toString
        perHalfBathTextField.text = adjustModel.perHalfBath?.toString
    }
    private func configUI(){
        self.title = LocalizableKey.navigationTitle.adjustable
        let leftButton = UIBarButtonItem.init(image: UIImage.init(named: "icn_close"), style: .plain, target: self, action: #selector(dismissAction))
        self.navigationItem.leftBarButtonItem = leftButton
        let rightButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneAction))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.rightBarButtonItem = rightButton
    }
    @objc func dismissAction(){
        self.dismiss(animated: true) {
            
        }
    }
    @objc func doneAction(){
        self.dismiss(animated: true) {
            self.delegate?.didChangeAdjustableValues(viewController: self, model: self.adjustedModel)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension AdjustableFormTableViewController{
    var adjustedModel: AdjustedModel{
        let model = AdjustedModel()
        model.garage = garageTextField.toDouble
        model.carport = carportTextField.toDouble
        model.fireplace = firePlaceTextField.toDouble
        model.centralAir = centralAirTextField.toDouble
        model.perFullBath = perFullPathTextField.toDouble   
        model.perThreeFourBath = perThreeByFourPathTextField.toDouble
        model.perBedRoom = perBedRoomPathTextField.toDouble
        model.bsmtFinish = bsmtFinishTextField.toDouble
        model.bsmtUnfinish = bsmtUnFinishTextField.toDouble
        model.mainFloor = mainFloorTextField.toDouble
        model.upperFloor = upperFloorTextField.toDouble
        model.fullLandscape = fullLandscapeTextField.toDouble
        model.ageVarience = ageVarienceTextField.toDouble
        model.appreciation = appreciationTextField.toDouble
        model.perHalfBath = perHalfBathTextField.toDouble
        return model
    }
}
extension AdjustableFormTableViewController: UITextFieldDelegate{

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateTextField{
            DatePicker.show(self) { (date) in
                let dateFormat = date.toString(format: "dd-MM-yyyy")
                self.dateTextField.text = dateFormat
            }
            return false
        }
        return true
    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.text = textField.text?.removeCharacters(from: CharacterSet.decimalDigits.inverted)
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        textField.text = textField.text?.toDouble.readableFormat
//    }
}
