//
//  ComparableContainerViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 17/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
enum ComparableType: Int {
    case subject = 0 , comparable1 = 1, comparable2 = 2, comparable3 = 3
}

class ComparableContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showComparable"
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var firstComparableContainerView: UIView!
    @IBOutlet weak var secondComparableContainerView: UIView!
    @IBOutlet weak var thirdComparableContainerView: UIView!
    @IBOutlet weak var subjectContainerView: UIView!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var adjustButton: UIButton!
    
    var adjustedModel = AdjustedModel()
    var subjectModel: SubjectModel?
    var acTypeList: Array<DropDownInfoModel>?
    var yesNoList: Array<DropDownInfoModel>?
    var statusList: Array<DropDownInfoModel>?

    var firstComparableController: FirstComparableTableViewController!
    var secondComparableController: SecondComparableTableViewController!
    var thirdComparableController: ThirdComparableTableViewController!
    var subjectController: SubjectPropertyTableViewController!
    
    var comparableDetails: MarketAnalysisDetailModel?
    var dispatchGroup = DispatchGroup.init()
    var subParam: Dictionary<String, Any>?
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setupLeftBackButton()
        self.configureUI()
        self.fetch()
        self.mapping()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    private func mapping(){
        adjustedModel.garage = "10000".toDouble
        adjustedModel.carport = "1000".toDouble
        adjustedModel.fireplace = "2000".toDouble
        adjustedModel.centralAir = "2500".toDouble
        adjustedModel.perFullBath = "5000".toDouble
        adjustedModel.perThreeFourBath = "3500".toDouble
        adjustedModel.perBedRoom = "1000".toDouble
        adjustedModel.bsmtFinish = "15".toDouble
        adjustedModel.bsmtUnfinish = "10".toDouble
        adjustedModel.mainFloor = "110".toDouble
        adjustedModel.upperFloor = "110".toDouble
        adjustedModel.fullLandscape = "10000".toDouble
        adjustedModel.ageVarience = "500".toDouble
        adjustedModel.appreciation = "3.50".toDouble
 //        self.firstComparableController.comparableOneDetails = comparableDetails?.comparable1_info
//        self.secondComparableController.comparableTwoDetails = comparableDetails?.comparable2_info
//        self.thirdComparableController.comparableThreeDetails = comparableDetails?.comparable3_info
     }
    private func configureUI(){
        self.adjustButton.layer.cornerRadius = 5
        self.calculateButton.layer.cornerRadius = 5
        self.adjustButton.layer.borderColor = UIColor.primaryColor.cgColor
        self.adjustButton.layer.borderWidth = 1
        subjectContainerView.isHidden = false
        firstComparableContainerView.isHidden = true
        secondComparableContainerView.isHidden = true
        thirdComparableContainerView.isHidden = true
        
        self.title = LocalizableKey.navigationTitle.marketAnalysis
    }
   
    @IBAction func adjustAction(_ sender: UIButton){
        let adjustableController = Storyboard.MarketAnalysis.storyboard.instantiateViewController(withIdentifier:
            "AdjustableFormTableViewController") as! AdjustableFormTableViewController
        adjustableController.adjustModel = self.adjustedModel
        adjustableController.delegate = self
        self.presentWithNavigationController(adjustableController)
    }
    @IBAction func segementAction(_ sender: UISegmentedControl){
        let type = ComparableType.init(rawValue: sender.selectedSegmentIndex)
        switch type! {
        case .subject:
            subjectContainerView.isHidden = false
            firstComparableContainerView.isHidden = true
            secondComparableContainerView.isHidden = true
            thirdComparableContainerView.isHidden = true
            break
        case .comparable1:
            subjectContainerView.isHidden = true
            firstComparableContainerView.isHidden = false
            secondComparableContainerView.isHidden = true
            thirdComparableContainerView.isHidden = true
            break
        case .comparable2:
            subjectContainerView.isHidden = true
            firstComparableContainerView.isHidden = true
            secondComparableContainerView.isHidden = false
            thirdComparableContainerView.isHidden = true
            break
        case .comparable3:
            subjectContainerView.isHidden = true
            firstComparableContainerView.isHidden = true
            secondComparableContainerView.isHidden = true
            thirdComparableContainerView.isHidden = false
            break
       
        }
       
    }
    @IBAction func calcAction(_ sender: UIButton){
        self.postFormValues()
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FirstComparableTableViewController.segueIdentifier{
            firstComparableController = segue.destination as? FirstComparableTableViewController
        }else if segue.identifier == SecondComparableTableViewController.segueIdentifier{
            secondComparableController = segue.destination as? SecondComparableTableViewController
        }else if segue.identifier == ThirdComparableTableViewController.segueIdentifier{
            thirdComparableController = segue.destination as? ThirdComparableTableViewController
        }else if segue.identifier == MarketAnalysisOutputViewController.segueIdentifier{
            let marketOutputController = segue.destination as! MarketAnalysisOutputViewController
            marketOutputController.outputList = compareOutputList
            marketOutputController.contactModel = self.contactModel
        }else if segue.identifier == SubjectPropertyTableViewController.segueIdentifier{
            self.subjectController = segue.destination as? SubjectPropertyTableViewController
        }else if segue.identifier == AdjustableFormTableViewController.segueIdentifier{
            let adjustController = segue.destination as! AdjustableFormTableViewController
            adjustController.delegate = self
            adjustController.adjustModel = self.adjustedModel
        }else if segue.identifier == MLSNumberViewController.segueIdentifier{
            let mlsController = segue.destination as? MLSNumberViewController
            mlsController?.mlsCallback({ (mlsDetails) in
                self.fetchMLS(mlsModel: mlsDetails)
            })
        }else if segue.identifier == InvestmentOutputViewController.segueIdentifier{
            let outputController = segue.destination as? InvestmentOutputViewController
            outputController?.dataModel.type = .cma
            var outputList = Array<RefinanceOutputModel>()
            
            var subjectList = subjectController.outputList
            subjectList.append(OutputDetailsModel.init(key: LocalizableKey.cma.adjusted, value: subjectAdjusted, valueTwo: nil))
            
            outputList.append(RefinanceOutputModel.init(title: LocalizableKey.cma.subjectProperty, list: subjectList))
            outputList.append(firstComparableController.firstComparableOutput)
            outputList.append(secondComparableController.secondComparableOutput)
            outputList.append(thirdComparableController.thirdComparableOutput)
            outputController?.dataModel.outputList = outputList
            outputController?.dataModel.contactModel = self.contactModel
        }
    }
}
extension ComparableContainerViewController{
    var subjectAdjusted: Double{
        var total = firstComparableController.totalAdjusted
        total += secondComparableController.totalAdjusted
        total += thirdComparableController.totalAdjusted
        return (total/3).rounded()
    }
    
    var subjectProperty: MarketOutputModel{
        let model = MarketOutputModel()
        model.title = "Subject Property"
        model.address = ""
        model.proximity = ""
        model.adjSalesPrice = subjectAdjusted
        return model
    }
    var comparableFirst: MarketOutputModel{
        let model = MarketOutputModel()
        model.title = "Comparable 1 of 3"
        model.address = firstComparableController.address
        model.proximity = firstComparableController.proximity
        model.adjSalesPrice = firstComparableController.totalAdjusted
        model.adjustValue = firstComparableController.adjusted
        model.listedPrice = firstComparableController.listedPrice
        model.salesPrice = firstComparableController.salesPrice
        return model
    }
    var comparableSecond: MarketOutputModel{
        let model = MarketOutputModel()
        model.title = "Comparable 2 of 3"
        model.address = secondComparableController.address
        model.proximity = secondComparableController.proximity
        model.adjSalesPrice = secondComparableController.totalAdjusted
        model.adjustValue = secondComparableController.adjusted
        model.listedPrice = secondComparableController.listedPrice
        model.salesPrice = secondComparableController.salesPrice
        return model
    }
    var comparableThird: MarketOutputModel{
        let model = MarketOutputModel()
        model.title = "Comparable 3 of 3"
        model.address = thirdComparableController.address
        model.proximity = thirdComparableController.proximity
        model.adjSalesPrice = thirdComparableController.totalAdjusted
        model.adjustValue = thirdComparableController.adjusted
        model.listedPrice = thirdComparableController.listedPrice
        model.salesPrice = thirdComparableController.salesPrice
        return model
    }
    var compareOutputList: Array<MarketOutputModel>{
        var outputList: Array<MarketOutputModel> = []
        outputList.append(subjectProperty)
        outputList.append(comparableFirst)
        outputList.append(comparableSecond)
        outputList.append(comparableThird)
        return outputList
    }
}
extension ComparableContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        if let subParam = subParam{
            subParam.forEach { (key,value) in
                dict.updateValue(value, forKey: key)
            }
        }
        firstComparableController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        secondComparableController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        thirdComparableController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        subjectController.updateParam.forEach { (key,value) in
            dict.updateValue(value, forKey: key)
        }
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.reportNames.CMA, forKey: APIKey.common.key.action)
        dict.updateValue(Constants.tagValue.CMAAdd, forKey: APIKey.common.key.tag)
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.performSegue(withIdentifier: InvestmentOutputViewController.segueIdentifier, sender: self)
//            self.performSegue(withIdentifier: MarketAnalysisOutputViewController.segueIdentifier, sender: self)
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
extension ComparableContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getCMA()
    }
}
extension ComparableContainerViewController{
    var paramViewCMA: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.CMAView, contactId: self.contactModel!.contactId!)
        
    }
    private func showMLS(){
        if let mlsId = UserManager.instance.userModel?.mlsId, mlsId != "1"{
            self.performSegue(withIdentifier: MLSNumberViewController.segueIdentifier, sender: self)
        }
    }
    private func getCMA(mlsParm: Dictionary<String, Any>? = nil){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewCMA, encoding: .queryString, { (response: ResponseModel<MarketAnalysisModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            let item = response.item?.response
            self.subjectController.subjectDetails = item?.subject_info
            self.firstComparableController.comparableOneDetails = item?.comparable1_info
            self.secondComparableController.comparableTwoDetails = item?.comparable2_info
            self.thirdComparableController.comparableThreeDetails = item?.comparable3_info
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showMLS()
        }
    }
    private func mlsCMA(mlsParm: Dictionary<String, Any>){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: mlsParm, encoding: .queryString, { (response: ResponseModel<MarketAnalysisModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            let item = response.item?.response
            if let sub = item?.subject_info{
                self.subjectController.subjectDetails = sub
            }
//            if self.comparableDetails == nil{
//                self.comparableDetails = item
//            }else{
                if let comp1Values = item?.comparable1_info{
                    self.firstComparableController.comparableOneDetails = comp1Values
                }
                if let comp2Values = item?.comparable2_info{
                    self.secondComparableController.comparableTwoDetails = comp2Values
                }
                if let comp3Values = item?.comparable3_info{
                    self.thirdComparableController.comparableThreeDetails = comp3Values
                }
//            }
            
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    private func fetchMLS(mlsModel: MLSModel){
        let key = APIKey.cma.key.self
        var param = Param.viewReportParam(tagReport: Constants.tagValue.MLSData, contactId: self.contactModel!.contactId!)
        if let submls = mlsModel.subjectMLSNumber, submls != ""{
            param.updateValue(submls, forKey: key.mlsS)
        }
        if let comp1MLS = mlsModel.comp1MLSNumber{
            param.updateValue(comp1MLS, forKey: key.mlsC1)
        }
        if let comp2MLS = mlsModel.comp2MLSNumber{
            param.updateValue(comp2MLS, forKey: key.mlsC2)
        }
        if let comp3MLS = mlsModel.comp3MLSNumber{
            param.updateValue(comp3MLS, forKey: key.mlsC3)
        }
        self.mlsCMA(mlsParm: param)
    }
    
}
extension ComparableContainerViewController{
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        self.dispatchGroup.enter()
        CommonRequest.dropDownAPI(type: .acBox) { (acList) in
            self.acTypeList = acList
            self.dispatchGroup.leave()
            self.firstComparableController.acTypeList = self.acTypeList
            self.secondComparableController.acTypeList = self.acTypeList
            self.thirdComparableController.acTypeList = self.acTypeList
            self.subjectController.acTypeList = self.acTypeList
        }
        self.dispatchGroup.enter()
        CommonRequest.dropDownAPI(type: .statusBox, model: { (statusList) in
            self.statusList = statusList
            self.dispatchGroup.leave()
            self.firstComparableController.statusList = self.statusList
            self.secondComparableController.statusList = self.statusList
            self.thirdComparableController.statusList = self.statusList
            self.subjectController.statusList = self.statusList

        })
        self.dispatchGroup.enter()
        CommonRequest.dropDownAPI(type: .yesnoBox, model: { (yesnoList) in
            self.yesNoList = yesnoList
            self.dispatchGroup.leave()
            self.firstComparableController.yesNoList = self.yesNoList
            self.secondComparableController.yesNoList = self.yesNoList
            self.thirdComparableController.yesNoList = self.yesNoList
            self.subjectController.yesNoList = self.yesNoList
        })
        self.dispatchGroup.notify(queue: DispatchQueue.main) {
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
        }
    }
    
}
extension ComparableContainerViewController: AdjustableFormTableViewControllerDelegate{
    func didChangeAdjustableValues(viewController: AdjustableFormTableViewController, model: AdjustedModel) {
        self.adjustedModel = model
    }
}
