//
//  FirstComparableTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 17/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FirstComparableTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showComp1"
    
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var dateSoldTextField: UITextField!
    @IBOutlet weak var styleTextField: UITextField!
    @IBOutlet weak var yearBuiltTextField: UITextField!
    @IBOutlet weak var acresTextField: UITextField!
    @IBOutlet weak var landscapeTextField: UITextField!
    @IBOutlet weak var lotValueTextField: UITextField!
    @IBOutlet weak var garageValueTextField: UITextField!
    @IBOutlet weak var carportTextField: UITextField!
    @IBOutlet weak var squareFtMainTextField: UITextField!
    @IBOutlet weak var squareFtUpTextField: UITextField!
    @IBOutlet weak var squareFtDownTextField: UITextField!
    @IBOutlet weak var finishDNTextField: UITextField!
    @IBOutlet weak var bedsMainTextField: UITextField!
    @IBOutlet weak var bedsUpTextField: UITextField!
    @IBOutlet weak var bedsDnTextField: UITextField!
//    @IBOutlet weak var bathsMnTextField: UITextField!
//    @IBOutlet weak var bathsUpTextField: UITextField!
//    @IBOutlet weak var bathsDnTextField: UITextField!
    @IBOutlet weak var firePlacesTextField: UITextField!
    @IBOutlet weak var acTextField: UITextField!
    @IBOutlet weak var secSysTextField: UITextField!
    @IBOutlet weak var conditionTextField: UITextField!
    @IBOutlet weak var poolTextField: UITextField!
    @IBOutlet weak var hotTubTextField: UITextField!
    @IBOutlet weak var viewTextField: UITextField!
    @IBOutlet weak var concesTextField: UITextField!
    @IBOutlet weak var listedTextField: UITextField!
    @IBOutlet weak var soldTextField: UITextField!
    
    @IBOutlet weak var acValueTextField: UITextField!
    @IBOutlet weak var secSysValueTextField: UITextField!
    @IBOutlet weak var hotTubValueTextField: UITextField!
    @IBOutlet weak var conditionValueTextField: UITextField!
    @IBOutlet weak var poolValueTextField: UITextField!
    @IBOutlet weak var viewValueTextField: UITextField!
    @IBOutlet weak var concesValueTextField: UITextField!

    @IBOutlet weak var propertyDetailsView: ImagePickerView!
    @IBOutlet weak var f1FullBathTextField: UITextField!
    @IBOutlet weak var f1ThreeFourBathTextField: UITextField!
    @IBOutlet weak var f1HalfBathTextField: UITextField!
    @IBOutlet weak var f2FullBathTextField: UITextField!
    @IBOutlet weak var f2ThreeFourBathTextField: UITextField!
    @IBOutlet weak var f2HalfBathTextField: UITextField!
    @IBOutlet weak var bFullBathTextField: UITextField!
    @IBOutlet weak var bThreeFourBathTextField: UITextField!
    @IBOutlet weak var bHalfBathTextField: UITextField!

    var comparableOneDetails: ComparableOneModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    var acTypeList: Array<DropDownInfoModel>?
    var yesNoList: Array<DropDownInfoModel>?
    var statusList: Array<DropDownInfoModel>?
    var selectedAcType: DropDownInfoModel?
    var selectedStatus: DropDownInfoModel?
    private var selectedPool: DropDownInfoModel?
    private var selectedHotTub: DropDownInfoModel?
    private var selectedSecSys: DropDownInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.setup()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        yearBuiltTextField.text = "2002"
        acresTextField.text = "0.25"
        landscapeTextField.text = "100"
        lotValueTextField.text = "270000"
        garageValueTextField.text = "3.0"
        carportTextField.text = "0.0"
        squareFtMainTextField.text = "2100"
        squareFtUpTextField.text = "1750"
        squareFtDownTextField.text = "2100"
        finishDNTextField.text = "50"
        bedsMainTextField.text = "1"
        bedsUpTextField.text = "4"
        bedsDnTextField.text = "1"
//        bathsMnTextField.text = "1.5"
//        bathsDnTextField.text = "2.0"
//        bathsUpTextField.text = "1.0"
        firePlacesTextField.text = "2"
        listedTextField.text = "545900"
        dateSoldTextField.text = "28-02-2019"
        soldTextField.text = "535000"
    }
    private func setup(){
        self.callBack()
    }
    func callBack(){
        self.connectDelegate()
        propertyDetailsView.imagePickerCallBack { (bool) in
            self.openMediaLibrary(delegate: self, button: self.propertyDetailsView.propertyImageButton)
        }
    }
    private func selectedValues(){
        if let acBox = self.comparableOneDetails?.acC1{
            if let index = self.acTypeList?.index(of: DropDownInfoModel.init(value: acBox)){
                self.selectedAcType = self.acTypeList?[index]
                self.acTextField.text = selectedAcType?.name
            }
        }
        if let status = self.comparableOneDetails?.statusC1{
            if let index = self.statusList?.index(of: DropDownInfoModel.init(value: status)){
                self.selectedStatus = self.statusList?[index]
                self.statusTextField.text = selectedStatus?.name
            }
        }
        if let secSys = self.comparableOneDetails?.secSystC1{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: secSys)){
                self.selectedSecSys = self.yesNoList?[index]
                self.secSysTextField.text = selectedSecSys?.name
            }
        }
        if let pool = self.comparableOneDetails?.poolC1{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: pool)){
                self.selectedPool = self.yesNoList?[index]
                self.poolTextField.text = selectedPool?.name
            }
        }
        if let hotTub = self.comparableOneDetails?.hotTubC1{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: hotTub)){
                self.selectedHotTub = self.yesNoList?[index]
                self.hotTubTextField.text = selectedHotTub?.name
            }
        }
    }
    func connectDelegate(){
        self.dateSoldTextField.delegate = self
        self.acTextField.delegate = self
        self.statusTextField.delegate = self
        self.secSysTextField.delegate = self
        self.poolTextField.delegate = self
        self.hotTubTextField.delegate = self
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FirstComparableTableViewController{
    var totalSquareFt: Double{
        var total = squareFtMainTextField.toDouble
        total += squareFtUpTextField.toDouble
        total += squareFtDownTextField.toDouble
        return total
    }
    var totalBeds: Double{
        var total = bedsMainTextField.toDouble
        total += bedsUpTextField.toDouble
        total += bedsDnTextField.toDouble
        return total
    }
//    var totalBaths: Double{
//        var total = bathsMnTextField.toDouble
//        total += bathsUpTextField.toDouble
//        total += bathsDnTextField.toDouble
//        return total
//    }
    var dayFromCurrent: Double{
        return Double(dateSoldTextField.text!.daysBetween)
    }
    var firstAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let soldValue = soldTextField.toDouble
        let appreciation = parent.adjustedModel.appreciation ?? 0
        let value = soldValue * appreciation.toValue / 365 * dayFromCurrent
        print("=========Check==========")
        print("FirstAdjValue====>\(value)")
        return value
    }
    var secondAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subYear = parent.subjectController.subjectModel.year ?? 0
        let firstCompYear = self.yearBuiltTextField.toDouble
        let yearDiff = (subYear - firstCompYear)
        let value = yearDiff * (parent.adjustedModel.ageVarience ?? 0)
        print("SecondAdjValue====>\(value)")
        return value
    }
    var thirdAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subLandscape = parent.subjectController.subjectModel.landscape ?? 0
        let compLandscape = self.landscapeTextField.toDouble
        let landscapeDiff = (subLandscape - compLandscape)
        let value = landscapeDiff * (parent.adjustedModel.fullLandscape ?? 0)
        print("ThirdAdjValue====>\(value)")
        return value
    }
    var fourthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subLot = parent.subjectController.subjectModel.lotValue ?? 0
        let value = (subLot - lotValueTextField.toDouble)
        print("FourAdjValue====>\(value)")
        return value
        
    }
    var fiveAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subGarage = parent.subjectController.subjectModel.garage ?? 0
        let compGarage = self.garageValueTextField.toDouble
        let garageDiff = (subGarage - compGarage)
        let value = garageDiff * (parent.adjustedModel.garage ?? 0)
        print("fiveAdjValue====>\(value)")
        return value
        
    }
    var sixthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subCarport = parent.subjectController.subjectModel.carport ?? 0
        let compCarport = self.carportTextField.toDouble
        let carportDiff = (subCarport - compCarport)
        let value = carportDiff * (parent.adjustedModel.carport ?? 0)
        print("sixAdjValue====>\(value)")
        return value
    }
    var seventhAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subSqFtMn = parent.subjectController.subjectModel.sqFtmain ?? 0
        let compSqFtMn = self.squareFtMainTextField.toDouble
        let sqFtMnDiff = (subSqFtMn - compSqFtMn)
        let value = sqFtMnDiff * (parent.adjustedModel.mainFloor ?? 0)
        print("sevenAdjValue====>\(value)")
        return value
    }
    var eighthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subSqFtUp = parent.subjectController.subjectModel.sqFtUp ?? 0
        let compSqFtUp = self.squareFtUpTextField.toDouble
        let sqFtUpDiff = (subSqFtUp - compSqFtUp)
        let value = sqFtUpDiff * (parent.adjustedModel.upperFloor ?? 0)
        print("eightAdjValue====>\(value)")
        return value
    }
    var nineAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subSqFtDn = parent.subjectController.subjectModel.sqFtDown ?? 0
        let compSqFtDn = self.squareFtDownTextField.toDouble
        let sqFtDnDiff = (subSqFtDn - compSqFtDn)
        let value = sqFtDnDiff * (parent.adjustedModel.bsmtUnfinish ?? 0)
        print("nineAdjValue====>\(value)")
        return value
    }
   
    var tenthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subFinishDown = (+(parent.subjectController.subjectModel.sqFtDown ?? 0) * (parent.subjectController.subjectModel.finishDown ?? 0).toValue)
        let compFinishDown = self.finishDNTextField.toDouble.toValue * self.squareFtDownTextField.toDouble
        let compDnDiff = (subFinishDown - compFinishDown) * (parent.adjustedModel.bsmtFinish ?? 0)
        print("TenDnDiff===>\(compDnDiff)")
        return compDnDiff
    }
    var eleventhAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subTotalBeds = parent.subjectController.subjectModel.bedsTotal
        let compTotalBeds = self.totalBeds
        let totalBedsDiff = (subTotalBeds - compTotalBeds)
        let value = totalBedsDiff * (parent.adjustedModel.perBedRoom ?? 0)
        print("eleventh===>\(value)")
        return value
    }
    var twelveAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subTotalBaths = parent.subjectController.subjectModel.bathTotal
        let compTotalBaths = self.bathTotal
        let totalBathDiff = (subTotalBaths - compTotalBaths)
        let value = totalBathDiff * (parent.adjustedModel.perThreeFourBath ?? 0)
        print("twelve===>\(value.removeMinus)")
        return value.removeMinus
    }
    var thirteenAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subFirePlace = parent.subjectController.subjectModel.fireplace ?? 0
        let compFirePlace = self.firePlacesTextField.toDouble
        let firePlaceDiff = (subFirePlace - compFirePlace)
        let value = firePlaceDiff * (parent.adjustedModel.fireplace ?? 0)
        print("thirteen===>\(value)")
        return value
    }
    var totalAdjusted: Double{
        return (adjusted + soldTextField.toDouble).rounded()
    }
    var listedPrice: Double{
        return listedTextField.toDouble
    }
    var salesPrice: Double{
        return soldTextField.toDouble
    }
    var adjusted: Double{
        var total = firstAdjustValue
        total += secondAdjustValue
        total += thirdAdjustValue
        total += fourthAdjustValue
        total += fiveAdjustValue
        total += sixthAdjustValue
        total += seventhAdjustValue
        total += eighthAdjustValue
        total += nineAdjustValue
        total += tenthAdjustValue
        total += eleventhAdjustValue
        total += twelveAdjustValue
        total += thirteenAdjustValue
        total += acValueTextField.toDouble
        total += viewValueTextField.toDouble
        total += secSysValueTextField.toDouble
        total += conditionValueTextField.toDouble
        total += poolValueTextField.toDouble
        total += hotTubValueTextField.toDouble
        total += concesValueTextField.toDouble
        print("firstComparTotalAd=====>", total)
        return total.rounded()
    }
    var address: String{
        return propertyDetailsView.addressTextView.text
    }
    var proximity: String?{
        return propertyDetailsView.proximityTextField.text
    }
    var f1FullBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF1FullBath = parent.subjectController.subjectModel.f1FullBath ?? 0
        let adjFullBath = parent.adjustedModel.perFullBath ?? 0
        return (subF1FullBath - self.f1FullBathTextField.toDouble) * adjFullBath
    }
    var f1HalfBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF1HalfBath = parent.subjectController.subjectModel.f1HalfBath ?? 0
        let adjHalfBath = parent.adjustedModel.perHalfBath ?? 0
        return (subF1HalfBath - self.f1HalfBathTextField.toDouble) * adjHalfBath
    }
    var f1ThreeFourBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF1TFBath = parent.subjectController.subjectModel.f1ThreeFourBath ?? 0
        let adjTFBath = parent.adjustedModel.perThreeFourBath ?? 0
        return (subF1TFBath - self.f1ThreeFourBathTextField.toDouble) * adjTFBath
    }
    var f2FullBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF2FullBath = parent.subjectController.subjectModel.f2FullBath ?? 0
        let adjFullBath = parent.adjustedModel.perFullBath ?? 0
        return (subF2FullBath - self.f2FullBathTextField.toDouble) * adjFullBath
    }
    var f2HalfBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF2HalfBath = parent.subjectController.subjectModel.f2HalfBath ?? 0
        let adjHalfBath = parent.adjustedModel.perHalfBath ?? 0
        return (subF2HalfBath - self.f2HalfBathTextField.toDouble) * adjHalfBath
    }
    var f2ThreeFourBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF2TFBath = parent.subjectController.subjectModel.f2ThreeFourBath ?? 0
        let adjTFBath = parent.adjustedModel.perThreeFourBath ?? 0
        return (subF2TFBath - self.f2ThreeFourBathTextField.toDouble) * adjTFBath
    }
    var bFullBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subBFullBath = parent.subjectController.subjectModel.bFullBath ?? 0
        let adjFullBath = parent.adjustedModel.perFullBath ?? 0
        return (subBFullBath - self.bFullBathTextField.toDouble) * adjFullBath
    }
    var bHalfBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subBHalfBath = parent.subjectController.subjectModel.bHalfBath ?? 0
        let adjHalfBath = parent.adjustedModel.perHalfBath ?? 0
        return (subBHalfBath - self.bHalfBathTextField.toDouble) * adjHalfBath
    }
    var bThreeFourBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subBTFBath = parent.subjectController.subjectModel.bThreeFourBath ?? 0
        let adjTFBath = parent.adjustedModel.perThreeFourBath ?? 0
        return (subBTFBath - self.bThreeFourBathTextField.toDouble) * adjTFBath
    }
    var bathTotal: Double{
        var total: Double = f1FullBathTextField.toDouble
        total += f1HalfBathTextField.toDouble
        total += f1ThreeFourBathTextField.toDouble
        total += f2FullBathTextField.toDouble
        total += f2HalfBathTextField.toDouble
        total += f2ThreeFourBathTextField.toDouble
        total += bFullBathTextField.toDouble
        total += bHalfBathTextField.toDouble
        total += bThreeFourBathTextField.toDouble
        return total
    }
    var bathAdjTotal: Double{
        var total: Double = f1FullBathAdj
        total += f1HalfBathAdj
        total += f1ThreeFourBathAdj
        total += f2FullBathAdj
        total += f2HalfBathAdj
        total += f2ThreeFourBathAdj
        total += bFullBathAdj
        total += bHalfBathAdj
        total += bThreeFourBathAdj
        return total
    }
    private var outputList: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        let key = LocalizableKey.cma.self
//        list.append(OutputDetailsModel.init(key: key.address, value: address, valueTwo: "-"))
//        list.append(OutputDetailsModel.init(key: key.proximity, value: proximity, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.status, value: statusTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.dateSold, value: dateSoldTextField.text, valueTwo: firstAdjustValue))
        list.append(OutputDetailsModel.init(key: key.style, value: styleTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.yearBuilt, value: yearBuiltTextField.text, valueTwo: secondAdjustValue))
        list.append(OutputDetailsModel.init(key: key.acres, value: acresTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.landscape, value: landscapeTextField.text, valueTwo: thirdAdjustValue))
        list.append(OutputDetailsModel.init(key: key.lotValue, value: lotValueTextField.toDouble, valueTwo: fourthAdjustValue))
        list.append(OutputDetailsModel.init(key: key.garage, value: garageValueTextField.text, valueTwo: fiveAdjustValue))
        list.append(OutputDetailsModel.init(key: key.carport, value: carportTextField.text, valueTwo: sixthAdjustValue))
        list.append(OutputDetailsModel.init(key: key.sqTotal, value: totalSquareFt, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqMain, value: squareFtMainTextField.toDouble, valueTwo: seventhAdjustValue, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqUp, value: squareFtUpTextField.toDouble, valueTwo: eighthAdjustValue, isCurrency:  false))
        list.append(OutputDetailsModel.init(key: key.sqDown, value: squareFtDownTextField.toDouble, valueTwo: nineAdjustValue, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.finishDown, value: finishDNTextField.text, valueTwo: tenthAdjustValue))
        list.append(OutputDetailsModel.init(key: key.bedsTotal, value: totalBeds, valueTwo: eleventhAdjustValue, isCurrency:  false))
        list.append(OutputDetailsModel.init(key: key.bedsMain, value: bedsMainTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bedsUp, value: bedsUpTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bedsDn, value: bedsDnTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f1FullBath, value: f1FullBathTextField.text, valueTwo: f1FullBathAdj))
        list.append(OutputDetailsModel.init(key: key.f1HalfBath, value: f1HalfBathTextField.text, valueTwo: f1HalfBathAdj))
        list.append(OutputDetailsModel.init(key: key.f1TFBath, value: f1ThreeFourBathTextField.text, valueTwo: f1ThreeFourBathAdj))
        list.append(OutputDetailsModel.init(key: key.f2FullBath, value: f2FullBathTextField.text, valueTwo: f2FullBathAdj))
        list.append(OutputDetailsModel.init(key: key.f2HalfBath, value: f2HalfBathTextField.text, valueTwo: f2HalfBathAdj))
        list.append(OutputDetailsModel.init(key: key.f2TFBath, value: f2ThreeFourBathTextField.text, valueTwo: f2ThreeFourBathAdj))
        list.append(OutputDetailsModel.init(key: key.bFullBath, value: bFullBathTextField.text, valueTwo: bFullBathAdj))
        list.append(OutputDetailsModel.init(key: key.bHalfBath, value: bHalfBathTextField.text, valueTwo: bHalfBathAdj))
        list.append(OutputDetailsModel.init(key: key.bTFBath, value: bThreeFourBathTextField.text, valueTwo: bThreeFourBathAdj))
        list.append(OutputDetailsModel.init(key: key.bathsTotal, value: bathTotal, valueTwo: bathAdjTotal, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.fireplaces, value: firePlacesTextField.text, valueTwo: thirteenAdjustValue))
        list.append(OutputDetailsModel.init(key: key.ac, value: acTextField.text, valueTwo: acValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.secSys, value: secSysTextField.text, valueTwo: secSysValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.condition, value: conditionTextField.text, valueTwo: conditionValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.pool, value: poolTextField.text, valueTwo: poolValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.hotTub, value: hotTubTextField.text, valueTwo: hotTubValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.view, value: viewTextField.text, valueTwo: viewValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.concession, value: concesTextField.text, valueTwo: concesValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.listedPrice, value: listedTextField.toDouble, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.sold, value: soldTextField.toDouble, valueTwo: adjusted))

        list.append(OutputDetailsModel.init(key: key.adjusted, value: totalAdjusted, valueTwo: nil))
        return list
    }
    var firstComparableOutput: RefinanceOutputModel{
        return RefinanceOutputModel.init(title: LocalizableKey.cma.comparableOne, list: outputList)
    }
}
extension FirstComparableTableViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.selectedAcType = model
            self.acTextField.text = self.selectedAcType?.name
        }else if viewController.dataModel.tag == 1{
            statusTextField.text = model?.name
            selectedStatus = model
        }else if viewController.dataModel.tag == 2{
            secSysTextField.text = model?.name
            selectedSecSys = model
        }else if viewController.dataModel.tag == 3{
            hotTubTextField.text = model?.name
            selectedHotTub = model
        }else if viewController.dataModel.tag == 4{
            poolTextField.text = model?.name
            selectedPool = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == acTextField{
             self.showBottomPicker(delegate: self, items: acTypeList ?? [], tag: 0, title: LocalizableKey.cma.acBox)
            return false
        }else  if textField == statusTextField{
            self.showBottomPicker(delegate: self, items: statusList ?? [], tag: 1, title: LocalizableKey.cma.statusType)
            return false
        }else  if textField == secSysTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 2, title: LocalizableKey.cma.secSys)
            return false
        }else  if textField == hotTubTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 3, title: LocalizableKey.cma.hotTub)
            return false
        }else  if textField == poolTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 4, title: LocalizableKey.cma.pool)
            return false
        }else if textField == dateSoldTextField{
            DatePicker.show(self) { (date) in
                let dateFormat = date.toString(format: "dd-MM-yyyy")
                self.dateSoldTextField.text = dateFormat
            }
            return false
        }
        return true
    }
}
//MARK: - UIImage Picker Delegate
extension FirstComparableTableViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[.originalImage] as? UIImage else { return }
//        self.propertyDetailsView.image = pickedImage
        self.propertyDetailsView.propertyImageButton.setImage(pickedImage, for: .normal)
//        imageData = pickedImage.jpeg(UIImage.JPEGQuality.medium)
        picker.dismiss(animated: true, completion: nil)
    }
}
//
extension FirstComparableTableViewController{
    private func mapping(){
        yearBuiltTextField.text = comparableOneDetails?.yearBuiltC1
        acresTextField.text = comparableOneDetails?.acresC1
        landscapeTextField.text = comparableOneDetails?.landscapeC1?.toNumber.toPercentage
        lotValueTextField.text = comparableOneDetails?.lotValueC1
        garageValueTextField.text = comparableOneDetails?.garageC1
        carportTextField.text = comparableOneDetails?.carportC1
        squareFtMainTextField.text = comparableOneDetails?.sqFtMainC1
        squareFtUpTextField.text = comparableOneDetails?.sqFtUpC1
        squareFtDownTextField.text = comparableOneDetails?.sqFtDnC1
        finishDNTextField.text = comparableOneDetails?.finishDnC1?.toNumber.toPercentage
        bedsMainTextField.text = comparableOneDetails?.bedsMainC1
        bedsUpTextField.text = comparableOneDetails?.bedsUpC1
        bedsDnTextField.text = comparableOneDetails?.bedsDnC1
//        bathsMnTextField.text = comparableOneDetails?.bathsMainC1
//        bathsDnTextField.text = comparableOneDetails?.bathsDnC1
//        bathsUpTextField.text = comparableOneDetails?.bathsUpC1
        firePlacesTextField.text = comparableOneDetails?.firePlacesC1
        listedTextField.text = comparableOneDetails?.listedC1
        dateSoldTextField.text = comparableOneDetails?.dateSoldC1
        soldTextField.text = comparableOneDetails?.soldC1
        propertyDetailsView.imageURL = comparableOneDetails?.imageURL
        statusTextField.text = comparableOneDetails?.statusC1
        conditionTextField.text = comparableOneDetails?.conditionsC1
        viewTextField.text = comparableOneDetails?.viewC1
        concesTextField.text = comparableOneDetails?.concesC1
        propertyDetailsView.address = comparableOneDetails?.addressC1
        propertyDetailsView.proximityTextField.text = comparableOneDetails?.proximityC1
        styleTextField.text = comparableOneDetails?.styleC1
        poolValueTextField.text = comparableOneDetails?.poolAdjC1
        hotTubValueTextField.text = comparableOneDetails?.hotTubAdjC1
        secSysValueTextField.text = comparableOneDetails?.secSystAdjC1
        acValueTextField.text = comparableOneDetails?.acAdjC1
        conditionValueTextField.text = comparableOneDetails?.conditionsAdjC1
        viewValueTextField.text = comparableOneDetails?.viewAdjC1
        f1FullBathTextField.text = comparableOneDetails?.stFullBathC1
        f1HalfBathTextField.text = comparableOneDetails?.stHFBathC1
        f1ThreeFourBathTextField.text = comparableOneDetails?.stTHBathC1
        f2FullBathTextField.text = comparableOneDetails?.ndFullBathC1
        f2HalfBathTextField.text = comparableOneDetails?.ndHFBathC1
        f2ThreeFourBathTextField.text = comparableOneDetails?.ndTHBathC1
        bFullBathTextField.text = comparableOneDetails?.baseFullBathC1
        bHalfBathTextField.text = comparableOneDetails?.baseHFBathC1
        bThreeFourBathTextField.text = comparableOneDetails?.baseTFBathC1

    }
}
extension FirstComparableTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.cma.key.self
        var dict: Dictionary<String, Any> = [key.yearBuiltCO: yearBuiltTextField.toDouble]
        dict.updateValue(acresTextField.toDouble, forKey: key.acresCO)
        dict.updateValue(landscapeTextField.toDouble, forKey: key.landscapeCO)
        dict.updateValue(lotValueTextField.toDouble, forKey: key.lotValueCO)
        dict.updateValue(garageValueTextField.toDouble, forKey: key.garageCO)
        dict.updateValue(carportTextField.toDouble, forKey: key.carportCO)
        dict.updateValue(squareFtMainTextField.toDouble, forKey: key.sqFtMainCO)
        dict.updateValue(squareFtUpTextField.toDouble, forKey: key.sqFtUpCO)
        dict.updateValue(squareFtDownTextField.toDouble, forKey: key.sqFtDownCO)
        dict.updateValue(finishDNTextField.toDouble, forKey: key.finishDnCO)
        dict.updateValue(bedsMainTextField.toDouble, forKey: key.bedsMainCO)
        dict.updateValue(bedsUpTextField.toDouble, forKey: key.bedsUpCO)
        dict.updateValue(bedsDnTextField.toDouble, forKey: key.bedsDnCO)
//        dict.updateValue(eleventhAdjustValue, forKey: key.bedsTotalCO)
//        dict.updateValue(twelveAdjustValue, forKey: key.bathsTotCO)
//        dict.updateValue(bathsMnTextField.toDouble, forKey: key.bathsMnCO)
//        dict.updateValue(bathsDnTextField.toDouble, forKey: key.bathsDnCO)
//        dict.updateValue(bathsUpTextField.toDouble, forKey: key.bathsUpCO)
        dict.updateValue(firePlacesTextField.toDouble, forKey: key.firePlacesCO)
        dict.updateValue(listedTextField.toDouble, forKey: key.listedCO)
        dict.updateValue(dateSoldTextField.text ?? "", forKey: key.dateSoldCO)
        dict.updateValue(soldTextField.text ?? "", forKey: key.soldCO)
        dict.updateValue(selectedAcType?.value ?? "", forKey: key.acCO)
        dict.updateValue(selectedStatus?.value ?? "", forKey: key.statusCO)
        dict.updateValue(selectedPool?.value ?? "", forKey: key.poolCO)
        dict.updateValue(selectedHotTub?.value ?? "", forKey: key.hotTubCO)
        dict.updateValue(selectedSecSys?.value ?? "", forKey: key.secSysCO)
        dict.updateValue(propertyDetailsView.address ?? "", forKey: key.addressCO)
        dict.updateValue(propertyDetailsView.proximityTextField.text ?? "", forKey: key.proxmityCO)
        dict.updateValue(poolValueTextField.toDouble, forKey: key.poolAdjCO)
        dict.updateValue(secSysValueTextField.toDouble, forKey: key.secSysAdjCO)
        dict.updateValue(acValueTextField.toDouble, forKey: key.acAdjCO)
        dict.updateValue(hotTubValueTextField.toDouble, forKey: key.hotTubAdjCO)
        dict.updateValue(conditionValueTextField.toDouble, forKey: key.conditionAdjCO)
        dict.updateValue(conditionTextField.text ?? "", forKey: key.conditionCO)
        dict.updateValue(viewTextField.text ?? "", forKey: key.viewCO)
        dict.updateValue(viewValueTextField.text ?? "", forKey: key.viewAdjCO)
        dict.updateValue(concesTextField.text ?? "", forKey: key.concesCO)
        
        dict.updateValue(f1FullBathTextField.toDouble, forKey: key.stFullBathCO)
        dict.updateValue(f1HalfBathTextField.toDouble, forKey: key.stHFBathCO)
        dict.updateValue(f1ThreeFourBathTextField.toDouble, forKey: key.stTFBathCO)
        dict.updateValue(f2FullBathTextField.toDouble, forKey: key.ndFullBathCO)
        dict.updateValue(f2HalfBathTextField.toDouble, forKey: key.ndHFBathCO)
        dict.updateValue(f2ThreeFourBathTextField.toDouble, forKey: key.ndTFBathCO)
        dict.updateValue(bFullBathTextField.toDouble, forKey: key.baseFullBathCO)
        dict.updateValue(bHalfBathTextField.toDouble, forKey: key.baseHFBathCO)
        dict.updateValue(bThreeFourBathTextField.toDouble, forKey: key.baseTFBathCO)
        dict.updateValue(bathTotal, forKey: key.bathsTotCO)
        dict.updateValue(bathAdjTotal, forKey: key.bathsTotalAdCO)

        return dict
    }
}
extension FirstComparableTableViewController{
    @IBAction func didChangeTextField(_ sender: UITextField){
//        let value = sender.text?.toDouble.roundUp
//         sender.text = value?.readableWithoutCurrency
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == landscapeTextField || textField == finishDNTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
}
