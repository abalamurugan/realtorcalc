//
//  MLSNumberViewController.swift
//  FINANCIALKeys
//
//  Created by Bala on 22/09/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MLSNumberViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showMLS"
    
    
    @IBOutlet private var subTextField: UITextField!
    @IBOutlet private var comp1TextField: UITextField!
    @IBOutlet private var comp2TextField: UITextField!
    @IBOutlet private var comp3TextField: UITextField!

    
    private var mlsCompletion: ((MLSModel)->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        self.subTextField.text = nil
        self.comp1TextField.text = "1629208"
        self.comp2TextField.text = "1629600"
        self.comp3TextField.text = "1629418"
    }
    
    @IBAction private func closeAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction private func getAction(_ sender: UIButton){
        let mlsModel = MLSModel.init()
        mlsModel.comp1MLSNumber = comp1TextField.text
        mlsModel.subjectMLSNumber = subTextField.text
        mlsModel.comp2MLSNumber = comp2TextField.text
        mlsModel.comp3MLSNumber = comp3TextField.text
        self.dismiss(animated: true) {
            self.mlsCompletion?(mlsModel)
        }
    }
    
    func mlsCallback(_ sender: @escaping((MLSModel)->Void)){
        self.mlsCompletion = sender
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class MLSModel{
    var subjectMLSNumber: String?
    var comp1MLSNumber: String?
    var comp2MLSNumber: String?
    var comp3MLSNumber: String?
    
}
