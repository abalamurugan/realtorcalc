//
//  MarketAnalysisContainerViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 21/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MarketAnalysisContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showMarketAnalysis"
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var subjectPropertyContainerView: UIView!
    @IBOutlet weak var AdjustedContainerView: UIView!
    @IBOutlet weak var submitButton: UIButton!

    var adjustableController: AdjustableFormTableViewController!
    var subjectController: SubjectPropertyTableViewController!
    
    var comparableDetails: MarketAnalysisDetailModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        AdjustedContainerView.isHidden = true
        subjectPropertyContainerView.isHidden = false
        self.title = LocalizableKey.navigationTitle.marketAnalysis
    }
    @IBAction func calcAction(_ sender: UIButton){
         self.performSegue(withIdentifier: ComparableContainerViewController.segueIdentifier, sender: self)
    }
    @IBAction func segmentAction(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            AdjustedContainerView.isHidden = true
            subjectPropertyContainerView.isHidden = false
        }else{
            AdjustedContainerView.isHidden = false
            subjectPropertyContainerView.isHidden = true
        }
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == ComparableContainerViewController.segueIdentifier{
            let comparableController = segue.destination as! ComparableContainerViewController
            comparableController.adjustedModel = adjustableController.adjustedModel
            comparableController.subjectModel = subjectController.subjectModel
            comparableController.comparableDetails = self.comparableDetails
            comparableController.contactModel = self.contactModel
            comparableController.subParam = self.subjectController.updateParam
            comparableController.acTypeList = self.subjectController.acTypeList
            comparableController.statusList = self.subjectController.statusList
            comparableController.yesNoList = self.subjectController.yesNoList
        }else if segue.identifier == AdjustableFormTableViewController.segueIdentifier{
            adjustableController = segue.destination as? AdjustableFormTableViewController
        }else if segue.identifier == SubjectPropertyTableViewController.segueIdentifier{
            subjectController = segue.destination as? SubjectPropertyTableViewController
        }else if segue.identifier == MLSNumberViewController.segueIdentifier{
            let mlsController = segue.destination as? MLSNumberViewController
            mlsController?.mlsCallback({ (mlsDetails) in
                self.fetchMLS(mlsModel: mlsDetails)
            })
        }
    }
    private func fetchMLS(mlsModel: MLSModel){
        let key = APIKey.cma.key.self
        var param = Param.viewReportParam(tagReport: Constants.tagValue.MLSData, contactId: self.contactModel!.contactId!)
        if let submls = mlsModel.subjectMLSNumber, submls != ""{
            param.updateValue(submls, forKey: key.mlsS)
        }
        if let comp1MLS = mlsModel.comp1MLSNumber{
            param.updateValue(comp1MLS, forKey: key.mlsC1)
        }
        if let comp2MLS = mlsModel.comp2MLSNumber{
            param.updateValue(comp2MLS, forKey: key.mlsC2)
        }
        if let comp3MLS = mlsModel.comp3MLSNumber{
            param.updateValue(comp3MLS, forKey: key.mlsC3)
        }
        self.mlsCMA(mlsParm: param)
    }
}
extension MarketAnalysisContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getCMA()
    }
}
extension MarketAnalysisContainerViewController{
    var paramViewCMA: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.CMAView, contactId: self.contactModel!.contactId!)
        
    }
    private func showMLS(){
        if let mlsId = UserManager.instance.userModel?.mlsId, mlsId != "1"{
            self.performSegue(withIdentifier: MLSNumberViewController.segueIdentifier, sender: self)
        }
    }
    private func getCMA(mlsParm: Dictionary<String, Any>? = nil){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewCMA, encoding: .queryString, { (response: ResponseModel<MarketAnalysisModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            let item = response.item?.response
            self.subjectController.subjectDetails = item?.subject_info
            self.comparableDetails = item
//            self.showMLS()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showMLS()
//            self.showError(error)
        }
    }
    private func mlsCMA(mlsParm: Dictionary<String, Any>){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: mlsParm, encoding: .queryString, { (response: ResponseModel<MarketAnalysisModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            let item = response.item?.response
            if let sub = item?.subject_info{
                self.subjectController.subjectDetails = sub
            }
            if self.comparableDetails == nil{
                self.comparableDetails = item
            }else{
                if let comp1Values = item?.comparable1_info{
                    self.comparableDetails?.comparable1_info = comp1Values
                }
                if let comp2Values = item?.comparable2_info{
                    self.comparableDetails?.comparable2_info = comp2Values
                }
                if let comp3Values = item?.comparable3_info{
                    self.comparableDetails?.comparable3_info = comp3Values
                }
            }
          
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    
}
