//
//  ThirdComparableTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 17/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ThirdComparableTableViewController: UITableViewController, StoryboardSegueIdentifier{
    
    static var segueIdentifier: String = "showComp3"
    
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var dateSoldTextField: UITextField!
    @IBOutlet weak var styleTextField: UITextField!
    @IBOutlet weak var yearBuiltTextField: UITextField!
    @IBOutlet weak var acresTextField: UITextField!
    @IBOutlet weak var landscapeTextField: UITextField!
    @IBOutlet weak var lotValueTextField: UITextField!
    @IBOutlet weak var garageValueTextField: UITextField!
    @IBOutlet weak var carportTextField: UITextField!
    @IBOutlet weak var squareFtMainTextField: UITextField!
    @IBOutlet weak var squareFtUpTextField: UITextField!
    @IBOutlet weak var squareFtDownTextField: UITextField!
    @IBOutlet weak var finishDNTextField: UITextField!
    @IBOutlet weak var bedsMainTextField: UITextField!
    @IBOutlet weak var bedsUpTextField: UITextField!
    @IBOutlet weak var bedsDnTextField: UITextField!
//    @IBOutlet weak var bathsMnTextField: UITextField!
//    @IBOutlet weak var bathsUpTextField: UITextField!
//    @IBOutlet weak var bathsDnTextField: UITextField!
    @IBOutlet weak var firePlacesTextField: UITextField!
    @IBOutlet weak var acTextField: UITextField!
    @IBOutlet weak var secSysTextField: UITextField!
    @IBOutlet weak var conditionTextField: UITextField!
    @IBOutlet weak var poolTextField: UITextField!
    @IBOutlet weak var hotTubTextField: UITextField!
    @IBOutlet weak var viewTextField: UITextField!
    @IBOutlet weak var concesTextField: UITextField!
    @IBOutlet weak var listedTextField: UITextField!
    @IBOutlet weak var soldTextField: UITextField!
    
    @IBOutlet weak var acValueTextField: UITextField!
    @IBOutlet weak var secSysValueTextField: UITextField!
    @IBOutlet weak var hotTubValueTextField: UITextField!
    @IBOutlet weak var conditionValueTextField: UITextField!
    @IBOutlet weak var poolValueTextField: UITextField!
    @IBOutlet weak var viewValueTextField: UITextField!
    @IBOutlet weak var concesValueTextField: UITextField!
    @IBOutlet weak var propertyDetailsView: ImagePickerView!
    @IBOutlet weak var f1FullBathTextField: UITextField!
    @IBOutlet weak var f1ThreeFourBathTextField: UITextField!
    @IBOutlet weak var f1HalfBathTextField: UITextField!
    @IBOutlet weak var f2FullBathTextField: UITextField!
    @IBOutlet weak var f2ThreeFourBathTextField: UITextField!
    @IBOutlet weak var f2HalfBathTextField: UITextField!
    @IBOutlet weak var bFullBathTextField: UITextField!
    @IBOutlet weak var bThreeFourBathTextField: UITextField!
    @IBOutlet weak var bHalfBathTextField: UITextField!

    var comparableThreeDetails: ComparableThreeModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    var acTypeList: Array<DropDownInfoModel>?
    var statusList: Array<DropDownInfoModel>?
    var yesNoList: Array<DropDownInfoModel>?
    var selectedAcType: DropDownInfoModel?
    var selectedStatus: DropDownInfoModel?
    private var selectedPool: DropDownInfoModel?
    private var selectedHotTub: DropDownInfoModel?
    private var selectedSecSys: DropDownInfoModel?
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.setup()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        yearBuiltTextField.text = "2001"
        acresTextField.text = "0.30"
        landscapeTextField.text = "100"
        lotValueTextField.text = "275000"
        garageValueTextField.text = "2.0"
        carportTextField.text = "0.0"
        squareFtMainTextField.text = "1850"
        squareFtUpTextField.text = "1625"
        squareFtDownTextField.text = "1800"
        finishDNTextField.text = "95"
        bedsMainTextField.text = "1"
        bedsUpTextField.text = "1"
        bedsDnTextField.text = "3"
//        bathsMnTextField.text = "2"
//        bathsDnTextField.text = "1"
//        bathsUpTextField.text = "2"
        firePlacesTextField.text = "1"
        listedTextField.text = "595000"
        soldTextField.text = "520000"
        dateSoldTextField.text = "15-12-2018"
    }
    func callBack(){
        self.connectDelegate()
        propertyDetailsView.imagePickerCallBack { (bool) in
            self.openMediaLibrary(delegate: self, button: self.propertyDetailsView.propertyImageButton)
        }
    }
    private func setup(){
        self.callBack()
    }
    private func selectedValues(){
        if let acBox = self.comparableThreeDetails?.acC3{
            if let index = self.acTypeList?.index(of: DropDownInfoModel.init(value: acBox)){
                self.selectedAcType = self.acTypeList?[index]
                self.acTextField.text = selectedAcType?.name
            }
        }
        if let status = self.comparableThreeDetails?.statusC3{
            if let index = self.statusList?.index(of: DropDownInfoModel.init(value: status)){
                self.selectedStatus = self.statusList?[index]
                self.statusTextField.text = selectedStatus?.name
            }
        }
        if let secSys = self.comparableThreeDetails?.secSystC3{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: secSys)){
                self.selectedSecSys = self.yesNoList?[index]
                self.secSysTextField.text = selectedSecSys?.name
            }
        }
        if let pool = self.comparableThreeDetails?.poolC3{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: pool)){
                self.selectedPool = self.yesNoList?[index]
                self.poolTextField.text = selectedPool?.name
            }
        }
        if let hotTub = self.comparableThreeDetails?.hotTubC3{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: hotTub)){
                self.selectedHotTub = self.yesNoList?[index]
                self.hotTubTextField.text = selectedHotTub?.name
            }
        }
    }
    func connectDelegate(){
        self.statusTextField.delegate = self
        self.dateSoldTextField.delegate = self
        self.acTextField.delegate = self
        self.secSysTextField.delegate = self
        self.poolTextField.delegate = self
        self.hotTubTextField.delegate = self
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ThirdComparableTableViewController{
    var totalSquareFt: Double{
        var total = squareFtMainTextField.toDouble
        total += squareFtUpTextField.toDouble
        total += squareFtDownTextField.toDouble
        return total
    }
    var totalBeds: Double{
        var total = bedsMainTextField.toDouble
        total += bedsUpTextField.toDouble
        total += bedsDnTextField.toDouble
        return total
    }
//    var totalBaths: Double{
//        var total = bathsMnTextField.toDouble
//        total += bathsUpTextField.toDouble
//        total += bathsDnTextField.toDouble
//        return total
//    }
    var dayFromCurrent: Double{
        return Double(dateSoldTextField.text!.daysBetween)
    }
    var firstAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let soldValue = soldTextField.toDouble
        let appreciation = parent.adjustedModel.appreciation ?? 0
        return soldValue * appreciation/100 / 365 * dayFromCurrent
    }
    var secondAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subYear = parent.subjectController.subjectModel.year ?? 0
        let firstCompYear = self.yearBuiltTextField.toDouble
        let yearDiff = (subYear - firstCompYear)
        return yearDiff * (parent.adjustedModel.ageVarience ?? 0)
    }
    var thirdAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subLandscape = parent.subjectController.subjectModel.landscape ?? 0
        let compLandscape = self.landscapeTextField.toDouble
        let landscapeDiff = (subLandscape - compLandscape)
        return landscapeDiff * (parent.adjustedModel.fullLandscape ?? 0)
    }
    var fourthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subLot = parent.subjectController.subjectModel.lotValue ?? 0
        return (subLot - lotValueTextField.toDouble)
        
    }
    var fiveAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subGarage = parent.subjectController.subjectModel.garage ?? 0
        let compGarage = self.garageValueTextField.toDouble
        let garageDiff = (subGarage - compGarage)
        return garageDiff * (parent.adjustedModel.garage ?? 0)
    }
    var sixthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subCarport = parent.subjectController.subjectModel.carport ?? 0
        let compCarport = self.carportTextField.toDouble
        let carportDiff = (subCarport - compCarport)
        return carportDiff * (parent.adjustedModel.carport ?? 0)
    }
    var seventhAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subSqFtMn = parent.subjectController.subjectModel.sqFtmain ?? 0
        let compSqFtMn = self.squareFtMainTextField.toDouble
        let sqFtMnDiff = (subSqFtMn - compSqFtMn)
        return sqFtMnDiff * (parent.adjustedModel.mainFloor ?? 0)
    }
    var eighthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subSqFtUp = parent.subjectController.subjectModel.sqFtUp ?? 0
        let compSqFtUp = self.squareFtUpTextField.toDouble
        let sqFtUpDiff = (subSqFtUp - compSqFtUp)
        return sqFtUpDiff * (parent.adjustedModel.upperFloor ?? 0)
    }
    var nineAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subSqFtDn = parent.subjectController.subjectModel.sqFtDown ?? 0
        let compSqFtDn = self.squareFtDownTextField.toDouble
        let sqFtDnDiff = (subSqFtDn - compSqFtDn)
        return sqFtDnDiff * (parent.adjustedModel.bsmtUnfinish ?? 0)
    }
    
    var tenthAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subFinishDown = (parent.subjectController.subjectModel.finishDown ?? 0)/100 * (parent.subjectController.subjectModel.sqFtDown ?? 0)
        let compFinishDown = self.finishDNTextField.toDouble/100 * self.squareFtDownTextField.toDouble
        let compDnDiff = (subFinishDown - compFinishDown) * (parent.adjustedModel.bsmtFinish ?? 0)
        return compDnDiff
    }
    var eleventhAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subTotalBeds = parent.subjectController.subjectModel.bedsTotal
        let compTotalBeds = self.totalBeds
        let totalBedsDiff = (subTotalBeds - compTotalBeds)
        return totalBedsDiff * (parent.adjustedModel.perBedRoom ?? 0)
    }
    var twelveAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subTotalBaths = parent.subjectController.subjectModel.bathTotal
        let compTotalBaths = self.bathTotal
        let totalBathDiff = (subTotalBaths - compTotalBaths)
        return totalBathDiff * (parent.adjustedModel.perThreeFourBath ?? 0)
    }
    var thirteenAdjustValue: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subFirePlace = parent.subjectController.subjectModel.fireplace ?? 0
        let compFirePlace = self.firePlacesTextField.toDouble
        let firePlaceDiff = (subFirePlace - compFirePlace)
        return firePlaceDiff * (parent.adjustedModel.fireplace ?? 0)
    }
    var totalAdjusted: Double{
        return (adjusted + soldTextField.toDouble).rounded()
    }
    var adjusted: Double{
        var total = firstAdjustValue
        total += secondAdjustValue
        total += thirdAdjustValue
        total += fourthAdjustValue
        total += fiveAdjustValue
        total += sixthAdjustValue
        total += seventhAdjustValue
        total += eighthAdjustValue
        total += nineAdjustValue
        total += tenthAdjustValue
        total += eleventhAdjustValue
        total += twelveAdjustValue
        total += thirteenAdjustValue
        total += acValueTextField.toDouble
        total += viewValueTextField.toDouble
        total += secSysValueTextField.toDouble
        total += conditionValueTextField.toDouble
        total += poolValueTextField.toDouble
        total += hotTubValueTextField.toDouble
        total += concesValueTextField.toDouble
        print("Total====>\(total)")
        return total.rounded()
    }
    var listedPrice: Double{
        return listedTextField.toDouble
    }
    var salesPrice: Double{
        return soldTextField.toDouble
    }
    var address: String{
        return propertyDetailsView.addressTextView.text
    }
    var proximity: String?{
        return propertyDetailsView.proximityTextField.text
    }
    var f1FullBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF1FullBath = parent.subjectController.subjectModel.f1FullBath ?? 0
        let adjFullBath = parent.adjustedModel.perFullBath ?? 0
        return (subF1FullBath - self.f1FullBathTextField.toDouble) * adjFullBath
    }
    var f1HalfBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF1HalfBath = parent.subjectController.subjectModel.f1HalfBath ?? 0
        let adjHalfBath = parent.adjustedModel.perHalfBath ?? 0
        return (subF1HalfBath - self.f1HalfBathTextField.toDouble) * adjHalfBath
    }
    var f1ThreeFourBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF1TFBath = parent.subjectController.subjectModel.f1ThreeFourBath ?? 0
        let adjTFBath = parent.adjustedModel.perThreeFourBath ?? 0
        return (subF1TFBath - self.f1ThreeFourBathTextField.toDouble) * adjTFBath
    }
    var f2FullBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF2FullBath = parent.subjectController.subjectModel.f2FullBath ?? 0
        let adjFullBath = parent.adjustedModel.perFullBath ?? 0
        return (subF2FullBath - self.f2FullBathTextField.toDouble) * adjFullBath
    }
    var f2HalfBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF2HalfBath = parent.subjectController.subjectModel.f2HalfBath ?? 0
        let adjHalfBath = parent.adjustedModel.perHalfBath ?? 0
        return (subF2HalfBath - self.f2HalfBathTextField.toDouble) * adjHalfBath
    }
    var f2ThreeFourBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subF2TFBath = parent.subjectController.subjectModel.f2ThreeFourBath ?? 0
        let adjTFBath = parent.adjustedModel.perThreeFourBath ?? 0
        return (subF2TFBath - self.f2ThreeFourBathTextField.toDouble) * adjTFBath
    }
    var bFullBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subBFullBath = parent.subjectController.subjectModel.bFullBath ?? 0
        let adjFullBath = parent.adjustedModel.perFullBath ?? 0
        return (subBFullBath - self.bFullBathTextField.toDouble) * adjFullBath
    }
    var bHalfBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subBHalfBath = parent.subjectController.subjectModel.bHalfBath ?? 0
        let adjHalfBath = parent.adjustedModel.perHalfBath ?? 0
        return (subBHalfBath - self.bHalfBathTextField.toDouble) * adjHalfBath
    }
    var bThreeFourBathAdj: Double{
        guard let parent = self.parent as? ComparableContainerViewController else {
            return 0
        }
        let subBTFBath = parent.subjectController.subjectModel.bThreeFourBath ?? 0
        let adjTFBath = parent.adjustedModel.perThreeFourBath ?? 0
        return (subBTFBath - self.bThreeFourBathTextField.toDouble) * adjTFBath
    }
    var bathTotal: Double{
        var total: Double = f1FullBathTextField.toDouble
        total += f1HalfBathTextField.toDouble
        total += f1ThreeFourBathTextField.toDouble
        total += f2FullBathTextField.toDouble
        total += f2HalfBathTextField.toDouble
        total += f2ThreeFourBathTextField.toDouble
        total += bFullBathTextField.toDouble
        total += bHalfBathTextField.toDouble
        total += bThreeFourBathTextField.toDouble
        return total
    }
    var bathAdjTotal: Double{
        var total: Double = f1FullBathAdj
        total += f1HalfBathAdj
        total += f1ThreeFourBathAdj
        total += f2FullBathAdj
        total += f2HalfBathAdj
        total += f2ThreeFourBathAdj
        total += bFullBathAdj
        total += bHalfBathAdj
        total += bThreeFourBathAdj
        return total
    }
}
//MARK: - UIImage Picker Delegate
extension ThirdComparableTableViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[.originalImage] as? UIImage else { return }
        //        self.propertyDetailsView.image = pickedImage
        self.propertyDetailsView.propertyImageButton.setImage(pickedImage, for: .normal)
        //        imageData = pickedImage.jpeg(UIImage.JPEGQuality.medium)
        picker.dismiss(animated: true, completion: nil)
    }
}
extension ThirdComparableTableViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.selectedAcType = model
            self.acTextField.text = self.selectedAcType?.name
        }else if viewController.dataModel.tag == 1{
            self.selectedStatus = model
            self.statusTextField.text = self.selectedStatus?.name
        }else if viewController.dataModel.tag == 2{
            secSysTextField.text = model?.name
            selectedSecSys = model
        }else if viewController.dataModel.tag == 3{
            hotTubTextField.text = model?.name
            selectedHotTub = model
        }else if viewController.dataModel.tag == 4{
            poolTextField.text = model?.name
            selectedPool = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == acTextField{
            self.showBottomPicker(delegate: self, items: acTypeList ?? [], tag: 0, title: LocalizableKey.cma.acBox)
            return false
        }else if textField == statusTextField{
            self.showBottomPicker(delegate: self, items: statusList ?? [], tag: 1, title: LocalizableKey.cma.statusType)
            return false
        }else  if textField == secSysTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 2, title: LocalizableKey.cma.secSys)
            return false
        }else  if textField == hotTubTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 3, title: LocalizableKey.cma.hotTub)
            return false
        }else  if textField == poolTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 4, title: LocalizableKey.cma.pool)
            return false
        }else if textField == dateSoldTextField{
            DatePicker.show(self) { (date) in
                let dateFormat = date.toString(format: "dd-MM-yyyy")
                self.dateSoldTextField.text = dateFormat
            }
            return false
        }
        return true
    }
}
extension ThirdComparableTableViewController{
    private func mapping(){
        let item = comparableThreeDetails
        yearBuiltTextField.text = item?.yearBuiltC3
        acresTextField.text = item?.acresC3
        landscapeTextField.text = item?.landscapeC3?.toNumber.toPercentage
        lotValueTextField.text = item?.lotValueC3
        garageValueTextField.text = item?.garageC3
        carportTextField.text = item?.carportC3
        squareFtMainTextField.text = item?.sqFtMainC3
        squareFtUpTextField.text = item?.sqFtUpC3
        squareFtDownTextField.text = item?.sqFtDnC3
        finishDNTextField.text = item?.finishDnC3?.toNumber.toPercentage
        bedsMainTextField.text = item?.bedsMainC3
        bedsUpTextField.text = item?.bedsUpC3
        bedsDnTextField.text = item?.bedsDnC3
//        bathsMnTextField.text = item?.bathsMainC3
//        bathsDnTextField.text = item?.bathsDnC3
//        bathsUpTextField.text = item?.bathsUpC3
        firePlacesTextField.text = item?.firePlacesC3
        listedTextField.text = item?.listedC3
        dateSoldTextField.text = item?.dateSoldC3
        soldTextField.text = item?.soldC3
        propertyDetailsView.imageURL = item?.imageURL
        statusTextField.text = item?.statusC3
        conditionTextField.text = item?.conditionsC3
        viewTextField.text = item?.viewC3
        concesTextField.text = item?.concesC3
        propertyDetailsView.address = item?.addressC3
        propertyDetailsView.proximityTextField.text = item?.proximityC3
        styleTextField.text = item?.styleC3
        poolValueTextField.text = item?.poolAdjC3
        hotTubValueTextField.text = item?.hotTubAdjC3
        secSysValueTextField.text = item?.secSystAdjC3
        acValueTextField.text = item?.acAdjC3
        conditionValueTextField.text = item?.conditionsAdjC3
        viewValueTextField.text = item?.viewAdjC3
        f1FullBathTextField.text = item?.stFullBathC3
        f1HalfBathTextField.text = item?.stHFBathC3
        f1ThreeFourBathTextField.text = item?.stTHBathC3
        f2FullBathTextField.text = item?.ndFullBathC3
        f2HalfBathTextField.text = item?.ndHFBathC3
        f2ThreeFourBathTextField.text = item?.ndTHBathC3
        bFullBathTextField.text = item?.baseFullBathC3
        bHalfBathTextField.text = item?.baseHFBathC3
        bThreeFourBathTextField.text = item?.baseTFBathC3
    }
    private var outputList: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        let key = LocalizableKey.cma.self
        //        list.append(OutputDetailsModel.init(key: key.address, value: address, valueTwo: "-"))
        //        list.append(OutputDetailsModel.init(key: key.proximity, value: proximity, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.status, value: statusTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.dateSold, value: dateSoldTextField.text, valueTwo: firstAdjustValue))
        list.append(OutputDetailsModel.init(key: key.style, value: styleTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.yearBuilt, value: yearBuiltTextField.text, valueTwo: secondAdjustValue))
        list.append(OutputDetailsModel.init(key: key.acres, value: acresTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.landscape, value: landscapeTextField.text, valueTwo: thirdAdjustValue))
        list.append(OutputDetailsModel.init(key: key.lotValue, value: lotValueTextField.toDouble, valueTwo: fourthAdjustValue))
        list.append(OutputDetailsModel.init(key: key.garage, value: garageValueTextField.text, valueTwo: fiveAdjustValue))
        list.append(OutputDetailsModel.init(key: key.carport, value: carportTextField.text, valueTwo: sixthAdjustValue))
        list.append(OutputDetailsModel.init(key: key.sqTotal, value: totalSquareFt, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqMain, value: squareFtMainTextField.toDouble, valueTwo: seventhAdjustValue, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqUp, value: squareFtUpTextField.toDouble, valueTwo: eighthAdjustValue, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqDown, value: squareFtDownTextField.toDouble, valueTwo: nineAdjustValue, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.finishDown, value: finishDNTextField.text, valueTwo: tenthAdjustValue))
        list.append(OutputDetailsModel.init(key: key.bedsTotal, value: totalBeds, valueTwo: eleventhAdjustValue, isCurrency:  false))
        list.append(OutputDetailsModel.init(key: key.bedsMain, value: bedsMainTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.bedsUp, value: bedsUpTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.bedsDn, value: bedsDnTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.f1FullBath, value: f1FullBathTextField.text, valueTwo: f1FullBathAdj))
        list.append(OutputDetailsModel.init(key: key.f1HalfBath, value: f1HalfBathTextField.text, valueTwo: f1HalfBathAdj))
        list.append(OutputDetailsModel.init(key: key.f1TFBath, value: f1ThreeFourBathTextField.text, valueTwo: f1ThreeFourBathAdj))
        list.append(OutputDetailsModel.init(key: key.f2FullBath, value: f2FullBathTextField.text, valueTwo: f2FullBathAdj))
        list.append(OutputDetailsModel.init(key: key.f2HalfBath, value: f2HalfBathTextField.text, valueTwo: f2HalfBathAdj))
        list.append(OutputDetailsModel.init(key: key.f2TFBath, value: f2ThreeFourBathTextField.text, valueTwo: f2ThreeFourBathAdj))
        list.append(OutputDetailsModel.init(key: key.bFullBath, value: bFullBathTextField.text, valueTwo: bFullBathAdj))
        list.append(OutputDetailsModel.init(key: key.bHalfBath, value: bHalfBathTextField.text, valueTwo: bHalfBathAdj))
        list.append(OutputDetailsModel.init(key: key.bTFBath, value: bThreeFourBathTextField.text, valueTwo: bThreeFourBathAdj))
        list.append(OutputDetailsModel.init(key: key.bathsTotal, value: bathTotal, valueTwo: bathAdjTotal, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.fireplaces, value: firePlacesTextField.text, valueTwo: thirteenAdjustValue))
        list.append(OutputDetailsModel.init(key: key.ac, value: acTextField.text, valueTwo: acValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.secSys, value: secSysTextField.text, valueTwo: secSysValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.condition, value: conditionTextField.text, valueTwo: conditionValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.pool, value: poolTextField.text, valueTwo: poolValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.hotTub, value: hotTubTextField.text, valueTwo: hotTubValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.view, value: viewTextField.text, valueTwo: viewValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.concession, value: concesTextField.text, valueTwo: concesValueTextField.text))
        list.append(OutputDetailsModel.init(key: key.listedPrice, value: listedTextField.toDouble, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.sold, value: soldTextField.toDouble, valueTwo: adjusted))
        
        list.append(OutputDetailsModel.init(key: key.adjusted, value: totalAdjusted, valueTwo: nil))
        return list
    }
    var thirdComparableOutput: RefinanceOutputModel{
        return RefinanceOutputModel.init(title: LocalizableKey.cma.comparableThree, list: outputList)
    }
}
extension ThirdComparableTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.cma.key.self
        var dict: Dictionary<String, Any> = [key.yearBuiltCL: yearBuiltTextField.toDouble]
        dict.updateValue(acresTextField.toDouble, forKey: key.acresCL)
        dict.updateValue(landscapeTextField.toDouble, forKey: key.landscapeCL)
        dict.updateValue(lotValueTextField.toDouble, forKey: key.lotValueCL)
        dict.updateValue(garageValueTextField.toDouble, forKey: key.garageCL)
        dict.updateValue(carportTextField.toDouble, forKey: key.carportCL)
        dict.updateValue(squareFtMainTextField.toDouble, forKey: key.sqFtMainCL)
        dict.updateValue(squareFtUpTextField.toDouble, forKey: key.sqFtUpCL)
        dict.updateValue(squareFtDownTextField.toDouble, forKey: key.sqFtDownCL)
        dict.updateValue(finishDNTextField.toDouble, forKey: key.finishDnCL)
        dict.updateValue(bedsMainTextField.toDouble, forKey: key.bedsMainCL)
        dict.updateValue(bedsUpTextField.toDouble, forKey: key.bedsUpCL)
        dict.updateValue(bedsDnTextField.toDouble, forKey: key.bedsDnCL)
//        dict.updateValue(totalBeds, forKey: key.bedsTotalCO)
//        dict.updateValue(totalBaths, forKey: key.bathsTotCO)
//        dict.updateValue(bathsMnTextField.toDouble, forKey: key.bathsMnCL)
//        dict.updateValue(bathsDnTextField.toDouble, forKey: key.bathsDnCL)
//        dict.updateValue(bathsUpTextField.toDouble, forKey: key.bathsUpCL)
        dict.updateValue(firePlacesTextField.toDouble, forKey: key.firePlacesCL)
        dict.updateValue(listedTextField.toDouble, forKey: key.listedCL)
        dict.updateValue(dateSoldTextField.text ?? "", forKey: key.dateSoldCL)
        dict.updateValue(soldTextField.text ?? "", forKey: key.soldCL)
        dict.updateValue(selectedAcType?.value ?? "", forKey: key.acCL)
        dict.updateValue(selectedStatus?.value ?? "", forKey: key.statusCL)
        dict.updateValue(selectedPool?.value ?? "", forKey: key.poolCL)
        dict.updateValue(selectedHotTub?.value ?? "", forKey: key.hotTubCL)
        dict.updateValue(selectedSecSys?.value ?? "", forKey: key.secSysCL)
        dict.updateValue(propertyDetailsView.address ?? "", forKey: key.addressCL)
        dict.updateValue(propertyDetailsView.proximityTextField.text ?? "", forKey: key.proxmityCL)
        
        dict.updateValue(poolValueTextField.toDouble, forKey: key.poolAdjCL)
        dict.updateValue(secSysValueTextField.toDouble, forKey: key.secSysAdjCL)
        dict.updateValue(acValueTextField.toDouble, forKey: key.acAdjCL)
        dict.updateValue(hotTubValueTextField.toDouble, forKey: key.hotTubAdjCL)
        dict.updateValue(conditionValueTextField.toDouble, forKey: key.conditionAdjCL)
        dict.updateValue(conditionTextField.text ?? "", forKey: key.conditionCL)
        dict.updateValue(viewTextField.text ?? "", forKey: key.viewCL)
        dict.updateValue(viewValueTextField.text ?? "", forKey: key.viewAdjCL)
        dict.updateValue(concesTextField.text ?? "", forKey: key.concesCL)
        
        dict.updateValue(f1FullBathTextField.toDouble, forKey: key.stFullBathCL)
        dict.updateValue(f1HalfBathTextField.toDouble, forKey: key.stHFBathCL)
        dict.updateValue(f1ThreeFourBathTextField.toDouble, forKey: key.stTFBathCL)
        dict.updateValue(f2FullBathTextField.toDouble, forKey: key.ndFullBathCL)
        dict.updateValue(f2HalfBathTextField.toDouble, forKey: key.ndHFBathCL)
        dict.updateValue(f2ThreeFourBathTextField.toDouble, forKey: key.ndTFBathCL)
        dict.updateValue(bFullBathTextField.toDouble, forKey: key.baseFullBathCL)
        dict.updateValue(bHalfBathTextField.toDouble, forKey: key.baseHFBathCL)
        dict.updateValue(bThreeFourBathTextField.toDouble, forKey: key.baseTFBathCL)
        
        dict.updateValue(bathTotal, forKey: key.bathsTotCL)
        dict.updateValue(bathAdjTotal, forKey: key.bathsTotalAdCL)
        return dict
    }
}
extension ThirdComparableTableViewController{
    @IBAction func didChangeTextField(_ sender: UITextField){
//        sender.text = sender.text?.toNumber.readableWithoutCurrency
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == landscapeTextField || textField == finishDNTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
}
