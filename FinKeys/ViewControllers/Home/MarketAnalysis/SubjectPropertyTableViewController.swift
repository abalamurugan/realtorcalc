//
//  SubjectPropertyTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 21/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class SubjectPropertyTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showSubjectProperty"
    
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var dateSoldTextField: UITextField!
    @IBOutlet weak var styleTextField: UITextField!
    @IBOutlet weak var yearBuiltTextField: UITextField!
    @IBOutlet weak var acresTextField: UITextField!
    @IBOutlet weak var landscapeTextField: UITextField!
    @IBOutlet weak var lotValueTextField: UITextField!
    @IBOutlet weak var garageValueTextField: UITextField!
    @IBOutlet weak var carportTextField: UITextField!
    @IBOutlet weak var squareFtMainTextField: UITextField!
    @IBOutlet weak var squareFtUpTextField: UITextField!
    @IBOutlet weak var squareFtDownTextField: UITextField!
    @IBOutlet weak var finishDNTextField: UITextField!
    @IBOutlet weak var bedsMainTextField: UITextField!
    @IBOutlet weak var bedsUpTextField: UITextField!
    @IBOutlet weak var bedsDnTextField: UITextField!
//    @IBOutlet weak var bathsMnTextField: UITextField!
//    @IBOutlet weak var bathsUpTextField: UITextField!
//    @IBOutlet weak var bathsDnTextField: UITextField!
    @IBOutlet weak var firePlacesTextField: UITextField!
    @IBOutlet weak var acTextField: UITextField!
    @IBOutlet weak var secSysTextField: UITextField!
    @IBOutlet weak var conditionTextField: UITextField!
    @IBOutlet weak var poolTextField: UITextField!
    @IBOutlet weak var hotTubTextField: UITextField!
    @IBOutlet weak var viewTextField: UITextField!
    @IBOutlet weak var concesTextField: UITextField!
    @IBOutlet weak var listedTextField: UITextField!
    @IBOutlet weak var soldTextField: UITextField!
    @IBOutlet weak var propertyDetailsView: ImagePickerView!
    @IBOutlet weak var f1FullBathTextField: UITextField!
    @IBOutlet weak var f1ThreeFourBathTextField: UITextField!
    @IBOutlet weak var f1HalfBathTextField: UITextField!
    @IBOutlet weak var f2FullBathTextField: UITextField!
    @IBOutlet weak var f2ThreeFourBathTextField: UITextField!
    @IBOutlet weak var f2HalfBathTextField: UITextField!
    @IBOutlet weak var bFullBathTextField: UITextField!
    @IBOutlet weak var bThreeFourBathTextField: UITextField!
    @IBOutlet weak var bHalfBathTextField: UITextField!

    
    var subjectDetails: SubjectInfoModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    var dispatchGroup = DispatchGroup.init()
    var acTypeList: Array<DropDownInfoModel>?
    var statusList: Array<DropDownInfoModel>?
    var yesNoList: Array<DropDownInfoModel>?

    private var selectedPool: DropDownInfoModel?
    private var selectedHotTub: DropDownInfoModel?
    private var selectedSecSys: DropDownInfoModel?
    private var selectedAcType: DropDownInfoModel?
    private var selectedStatus: DropDownInfoModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        self.defaultValues()
    }
    private func defaultValues(){
        yearBuiltTextField.text = "2005"
        acresTextField.text = "0.31"
        landscapeTextField.text = "100"
        lotValueTextField.text = "250000"
        garageValueTextField.text = "3.0"
        carportTextField.text = "0.0"
        squareFtMainTextField.text = "1950"
        squareFtUpTextField.text = "1800"
        squareFtDownTextField.text = "1850"
        finishDNTextField.text = "100"
        bedsMainTextField.text = "2"
        bedsUpTextField.text = "0"
        bedsDnTextField.text = "3"
//        bathsMnTextField.text = "1.5"
//        bathsDnTextField.text = "2.5"
//        bathsUpTextField.text = "1.0"
        firePlacesTextField.text = "2"
        listedTextField.text = nil
        soldTextField.text = nil
    }
    private func setup(){
        self.statusTextField.delegate = self
        self.acTextField.delegate = self
        self.secSysTextField.delegate = self
        self.poolTextField.delegate = self
        self.hotTubTextField.delegate = self
        propertyDetailsView.imagePickerCallBack { (bool) in
            self.openMediaLibrary(delegate: self, button: self.propertyDetailsView.propertyImageButton)
        }
        self.fetch()
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        self.dispatchGroup.enter()
        CommonRequest.dropDownAPI(type: .acBox) { (acList) in
            self.acTypeList = acList
            self.dispatchGroup.leave()
        }
        self.dispatchGroup.enter()
        CommonRequest.dropDownAPI(type: .statusBox, model: { (statusList) in
            self.statusList = statusList
            self.dispatchGroup.leave()
        })
        self.dispatchGroup.enter()
        CommonRequest.dropDownAPI(type: .yesnoBox, model: { (yesnoList) in
            self.yesNoList = yesnoList
            self.dispatchGroup.leave()
        })
        self.dispatchGroup.notify(queue: DispatchQueue.main) {
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
        }
    }
    private func selectedValues(){
        if let acBox = self.subjectDetails?.acS{
            if let index = self.acTypeList?.index(of: DropDownInfoModel.init(value: acBox)){
                self.selectedAcType = self.acTypeList?[index]
                self.acTextField.text = selectedAcType?.name
            }
        }
        if let status = self.subjectDetails?.statusS{
            if let index = self.statusList?.index(of: DropDownInfoModel.init(value: status)){
                self.selectedStatus = self.statusList?[index]
                self.statusTextField.text = selectedStatus?.name
            }
        }
        if let secSys = self.subjectDetails?.secSystS{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: secSys)){
                self.selectedSecSys = self.yesNoList?[index]
                self.secSysTextField.text = selectedSecSys?.name
            }
        }
        if let pool = self.subjectDetails?.poolS{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: pool)){
                self.selectedPool = self.yesNoList?[index]
                self.poolTextField.text = selectedPool?.name
            }
        }
        if let hotTub = self.subjectDetails?.hotTubS{
            if let index = self.yesNoList?.index(of: DropDownInfoModel.init(value: hotTub)){
                self.selectedHotTub = self.yesNoList?[index]
                self.hotTubTextField.text = selectedHotTub?.name
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SubjectPropertyTableViewController{
    var totalSquareFt: Double{
        var total = squareFtMainTextField.toDouble
        total += squareFtUpTextField.toDouble
        total += squareFtDownTextField.toDouble
        return total
    }
    var totalBeds: Double{
        var total = bedsMainTextField.toDouble
        total += bedsUpTextField.toDouble
        total += bedsDnTextField.toDouble
        return total
    }
//    var totalBaths: Double{
//        var total = bathsMnTextField.toDouble
//        total += bathsUpTextField.toDouble
//        total += bathsDnTextField.toDouble
//        return total
//    }
    var subjectModel: SubjectModel{
        let subjectModel = SubjectModel()
        subjectModel.year = yearBuiltTextField.toDouble
        subjectModel.acres = acresTextField.toDouble
        subjectModel.landscape = landscapeTextField.toDouble
        subjectModel.lotValue = lotValueTextField.toDouble
        subjectModel.garage = garageValueTextField.toDouble
        subjectModel.carport = carportTextField.toDouble
        subjectModel.sqFtmain = squareFtMainTextField.toDouble
        subjectModel.sqFtUp = squareFtUpTextField.toDouble
        subjectModel.sqFtDown = squareFtDownTextField.toDouble
        subjectModel.finishDown = finishDNTextField.toDouble
        subjectModel.bedsMain = bedsMainTextField.toDouble
        subjectModel.bedsUp = bedsUpTextField.toDouble
        subjectModel.bedsDn = bedsDnTextField.toDouble
//        subjectModel.bathMain = bathsMnTextField.toDouble
//        subjectModel.bathDn = bathsDnTextField.toDouble
//        subjectModel.bathUp = bathsUpTextField.toDouble
        subjectModel.fireplace = firePlacesTextField.toDouble
        subjectModel.listed = listedTextField.toDouble
        subjectModel.sold = soldTextField.toDouble
        subjectModel.f1FullBath = f1FullBathTextField.toDouble
        subjectModel.f1HalfBath = f1HalfBathTextField.toDouble
        subjectModel.f1ThreeFourBath = f1ThreeFourBathTextField.toDouble
        subjectModel.f2FullBath = f2FullBathTextField.toDouble
        subjectModel.f2HalfBath = f2HalfBathTextField.toDouble
        subjectModel.f2ThreeFourBath = f2ThreeFourBathTextField.toDouble
        subjectModel.bFullBath = bFullBathTextField.toDouble
        subjectModel.bHalfBath = bHalfBathTextField.toDouble
        subjectModel.bThreeFourBath = bThreeFourBathTextField.toDouble
        return subjectModel
    }
    var address: String{
        return propertyDetailsView.addressTextView.text
    }
    var proximity: String?{
        return propertyDetailsView.proximityTextField.text
    }
    var bathTotal: Double{
        var total: Double = f1FullBathTextField.toDouble
        total += f1HalfBathTextField.toDouble
        total += f1ThreeFourBathTextField.toDouble
        total += f2FullBathTextField.toDouble
        total += f2HalfBathTextField.toDouble
        total += f2ThreeFourBathTextField.toDouble
        total += bFullBathTextField.toDouble
        total += bHalfBathTextField.toDouble
        total += bThreeFourBathTextField.toDouble
        return total
    }
    
}
extension SubjectPropertyTableViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == acTextField{
            self.showBottomPicker(delegate: self, items: self.acTypeList ?? [], tag: 0, title: "AC Box")
            return false
        }else if textField == statusTextField{
            self.showBottomPicker(delegate: self, items: self.statusList ?? [], tag: 1, title: "Status Type")
            return false
        }else  if textField == secSysTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 2, title: LocalizableKey.cma.secSys)
            return false
        }else  if textField == hotTubTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 3, title: LocalizableKey.cma.hotTub)
            return false
        }else  if textField == poolTextField{
            self.showBottomPicker(delegate: self, items: yesNoList ?? [], tag: 4, title: LocalizableKey.cma.pool)
            return false
        }
        return true
    }
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            acTextField.text = model?.name
            selectedAcType = model
        }else if viewController.dataModel.tag == 1{
            statusTextField.text = model?.name
            selectedStatus = model
        }else if viewController.dataModel.tag == 2{
            secSysTextField.text = model?.name
            selectedSecSys = model
        }else if viewController.dataModel.tag == 3{
            hotTubTextField.text = model?.name
            selectedHotTub = model
        }else if viewController.dataModel.tag == 4{
            poolTextField.text = model?.name
            selectedPool = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    
}
extension SubjectPropertyTableViewController{
    private func mapping(){
        yearBuiltTextField.text = subjectDetails?.yearBuiltS
        acresTextField.text = subjectDetails?.acresS
        landscapeTextField.text = subjectDetails?.landscapeS
        lotValueTextField.text = subjectDetails?.lotValueS
        garageValueTextField.text = subjectDetails?.garagev
        carportTextField.text = subjectDetails?.carportS
        squareFtMainTextField.text = subjectDetails?.sqFtMainS
        squareFtUpTextField.text = subjectDetails?.sqFtUpS
        squareFtDownTextField.text = subjectDetails?.sqFtDnS
        finishDNTextField.text = subjectDetails?.finishDnS
        bedsMainTextField.text = subjectDetails?.bedsMainS
        bedsUpTextField.text = subjectDetails?.bedsUpS
        bedsDnTextField.text = subjectDetails?.bedsDnS
//        bathsMnTextField.text = subjectDetails?.bathsMainS
//        bathsDnTextField.text = subjectDetails?.bathsDnS
//        bathsUpTextField.text = subjectDetails?.bathsUpS
        firePlacesTextField.text = subjectDetails?.firePlacesS
        listedTextField.text = nil
        soldTextField.text = nil
        propertyDetailsView.imageURL = subjectDetails?.imageURL
        propertyDetailsView.address = subjectDetails?.addressS
        styleTextField.text = subjectDetails?.styleS
        f1FullBathTextField.text = subjectDetails?.stFullBathS
        f1HalfBathTextField.text = subjectDetails?.stHFBathS
        f1ThreeFourBathTextField.text = subjectDetails?.stTHBathS
        f2FullBathTextField.text = subjectDetails?.ndFullBathS
        f2HalfBathTextField.text = subjectDetails?.ndHFBathS
        f2ThreeFourBathTextField.text = subjectDetails?.ndTHBathS
        bFullBathTextField.text = subjectDetails?.baseFullBathS
        bHalfBathTextField.text = subjectDetails?.baseHFBathS
        bThreeFourBathTextField.text = subjectDetails?.baseTFBathS
    }
    var outputList: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        let key = LocalizableKey.cma.self
        //        list.append(OutputDetailsModel.init(key: key.address, value: address, valueTwo: "-"))
        //        list.append(OutputDetailsModel.init(key: key.proximity, value: proximity, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.status, value: statusTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.style, value: styleTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.yearBuilt, value: yearBuiltTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.acres, value: acresTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.landscape, value: landscapeTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.lotValue, value: lotValueTextField.toDouble, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.garage, value: garageValueTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.carport, value: carportTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.sqTotal, value: totalSquareFt, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqMain, value: squareFtMainTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqUp, value: squareFtUpTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.sqDown, value: squareFtDownTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.finishDown, value: finishDNTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.bedsTotal, value: totalBeds, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bedsMain, value: bedsMainTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bedsUp, value: bedsUpTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bedsDn, value: bedsDnTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f1FullBath, value: f1FullBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f1HalfBath, value: f1HalfBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f1TFBath, value: f1ThreeFourBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f2FullBath, value: f2FullBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f2HalfBath, value: f2HalfBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.f2TFBath, value: f2ThreeFourBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bFullBath, value: bFullBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bHalfBath, value: bHalfBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bTFBath, value: bThreeFourBathTextField.toDouble, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.bathsTotal, value: bathTotal, valueTwo: nil, isCurrency: false))
        list.append(OutputDetailsModel.init(key: key.fireplaces, value: firePlacesTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.ac, value: acTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.secSys, value: secSysTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.condition, value: conditionTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.pool, value: poolTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.hotTub, value: hotTubTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.view, value: viewTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.concession, value: concesTextField.text, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.listedPrice, value: listedTextField.toDouble, valueTwo: nil))
        list.append(OutputDetailsModel.init(key: key.sold, value: nil, valueTwo: nil))
        
        return list
    }
    
}
extension SubjectPropertyTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.cma.key.self
        var dict: Dictionary<String, Any> = [key.yearBuiltSub: yearBuiltTextField.toDouble]
        dict.updateValue(acresTextField.toDouble, forKey: key.acresSub)
        dict.updateValue(landscapeTextField.toDouble, forKey: key.landscapeSub)
        dict.updateValue(lotValueTextField.toDouble, forKey: key.lotValueSub)
        dict.updateValue(garageValueTextField.toDouble, forKey: key.garageSub)
        dict.updateValue(carportTextField.toDouble, forKey: key.carportSub)
        dict.updateValue(squareFtMainTextField.toDouble, forKey: key.sqFtMainSub)
        dict.updateValue(squareFtUpTextField.toDouble, forKey: key.sqFtUpSub)
        dict.updateValue(squareFtDownTextField.toDouble, forKey: key.sqFtDownSub)
        dict.updateValue(finishDNTextField.toDouble, forKey: key.finishDnSub)
        dict.updateValue(bedsMainTextField.toDouble, forKey: key.bedsMainSub)
        dict.updateValue(bedsUpTextField.toDouble, forKey: key.bedsUpSub)
        dict.updateValue(bedsDnTextField.toDouble, forKey: key.bedsDnSub)
//        dict.updateValue(bathsMnTextField.toDouble, forKey: key.bathsMnSub)
//        dict.updateValue(bathsDnTextField.toDouble, forKey: key.bathsDnsub)
//        dict.updateValue(bathsUpTextField.toDouble, forKey: key.bathsUpSub)
        dict.updateValue(firePlacesTextField.toDouble, forKey: key.firePlacesSub)
        dict.updateValue(selectedAcType?.value ?? "", forKey: key.acSub)
        dict.updateValue(selectedStatus?.value ?? "", forKey: key.statusSub)
        dict.updateValue(selectedPool?.value ?? "", forKey: key.poolSub)
        dict.updateValue(selectedHotTub?.value ?? "", forKey: key.hotTubSub)
        dict.updateValue(selectedSecSys?.value ?? "", forKey: key.secSysSub)
        dict.updateValue(propertyDetailsView.address ?? "", forKey: key.addressSub)
        
        dict.updateValue(f1FullBathTextField.toDouble, forKey: key.stFullBathSub)
        dict.updateValue(f1HalfBathTextField.toDouble, forKey: key.stHFBathSub)
        dict.updateValue(f1ThreeFourBathTextField.toDouble, forKey: key.stTFBathSub)
        dict.updateValue(f2FullBathTextField.toDouble, forKey: key.ndFullBathSub)
        dict.updateValue(f2HalfBathTextField.toDouble, forKey: key.ndHFBathSub)
        dict.updateValue(f2ThreeFourBathTextField.toDouble, forKey: key.ndTFBathSub)
        dict.updateValue(bFullBathTextField.toDouble, forKey: key.baseFullBathSub)
        dict.updateValue(bHalfBathTextField.toDouble, forKey: key.baseHFBathSub)
        dict.updateValue(bThreeFourBathTextField.toDouble, forKey: key.baseTFBathSub)
        
        dict.updateValue(bathTotal, forKey: key.bathsTotSub)
 
        return dict
    }
}
//MARK: - UIImage Picker Delegate
extension SubjectPropertyTableViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[.originalImage] as? UIImage else { return }
        //        self.propertyDetailsView.image = pickedImage
        self.propertyDetailsView.propertyImageButton.setImage(pickedImage, for: .normal)
        //        imageData = pickedImage.jpeg(UIImage.JPEGQuality.medium)
        picker.dismiss(animated: true, completion: nil)
    }
}
