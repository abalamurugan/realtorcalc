//
//  RentBuyFormOneViewController.swift
//  RealtorCalc
//
//  Created by Bala on 12/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RentBuyFormOneViewController: UITableViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showFormOne"
    
    
    @IBOutlet weak var savingAccountTextField : UITextField!
    @IBOutlet weak var otherLiquidAssTextField : UITextField!
    @IBOutlet weak var familyAssistedTextField : UITextField!
    @IBOutlet weak var debtsToLiquidateTextField : UITextField!
    @IBOutlet weak var savingCushionTextField : UITextField!
    @IBOutlet weak var investmentInterestTextField : UITextField!
    @IBOutlet weak var homeOwnerTaxTextField : UITextField!
    @IBOutlet weak var currentMonthlyRentTextField : UITextField!
    @IBOutlet weak var hoaCondoFeesTextField : UITextField!
    @IBOutlet weak var homeOwnerInsuranceTextField : UITextField!
    
    var rentBuyDetails: RentBuyDetailModel?{
        didSet{
            self.mapping()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    
    private func defaultValues(){
        savingAccountTextField.text = "8500"
        otherLiquidAssTextField.text = "3000"
        familyAssistedTextField.text = "8000"
        debtsToLiquidateTextField.text = "1500"
        savingCushionTextField.text = "2000"
        investmentInterestTextField.text = "1.75"
        homeOwnerTaxTextField.text = "31"
        currentMonthlyRentTextField.text = "1550"
        hoaCondoFeesTextField.text = "180"
        homeOwnerInsuranceTextField.text = "65"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RentBuyFormOneViewController{
    var currentRentPayment : Double{
        var total : Double = 0
        total += currentMonthlyRentTextField.toDouble
        total += hoaCondoFeesTextField.toDouble
        total += homeOwnerInsuranceTextField.toDouble
        return total
    }
    
    var totalCashAssets: Double{
        var cashAssets: Double = 0
        cashAssets += savingAccountTextField.toDouble
        cashAssets += otherLiquidAssTextField.toDouble
        cashAssets += familyAssistedTextField.toDouble
        return cashAssets
    }
    
    var totalCashNeeds: Double{
        var cashNeeds: Double = 0
        cashNeeds += debtsToLiquidateTextField.toDouble
        cashNeeds += savingCushionTextField.toDouble
        return cashNeeds
    }
    var totalCashAvailable: Double{
        return totalCashAssets - totalCashNeeds
    }
}
extension RentBuyFormOneViewController{
    private func mapping(){
        savingAccountTextField.text = rentBuyDetails?.savingAccount?.readableFormat
        otherLiquidAssTextField.text = rentBuyDetails?.otherLiquidAss?.readableFormat
        familyAssistedTextField.text = rentBuyDetails?.familyAss?.readableFormat
        debtsToLiquidateTextField.text = rentBuyDetails?.debtsLiquidate?.readableFormat
        savingCushionTextField.text = rentBuyDetails?.saveCushionNeed?.readableFormat
        investmentInterestTextField.text = rentBuyDetails?.investInterestRate?.toNumber.toPercentage
        homeOwnerTaxTextField.text = rentBuyDetails?.homeOwnerTax?.toNumber.toPercentage
        currentMonthlyRentTextField.text = rentBuyDetails?.currentMonthlyRent?.readableFormat
        hoaCondoFeesTextField.text = rentBuyDetails?.hoaConda?.readableFormat
        homeOwnerInsuranceTextField.text = rentBuyDetails?.homeOwnerInsur?.readableFormat
    }
}
extension RentBuyFormOneViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.rentVsBuy.key.self
        var dict: Dictionary<String, Any> = [key.savingAccount: savingAccountTextField.toDouble]
        dict.updateValue(otherLiquidAssTextField.toDouble, forKey: key.otherLiquidAss)
        dict.updateValue(familyAssistedTextField.toDouble, forKey: key.familyAss)
        dict.updateValue(debtsToLiquidateTextField.toDouble, forKey: key.debtsLiquidate)
        dict.updateValue(savingCushionTextField.toDouble, forKey: key.saveCushionNeed)
        dict.updateValue(investmentInterestTextField.toDouble, forKey: key.investInterestRate)
        dict.updateValue(homeOwnerTaxTextField.toDouble, forKey: key.homeOwnerTax)
        dict.updateValue(currentMonthlyRentTextField.toDouble, forKey: key.currentMonthlyRent)
        dict.updateValue(hoaCondoFeesTextField.toDouble, forKey: key.hoaConda)
        dict.updateValue(homeOwnerInsuranceTextField.toDouble, forKey: key.homeOwnerInsur)
        return dict
    }
}
extension RentBuyFormOneViewController{
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension RentBuyFormOneViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == investmentInterestTextField || textField == homeOwnerTaxTextField {
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
}
