//
//  RentBuyContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 12/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RentBuyContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showRentBuy"
    
    @IBOutlet weak var calculateButton : UIButton!
    
    @IBOutlet weak var segmentedControler : UISegmentedControl!
    @IBOutlet weak var formOneContainer : UIView!
    @IBOutlet weak var formTwoContainer : UIView!

    var firstFormController : RentBuyFormOneViewController!
    var secondFormController : RentBuyFormTwoViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        self.title = LocalizableKey.navigationTitle.rentBuy
        formTwoContainer.isHidden = true
    }
    @IBAction func segmentAction(_ sender : UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            formOneContainer.isHidden = false
            formTwoContainer.isHidden = true
        }else{
            formOneContainer.isHidden = true
            formTwoContainer.isHidden = false
        }
    }

    @IBAction func calculateAction(_ sender: UIButton){
        if secondFormController.isValidationSuccess(){
            self.postFormValues()
        }
    }
    private func showOutput(){
        let equityController = EquityViewController.showEquity()
        equityController.dataModel.list = self.outputList
        equityController.dataModel.title = LocalizableKey.navigationTitle.rentBuy
        equityController.contactModel = self.contactModel
        equityController.isHideTotalView = true
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: equityController)
        self.present(navigationController, animated: true, completion: nil)
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == RentBuyFormOneViewController.segueIdentifier{
            firstFormController = segue.destination as? RentBuyFormOneViewController
        }else if segue.identifier == RentBuyFormTwoViewController.segueIdentifier{
            secondFormController = segue.destination as? RentBuyFormTwoViewController
        }
    }

}
extension RentBuyContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getRentBuy()
    }
}
extension RentBuyContainerViewController{
    var differenceMonthlyPayment: Double{
        return (firstFormController.currentRentPayment - secondFormController.totalHousingPayment).removeMinus
    }
    var newTaxDeduction: Double{
        let interest = firstFormController.homeOwnerTaxTextField.toDouble/100
        let interestSecond = secondFormController.interestRateTextField.toDouble/100
        let paymentPerYear = secondFormController.paymentPerYearTextField.toDouble
        let expFirst = secondFormController.monthlyTaxesTextField.toDouble * interest
        let expSecond = interest * secondFormController.firstMtgAmount * interestSecond/paymentPerYear
        return expFirst + expSecond
    }
    var cashToSaving: Double{
        return secondFormController.cashNeededToClose - firstFormController.totalCashAvailable
    }
    var savingInvestedGain: Double{
        var gain: Double = 0
        let interest = firstFormController.investmentInterestTextField.toDouble/100
        let paymentPerYear = secondFormController.paymentPerYearTextField.toDouble
        gain += cashToSaving
        gain = gain * interest/paymentPerYear
        return gain.removeMinus
    }
    var netCashFlow: Double{
        var total: Double = differenceMonthlyPayment
        total -= newTaxDeduction
        total -= savingInvestedGain
        return total
    }
    var increaseDecreaseAppre: Double{
        return (netCashFlow - secondFormController.appreciationGainPerMonth)
    }
    var outputList : Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        list.append(OutputDetailsModel.init(key: "Current Rental Payment", value: firstFormController.currentRentPayment))
        list.append(OutputDetailsModel.init(key: "Minimum Downpayment", value: secondFormController.isDownPayment))
        list.append(OutputDetailsModel.init(key: "New Housing Payment", value: secondFormController.totalHousingPayment))
        list.append(OutputDetailsModel.init(key: "Difference in Monthly Payment", value: differenceMonthlyPayment))
        list.append(OutputDetailsModel.init(key: "  - New Tax Deductions Gain/(Loss)", value: newTaxDeduction))
        list.append(OutputDetailsModel.init(key: "  - Savings Invested Gain/(Loss)", value: savingInvestedGain))
        list.append(OutputDetailsModel.init(key: "Net Cash Flow Increase/(Decrease)", value: netCashFlow))
        list.append(OutputDetailsModel.init(key: "  - Appreciation per Month Gain/(Loss)", value: secondFormController.appreciationGainPerMonth))
        list.append(OutputDetailsModel.init(key: "Increase/(Decrease) Including Appreciation", value: increaseDecreaseAppre))
        return list
        
    }
}
extension RentBuyContainerViewController{
    var paramViewRentBuy: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.rentvsbuyView, contactId: self.contactModel!.contactId!)
        
    }
    private func getRentBuy(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewRentBuy, encoding: .queryString, { (response: ResponseModel<RentBuyModel>) in
            self.firstFormController.rentBuyDetails = response.item?.response
            self.secondFormController.rentBuyDetails = response.item?.response
        }) { (error) in
            self.showError(error)
        }
    }
}
extension RentBuyContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.userId: UserManager.instance.userID]
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.tagValue.rentvsbuyAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(Constants.reportNames.rentBuy, forKey: APIKey.common.key.action)
        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        firstFormController.updateParam.forEach { (key,value) in
            dict.updateValue(value, forKey: key)
        }
        secondFormController.updateParam.forEach { (key,value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
