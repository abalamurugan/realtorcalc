//
//  RentBuyFormTwoViewController.swift
//  RealtorCalc
//
//  Created by Bala on 12/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RentBuyFormTwoViewController: UITableViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showFormTwo"

    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var downpaymentAmountTextField : UITextField!
    @IBOutlet weak var secondMtgAmountTextField : UITextField!
    @IBOutlet weak var loanFeesChargesTextField : UITextField!
    @IBOutlet weak var prepaidMIPChargesTextField : UITextField!
    @IBOutlet weak var titleRecordFeesTextField : UITextField!
    @IBOutlet weak var yearlyApprecRateTextField : UITextField!
    @IBOutlet weak var minDownpaymentSwitch : UISwitch!
    
    //New housing payment
    @IBOutlet weak var interestRateTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var monthlyTaxesTextField : UITextField!
    @IBOutlet weak var monthlyInsuranceTextField : UITextField!
    @IBOutlet weak var monthlyMtgInsuranceTextField : UITextField!
    @IBOutlet weak var monthlyHOAFeesTextField : UITextField!
    @IBOutlet weak var monthlySecondMtgTextField : UITextField!

    private var paymentTypeList: Array<DropDownInfoModel>?
    private var loanPlanList: Array<DropDownInfoModel>?
    private var selectedPaymentType: DropDownInfoModel?
    private var selectedLoanPlan: DropDownInfoModel?
    
    var rentBuyDetails: RentBuyDetailModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    var upFrontMtgInsurance: Double{
        let interestOne = 96.5/100
        let interestTwo = 1.75/100
        return (purchasePriceTextField.toDouble * interestOne) * interestTwo
    }
    
    var downPayment: Double{
        let interest = 3.5/100
        return purchasePriceTextField.toDouble * interest
    }
    
    var firstMtgAmount : Double{
        var mtg : Double = purchasePriceTextField.toDouble
        mtg -= downPayment
        mtg += upFrontMtgInsurance
        return mtg
    }
    var isDownPayment: String{
        return minDownpaymentSwitch.isOn ? "YES" : "NO"
    }
    private var newHousingPmt : Double{
        return Formula.calculatePMT(interestRateTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, firstMtgAmount)
    }
    
    var totalHousingPayment : Double{
        var total: Double = 0
//        total += paymentPerYearTextField.toDouble
        total += newHousingPmt
        total += monthlyTaxesTextField.toDouble
        total += monthlyInsuranceTextField.toDouble
        total += monthlyMtgInsuranceTextField.toDouble
        total += monthlyHOAFeesTextField.toDouble
        total += monthlySecondMtgTextField.toDouble
        return total.removeMinus
    }
    
    var cashNeededToClose: Double{
        var cashNeeded: Double = 0
        cashNeeded += purchasePriceTextField.toDouble
        cashNeeded -= firstMtgAmount
        cashNeeded -= secondMtgAmountTextField.toDouble
        cashNeeded += loanFeesChargesTextField.toDouble
        cashNeeded += prepaidMIPChargesTextField.toDouble
        cashNeeded += titleRecordFeesTextField.toDouble
        return cashNeeded.removeMinus
        
    }
    
    var appreciationGainPerMonth: Double{
        let purchasePrice = purchasePriceTextField.toDouble
        let yearlyInterest = yearlyApprecRateTextField.toDouble.toValue
        return purchasePrice * yearlyInterest/paymentPerYearTextField.toDouble
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    
    private func defaultValues(){
        purchasePriceTextField.text = "295000"
        downpaymentAmountTextField.text = "10325"
        secondMtgAmountTextField.text = "0"
        loanFeesChargesTextField.text = "2000"
        prepaidMIPChargesTextField.text = "5163"
        titleRecordFeesTextField.text = "1500"
        yearlyApprecRateTextField.text = "4"
        interestRateTextField.text = "4.175"
        loanTermTextField.text = "30"
        paymentPerYearTextField.text = "12"
        monthlyTaxesTextField.text = "285"
        monthlyInsuranceTextField.text = "95"
        monthlyMtgInsuranceTextField.text = "295"
    }
    private func setup(){
        self.configureUI()
        self.fetch()
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        CommonRequest.dropDownAPI(type: .paymentType) { (paymentList) in
            self.paymentTypeList = paymentList
            
            CommonRequest.dropDownAPI(type: .loanPlan) { (loanList) in
                LoadingView.instance.hideActivityIndicator(uiView: self.view)
                self.loanPlanList = loanList
            }
        }
        
    }
    private func selectedValues(){
        if let loanPlan = self.rentBuyDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.loanPlanTextField.text = selectedLoanPlan?.name
            }
        }
        if let paymentType = self.rentBuyDetails?.paymentType{
            if let index = self.paymentTypeList?.index(of: DropDownInfoModel.init(value: paymentType)){
                self.selectedPaymentType = self.paymentTypeList?[index]
                self.paymentTypeTextField.text = selectedPaymentType?.name
            }
        }
    }
    private func configureUI(){
        downpaymentAmountTextField.isEnabled = false
        downpaymentAmountTextField.text = downPayment.toString
        loanPlanTextField.delegate = self
        paymentTypeTextField.delegate = self
    }
    @IBAction func downPaymentSwitchAction(_ sender: UISwitch){
        if !sender.isOn{
            self.showAlert("", "Down payment amount should be greater than \(downPayment.roundUp)")
        }
        downpaymentAmountTextField.isEnabled = sender.isOn ? false : true
    }
    func isValidationSuccess()->Bool{
        if !minDownpaymentSwitch.isOn{
            if downpaymentAmountTextField.toDouble <= downPayment{
                self.showAlert("", "Down payment amount should be greater than \(downPayment.roundUp)")
                return false
            }
            return true
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RentBuyFormTwoViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            loanPlanTextField.text = model?.name
            selectedLoanPlan = model
        }else if viewController.dataModel.tag == 1{
            paymentTypeTextField.text = model?.name
            selectedPaymentType = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanPlanTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 0)
            return false
        }else if textField == paymentTypeTextField{
            self.showBottomPicker(delegate: self, items: paymentTypeList ?? [], tag: 1)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == yearlyApprecRateTextField || textField == interestRateTextField {
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension RentBuyFormTwoViewController{
    private func mapping(){
        purchasePriceTextField.text = rentBuyDetails?.purchasePrice?.readableFormat
        downpaymentAmountTextField.text = rentBuyDetails?.downPayAmount?.readableFormat
        secondMtgAmountTextField.text = rentBuyDetails?.ndmtgAmt?.readableFormat
        loanFeesChargesTextField.text = rentBuyDetails?.loanFeesCharge?.readableFormat
        prepaidMIPChargesTextField.text = rentBuyDetails?.prepaids?.readableFormat
        titleRecordFeesTextField.text = rentBuyDetails?.titleRecord?.readableFormat
        yearlyApprecRateTextField.text = rentBuyDetails?.yearlyApprec?.toNumber.toPercentage
        interestRateTextField.text = rentBuyDetails?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = rentBuyDetails?.loanTerm
        paymentPerYearTextField.text = rentBuyDetails?.payPerYear
        monthlyTaxesTextField.text = rentBuyDetails?.monthlyTaxes?.readableFormat
        monthlyInsuranceTextField.text = rentBuyDetails?.monthlyInsur?.readableFormat
        monthlyMtgInsuranceTextField.text = rentBuyDetails?.monthlyMtgInsur?.readableFormat
        monthlyHOAFeesTextField.text = rentBuyDetails?.monthlyHoa?.readableFormat
        monthlySecondMtgTextField.text = rentBuyDetails?.monthlyNdMtgPmt?.readableFormat
    }
}
extension RentBuyFormTwoViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.rentVsBuy.key.self
        var dict: Dictionary<String, Any> = [key.purchasePrice: purchasePriceTextField.toDouble]
        dict.updateValue(downpaymentAmountTextField.toDouble, forKey: key.downPayAmount)
        dict.updateValue(secondMtgAmountTextField.toDouble, forKey: key.ndmtgAmt)
        dict.updateValue(loanFeesChargesTextField.toDouble, forKey: key.loanFeesCharge)
        dict.updateValue(prepaidMIPChargesTextField.toDouble, forKey: key.prepaids)
        dict.updateValue(titleRecordFeesTextField.toDouble, forKey: key.titleRecord)
        dict.updateValue(yearlyApprecRateTextField.toDouble, forKey: key.yearlyApprec)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear)
        dict.updateValue(monthlyTaxesTextField.toDouble, forKey: key.monthlyTaxes)
        dict.updateValue(monthlyInsuranceTextField.toDouble, forKey: key.monthlyInsur)
        dict.updateValue(monthlyMtgInsuranceTextField.toDouble, forKey: key.monthlyMtgInsur)
        dict.updateValue(monthlySecondMtgTextField.toDouble, forKey: key.monthlyNdMtgPmt)
        dict.updateValue(monthlyHOAFeesTextField.toDouble, forKey: key.monthlyHoa)
        dict.updateValue(selectedLoanPlan?.value ?? "", forKey: key.loanPlan)
        dict.updateValue(selectedPaymentType?.value ?? "", forKey: key.paymentType)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTerm)

        return dict
    }
}
