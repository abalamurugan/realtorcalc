//
//  SettingsViewController.swift
//  RealtorCalc
//
//  Created by Bala on 19/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SideMenu

class SettingsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var dataModel: SettingsDataModel = {return SettingsDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        // Do any additional setup after loading the view.
    }
    @IBAction func logoutAction(_ sender : UIButton){
        
    }
    private func configUI(){
        self.tableView.register(SettingsTableViewCell.nib, forCellReuseIdentifier: SettingsTableViewCell.reuseIdentifier)
        self.setupLeftMenu()
        self.sideMenuSetup()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == ChangePasswordViewController.segueIdentifier{
            let changePasswordController = segue.destination as! ChangePasswordViewController
            changePasswordController.type = .change
        }
    }
    

}
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.reuseIdentifier, for: indexPath) as! SettingsTableViewCell
        cell.set(item: dataModel[indexPath])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = dataModel[indexPath]
        if let segueId = model.segueID, segueId != ""{
            self.performSegue(withIdentifier: segueId, sender: self)
        }
    }
}
//MARK:- SideMenu Delegate
extension SettingsViewController: UISideMenuNavigationControllerDelegate, SideMenuViewControllerDelegate {
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        if let sideMenu = menu.topViewController as? SideMenuViewController{
            sideMenu.delegate = self
        }
    }
    func menuItemDidSelected(_ item: MenuItem) {
        self.tabBarController?.selectedIndex = item.index
    }
}
