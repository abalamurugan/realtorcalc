//
//  InvestmentOutputViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 26/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class InvestmentOutputViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showInvestmentOutput"
    
    
    lazy var dataModel: InvestmentOutputDataModel = {return InvestmentOutputDataModel()}()
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        // Do any additional setup after loading the view.
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.getPDf()
    }
    private func configureUI(){
        self.tableView.register(EquityBuildupTableViewCell.nib, forCellReuseIdentifier: EquityBuildupTableViewCell.reuseIdentifier)
        self.tableView.register(CMATableViewCell.nib, forCellReuseIdentifier: CMATableViewCell.reuseIdentifier)

        self.tableView.register(RefinanceHeaderView.nib, forHeaderFooterViewReuseIdentifier: RefinanceHeaderView.reuseIdentifier)
        self.title = LocalizableKey.navigationTitle.output
        self.configurePrint()
    }
    private func configurePrint(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID, APIKey.common.key.tag : Constants.tagValue.printOption]
        connection.networkRequest(urlPath: nil, method: .get, param: param,encoding: .queryString, { (response: ResponseModel<PrintPaymentModel>) in
            let model = response.item?.response
            let isRightButton = (model?.isPrintAvailable ?? "NO").caseInsensitiveCompare("YES") == .orderedSame ? true : false
            if isRightButton{
                self.setupRightBarButton(buttonTitle: LocalizableKey.reports.print)
            }
        }) { (error) in
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension InvestmentOutputViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.outputList![section].list?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataModel.type == .cma{
            let cell = tableView.dequeueReusableCell(withIdentifier: CMATableViewCell.reuseIdentifier, for: indexPath) as! CMATableViewCell
            cell.set(dataModel[indexPath])
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: EquityBuildupTableViewCell.reuseIdentifier, for: indexPath) as! EquityBuildupTableViewCell
        cell.set(dataModel[indexPath])
        cell.equityBackgroundView.layer.backgroundColor = indexPath.row % 2 == 0 ? UIColor.white.cgColor : UIColor.cellBgColor.cgColor
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: RefinanceHeaderView.reuseIdentifier) as! RefinanceHeaderView
        headerView.titleLabel.text = dataModel.outputList![section].title
        headerView.layer.backgroundColor = UIColor.primaryColor.cgColor
        headerView.titleLabel.textColor = UIColor.white
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dataModel.type == .cma ? 80 : 35
    }
}

extension InvestmentOutputViewController{
   
    private func pdfParam()->Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        var reportName: String?
        switch dataModel.type {
        case .cma:
            reportName = Constants.reportNames.CMA
        case .financing:
            reportName = Constants.reportNames.financeOption
        case .investment:
            reportName = Constants.reportNames.investment
        case .lender:
            reportName = Constants.reportNames.lender
        case .maximumQualification:
            reportName = Constants.reportNames.maxQualification
        default:
            break;
        }
        dict.updateValue(Constants.pdfDetect, forKey: APIKey.pdf.key.detect)
        dict.updateValue(dataModel.contactModel!.contactId!, forKey: APIKey.pdf.key.contactId)
        if let name = reportName{
            dict.updateValue(name, forKey: APIKey.pdf.key.name)
        }
        dict.updateValue(Constants.tagValue.reportPdf, forKey: APIKey.pdf.key.tag)
        return dict
    }
    private func getPDf(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: pdfParam(), encoding: .queryString, { (response: ResponseModel<PDFModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let pdfURL = response.item?.pdfURL, pdfURL != ""{
                UIApplication.shared.open(URL(string : pdfURL)!, options: [:], completionHandler: { (status) in
                })
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
