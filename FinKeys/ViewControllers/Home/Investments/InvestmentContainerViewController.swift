//
//  InvestmentContainerViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class InvestmentContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showInvestments"
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var financeContainerView: UIView!
    @IBOutlet weak var investementContainerView: UIView!
    @IBOutlet weak var operatingExpContainerView: UIView!

    var financeController: FinanceTableViewController!
    var investmentsController: InvestementsTableViewController!
    var operatingExpController: OperatingExpTableViewController!
    
    lazy var dataModel: InvestmentDataModel = {return InvestmentDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        investementContainerView.isHidden = true
        operatingExpContainerView.isHidden = true
        financeContainerView.isHidden = false
        self.title = LocalizableKey.navigationTitle.investment
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            financeContainerView.isHidden = false
            investementContainerView.isHidden = true
            operatingExpContainerView.isHidden = true
        }else if sender.selectedSegmentIndex == 1{
            financeContainerView.isHidden = true
            investementContainerView.isHidden = false
            operatingExpContainerView.isHidden = true
        }else if sender.selectedSegmentIndex == 2{
            financeContainerView.isHidden = true
            investementContainerView.isHidden = true
            operatingExpContainerView.isHidden = false
        }
    }
    @IBAction func calculateAction(_ sender: UIButton){
        self.postFormValues()
    }
    internal func showOutput(){
        self.calcEOYList()
        self.calcIRRList()
        dataModel.outputList.append(investmentOutput(yearOutput: firstYearOutput, title: "Year 1"))
        dataModel.outputList.append(investmentOutput(yearOutput: secondYearOutput, title: "Year 2"))
        dataModel.outputList.append(investmentOutput(yearOutput: thirdYearOutput, title: "Year 3"))
        dataModel.outputList.append(investmentOutput(yearOutput: fourthYearOutput, title: "Year 4"))
        dataModel.outputList.append(investmentOutput(yearOutput: fifthYearOutput, title: "Year 5"))
        dataModel.outputList.append(RefinanceOutputModel.init(title: "Adjusted Basis", list: adjustedBasisOutput))
        dataModel.outputList.append(RefinanceOutputModel.init(title: "Tax on Sale", list: taxOnSaleOutput))
        dataModel.outputList.append(RefinanceOutputModel.init(title: "Proceeds after taxes", list: proceedsAfterTaxesOutput))
        
        self.performSegue(withIdentifier: InvestmentOutputViewController.segueIdentifier, sender: self)
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FinanceTableViewController.segueIdentifier{
            financeController = segue.destination as? FinanceTableViewController
        }else if segue.identifier == InvestementsTableViewController.segueIdentifier{
            investmentsController = segue.destination as? InvestementsTableViewController
        }else if segue.identifier == OperatingExpTableViewController.segueIdentifier{
            operatingExpController = segue.destination as? OperatingExpTableViewController
        }else if segue.identifier == InvestmentOutputViewController.segueIdentifier{
            let outputController = segue.destination as! InvestmentOutputViewController
            outputController.dataModel.outputList = dataModel.outputList
            outputController.dataModel.contactModel = self.contactModel
        }
    }
}
extension InvestmentContainerViewController{
    func calcGrossIncome(income: Double, rentPercentage: Double)->Double{
        return income * (1 + rentPercentage.toValue)
    }
    func calcLessVacancyCreditLoss(income: Double, vacantPercentage: Double)->Double{
        return income * vacantPercentage.toValue
    }
    func eoyMortgageCalc(year: Double, isFirstMortage: Bool = true)->Double{
        let rate = isFirstMortage ? financeController.rateFirstMortgageTextField.toDouble : financeController.rateSecondMortgageTextField.toDouble
        let pmtYear = isFirstMortage ? financeController.pmtFirstMortgageTextField.toDouble : financeController.pmtSecondMortgageTextField.toDouble
        let term = isFirstMortage ? financeController.termFirstMortgageTextField.toDouble : financeController.termSecondMortgageTextField.toDouble
        let mortageAmount = isFirstMortage ? financeController.pmtFirstMortgage : financeController.pmtSecondMortgage

        let interest = rate.toValue/pmtYear
        let paymentTerm = (term - year) * pmtYear
        let pv = Formula.PVCalculation(rate: interest, nper: paymentTerm, pmt: mortageAmount).removeMinus
        return pv
    }
    func calcEOYList(){
        for i in 1...5{
            let eoy = self.eoyMortgageCalc(year: Double(i))
            print("EOY===>", eoy)
            dataModel.eoyFirstMortgageList.append(eoy)
        }
        for i in 1...5{
            let eoy = self.eoyMortgageCalc(year: Double(i), isFirstMortage: false)
            dataModel.eoySecondMortgageList.append(eoy)
        }
    }
    var depreciation: Double{
        return financeController.salesPriceTextField.toDouble * investmentsController.improvementsTextField.toDouble.toValue/27.5
    }
    
    var firstYearOutput: InvestmentOutputModel{
        let model = InvestmentOutputModel()
        model.grossScheduleIncome = financeController.yearlyRent
        model.lessVacantCreditLoss = self.calcLessVacancyCreditLoss(income: model.grossScheduleIncome!, vacantPercentage: investmentsController.vacancyTextField.toDouble)
        model.grossOperatingIncome = model.grossScheduleIncome! - model.lessVacantCreditLoss!
        model.operatingExpenses = operatingExpController.operatingExpenses
        model.netOperatingExpenses =  model.grossOperatingIncome! - model.operatingExpenses!
        model.nonOperatingExpenses = 0
        model.firstMtgDebtServices = financeController.debtServiceFirstMortgage
        model.secondMtgDebtServices = financeController.debtServiceSecondMortgage
        model.cashFlowBeforeTaxes = model.netOperatingExpenses! - model.firstMtgDebtServices! - model.secondMtgDebtServices!
         let interestExpense = financeController.totalDebtService - (financeController.totalMortgage - dataModel.eoyFirstMortgageList[0] - dataModel.eoySecondMortgageList[0])
        model.interestExpenses = interestExpense
        model.depreciation = depreciation
        model.taxableIncome = model.netOperatingExpenses! - model.interestExpenses! - model.depreciation!
        model.xMarginalTaxRate = investmentsController.invTaxBracketsTextField.toDouble
        model.incomeTaxes = model.taxableIncome! * model.xMarginalTaxRate!.toValue
        model.cashFlowAfterTaxes = model.cashFlowBeforeTaxes! - model.incomeTaxes!
        return model
    }
    var secondYearOutput: InvestmentOutputModel{
        let model = InvestmentOutputModel()
        model.grossScheduleIncome = self.calcGrossIncome(income: firstYearOutput.grossScheduleIncome!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.lessVacantCreditLoss = self.calcLessVacancyCreditLoss(income: model.grossScheduleIncome!, vacantPercentage: investmentsController.vacancyTextField.toDouble)
        model.grossOperatingIncome = model.grossScheduleIncome! - model.lessVacantCreditLoss!
        model.operatingExpenses = self.calcGrossIncome(income: firstYearOutput.operatingExpenses!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.netOperatingExpenses = model.grossOperatingIncome! - model.operatingExpenses!
        model.nonOperatingExpenses = 0
        model.firstMtgDebtServices = financeController.debtServiceFirstMortgage
        model.secondMtgDebtServices = financeController.debtServiceSecondMortgage
        model.cashFlowBeforeTaxes = model.netOperatingExpenses! - model.firstMtgDebtServices! - model.secondMtgDebtServices!
        let firstEOYDiff = dataModel.eoyFirstMortgageList[0] - dataModel.eoyFirstMortgageList[1]
        let secondEOYDiff = dataModel.eoySecondMortgageList[0] - dataModel.eoySecondMortgageList[1]

        let interestExpense = financeController.totalDebtService - firstEOYDiff - secondEOYDiff
        model.interestExpenses = interestExpense
        model.depreciation = depreciation
        model.taxableIncome = model.netOperatingExpenses! - model.interestExpenses! - model.depreciation!
        model.xMarginalTaxRate = investmentsController.invTaxBracketsTextField.toDouble
        model.incomeTaxes = model.taxableIncome! * model.xMarginalTaxRate!.toValue
        model.cashFlowAfterTaxes = model.cashFlowBeforeTaxes! - model.incomeTaxes!
        return model
    }
    var thirdYearOutput: InvestmentOutputModel{
        let model = InvestmentOutputModel()
        model.grossScheduleIncome = self.calcGrossIncome(income: secondYearOutput.grossScheduleIncome!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.lessVacantCreditLoss = self.calcLessVacancyCreditLoss(income: model.grossScheduleIncome!, vacantPercentage: investmentsController.vacancyTextField.toDouble)
        model.grossOperatingIncome = model.grossScheduleIncome! - model.lessVacantCreditLoss!
        model.operatingExpenses = self.calcGrossIncome(income: secondYearOutput.operatingExpenses!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.netOperatingExpenses = model.grossOperatingIncome! - model.operatingExpenses!
        model.nonOperatingExpenses = 0
        model.firstMtgDebtServices = financeController.debtServiceFirstMortgage
        model.secondMtgDebtServices = financeController.debtServiceSecondMortgage
        model.cashFlowBeforeTaxes = model.netOperatingExpenses! - model.firstMtgDebtServices! - model.secondMtgDebtServices!
        let firstEOYDiff = dataModel.eoyFirstMortgageList[1] - dataModel.eoyFirstMortgageList[2]
        let secondEOYDiff = dataModel.eoySecondMortgageList[1] - dataModel.eoySecondMortgageList[2]
        
        let interestExpense = financeController.totalDebtService - firstEOYDiff - secondEOYDiff
//        let interestExpense = financeController.debtServiceFirstMortgage - (dataModel.eoyFirstMortgageList[1] - dataModel.eoyFirstMortgageList[2])
        model.interestExpenses = interestExpense
        model.depreciation = depreciation
        model.taxableIncome = model.netOperatingExpenses! - model.interestExpenses! - model.depreciation!
        model.xMarginalTaxRate = investmentsController.invTaxBracketsTextField.toDouble
        model.incomeTaxes = model.taxableIncome! * model.xMarginalTaxRate!.toValue
        model.cashFlowAfterTaxes = model.cashFlowBeforeTaxes! - model.incomeTaxes!
        return model
    }
    var fourthYearOutput: InvestmentOutputModel{
        let model = InvestmentOutputModel()
        model.grossScheduleIncome = self.calcGrossIncome(income: thirdYearOutput.grossScheduleIncome!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.lessVacantCreditLoss = self.calcLessVacancyCreditLoss(income: model.grossScheduleIncome!, vacantPercentage: investmentsController.vacancyTextField.toDouble)
        model.grossOperatingIncome = model.grossScheduleIncome! - model.lessVacantCreditLoss!
        model.operatingExpenses = self.calcGrossIncome(income: thirdYearOutput.operatingExpenses!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.netOperatingExpenses = model.grossOperatingIncome! - model.operatingExpenses!
        model.nonOperatingExpenses = 0
        model.firstMtgDebtServices = financeController.debtServiceFirstMortgage
        model.secondMtgDebtServices = financeController.debtServiceSecondMortgage
        model.cashFlowBeforeTaxes = model.netOperatingExpenses! - model.firstMtgDebtServices! - model.secondMtgDebtServices!
        let firstEOYDiff = dataModel.eoyFirstMortgageList[2] - dataModel.eoyFirstMortgageList[3]
        let secondEOYDiff = dataModel.eoySecondMortgageList[2] - dataModel.eoySecondMortgageList[3]
        
        let interestExpense = financeController.totalDebtService - firstEOYDiff - secondEOYDiff
//        let interestExpense = financeController.debtServiceFirstMortgage - (dataModel.eoyFirstMortgageList[2] - dataModel.eoyFirstMortgageList[3])
        model.interestExpenses = interestExpense
        model.depreciation = depreciation
        model.taxableIncome = model.netOperatingExpenses! - model.interestExpenses! - model.depreciation!
        model.xMarginalTaxRate = investmentsController.invTaxBracketsTextField.toDouble
        model.incomeTaxes = model.taxableIncome! * model.xMarginalTaxRate!.toValue
        model.cashFlowAfterTaxes = model.cashFlowBeforeTaxes! - model.incomeTaxes!
        return model
    }
    var fifthYearOutput: InvestmentOutputModel{
        let model = InvestmentOutputModel()
        model.grossScheduleIncome = self.calcGrossIncome(income: fourthYearOutput.grossScheduleIncome!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.lessVacantCreditLoss = self.calcLessVacancyCreditLoss(income: model.grossScheduleIncome!, vacantPercentage: investmentsController.vacancyTextField.toDouble)
        model.grossOperatingIncome = model.grossScheduleIncome! - model.lessVacantCreditLoss!
        model.operatingExpenses = self.calcGrossIncome(income: fourthYearOutput.operatingExpenses!, rentPercentage: investmentsController.rentalApprecTextField.toDouble)
        model.netOperatingExpenses = model.grossOperatingIncome! - model.operatingExpenses!
        model.nonOperatingExpenses = 0
        model.firstMtgDebtServices = financeController.debtServiceFirstMortgage
        model.secondMtgDebtServices = financeController.debtServiceSecondMortgage
        model.cashFlowBeforeTaxes = model.netOperatingExpenses! - model.firstMtgDebtServices! - model.secondMtgDebtServices!
        let firstEOYDiff = dataModel.eoyFirstMortgageList[3] - dataModel.eoyFirstMortgageList[4]
        let secondEOYDiff = dataModel.eoySecondMortgageList[3] - dataModel.eoySecondMortgageList[4]
        
        let interestExpense = financeController.totalDebtService - firstEOYDiff - secondEOYDiff
//        let interestExpense = financeController.debtServiceFirstMortgage - (dataModel.eoyFirstMortgageList[3] - dataModel.eoyFirstMortgageList[4])
        model.interestExpenses = interestExpense
        model.depreciation = depreciation
        model.taxableIncome = model.netOperatingExpenses! - model.interestExpenses! - model.depreciation!
        model.xMarginalTaxRate = investmentsController.invTaxBracketsTextField.toDouble
        model.incomeTaxes = model.taxableIncome! * model.xMarginalTaxRate!.toValue
        model.cashFlowAfterTaxes = model.cashFlowBeforeTaxes! - model.incomeTaxes!
        return model
    }
    
    /// Method used to split up class model object to key value pair
    ///
    /// - Parameters:
    ///   - yearOutput: investment year output list
    ///   - title: title for the year for eg: year 1, year 2
    /// - Returns: title with list of values for year 1
    func investmentOutput(yearOutput: InvestmentOutputModel, title: String)->RefinanceOutputModel{
        var outputList: Array<OutputDetailsModel> = []
        outputList.append(OutputDetailsModel.init(key: "Gross Scheduled Income", value: yearOutput.grossScheduleIncome!))
        outputList.append(OutputDetailsModel.init(key: "Less: Vcy.& Credit Losses", value: yearOutput.lessVacantCreditLoss!))
        outputList.append(OutputDetailsModel.init(key: "Gross Operating Income:", value: yearOutput.grossOperatingIncome!))
        outputList.append(OutputDetailsModel.init(key: "Operating Expenses", value: yearOutput.operatingExpenses!))
        outputList.append(OutputDetailsModel.init(key: "Net Operating Income:", value: yearOutput.netOperatingExpenses!))
        outputList.append(OutputDetailsModel.init(key: "Non-Operating Expenses", value: yearOutput.nonOperatingExpenses!))
        outputList.append(OutputDetailsModel.init(key: "1st Mtg Debt Service", value: yearOutput.firstMtgDebtServices!))
        outputList.append(OutputDetailsModel.init(key: "2nd Mtg Debt Service", value: yearOutput.secondMtgDebtServices!))
        outputList.append(OutputDetailsModel.init(key: "Cash Flow Before Taxes", value: yearOutput.cashFlowBeforeTaxes!))
        outputList.append(OutputDetailsModel.init(key: "Interest Expenses", value: yearOutput.interestExpenses!))
        outputList.append(OutputDetailsModel.init(key: "Depreciation", value: yearOutput.depreciation!))
        outputList.append(OutputDetailsModel.init(key: "Taxable Income (Loss)", value: yearOutput.taxableIncome!))
        outputList.append(OutputDetailsModel.init(key: "x Marginal Tax Rate", value: yearOutput.xMarginalTaxRate!.toPercentage))
        outputList.append(OutputDetailsModel.init(key: "Income Taxes", value: yearOutput.incomeTaxes!))
        outputList.append(OutputDetailsModel.init(key: "Cash Flow After Taxes", value: yearOutput.cashFlowAfterTaxes!))
        return RefinanceOutputModel.init(title: title, list: outputList)

    }
}
extension InvestmentContainerViewController{
    var originalBasis: Double{
        let exp1 = financeController.salesPriceTextField.toDouble * investmentsController.purchaseCostTextField.toDouble.toValue
        return financeController.salesPriceTextField.toDouble + exp1
    }
    var cummulativeDeprec: Double{
        var cummulative = firstYearOutput.depreciation ?? 0
        cummulative += secondYearOutput.depreciation ?? 0
        cummulative += thirdYearOutput.depreciation ?? 0
        cummulative += fourthYearOutput.depreciation ?? 0
        cummulative += fifthYearOutput.depreciation ?? 0
        return cummulative
    }
    var taxOnSalePrice: Double{
        return Formula.FVCalc(rate: investmentsController.apprecRateTextField.toDouble.toValue, nper: 5, pmt: 0, fv: -financeController.salesPriceTextField.toDouble)
     }
    var sellingCosts: Double{
        return taxOnSalePrice * investmentsController.projSaleCostTextField.toDouble.toValue
    }
    var adjustedBasisSale: Double{
        var adjusted = originalBasis
        adjusted += operatingExpController.capitalImprovementsTextField.toDouble
        adjusted -= cummulativeDeprec
        adjusted += sellingCosts
        return adjusted
    }
    var longTermGapGain: Double{
        var gapGain = taxOnSalePrice
        gapGain -= adjustedBasisSale
        gapGain -= cummulativeDeprec
        return gapGain
    }
    var depRecaptureTax: Double{
        return cummulativeDeprec * investmentsController.taxOnDepTextField.toDouble.toValue
    }
    var taxCapitalGain: Double{
        return longTermGapGain * investmentsController.capitalGainTextField.toDouble.toValue
    }
    var taxLiabOnSale: Double{
        return depRecaptureTax + taxCapitalGain
    }
    var netSaleProceeds: Double{
        return taxOnSalePrice - sellingCosts
    }
    var mtgBalance: Double{
        return dataModel.eoyFirstMortgageList[4] + dataModel.eoySecondMortgageList[4]
    }
    var beforeTaxEquityRev: Double{
        return netSaleProceeds - mtgBalance
    }
    var proceedsAfterTaxes: Double{
        return beforeTaxEquityRev - taxLiabOnSale
    }
}
extension InvestmentContainerViewController{
    var adjustedBasisOutput: Array<OutputDetailsModel>{
        var outputList = Array<OutputDetailsModel>()
        outputList.append(OutputDetailsModel.init(key: "Original Basis", value: originalBasis))
        outputList.append(OutputDetailsModel.init(key: "+Capital Improvements", value: operatingExpController.capitalImprovementsTextField.toDouble))
        outputList.append(OutputDetailsModel.init(key: "-Cumulative Deprec.", value: cummulativeDeprec))
        outputList.append(OutputDetailsModel.init(key: "+Selling Costs", value: sellingCosts))
        outputList.append(OutputDetailsModel.init(key: "Adjusted Basis at Sale", value: adjustedBasisSale))
        return outputList
    }
    var taxOnSaleOutput: Array<OutputDetailsModel>{
        var outputList = Array<OutputDetailsModel>()
        outputList.append(OutputDetailsModel.init(key: "Sale Price", value: taxOnSalePrice))
        outputList.append(OutputDetailsModel.init(key: "-Adj. Basis", value: adjustedBasisSale))
        outputList.append(OutputDetailsModel.init(key: "-Deprec. Recap Gain", value: cummulativeDeprec))
        outputList.append(OutputDetailsModel.init(key: "Long Term Cap. Gain", value: longTermGapGain))
        outputList.append(OutputDetailsModel.init(key: "Dep. Recapture Tax", value: depRecaptureTax))
        outputList.append(OutputDetailsModel.init(key: "Tax on Capital Gain", value: taxCapitalGain))
        outputList.append(OutputDetailsModel.init(key: "Tax Liab. On Sale", value: taxLiabOnSale))
        return outputList
    }
    var proceedsAfterTaxesOutput: Array<OutputDetailsModel>{
        var outputList = Array<OutputDetailsModel>()
        outputList.append(OutputDetailsModel.init(key: "Sale Price", value: taxOnSalePrice))
        outputList.append(OutputDetailsModel.init(key: "-Costs of Sale", value: sellingCosts))
        outputList.append(OutputDetailsModel.init(key: "Net Sales Proceeds", value: netSaleProceeds))
        outputList.append(OutputDetailsModel.init(key: "-Mtg Balance", value: mtgBalance))
        outputList.append(OutputDetailsModel.init(key: "Before Tax Equity Rev.", value: beforeTaxEquityRev))
        outputList.append(OutputDetailsModel.init(key: "-Taxes Due on Sale", value: taxLiabOnSale))
        outputList.append(OutputDetailsModel.init(key: "Proceeds after Taxes", value: proceedsAfterTaxes))
        return outputList
    }
}
extension InvestmentContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getInvestment()
    }
}
extension InvestmentContainerViewController{
    var paramViewInvestments: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.investmentView, contactId: self.contactModel!.contactId!)
        
    }
    private func getInvestment(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewInvestments, encoding: .queryString, { (response: ResponseModel<InvestmentsModel>) in
            self.financeController.investmentDetails = response.item?.response
            self.investmentsController.investmentsDetails = response.item?.response
            self.operatingExpController.investmentDetails = response.item?.response
            
        }) { (error) in
            self.showError(error)
        }
    }
}
