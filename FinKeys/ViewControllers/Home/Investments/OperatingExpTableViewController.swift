//
//  OperatingExpTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class OperatingExpTableViewController: UITableViewController,StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showOperatingExp"
    
    @IBOutlet weak var propertyTaxesTextField: UITextField!
    @IBOutlet weak var insuranceTextField: UITextField!
    @IBOutlet weak var managemntFeeTextField: UITextField!
    @IBOutlet weak var maintainenceTextField: UITextField!
    @IBOutlet weak var utilitiesTextField: UITextField!
    @IBOutlet weak var advertisingTextField: UITextField!
    @IBOutlet weak var otherExpensesTextField: UITextField!
    @IBOutlet weak var capitalImprovementsTextField: UITextField!
    @IBOutlet weak var safeRateTextField: UITextField!
    @IBOutlet weak var befTaxInvTextField: UITextField!
    
    var investmentDetails: InvestmentDetailsModel?{
        didSet{
            self.mapping()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
    }
    private func defaultValues(){
        propertyTaxesTextField.text = "4823"
        insuranceTextField.text = "1174"
        managemntFeeTextField.text = "10000"
        maintainenceTextField.text = "12000"
        utilitiesTextField.text = "9378"
        advertisingTextField.text = "1000"
        otherExpensesTextField.text = "5000"
        capitalImprovementsTextField.text = "10000"
        safeRateTextField.text = "4.55"
        befTaxInvTextField.text = "7.00"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OperatingExpTableViewController{
    var operatingExpenses: Double{
        var total = propertyTaxesTextField.toDouble
        total += insuranceTextField.toDouble
        total += managemntFeeTextField.toDouble
        total += maintainenceTextField.toDouble
        total += utilitiesTextField.toDouble
        total += advertisingTextField.toDouble
        total += otherExpensesTextField.toDouble
        return total
    }
}
extension OperatingExpTableViewController{
    private func mapping(){
        propertyTaxesTextField.text = investmentDetails?.propertyTaxes?.readableFormat
        insuranceTextField.text = investmentDetails?.insurance?.readableFormat
        managemntFeeTextField.text = investmentDetails?.managementFee?.readableFormat
        maintainenceTextField.text = investmentDetails?.maintenance?.readableFormat
        utilitiesTextField.text = investmentDetails?.utilities?.readableFormat
        advertisingTextField.text = investmentDetails?.advertising?.readableFormat
        otherExpensesTextField.text = investmentDetails?.otherExpenses?.readableFormat
        capitalImprovementsTextField.text = investmentDetails?.capitalImprovements?.readableFormat
        safeRateTextField.text = investmentDetails?.safeRate?.toNumber.toPercentage
        befTaxInvTextField.text = investmentDetails?.befTaxInvRate?.toNumber.toPercentage
    }
}
extension OperatingExpTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.investment.key.self
        var dict: Dictionary<String, Any> = [key.propertyTaxes : propertyTaxesTextField.toDouble]
        dict.updateValue(insuranceTextField.toDouble, forKey: key.insurance)
        dict.updateValue(managemntFeeTextField.toDouble, forKey: key.managementFee)
        dict.updateValue(maintainenceTextField.toDouble, forKey: key.maintenance)
        dict.updateValue(utilitiesTextField.toDouble, forKey: key.utilities)
        dict.updateValue(advertisingTextField.toDouble, forKey: key.advertising)
        dict.updateValue(otherExpensesTextField.toDouble, forKey: key.otherExpenses)
        dict.updateValue(capitalImprovementsTextField.toDouble, forKey: key.capitalImprovements)
        dict.updateValue(safeRateTextField.toDouble, forKey: key.safeRate)
        dict.updateValue(befTaxInvTextField.toDouble, forKey: key.befTaxInvRate)
        return dict
    }
}
extension OperatingExpTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField ==  safeRateTextField || textField ==  befTaxInvTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
