//
//  InvestementsTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class InvestementsTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showInvestmentsDetails"
    
    @IBOutlet weak var purchaseCostTextField: UITextField!
    @IBOutlet weak var vacancyTextField: UITextField!
    @IBOutlet weak var improvementsTextField: UITextField!
    @IBOutlet weak var invTaxBracketsTextField: UITextField!
    @IBOutlet weak var apprecRateTextField: UITextField!
    @IBOutlet weak var rentalApprecTextField: UITextField!
    @IBOutlet weak var expenseApprecTextField: UITextField!
    @IBOutlet weak var projSaleCostTextField: UITextField!
    @IBOutlet weak var taxOnDepTextField: UITextField!
    @IBOutlet weak var capitalGainTextField: UITextField!
    
    var investmentsDetails: InvestmentDetailsModel?{
        didSet{
            self.mapping()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
    }
    private func defaultValues(){
        purchaseCostTextField.text = "2"
        vacancyTextField.text = "5"
        improvementsTextField.text = "90"
        invTaxBracketsTextField.text = "33"
        apprecRateTextField.text = "3"
        rentalApprecTextField.text = "3"
        expenseApprecTextField.text = "3"
        projSaleCostTextField.text = "9"
        taxOnDepTextField.text = "25"
        capitalGainTextField.text = "20"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension InvestementsTableViewController{
    private func mapping(){
        purchaseCostTextField.text = investmentsDetails?.purchaseCosts?.toNumber.toPercentage
        vacancyTextField.text = investmentsDetails?.vacancy?.toNumber.toPercentage
        improvementsTextField.text = investmentsDetails?.improvement?.toNumber.toPercentage
        invTaxBracketsTextField.text = investmentsDetails?.invTaxBracket?.toNumber.toPercentage
        apprecRateTextField.text = investmentsDetails?.apprecRate?.toNumber.toPercentage
        rentalApprecTextField.text = investmentsDetails?.rentalApprec?.toNumber.toPercentage
        expenseApprecTextField.text = investmentsDetails?.expenseApprec?.toNumber.toPercentage
        projSaleCostTextField.text = investmentsDetails?.projSaleCosts?.toNumber.toPercentage
        taxOnDepTextField.text = investmentsDetails?.taxOnDep?.toNumber.toPercentage
        capitalGainTextField.text = investmentsDetails?.captialGainTax?.toNumber.toPercentage
    }
}
extension InvestementsTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.investment.key.self
        var dict: Dictionary<String, Any> = [key.purchaseCosts : purchaseCostTextField.toDouble]
        dict.updateValue(vacancyTextField.toDouble, forKey: key.vacancy)
        dict.updateValue(improvementsTextField.toDouble, forKey: key.improvement)
        dict.updateValue(invTaxBracketsTextField.toDouble, forKey: key.invTaxBracket)
        dict.updateValue(apprecRateTextField.toDouble, forKey: key.apprecRate)
        dict.updateValue(rentalApprecTextField.toDouble, forKey: key.rentalApprec)
        dict.updateValue(expenseApprecTextField.toDouble, forKey: key.expenseApprec)
        dict.updateValue(projSaleCostTextField.toDouble, forKey: key.projSaleCosts)
        dict.updateValue(taxOnDepTextField.toDouble, forKey: key.taxOnDep)
        dict.updateValue(capitalGainTextField.toDouble, forKey: key.captialGainTax)
        return dict
    }
}
extension InvestementsTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.toNumber.toPercentage
    }
}
