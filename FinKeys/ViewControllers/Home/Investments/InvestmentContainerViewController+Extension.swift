//
//  InvestmentContainerViewController+Extension.swift
//  RealtorCalc
//
//  Created by Bala on 12/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

extension InvestmentContainerViewController{
    
    func calcIRRList(){
        dataModel.fiveYearIRRList.append(-financeController.downPayment)
        dataModel.fiveYearIRRList.append(107090.13)
        dataModel.fiveYearIRRList.append(104170.37)
        dataModel.fiveYearIRRList.append(101149.46)
        dataModel.fiveYearIRRList.append(98023.67)
        dataModel.fiveYearIRRList.append(774488.22)


//        dataModel.fiveYearIRRList.append(firstYearOutput.cashFlowAfterTaxes ?? 0)
//        dataModel.fiveYearIRRList.append(secondYearOutput.cashFlowAfterTaxes ?? 0)
//        dataModel.fiveYearIRRList.append(thirdYearOutput.cashFlowAfterTaxes ?? 0)
//        dataModel.fiveYearIRRList.append(fourthYearOutput.cashFlowAfterTaxes ?? 0)
//        dataModel.fiveYearIRRList.append((fifthYearOutput.cashFlowAfterTaxes ?? 0) + (proceedsAfterTaxes))
        print("irr====>\(Formula.IRRCalc(values: dataModel.fiveYearIRRList) ?? 0)")
    }
}
 
extension InvestmentContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID]
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.reportNames.investment, forKey: APIKey.common.key.action)
        dict.updateValue(Constants.tagValue.investmentAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        financeController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        investmentsController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        operatingExpController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    internal func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
