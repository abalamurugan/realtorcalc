//
//  FinanceTableViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 25/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinanceTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFinance"
    
    @IBOutlet weak var salesPriceTextField: UITextField!
    @IBOutlet weak var firstMortgageAmountTextField: UITextField!
    @IBOutlet weak var secondMortgageAmountTextField: UITextField!
    @IBOutlet weak var monthlyRentsTextField: UITextField!
    @IBOutlet weak var holdingPerYearTextField: UITextField!
    @IBOutlet weak var termFirstMortgageTextField: UITextField!
    @IBOutlet weak var termSecondMortgageTextField: UITextField!
    @IBOutlet weak var pmtFirstMortgageTextField: UITextField!
    @IBOutlet weak var pmtSecondMortgageTextField: UITextField!
    @IBOutlet weak var rateFirstMortgageTextField: UITextField!
    @IBOutlet weak var rateSecondMortgageTextField: UITextField!
    
    var investmentDetails: InvestmentDetailsModel?{
        didSet{
            self.mapping()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
    }
    private func defaultValues(){
        salesPriceTextField.text = "2300000"
//        firstMortgageAmountTextField.text = "1840000"
        secondMortgageAmountTextField.text = "10000"
        termFirstMortgageTextField.text = "25"
        termSecondMortgageTextField.text = "20"
        pmtFirstMortgageTextField.text = "12"
        pmtSecondMortgageTextField.text = "12"
        rateFirstMortgageTextField.text = "5.0"
        rateSecondMortgageTextField.text = "7"
        firstMortgageAmountTextField.text = "\(firstMortgageAmount)"
//        termFirstMortgageTextField.text = "\(termFirstMortgage)"
        pmtFirstMortgageTextField.text = "12"
        rateFirstMortgageTextField.text = "5"

        monthlyRentsTextField.text = "\(monthlyRent)"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension FinanceTableViewController{
    var downPayment: Double{
        var payment = salesPriceTextField.toDouble
        payment -= firstMortgageAmount
        payment -= secondMortgageAmountTextField.toDouble
        return payment
//        return salesPriceTextField.toDouble * 0.2
    }
    var yearlyRent: Double{
        return monthlyRentsTextField.toDouble * 12
    }
    var termFirstMortgage: Double{
        return 300/12
    }
    var monthlyRent: Double{
        return 2000*4*3
    }
    var totalMortgage: Double{
        var total = firstMortgageAmountTextField.toDouble
        total += secondMortgageAmountTextField.toDouble
        return total
    }
    var firstMortgageAmount: Double{
        return salesPriceTextField.toDouble * 0.8
 //        return salesPriceTextField.toDouble - downPayment
    }
    var pmtFirstMortgage: Double{
        return Formula.calculatePMT(rateFirstMortgageTextField.toDouble, pmtFirstMortgageTextField.toDouble, termFirstMortgageTextField.toDouble, firstMortgageAmountTextField.toDouble)
    }
    var pmtSecondMortgage: Double{
        return Formula.calculatePMT(rateSecondMortgageTextField.toDouble, pmtSecondMortgageTextField.toDouble, termSecondMortgageTextField.toDouble, secondMortgageAmountTextField.toDouble)
    }
    var debtServiceFirstMortgage: Double{
        print("pmt first mortgage====>\(pmtFirstMortgage)")
        return pmtFirstMortgage * pmtFirstMortgageTextField.toDouble
    }
    var debtServiceSecondMortgage: Double{
        if pmtSecondMortgage.isNaN{
            return 0
        }
        return pmtSecondMortgage * pmtSecondMortgageTextField.toDouble
    }
    var totalDebtService: Double{
        return debtServiceFirstMortgage + debtServiceSecondMortgage
    }
}
extension FinanceTableViewController{
    private func mapping(){
        salesPriceTextField.text = investmentDetails?.salesPrice?.readableFormat
        //        firstMortgageAmountTextField.text = "1840000"
        secondMortgageAmountTextField.text = investmentDetails?.ndMortgage?.readableFormat
        termFirstMortgageTextField.text = investmentDetails?.stTerm
        termSecondMortgageTextField.text = investmentDetails?.ndTerm
        pmtFirstMortgageTextField.text = investmentDetails?.stPmtYr
        pmtSecondMortgageTextField.text = investmentDetails?.ndPmtYr
        rateFirstMortgageTextField.text = investmentDetails?.stRate?.toNumber.toPercentage
        rateSecondMortgageTextField.text = investmentDetails?.ndRate?.toNumber.toPercentage
        firstMortgageAmountTextField.text = "\(firstMortgageAmount)".readableFormat
        //        termFirstMortgageTextField.text = "\(termFirstMortgage)"
        monthlyRentsTextField.text = "\(monthlyRent)".readableFormat
    }
}
extension FinanceTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.investment.key.self
        var dict: Dictionary<String, Any> = [key.salesPrice : salesPriceTextField.toDouble]
        dict.updateValue(secondMortgageAmountTextField.toDouble, forKey: key.ndMortgage)
        dict.updateValue(termFirstMortgageTextField.toDouble, forKey: key.stTerm)
        dict.updateValue(termSecondMortgageTextField.toDouble, forKey: key.ndTerm)
        dict.updateValue(pmtFirstMortgageTextField.toDouble, forKey: key.stPmtYr)
        dict.updateValue(pmtSecondMortgageTextField.toDouble, forKey: key.ndPmtYr)
        dict.updateValue(rateFirstMortgageTextField.toDouble, forKey: key.stRate)
        dict.updateValue(rateSecondMortgageTextField.toDouble, forKey: key.ndRate)
        dict.updateValue(firstMortgageAmountTextField.toDouble, forKey: key.stMortgage)
        dict.updateValue(monthlyRentsTextField.toDouble, forKey: key.monthlyRents)
        dict.updateValue(yearlyRent, forKey: key.yearlyRents)
        dict.updateValue(holdingPerYearTextField.toDouble, forKey: key.holdPerYrs)
        return dict
    }
}
extension FinanceTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == rateFirstMortgageTextField || textField == rateSecondMortgageTextField {
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
