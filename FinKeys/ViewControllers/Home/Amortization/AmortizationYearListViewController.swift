//
//  AmortizationYearListViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class AmortizationYearListViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showAmortizationYear"
    
    
    @IBOutlet weak var regularPmtLabel : UILabel!
    @IBOutlet weak var balanceWithExtraPmtLabel : UILabel!
    @IBOutlet weak var balanceWithNoExtraPmtLabel : UILabel!
    @IBOutlet weak var savingsOverPeriodLabel : UILabel!
    
    lazy var dataModel : AmortizationYearDataModel = {return AmortizationYearDataModel()}()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.output
        self.setupRightBarButton(buttonTitle: LocalizableKey.reports.print)
        self.mapping()
        // Do any additional setup after loading the view.
        
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.getPDf()
    }
    private func getPDf(){
        let connection = NetworkManager.init()
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        connection.networkRequest(urlPath: nil, method: .get, param: pdfParam, encoding: .queryString, { (response: ResponseModel<PDFModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let pdfURL = response.item?.pdfURL, pdfURL != ""{
                UIApplication.shared.open(URL(string : pdfURL)!, options: [:], completionHandler: { (status) in
                    
                })
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    private func mapping(){
        self.balanceWithNoExtraPmtLabel.text = dataModel.balanceWithNoExtraPmt?.readableFormat
        self.balanceWithExtraPmtLabel.text = dataModel.balanceWithExtraPmt?.readableFormat
        self.savingsOverPeriodLabel.text = dataModel.savingsOverPeriod?.readableFormat
        self.regularPmtLabel.text = dataModel.regularPmt?.readableFormat
    }
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == AmortizationOutputViewController.segueIdentifier{
            let outputController = segue.destination as! AmortizationOutputViewController
            outputController.dataModel.loanSummaryList = sender as! Array<AmortizationLoanDetailsModel>
            outputController.dataModel.contactModel = dataModel.contactModel
        }
    }
 

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension AmortizationYearListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AmoritizationYearTableViewCell.reuseIdentifier, for: indexPath) as! AmoritizationYearTableViewCell
        cell.totalLoanBalanceLabel.text = dataModel.totalLoanBalance(indexPath).readableFormat
        cell.totalPaymentLabel.text = dataModel.totalPayment(indexPath).readableFormat
        cell.interestSumLabel.text = dataModel.totalInterest(indexPath).readableFormat
        cell.extraSumLabel.text = dataModel.totalExtra(indexPath).readableFormat
        cell.principalSumLabel.text = dataModel.totalPrincipal(indexPath).readableFormat
        cell.dateLabel.text = dataModel.paymentDuration(indexPath)
         return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let loanList = self.dataModel.summaryList[indexPath.row]
        self.performSegue(withIdentifier: AmortizationOutputViewController.segueIdentifier, sender: loanList)
        
    }
}
extension AmortizationYearListViewController{
    var pdfParam: Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict.updateValue(Constants.pdfDetect, forKey: APIKey.pdf.key.detect)
        dict.updateValue(dataModel.contactModel!.contactId!, forKey: APIKey.pdf.key.contactId)
        dict.updateValue(Constants.reportNames.amortization, forKey: APIKey.pdf.key.name)
        dict.updateValue(Constants.tagValue.reportPdf, forKey: APIKey.pdf.key.tag)
        return dict
    }
}
