//
//  AmortizationInputViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 29/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

enum DropDownType: Int{
    case loanType
    case paymentType
    
    var index: Int{
        switch self {
        case .loanType:
            return 0
        case .paymentType:
            return 1
        }
    }
}

class AmortizationInputViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showAmortizationInput"
    
    @IBOutlet weak var mtgAmountTextField : UITextField!
    @IBOutlet weak var interestRateTextField : UITextField!
    @IBOutlet weak var periodicInterestRateTextField : UITextField!
    @IBOutlet weak var lifeTimeRateGapTextField : UITextField!
    @IBOutlet weak var loantermTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var noPaymentMadeTextField : UITextField!
    @IBOutlet weak var dateTextField : UITextField!
    @IBOutlet weak var extraPrincipalTextField : UITextField!
    @IBOutlet weak var calculateButton : UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var loanPlanLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!


    lazy var dataModel : AmortizationDataModel = {return AmortizationDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.amortization
//        self.defaultValues()
        self.contactPickerAction(UIButton())
        self.dateTextField.delegate = self
        self.contactNameTextField.becomeFirstResponder()
        self.fetch()
//        self.dataModel.getSummaryList()
        
        // Do any additional setup after loading the view.
    }
    private func fetch(){
        CommonRequest.dropDownAPI(type: .loanPlan) { (list) in
            self.dataModel.loanList = list
        }
        CommonRequest.dropDownAPI(type: .paymentType) { (list) in
            self.dataModel.paymentTypeList = list
        }
    }
    
    private func defaultValues(){
        mtgAmountTextField.text = "940000"
        interestRateTextField.text = "4.125"
        periodicInterestRateTextField.text = "1.50"
        lifeTimeRateGapTextField.text = "7.125"
        loantermTextField.text = "25"
        paymentPerYearTextField.text = "12"
        noPaymentMadeTextField.text = "24"
        extraPrincipalTextField.text = "150"
    }
    @IBAction func calculateAction(_ sender : UIButton){
        if isValidationSuccess(){
            self.postFormValues()
        }
    }
    private func showOutput(){
        self.dataModel.interestRate = self.interestRateTextField.text?.toDouble
        self.dataModel.loanTerm = self.loantermTextField.text?.toDouble
        self.dataModel.loanMtgAmount = self.mtgAmountTextField.text?.toDouble
        self.dataModel.paymentPerYear = self.paymentPerYearTextField.text?.toDouble
        self.dataModel.extraPrincipal = self.extraPrincipalTextField.toDouble
        self.dataModel.periodicIntRate = self.periodicInterestRateTextField.toDouble
        self.dataModel.noPaymentMade = Int(self.noPaymentMadeTextField.text!)
        self.dataModel.clearValues()
        self.dataModel.getSummaryList()
        self.performSegue(withIdentifier: AmortizationYearListViewController.segueIdentifier, sender: self)
        
    }
    @IBAction func dropDownAction(_ sender: UIButton){
        let type = DropDownType.init(rawValue: sender.tag)
        if type! == .loanType{
            self.showBottomPicker(delegate: self, items: dataModel.loanList ?? [], tag: sender.tag, title: LocalizableKey.financingTitle.loanPlan)
        }else{
            self.showBottomPicker(delegate: self, items: dataModel.paymentTypeList ?? [], tag: sender.tag, title: LocalizableKey.financingTitle.paymentType)
        }
    
    }
    func isValidationSuccess()->Bool{
        if dataModel.contactModel == nil{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("Contact"))
            return false
        }else if mtgAmountTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.mtgInvalid)
            return false
        }else if interestRateTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.interestInvalid)
            return false
        }else if periodicInterestRateTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.periodicInterestInvalid)
            return false
        }else if lifeTimeRateGapTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.lifeTimeInvalid)
            return false
        }else if paymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.paymentMadeInvalid)
            return false
        }else if paymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.paymentPerYearInvalid)
            return false
        }else if noPaymentMadeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.paymentMadeInvalid)
            return false
        }else if dateTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.dateInvalid)
            return false
        }
        return true
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == AmortizationOutputViewController.segueIdentifier{
            let outputController = segue.destination as! AmortizationOutputViewController
            outputController.dataModel = self.dataModel
        }else if segue.identifier == AmortizationYearListViewController.segueIdentifier{
            let outputController = segue.destination as! AmortizationYearListViewController
            outputController.dataModel.summaryList = self.dataModel.summaryList
            outputController.dataModel.contactModel = self.dataModel.contactModel
            outputController.dataModel.balanceWithExtraPmt = self.dataModel.balanceWithExtraPmt
            outputController.dataModel.balanceWithNoExtraPmt = self.dataModel.balanceWithNoExtraPmt
            outputController.dataModel.savingsOverPeriod = self.dataModel.savingsOverPeriod
            outputController.dataModel.regularPmt = self.dataModel.regularPmt
        }
    }
}
extension AmortizationInputViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateTextField{
            DatePicker.show(self) { (date) in
                self.dataModel.paymentDate = date
                let dateFormatter = DateFormatter.init()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                self.dateTextField.text = dateFormatter.string(from: date)
            }
            return false
        }else if textField == contactNameTextField{
            let contactPicker = ContactListViewController.showContactPicker()
            contactPicker.delegate = self
            contactPicker.dataModel.isPicker = true
            self.presentWithNavigationController(contactPicker)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestRateTextField || textField == lifeTimeRateGapTextField || textField == periodicInterestRateTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}

extension AmortizationInputViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.dataModel.contactModel = item
        contactNameTextField.text = item.fullName
        self.getAmortization()
    }
}
extension AmortizationInputViewController{
    var param: Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        let key = APIKey.amortization.key.self
        dict.updateValue(Date.init().toString(format: "dd-MM-yyyy") ?? "", forKey: key.currentDate)
        dict.updateValue(self.dateTextField.text!, forKey: key.datepicker)
        if !extraPrincipalTextField.isEmpty{
            dict.updateValue(self.extraPrincipalTextField.toDouble, forKey: key.extraPrincipalAmt)
        }
        dict.updateValue(self.interestRateTextField.toDouble, forKey: key.interestRate)
        dict.updateValue(self.lifeTimeRateGapTextField.toDouble, forKey: key.lifeTimeRate)
        if !loantermTextField.isEmpty{
            dict.updateValue(self.loantermTextField.toDouble, forKey: key.loanTerm)
        }
        dict.updateValue(dataModel.selectedLoan?.value ?? "", forKey: key.loanType)
        dict.updateValue(self.mtgAmountTextField.toDouble, forKey: key.mtgAmount)
        dict.updateValue(self.noPaymentMadeTextField.toDouble, forKey: key.paymentMade)
        dict.updateValue(self.paymentPerYearTextField.toDouble, forKey: key.payPerYr)
        dict.updateValue(self.periodicInterestRateTextField.toDouble, forKey: key.periodicInterest)
        dict.updateValue(dataModel.selectedPaymentType?.value ?? "", forKey: key.paymentType)
        dict.updateValue(self.dataModel.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(Constants.tagValue.amortizationAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(Constants.reportNames.amortization, forKey: APIKey.common.key.action)
        return dict
    }
    var paramViewAmortization: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        dict.updateValue(dataModel.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(Constants.tagValue.amortizationView, forKey: APIKey.common.key.tag)
        return dict
    }
}
extension AmortizationInputViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as! DropDownInfoModel
        if viewController.dataModel.tag == DropDownType.loanType.rawValue{
            self.loanPlanLabel.text = model.name
            self.dataModel.selectedLoan = model
        }else if viewController.dataModel.tag == DropDownType.paymentType.rawValue{
            self.paymentTypeLabel.text = model.name
            self.dataModel.selectedPaymentType = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension AmortizationInputViewController{
    private func dataMapping(amortizationView: AmortizationDetailsModel?){
        guard let _item = amortizationView else {
            return
        }
        self.mtgAmountTextField.text = _item.mtgAmount?.readableFormat
        if let loanType = _item.loanType{
            if let index = self.dataModel.loanList?.index(of: .init(value: loanType)){
                self.loanPlanLabel.text = self.dataModel.loanList?[index].name
                self.dataModel.selectedLoan = self.dataModel.loanList?[index]
            }
        }
        if let paymentType = _item.paymentType{
            if let index = self.dataModel.paymentTypeList?.index(of: .init(value: paymentType)){
                self.paymentTypeLabel.text = self.dataModel.paymentTypeList?[index].name
                self.dataModel.selectedPaymentType = self.dataModel.paymentTypeList?[index]
            }
        }
        self.interestRateTextField.text = _item.interestRate?.toNumber.toPercentage
        self.lifeTimeRateGapTextField.text = _item.lifeTimeRate?.toNumber.toPercentage
        self.loantermTextField.text = _item.loanTerm
        self.paymentPerYearTextField.text = _item.payPerYr
        self.noPaymentMadeTextField.text = _item.paymentMade
        self.extraPrincipalTextField.text = _item.extraPrincipal?.readableFormat
        self.periodicInterestRateTextField.text = _item.periodicInterest?.toNumber.toPercentage
        let date = _item.createdDate?.date(format: "dd-MM-yyyy")
        if date != nil{
            self.dateTextField.text = _item.createdDate
            self.dataModel.paymentDate = date
        }
    }
}
extension AmortizationInputViewController{
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: param, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    private func getAmortization(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewAmortization, encoding: .queryString, { (response: ResponseModel<AmortizationViewModel>) in
            self.dataMapping(amortizationView: response.item?.response)
        }) { (error) in
            self.showError(error)
        }
    }
}
