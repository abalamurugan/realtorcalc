//
//  FinancialKeysViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//
import UIKit
import SideMenu

class FinancialKeysViewController: BaseViewController{
 
    @IBOutlet weak var collectionView : UICollectionView!
    
    lazy var dataModel : FinancialKeysDataModel = {return FinancialKeysDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(FinancialKeyCollectionReusableView.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: FinancialKeyCollectionReusableView.reuseIdentifier)

//        self.title = LocalizableKey.navigationTitle.realtorCalc
        self.titleLogo()
        self.sideMenuSetup()
        self.setupLeftMenu()
        let ipmt = Formula.calculateIPMT(6.5, 12, 30, 99910)
        print("-------iPMT---------\(ipmt)")
        let fv = Formula.FVCalculation(3.00, 12, 30, 450000)
        print("-------FV---------\(fv)")
        print("----Rate------>\(Formula.rateCalculation(term: 30, paymentPerYear: 12, payment: 406.98, loanAmount: 90000))")
        let interestRate: Double = 5.0.toValue/12
        let pv = Formula.PVCalculation(rate: interestRate, nper: 276, pmt: -10756.46)
        print("-----PV------>\(pv)")
        let pmt = Formula.calculatePMT(Double(5), 12, 25, 1840000)
        print("-------investmetn pmt ---------\(pmt)")
//        self.fetch()
         // Do any additional setup after loading the view.
    }
    
    func fetch(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.contact.key.tag: Constants.tagValue.contact, APIKey.contact.key.userId: UserManager.instance.userID]
        connection.networkRequest(urlPath: nil, method: .get, param: param, encoding: .queryString, { (response: ResponseModel<ContactModel>) in
            
        }) { (error) in
            
        }

//        connection.sendRequest(urlPath: nil, method: .get, param: param, encoding: .queryString, { (json, data) in
//            let decoder = JSONDecoder()
//            do{
//                let model = try decoder.decode(ContactModel.self, from: data!)
//                print(model.success_message ?? "")
//            }catch let error{
//                print("error====>", error.localizedDescription)
//            }
//
//        }) { (error) in
//
//        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension FinancialKeysViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataModel.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataModel.newFinancialKeyList[section].items?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FinancialKeysCollectionViewCell.reuseIdentifier, for: indexPath) as! FinancialKeysCollectionViewCell
        cell.set(dataModel.newFinancialKeyList[indexPath.section].items![indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let segueId = dataModel.newFinancialKeyList[indexPath.section].items![indexPath.row].segueId{
            self.performSegue(withIdentifier: segueId, sender: self)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let column: CGFloat  = UIDevice.init().type == .iPhoneSE || UIDevice.init().type == .iPhone5S ? 3 : 4
        return CGSize.init(width: self.collectionView.frame.width/column, height: self.collectionView.frame.width/column)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: FinancialKeyCollectionReusableView.reuseIdentifier, for: indexPath) as! FinancialKeyCollectionReusableView
            view.titleLabel.text = dataModel.newFinancialKeyList[indexPath.section].title
            view.headerBackgroundView.backgroundColor = dataModel.newFinancialKeyList[indexPath.section].headerColor
            return view
        default:
            return UICollectionReusableView()
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    
        return CGSize.init(width: self.collectionView.frame.width, height: 30)
    }
}

//MARK:- SideMenu Delegate
extension FinancialKeysViewController: UISideMenuNavigationControllerDelegate, SideMenuViewControllerDelegate {
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        if let sideMenu = menu.topViewController as? SideMenuViewController{
            sideMenu.delegate = self
        }
    }
    func menuItemDidSelected(_ item: MenuItem) {
        if let type = item.type, type == .logout{
                self.showAlert(LocalizableKey.commonTitle.logout, LocalizableKey.commonMessages.logout, actionOneTitle: "Cancel", actionTwoTitle: "Logout", actionOnecompletion: {
                    
                }, actionTwocompletion: {
                    self.logout()
                })
            return
        }
        self.tabBarController?.selectedIndex = item.index
    }
}
