//
//  LoanCompareFormViewController.swift
//  RealtorCalc
//
//  Created by Bala on 25/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LoanCompareFormViewController: UITableViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showLoanForm"
    
    //Subject loan
    @IBOutlet weak var subInterestTextField: UITextField!
    @IBOutlet weak var subLoanPlanTextField: UITextField!
    @IBOutlet weak var subMtgAmountTextField: UITextField!
    @IBOutlet weak var subMtgTermTextField: UITextField!
    @IBOutlet weak var subPaymentPerTextField: UITextField!
    @IBOutlet weak var subExtraPaymentTextField: UITextField!
    @IBOutlet weak var subFinancingCostsTextField: UITextField!
    //Compare fields
    @IBOutlet weak var compInterestTextField: UITextField!
    @IBOutlet weak var compLoanPlanTextField: UITextField!
    @IBOutlet weak var compMtgAmountTextField: UITextField!
    @IBOutlet weak var compMtgTermTextField: UITextField!
    @IBOutlet weak var compPaymentPerTextField: UITextField!
    @IBOutlet weak var compExtraPaymentTextField: UITextField!
    @IBOutlet weak var compFinancingCostsTextField: UITextField!
    
    var dataModel: LoanComparisonDataModel = LoanComparisonDataModel()
    
    var loanDetails: LoanComparisonDetails?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    private var paymentTypeList: Array<DropDownInfoModel>?
    private var loanPlanList: Array<DropDownInfoModel>?
    private var selectedSubLoanPlan: DropDownInfoModel?
    private var selectedCompLoanPlan: DropDownInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    func defaultValues(){
        subInterestTextField.text = "4.175"
        subMtgAmountTextField.text = "525000"
        subMtgTermTextField.text = "25"
        subPaymentPerTextField.text = "12"
        subFinancingCostsTextField.text = "5600"
        subExtraPaymentTextField.text = "250"
        
        compInterestTextField.text = "4.875"
        compMtgAmountTextField.text = "525000"
        compMtgTermTextField.text = "25"
        compPaymentPerTextField.text = "12"
        compFinancingCostsTextField.text = "4850"
        compExtraPaymentTextField.text = "250"
    }
    func setup(){
        subLoanPlanTextField.delegate = self
        compLoanPlanTextField.delegate = self
        self.fetch()
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        CommonRequest.dropDownAPI(type: .loanPlan) { (loanList) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.loanPlanList = loanList
        }

    }
    private func selectedValues(){
        if let loanPlan = self.loanDetails?.comparsionLoan_info?.loanPlanC{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedCompLoanPlan = self.loanPlanList?[index]
                self.compLoanPlanTextField.text = selectedCompLoanPlan?.name
            }
        }
        if let loanPlanS = self.loanDetails?.subjectLoan_info?.loanPlanS{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlanS)){
                self.selectedSubLoanPlan = self.loanPlanList?[index]
                self.subLoanPlanTextField.text = selectedSubLoanPlan?.name
            }
        }
    }
    func map(){
        dataModel.subInterest = subInterestTextField.toDouble
        dataModel.subMtgTerm = subMtgTermTextField.toDouble
        dataModel.subMtgAmount = subMtgAmountTextField.toDouble
        dataModel.subPaymentPerYear = subPaymentPerTextField.toDouble
        dataModel.subExtraPayment = subExtraPaymentTextField.toDouble
        dataModel.subFinancingCost = subFinancingCostsTextField.toDouble
        //compare fields
        dataModel.comInterest = compInterestTextField.toDouble
        dataModel.comMtgTerm = compMtgTermTextField.toDouble
        dataModel.comMtgAmount = compMtgAmountTextField.toDouble
        dataModel.comPaymentPerYear = compPaymentPerTextField.toDouble
        dataModel.comExtraPayment = compExtraPaymentTextField.toDouble
        dataModel.comFinancingCost = compFinancingCostsTextField.toDouble
    }
    func isValidationSuccess()->Bool{
        if subInterestTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Subject Interest"))
            return false
        }else if subMtgAmountTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Subject Mtg Amount"))
            return false
        }else if subMtgTermTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Subject Mtg Term"))
            return false
        }else if subPaymentPerTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Subject payment per year"))
            return false
        }else if subExtraPaymentTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Subject extra payment"))
            return false
        }else if subFinancingCostsTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Subject financing costs"))
            return false
        }else if compInterestTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Comparison interest"))
            return false
        }else if compMtgAmountTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Comparison Mtg Amount"))
            return false
        }else if compMtgTermTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Comparison Mtg Term"))
            return false
        }else if compPaymentPerTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Comparison payment per year"))
            return false
        }else if compExtraPaymentTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Comparison Extra Payment"))
            return false
        }else if compFinancingCostsTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Comparison Financial Cost"))
            return false
        }
        return true
    }
}
extension LoanCompareFormViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            subLoanPlanTextField.text = model?.name
            selectedSubLoanPlan = model
        }else if viewController.dataModel.tag == 1{
            compLoanPlanTextField.text = model?.name
            selectedCompLoanPlan = model
        }
        
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == subLoanPlanTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 0, title: "Loan Plan")
            return false
        }else if textField == compLoanPlanTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 1, title: "Loan Plan")
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == subInterestTextField || textField == compInterestTextField {
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension LoanCompareFormViewController{
    func mapping(){
        let subDetails = loanDetails?.subjectLoan_info
        let comDetails = loanDetails?.comparsionLoan_info
        subInterestTextField.text = subDetails?.InterestRateS?.toNumber.toPercentage
        subMtgAmountTextField.text = subDetails?.mtgAmountS?.readableFormat
        subMtgTermTextField.text = subDetails?.mtgTermS
        subPaymentPerTextField.text = subDetails?.payPerYrS
        subFinancingCostsTextField.text = subDetails?.financeCostS?.readableFormat
        subExtraPaymentTextField.text = subDetails?.extraPaymentS?.readableFormat
        subLoanPlanTextField.text = subDetails?.loanPlanS

        compInterestTextField.text = comDetails?.InterestRateC?.toNumber.toPercentage
        compMtgAmountTextField.text = comDetails?.mtgAmountC?.readableFormat
        compMtgTermTextField.text = comDetails?.mtgTermC
        compPaymentPerTextField.text = comDetails?.payPerYrC
        compFinancingCostsTextField.text = comDetails?.financeCostC?.readableFormat
        compExtraPaymentTextField.text = comDetails?.extraPaymentC?.readableFormat
        compLoanPlanTextField.text = comDetails?.loanPlanC

    }
}
extension LoanCompareFormViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.loanComparison.key.self
        var dict: Dictionary<String, Any> = [key.interestRateS : subInterestTextField.toDouble]
        dict.updateValue(subMtgAmountTextField.toDouble, forKey: key.mtgAmountS)
        dict.updateValue(subMtgTermTextField.toDouble, forKey: key.mtgTermS)
        dict.updateValue(subPaymentPerTextField.toDouble, forKey: key.paymentPerYearS)
        dict.updateValue(subFinancingCostsTextField.toDouble, forKey: key.financingCostS)
        dict.updateValue(subExtraPaymentTextField.toDouble, forKey: key.extraPaymentS)
        dict.updateValue(compInterestTextField.toDouble, forKey: key.interestRateC)
        dict.updateValue(compMtgTermTextField.toDouble, forKey: key.mtgTermC)
        dict.updateValue(compMtgAmountTextField.toDouble, forKey: key.mtgAmountC)
        dict.updateValue(compPaymentPerTextField.toDouble, forKey: key.paymentPerYearC)
        dict.updateValue(compFinancingCostsTextField.toDouble, forKey: key.financingCostC)
        dict.updateValue(compExtraPaymentTextField.toDouble, forKey: key.extraPaymentC)
        dict.updateValue(selectedSubLoanPlan?.value ?? "", forKey: key.loanPlanS)
        dict.updateValue(selectedCompLoanPlan?.value ?? "", forKey: key.loanPlanC)
        return dict
    }
}

