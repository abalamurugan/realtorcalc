//
//  LoanCompareContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 25/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LoanCompareContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showLoanCompare"
    
    var formController: LoanCompareFormViewController!
    
    lazy var dataModel: LoanComparisonDataModel = {return LoanComparisonDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.loanComparison
        contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    
    @IBAction func calculateAction(_ sender: UIButton){
        self.postFormValues()
    }
    private func showOutput(){
        formController.map()
        formController.dataModel.calc()
        self.performSegue(withIdentifier: LoanComparisonOutputViewController.segueIdentifier, sender: self)
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == LoanCompareFormViewController.segueIdentifier{
            formController = segue.destination as? LoanCompareFormViewController
        }else if segue.identifier == LoanComparisonOutputViewController.segueIdentifier{
            let outputController = segue.destination as! LoanComparisonOutputViewController
            outputController.list = formController.dataModel.list
            outputController.summaryList = formController.dataModel.summaryList
            outputController.contactModel = self.contactModel
        }
    }

}
extension LoanCompareContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        contactNameTextField.text = item.fullName
        self.getLoanComparison()
    }
}
extension LoanCompareContainerViewController{
    var paramViewLoan: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.loancomparsionView, contactId: self.contactModel!.contactId!)
        
    }
    private func getLoanComparison(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewLoan, encoding: .queryString, { (response: ResponseModel<LoanComparisonModel>) in
            self.formController.loanDetails = response.item?.response
        }) { (error) in
            self.showError(error)
        }
    }
}
extension LoanCompareContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID]
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.reportNames.loanComparison, forKey: APIKey.common.key.action)
        dict.updateValue(Constants.tagValue.loancompasionAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        formController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
