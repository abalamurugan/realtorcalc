//
//  RepurchaseFormTwoViewController.swift
//  RealtorCalc
//
//  Created by Bala on 19/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RepurchaseFormTwoViewController: UITableViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showFormTwo"
    
    
    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var firstMtgAmountTextField : UITextField!
    @IBOutlet weak var secondMtgAmountTextField : UITextField!
    @IBOutlet weak var loanFeesChargesTextField : UITextField!
    @IBOutlet weak var prepaidChargesTextField : UITextField!
    @IBOutlet weak var titleRecordFeesTextField : UITextField!
    @IBOutlet weak var yearlyApprecRateTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestRateTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var monthlyTaxesTextField : UITextField!
    @IBOutlet weak var monthlyInsuranceTextField : UITextField!
    @IBOutlet weak var monthlyMtgInsuracneTextField : UITextField!
    @IBOutlet weak var monthlyHoaTextField : UITextField!
    @IBOutlet weak var monthlySecondMtgTextField : UITextField!
    
    var newDetails: TransitionNewModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    var paymentTypeList: Array<DropDownInfoModel>?
    var loanPlanList: Array<DropDownInfoModel>?
    private var selectedPaymentType: DropDownInfoModel?
    private var selectedLoanPlan: DropDownInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        purchasePriceTextField.text = "485000"
        firstMtgAmountTextField.text = "400000"
        secondMtgAmountTextField.text = "0"
        loanFeesChargesTextField.text = "8500"
        prepaidChargesTextField.text = "4200"
        titleRecordFeesTextField.text = "3900"
        yearlyApprecRateTextField.text = "7.5"
        interestRateTextField.text = "4.5"
        loanTermTextField.text = "30"
        paymentPerYearTextField.text = "12"
        monthlyTaxesTextField.text = "375"
        monthlyInsuranceTextField.text = "220"
        monthlyMtgInsuracneTextField.text = "0"
    }
    private func configUI(){
        self.loanPlanTextField.delegate = self
        self.paymentTypeTextField.delegate = self
    }
    private func selectedValues(){
        if let loanPlan = self.newDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.loanPlanTextField.text = selectedLoanPlan?.name
            }
        }
        if let paymentType = self.newDetails?.paymentType{
            if let index = self.paymentTypeList?.index(of: DropDownInfoModel.init(value: paymentType)){
                self.selectedPaymentType = self.paymentTypeList?[index]
                self.paymentTypeTextField.text = selectedPaymentType?.name
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RepurchaseFormTwoViewController{
    var cashClosing: Double{
        var cashClosing: Double = purchasePriceTextField.toDouble
        cashClosing -= firstMtgAmountTextField.toDouble
        cashClosing -= secondMtgAmountTextField.toDouble
        cashClosing += loanFeesChargesTextField.toDouble
        cashClosing += prepaidChargesTextField.toDouble
        cashClosing += titleRecordFeesTextField.toDouble
        return cashClosing.removeMinus
    }
    var pmtNewHousing: Double{
        let pmt = Formula.calculatePMT(interestRateTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, firstMtgAmountTextField.toDouble)
        if pmt < 0{
            return pmt
        }
        return pmt
    }
    var totalNewHouse: Double{
        var total: Double = pmtNewHousing
        total += monthlyTaxesTextField.toDouble
        total += monthlyInsuranceTextField.toDouble
        total += monthlyMtgInsuracneTextField.toDouble
        total += monthlyHoaTextField.toDouble
        total += monthlySecondMtgTextField.toDouble
        return total
    }
}
extension RepurchaseFormTwoViewController{
    private func mapping(){
        purchasePriceTextField.text = newDetails?.purchasePrice?.readableFormat
        firstMtgAmountTextField.text = newDetails?.stMtgAmount?.readableFormat
        secondMtgAmountTextField.text = newDetails?.ndMtgAmount?.readableFormat
        loanFeesChargesTextField.text = newDetails?.loanFeesCharges?.readableFormat
        prepaidChargesTextField.text = newDetails?.prepaidCharges?.readableFormat
        titleRecordFeesTextField.text = newDetails?.titleRecordMisc?.readableFormat
        yearlyApprecRateTextField.text = newDetails?.yearlyApprec?.toNumber.toPercentage
        interestRateTextField.text = newDetails?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = newDetails?.loanTerm
        paymentPerYearTextField.text = newDetails?.payPerYear
        monthlyTaxesTextField.text = newDetails?.monthlyTaxes?.readableFormat
        monthlyInsuranceTextField.text = newDetails?.monthlyIns?.readableFormat
        monthlyMtgInsuracneTextField.text = newDetails?.monthlyMtgIns?.readableFormat
        secondMtgAmountTextField.text = newDetails?.ndMtgAmount?.readableFormat
        monthlySecondMtgTextField.text = newDetails?.monthlyNdMtgPmt?.readableFormat
        monthlyMtgInsuracneTextField.text = newDetails?.monthlyMtgIns?.readableFormat
        monthlyHoaTextField.text = newDetails?.monthlyHoaDue?.readableFormat
    }
}
extension RepurchaseFormTwoViewController{
    private var updateNewValues: TransitionNewModel{
        let model = TransitionNewModel()
        model.purchasePrice = purchasePriceTextField.text
        model.stMtgAmount = firstMtgAmountTextField.text
        model.ndMtgAmount = secondMtgAmountTextField.text
        model.loanFeesCharges = loanFeesChargesTextField.text
        model.prepaidCharges = prepaidChargesTextField.text
        model.titleRecordMisc = titleRecordFeesTextField.text
        model.yearlyApprec = yearlyApprecRateTextField.text
        model.interestRate  = interestRateTextField.text
        model.loanTerm = loanTermTextField.text
        model.payPerYear = paymentPerYearTextField.text
        model.monthlyTaxes = monthlyTaxesTextField.text
        model.monthlyIns = monthlyInsuranceTextField.text
        model.monthlyMtgIns = monthlyMtgInsuracneTextField.text
        model.paymentType = self.selectedPaymentType?.value
        model.loanPlan = self.selectedLoanPlan?.value
        return model
    }
    var transitionNewParam: Dictionary<String, Any>?{
        return updateNewValues.toJson
    }
    var updateNewParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.transitionEquity.key.self
        dict.updateValue(purchasePriceTextField.toDouble, forKey: key.purchasePrice)
        dict.updateValue(firstMtgAmountTextField.toDouble, forKey: key.stMtgAmountN)
        dict.updateValue(secondMtgAmountTextField.toDouble, forKey: key.ndMtgAmountN)
        dict.updateValue(loanFeesChargesTextField.toDouble, forKey: key.loanFeesCharges)
        dict.updateValue(prepaidChargesTextField.toDouble, forKey: key.prepaidsCharge)
        dict.updateValue(titleRecordFeesTextField.toDouble, forKey: key.titleRecordMiscN)
        dict.updateValue(yearlyApprecRateTextField.toDouble, forKey: key.yearApprecRateN)
        dict.updateValue(selectedLoanPlan?.value ?? "", forKey: key.loanPlanN)
        dict.updateValue(selectedPaymentType?.value ?? "", forKey: key.payTypeN)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.paymentPerYearN)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRateN)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTermN)
        dict.updateValue(monthlyTaxesTextField.toDouble, forKey: key.monthlyTaxesN)
        dict.updateValue(monthlyInsuranceTextField.toDouble, forKey: key.monthlyInsuranceN)
        dict.updateValue(monthlyMtgInsuracneTextField.toDouble, forKey: key.monthlyMtgInsuranceN)
        dict.updateValue(monthlyHoaTextField.toDouble, forKey: key.monthlyHoaFeesDuesN)
        dict.updateValue(monthlySecondMtgTextField.toDouble, forKey: key.monthlyNDMtgInsuranceN)
        return dict
    }
}

extension RepurchaseFormTwoViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanPlanTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 0, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }else if textField == paymentTypeTextField{
            self.showBottomPicker(delegate: self, items: paymentTypeList ?? [], tag: 1, title: LocalizableKey.financingTitle.paymentType)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestRateTextField || textField == yearlyApprecRateTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension RepurchaseFormTwoViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            loanPlanTextField.text = model?.name
            selectedLoanPlan = model
        }else if viewController.dataModel.tag == 1{
            paymentTypeTextField.text = model?.name
            selectedPaymentType = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
