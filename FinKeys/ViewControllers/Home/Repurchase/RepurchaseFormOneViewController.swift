//
//  RepurchaseFormOneViewController.swift
//  RealtorCalc
//
//  Created by Bala on 19/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RepurchaseFormOneViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFormOne"

    @IBOutlet weak var salePriceTextField : UITextField!
    @IBOutlet weak var firstMtgBalanceTextField : UITextField!
    @IBOutlet weak var secondMtgBalanceTextField : UITextField!
    @IBOutlet weak var titleRecordTextField : UITextField!
    @IBOutlet weak var ownerCarrybackTextField : UITextField!
    @IBOutlet weak var commisionRateTextField : UITextField!
    @IBOutlet weak var savingInterestRateTextField : UITextField!
    @IBOutlet weak var yearlyAppreciationRateTextField : UITextField!
    @IBOutlet weak var homeOwnerTaxTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestRateTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var monthlyTaxTextField : UITextField!
    @IBOutlet weak var monthlyInsuranceTextField : UITextField!
    @IBOutlet weak var monthlyMtgInsuracneTextField : UITextField!
    @IBOutlet weak var monthlyHoaTextField : UITextField!
    @IBOutlet weak var monthlySecondMtgTextField : UITextField!
    
    var currentDetails: TransitionCurrentModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    var paymentTypeList: Array<DropDownInfoModel>?
    var loanPlanList: Array<DropDownInfoModel>?
    private var selectedPaymentType: DropDownInfoModel?
    private var selectedLoanPlan: DropDownInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func selectedValues(){
        if let loanPlan = self.currentDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.loanPlanTextField.text = selectedLoanPlan?.name
            }
        }
        if let paymentType = self.currentDetails?.paymentType{
            if let index = self.paymentTypeList?.index(of: DropDownInfoModel.init(value: paymentType)){
                self.selectedPaymentType = self.paymentTypeList?[index]
                self.paymentTypeTextField.text = selectedPaymentType?.name
            }
        }
    }
    private func defaultValues(){
        salePriceTextField.text = "235000"
        firstMtgBalanceTextField.text = "130000"
        titleRecordTextField.text = "3700"
        ownerCarrybackTextField.text = "500"
        commisionRateTextField.text = "6"
        savingInterestRateTextField.text = "1.5"
        yearlyAppreciationRateTextField.text = "7.5"
        homeOwnerTaxTextField.text = "32"
        interestRateTextField.text = "4.25"
        loanTermTextField.text = "25"
        paymentPerYearTextField.text = "12"
        monthlyTaxTextField.text = "220"
        monthlyInsuranceTextField.text = "175"
        monthlyHoaTextField.text = "125"
        

    }
    private func configUI(){
        self.loanPlanTextField.delegate = self
        self.paymentTypeTextField.delegate = self
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RepurchaseFormOneViewController {
    var realEstateCommision: Double{
        let commisionRate: Double = commisionRateTextField.toDouble/100
        return salePriceTextField.toDouble * commisionRate
    }
    var cashEquity: Double{
        var cash: Double = salePriceTextField.toDouble
        cash -= firstMtgBalanceTextField.toDouble
        cash -= secondMtgBalanceTextField.toDouble
        cash -= realEstateCommision
        cash -= titleRecordTextField.toDouble
        cash -= ownerCarrybackTextField.toDouble
        return cash
    }
    var pmtCurrentHousing: Double{
        let pmt = Formula.calculatePMT(interestRateTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, firstMtgBalanceTextField.toDouble)
        if pmt < 0{
            return pmt.magnitude
        }
        return pmt
    }
    var totalHousingPayment: Double{
        var total: Double = pmtCurrentHousing
        total += monthlyTaxTextField.toDouble
        total += monthlyInsuranceTextField.toDouble
        total += monthlyMtgInsuracneTextField.toDouble
        total += monthlyHoaTextField.toDouble
        total += monthlySecondMtgTextField.toDouble
        return total
    }
}
extension RepurchaseFormOneViewController{
    private func mapping(){
        salePriceTextField.text = currentDetails?.salesPrice?.readableFormat
        firstMtgBalanceTextField.text = currentDetails?.stMtgBalance?.readableFormat
        titleRecordTextField.text = currentDetails?.titleRecordMisc?.readableFormat
        ownerCarrybackTextField.text = currentDetails?.ownerCarryBack?.readableFormat
        commisionRateTextField.text = currentDetails?.commisionRate?.toNumber.toPercentage
        savingInterestRateTextField.text = currentDetails?.savingInterest?.toNumber.toPercentage
        yearlyAppreciationRateTextField.text = currentDetails?.yearlyApprec?.toNumber.toPercentage
        homeOwnerTaxTextField.text = currentDetails?.homeOwnerTax?.toNumber.toPercentage
        interestRateTextField.text = currentDetails?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = currentDetails?.loanTerm
        paymentPerYearTextField.text = currentDetails?.payPerYear
        monthlyTaxTextField.text = currentDetails?.monthlyTaxes?.readableFormat
        monthlyInsuranceTextField.text = currentDetails?.monthlyIns?.readableFormat
        monthlyHoaTextField.text = currentDetails?.monthlyHoaDue?.readableFormat
        secondMtgBalanceTextField.text = currentDetails?.ndMtgBalance?.readableFormat
        monthlySecondMtgTextField.text = currentDetails?.monthlyNdMtgPmt?.readableFormat
        monthlyMtgInsuracneTextField.text = currentDetails?.monthlyMtgIns?.readableFormat
        
    }
}
extension RepurchaseFormOneViewController{
    private var updateCurrentValues: TransitionCurrentModel{
        let model = TransitionCurrentModel()
        model.salesPrice = salePriceTextField.text
        model.stMtgBalance = firstMtgBalanceTextField.text
        model.titleRecordMisc = titleRecordTextField.text
        model.ownerCarryBack = ownerCarrybackTextField.text
        model.commisionRate = commisionRateTextField.text
        model.savingInterest = savingInterestRateTextField.text
        model.yearlyApprec = yearlyAppreciationRateTextField.text
        model.homeOwnerTax  = homeOwnerTaxTextField.text
        model.interestRate = interestRateTextField.text
        model.loanTerm = loanTermTextField.text
        model.payPerYear = paymentPerYearTextField.text
        model.monthlyTaxes = monthlyTaxTextField.text
        model.monthlyIns = monthlyInsuranceTextField.text
        model.monthlyHoaDue = monthlyHoaTextField.text
        return model
    }
    var updateCurrentParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.transitionEquity.key.self
        dict.updateValue(salePriceTextField.toDouble, forKey: key.salesPrice)
        dict.updateValue(firstMtgBalanceTextField.toDouble, forKey: key.stMtgAmountC)
        dict.updateValue(secondMtgBalanceTextField.toDouble, forKey: key.ndMtgAmountC)
        dict.updateValue(realEstateCommision, forKey: key.realEstateComm)
        dict.updateValue(titleRecordTextField.toDouble, forKey: key.titleRecordMiscC)
        dict.updateValue(ownerCarrybackTextField.toDouble, forKey: key.ownerCarryBack)
        dict.updateValue(commisionRateTextField.toDouble, forKey: key.commissionRate)
        dict.updateValue(savingInterestRateTextField.toDouble, forKey: key.savInterestRate)
        dict.updateValue(yearlyAppreciationRateTextField.toDouble, forKey: key.yearApprecRateC)
        dict.updateValue(homeOwnerTaxTextField.toDouble, forKey: key.homeownerTaxBrac)
        dict.updateValue(selectedLoanPlan?.value ?? "", forKey: key.loanPlanC)
        dict.updateValue(selectedPaymentType?.value ?? "", forKey: key.payTypeC)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.paymentPerYearC)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRateC)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTermC)
        dict.updateValue(monthlyTaxTextField.toDouble, forKey: key.monthlyTaxesC)
        dict.updateValue(monthlyInsuranceTextField.toDouble, forKey: key.monthlyInsuranceC)
        dict.updateValue(monthlyMtgInsuracneTextField.toDouble, forKey: key.monthlyMtgInsuranceC)
        dict.updateValue(monthlyHoaTextField.toDouble, forKey: key.monthlyHoaFeesDuesC)
        dict.updateValue(monthlySecondMtgTextField.toDouble, forKey: key.monthlyNDMtgInsuranceC)
        dict.updateValue(cashEquity, forKey: key.cashEquityN)
        return dict
    }
    var transitionCurrentParam: Dictionary<String, Any>?{
        return updateCurrentValues.toJson
    }
}
extension RepurchaseFormOneViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanPlanTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 0, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }else if textField == paymentTypeTextField{
            self.showBottomPicker(delegate: self, items: paymentTypeList ?? [], tag: 1, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestRateTextField || textField == commisionRateTextField || textField == savingInterestRateTextField || textField == yearlyAppreciationRateTextField || textField == homeOwnerTaxTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension RepurchaseFormOneViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            loanPlanTextField.text = model?.name
            selectedLoanPlan = model
        }else if viewController.dataModel.tag == 1{
            paymentTypeTextField.text = model?.name
            selectedPaymentType = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
