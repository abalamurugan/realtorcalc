//
//  RepurchaseContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 19/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RepurchaseContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showRepurchase"
    
    @IBOutlet weak var calculateButton : UIButton!
    
    @IBOutlet weak var segmentedControler : UISegmentedControl!
    @IBOutlet weak var formOneContainer : UIView!
    @IBOutlet weak var formTwoContainer : UIView!
    
    var firstFormController : RepurchaseFormOneViewController!
    var secondFormController : RepurchaseFormTwoViewController!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        self.fetch()
        self.title = LocalizableKey.navigationTitle.repurchase
        formTwoContainer.isHidden = true
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        CommonRequest.dropDownAPI(type: .paymentType) { (paymentList) in
            self.firstFormController.paymentTypeList = paymentList
            self.secondFormController.paymentTypeList = paymentList
            CommonRequest.dropDownAPI(type: .loanPlan) { (loanList) in
                LoadingView.instance.hideActivityIndicator(uiView: self.view)
                self.firstFormController.loanPlanList = loanList
                self.secondFormController.loanPlanList = loanList
                
            }
        }
        
    }

    @IBAction func segmentAction(_ sender : UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            formOneContainer.isHidden = false
            formTwoContainer.isHidden = true
        }else{
            formOneContainer.isHidden = true
            formTwoContainer.isHidden = false
        }
    }

    @IBAction func calculateAction(_ sender: UIButton){
        self.postFormValues()
    }
    private func showOutput(){
        let equityController = EquityViewController.showEquity()
        equityController.dataModel.list = self.outputList
        equityController.dataModel.title = LocalizableKey.navigationTitle.repurchase
        equityController.contactModel = self.contactModel
        equityController.dataModel.type = .transitionEquity
        equityController.isHideTotalView = true
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: equityController)
        self.present(navigationController, animated: true, completion: nil)
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == RepurchaseFormOneViewController.segueIdentifier{
            firstFormController = segue.destination as? RepurchaseFormOneViewController
        }else if segue.identifier == RepurchaseFormTwoViewController.segueIdentifier{
            secondFormController = segue.destination as? RepurchaseFormTwoViewController
        }
    }


}
extension RepurchaseContainerViewController{
    var cashToSaving: Double{
        return (secondFormController.cashClosing - firstFormController.cashEquity).removeMinus
    }
    var differnceMonthlyPayment: Double{
        return secondFormController.totalNewHouse - firstFormController.totalHousingPayment
    }
    var newTaxDeduction: Double{
        let interest = firstFormController.homeOwnerTaxTextField.toDouble.toValue
        let newHomeInterest = secondFormController.interestRateTextField.toDouble.toValue
        let formOnenewHomeInterest = firstFormController.interestRateTextField.toDouble.toValue

        let firstMtg = secondFormController.firstMtgAmountTextField.toDouble
        let formOnefirstMtg = firstFormController.firstMtgBalanceTextField.toDouble

        let paymentPerYear = secondFormController.paymentPerYearTextField.toDouble
        let formOnePaymentPerYear = firstFormController.paymentPerYearTextField.toDouble

        let exp1 = (secondFormController.monthlyTaxesTextField.toDouble * interest) + (interest * firstMtg * newHomeInterest / paymentPerYear)
        let exp2 = (firstFormController.monthlyTaxTextField.toDouble * interest) + (interest * formOnefirstMtg * formOnenewHomeInterest / formOnePaymentPerYear)
        return exp1 - exp2
    }
    
    var savingInvestedGain: Double{
        var gain: Double = 0
        let interest = firstFormController.savingInterestRateTextField.toDouble.toValue
        let paymentPerYear = secondFormController.paymentPerYearTextField.toDouble
        gain += cashToSaving
        gain = gain * interest/paymentPerYear
        return -gain
    }
    var netCashFlow: Double{
        var total: Double = 0
        total += differnceMonthlyPayment
        total -= newTaxDeduction
        total -= savingInvestedGain
        return total
    }
    var increaseDecreaseAppre: Double{
        return (netCashFlow - appreciationGainPerMonth)
    }
    var appreciationGainPerMonth: Double{
        let purchasePrice = secondFormController.purchasePriceTextField.toDouble
        let yearlyInterest = secondFormController.yearlyApprecRateTextField.toDouble
        let formOnePurchasePrice = firstFormController.salePriceTextField.toDouble
        let formOneYearlyInterest = firstFormController.yearlyAppreciationRateTextField.toDouble
        let exp1 = purchasePrice * yearlyInterest/secondFormController.paymentPerYearTextField.toDouble
        let exp2 = formOnePurchasePrice * formOneYearlyInterest/secondFormController.paymentPerYearTextField.toDouble
        return exp1.toValue - exp2.toValue
    }
    var outputList : Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        list.append(OutputDetailsModel.init(key: "Current Housing Payment", value: firstFormController.totalHousingPayment))
        list.append(OutputDetailsModel.init(key: "New Housing Payment", value: secondFormController.totalNewHouse))
        list.append(OutputDetailsModel.init(key: "Difference in Monthly Payment", value: differnceMonthlyPayment))
        list.append(OutputDetailsModel.init(key: "  - New Tax Deductions Gain/(Loss)", value: newTaxDeduction))
        list.append(OutputDetailsModel.init(key: "  - Savings Invested Gain/(Loss)", value: savingInvestedGain))
        list.append(OutputDetailsModel.init(key: "Net Cash Flow Increase/(Decrease)", value: netCashFlow))
        list.append(OutputDetailsModel.init(key: "  - Appreciation per Month Gain/(Loss)", value: appreciationGainPerMonth))
        list.append(OutputDetailsModel.init(key: "Increase/(Decrease) Including Appreciation", value: increaseDecreaseAppre))
        return list
        
    }
}
extension RepurchaseContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getTransition()
    }
}
extension RepurchaseContainerViewController{
    var paramViewTransition: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.transitionEquityView, contactId: self.contactModel!.contactId!)
        
    }
    private func getTransition(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewTransition, encoding: .queryString, { (response: ResponseModel<TransitionModel>) in
            self.firstFormController.currentDetails = response.item?.response?.current_home_net
            self.secondFormController.newDetails = response.item?.response?.new_home_net
        }) { (error) in
            self.showError(error)
        }
    }
}
extension RepurchaseContainerViewController{
    var updateParam: Dictionary<String, Any>{
        var param: Dictionary<String, Any> = [:]
        firstFormController.updateCurrentParam.forEach({ (key, value) in
            param.updateValue(value, forKey: key)
        })
        secondFormController.updateNewParam.forEach({ (key, value) in
            param.updateValue(value, forKey: key)
        })
        param.updateValue(Constants.tagValue.transitionEquityAdd, forKey: APIKey.common.key.tag)
        param.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        param.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        param.updateValue(Constants.reportNames.transitionEquity, forKey: APIKey.common.key.action)
        param.updateValue(Date().toString(format: Constants.dateFormat)!, forKey: APIKey.common.key.createdDate)
        return param
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: updateParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    
}
