//
//  ARMFixedFormViewController.swift
//  RealtorCalc
//
//  Created by Bala on 14/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ARMFixedFormViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showARMForm"
    
    @IBOutlet weak var mortageAmountTextField : UITextField!
    @IBOutlet weak var interestTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var holdingPeriodTextField : UITextField!
    
    @IBOutlet weak var periodicInterestTextField : UITextField!
    @IBOutlet weak var adjustableLoanTermTextField : UITextField!
    @IBOutlet weak var adjPaymentPerYearTextField : UITextField!
    @IBOutlet weak var lifeTimeTextField : UITextField!
    @IBOutlet weak var adjHoldingPeriodTextField : UITextField!
    @IBOutlet weak var adjPeriodicInterestTextField : UITextField!
    
    lazy var dataModel : ARMFixedDataModel = {return ARMFixedDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.arm
        self.contactPickerAction(UIButton())
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        mortageAmountTextField.text = "485000"
        interestTextField.text = "4.125"
        loanTermTextField.text = "25"
        paymentPerYearTextField.text = "12"
        holdingPeriodTextField.text = "48"
        periodicInterestTextField.text = "3.5"
        adjustableLoanTermTextField.text = "25"
        adjPaymentPerYearTextField.text = "12"
        adjPeriodicInterestTextField.text = "1.0"
        lifeTimeTextField.text = "6.5"
        adjHoldingPeriodTextField.text = "48"
    }
    @IBAction func calculateAction(_ sender : UIButton){
        self.postFormValues()
    }
    private func showOutput(){
        self.dataModel.equityBuildUpList = []
        self.dataModel.armFixedOutputList = []
        if self.isValidationSuccess(){
            print("===Fixed PMT====\(calculateFixedPMT())")
            print("===Adjustable PMT====\(calculateAdjustablePMT())")
            self.mapToModel()
            print("======Total count=======\(self.dataModel.totalInterestCount)")
            self.dataModel.calculateNewEqulityBuildUp()
            //            self.dataModel.calculateEqulityBuildUp()
            self.performSegue(withIdentifier: ARMFixedOutputViewController.segueIdentifier, sender: self)
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
    private func mapToModel(){
        self.dataModel.adjustInterestRate = periodicInterestTextField.text?.toDouble
        self.dataModel.fixedInterestRate = interestTextField.text?.toDouble
        self.dataModel.lifeTimeInterestRate = lifeTimeTextField.text?.toDouble
        self.dataModel.periodicInterestGapRate = adjPeriodicInterestTextField.text?.toDouble
        self.dataModel.loanMtgAmount = mortageAmountTextField.text?.toDouble
        self.dataModel.loanTerm = loanTermTextField.text?.toDouble
        self.dataModel.paymentPerYear = paymentPerYearTextField.text?.toDouble
        self.dataModel.adjPaymentPerYear = adjPaymentPerYearTextField.text?.toDouble
        self.dataModel.adjLoanTerm = adjustableLoanTermTextField.text?.toDouble
        self.dataModel.adjPrincipalIntPmt = calculateAdjustablePMT()
        self.dataModel.fixedPmt = calculateFixedPMT()
        self.dataModel.savingsPerMonth = savingPerMonth
    }
    private func calculateFixedPMT()->Double{
        let pmt = Formula.calculatePMT(interestTextField.text!.toDouble, paymentPerYearTextField.text!.toDouble, loanTermTextField.text!.toDouble, mortageAmountTextField.text!.toDouble)
        return pmt
    }
    private func calculateAdjustablePMT()->Double{
        let pmt = Formula.calculatePMT(periodicInterestTextField.text!.toDouble, adjPaymentPerYearTextField.text!.toDouble, adjustableLoanTermTextField.text!.toDouble, mortageAmountTextField.text!.toDouble)
        return pmt
    }
    var savingPerMonth: Double{
        return calculateFixedPMT() - calculateAdjustablePMT()
    }
    private func isValidationSuccess() -> Bool{
        if mortageAmountTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Mortgage Amount"))
            return false
        }else if interestTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Interest rate"))
            return false
        }else if loanTermTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Loan term"))
            return false
        }else if paymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Payment per year"))
            return false
        }else if holdingPeriodTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Holding period"))
            return false
        }else if periodicInterestTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Periodic Interest"))
            return false
        }else if adjustableLoanTermTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Adjustable loan term"))
            return false
        }else if adjPaymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Adjustable payment per year"))
            return false
        }else if adjPeriodicInterestTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Adjustable periodic interest"))
            return false
        }else if lifeTimeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Lifetime rate gap"))
            return false
        }else if adjHoldingPeriodTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Adjustable Holding"))
            return false
        }
        return true
        
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == ARMFixedOutputViewController.segueIdentifier{
            let armOutputController = segue.destination as! ARMFixedOutputViewController
            armOutputController.dataModel.armFixedOutputList = dataModel.armFixedOutputList
            armOutputController.dataModel.breakEvenPoint = dataModel.breakEvenDuration
            armOutputController.contactModel = self.contactModel
        }
    }
}

extension ARMFixedFormViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getARMFixed()
    }
}
extension ARMFixedFormViewController{
    var paramViewFinancing: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.armvsfixedView, contactId: self.contactModel!.contactId!)
        
    }
    private func getARMFixed(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewFinancing, encoding: .queryString, { (response: ResponseModel<ARMFixedModel>) in
            self.dataModel.armDetails = response.item?.response
            self.mapping()
        }) { (error) in
            self.showError(error)
        }
    }
}
extension ARMFixedFormViewController{
    private func mapping(){
        let details = dataModel.armDetails
        mortageAmountTextField.text = details?.mortgageAmt?.readableFormat
        interestTextField.text = details?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = details?.loanTermFix
        paymentPerYearTextField.text = details?.payPerYrFix
        holdingPeriodTextField.text = details?.holdingPeriodFix
        periodicInterestTextField.text = details?.periodicRate?.toNumber.toPercentage
        adjustableLoanTermTextField.text = details?.loanTermAdj
        adjPaymentPerYearTextField.text = details?.payPerYrAdj
        adjPeriodicInterestTextField.text = details?.periodicIntCap?.toNumber.toPercentage
        lifeTimeTextField.text = details?.lifeTimeRateCap?.toNumber.toPercentage
        adjHoldingPeriodTextField.text = details?.holdingPeriodAdj
    }
}
extension ARMFixedFormViewController{
    var postParam: Dictionary<String, Any>{
        let key = APIKey.armFixed.key.self
        var dict: Dictionary<String, Any> = [key.mortgageAmtFix:mortageAmountTextField.toDouble]
        dict.updateValue(interestTextField.toDouble, forKey: key.interestRate)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTermFix)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYearFix)
        dict.updateValue(holdingPeriodTextField.toDouble, forKey: key.holdingPeriodFix)
        dict.updateValue(periodicInterestTextField.toDouble, forKey: key.periodicRate)
        dict.updateValue(adjustableLoanTermTextField.toDouble, forKey: key.loanTermAdj)
        dict.updateValue(adjPaymentPerYearTextField.toDouble, forKey: key.payPerYearAdj)
        dict.updateValue(adjPeriodicInterestTextField.toDouble, forKey: key.periodicIntCap)
        dict.updateValue(lifeTimeTextField.toDouble, forKey: key.lifeRateCap)
        dict.updateValue(adjHoldingPeriodTextField.toDouble, forKey: key.holdingPeriodAdj)
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(Constants.reportNames.armFixed, forKey: APIKey.common.key.action)
        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        dict.updateValue(Constants.tagValue.armvsfixedAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(calculateFixedPMT(), forKey: key.principalIntPmtFix)
        dict.updateValue(calculateAdjustablePMT(), forKey: key.principalIntPmtAdj)
        dict.updateValue(savingPerMonth, forKey: key.savePerMonth)
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
extension ARMFixedFormViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestTextField || textField == lifeTimeTextField || textField == periodicInterestTextField || textField == adjPeriodicInterestTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
}
