//
//  ARMFixedOutputViewController.swift
//  RealtorCalc
//
//  Created by Bala on 14/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ARMFixedOutputViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showARMOutput"
    
    lazy var dataModel : ARMFixedOutputDataModel = {return ARMFixedOutputDataModel()}()
    
    @IBOutlet weak var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        // Do any additional setup after loading the view.
    }
    
    private func configUI(){
        self.configurePrint()
    }
    private func configurePrint(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID, APIKey.common.key.tag : Constants.tagValue.printOption]
        connection.networkRequest(urlPath: nil, method: .get, param: param,encoding: .queryString, { (response: ResponseModel<PrintPaymentModel>) in
            let model = response.item?.response
            let isRightButton = (model?.isPrintAvailable ?? "NO").caseInsensitiveCompare("YES") == .orderedSame ? true : false
            if isRightButton{
                self.setupRightBarButton(buttonTitle: LocalizableKey.reports.print)
            }
        }) { (error) in
            
        }
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.getPDf()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ARMFixedOutputViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == dataModel.count - 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: ARMBreakEvenPointTableViewCell.reuseIdentifier, for: indexPath) as! ARMBreakEvenPointTableViewCell
            cell.breakEvenLabel.text = dataModel.breakEvenPoint
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: ARMOutputTableViewCell.reuseIdentifier, for: indexPath) as! ARMOutputTableViewCell
        cell.set(dataModel[indexPath])
        cell.periodPrinTitleLabel.text = indexPath.row == 0 ? "1st Period Savings Per Month:" : "\((indexPath.row + 1).numberReadable) Period Principal & Interest Pmt."
        cell.fixedRateTitleLabel.text = indexPath.row == 0 ? "" : "Difference from Fixed Rate PMT"
        cell.fixedRateDiffLabel.text = indexPath.row == 0 ? "" : dataModel[indexPath].fixedPMTDifference?.readableFormat
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == dataModel.count - 1) ? 70 : 152
    }
}
extension ARMFixedOutputViewController{
    var pdfParam: Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict.updateValue(Constants.pdfDetect, forKey: APIKey.pdf.key.detect)
        dict.updateValue(contactModel!.contactId!, forKey: APIKey.pdf.key.contactId)
        dict.updateValue(Constants.reportNames.armFixed, forKey: APIKey.pdf.key.name)
        dict.updateValue(Constants.tagValue.reportPdf, forKey: APIKey.pdf.key.tag)
        return dict
    }
    private func getPDf(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: pdfParam, encoding: .queryString, { (response: ResponseModel<PDFModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let pdfURL = response.item?.pdfURL, pdfURL != ""{
                UIApplication.shared.open(URL(string : pdfURL)!, options: [:], completionHandler: { (status) in
                })
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
