//
//  ContactDetailsViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 05/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class ContactDetailsViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showContactDetails"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var faxNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!


    var contactDetails: ContactInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapping()
        // Do any additional setup after loading the view.
    }
    
    private func mapping(){
        nameLabel.text = contactDetails?.fullName
        emailLabel.text = contactDetails?.emailid
        faxNumberLabel.text = contactDetails?.fax
        phoneNumberLabel.text = contactDetails?.phone
        mobileNumberLabel.text = contactDetails?.cellNo
        addressLabel.text = contactDetails?.address
        stateLabel.text = contactDetails?.state
        cityLabel.text = contactDetails?.city
        companyNameLabel.text = contactDetails?.companyname
        titleLabel.text = contactDetails?.title
        userImageView.sd_setImage(with: contactDetails?.imageURL, placeholderImage: UIImage.init(named: "placeholder"))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Button Actions
extension ContactDetailsViewController{
    @IBAction func dismissAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
