//
//  ContactListViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 02/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SideMenu

protocol ContactListViewControllerDelegate {
    func didPickedContact(viewController: UIViewController, item:ContactInfoModel)
}

class ContactListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    lazy var dataModel: ContactDataModel = {return ContactDataModel()}()
    
    class func showContactPicker()->ContactListViewController{
       return Storyboard.contact.storyboard.instantiateViewController(withIdentifier: "ContactListViewController") as! ContactListViewController
        
    }
    
    var delegate: ContactListViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapping()
        self.configureUI()
//        self.showLoading()
        self.fetch()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.searchFirstResponder()
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        self.dataModel.fetchContacts(success: { (contactModel) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.tableView.isHidden = self.dataModel.count == 0 ? true : false //Show message to create contact
            self.tableView.reloadData()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    private func configureUI(){
        if dataModel.isPicker{
            self.setupLeftBackButton()
        }else{
            self.sideMenuSetup()
            self.setupLeftMenu()
        }
        self.setupRightBarButton(buttonTitle: "Add")
    }
    private func mapping(){
        self.title = LocalizableKey.navigationTitle.contact
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == ContactDetailsViewController.segueIdentifier{
            let detailController = segue.destination as! ContactDetailsViewController
            detailController.contactDetails = sender as? ContactInfoModel
        }else if segue.identifier == ContactFormViewController.segueIdentifier{
            let formController = segue.destination as! ContactFormViewController
            formController.didContactCreatedCallback { (bool) in
                self.fetch()
            }
        }
    }
  

}
extension ContactListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactListTableViewCell.reuseIdentifier, for: indexPath) as! ContactListTableViewCell
        cell.set(dataModel[indexPath])
        cell.moreButton.tag = indexPath.row
        cell.moreButton.addTarget(self, action: #selector(moreAction(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataModel.isPicker{
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    print("Contact ID====>\(self.dataModel[indexPath].contactId ?? "")")
                    self.delegate?.didPickedContact(viewController: self, item: self.dataModel[indexPath])
                }
            }
        }
    }
}
extension ContactListViewController{
    @objc func moreAction(_ sender: UIButton){
        let model = dataModel[IndexPath.init(row: sender.tag, section: 0)]
        self.performSegue(withIdentifier: ContactDetailsViewController.segueIdentifier, sender: model)
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: ContactFormViewController.segueIdentifier, sender: self)
    }
    @IBAction func createContactAction(_ sender: UIButton){
        self.performSegue(withIdentifier: ContactFormViewController.segueIdentifier, sender: self)
    }
}
//MARK:- SideMenu Delegate
extension ContactListViewController: UISideMenuNavigationControllerDelegate, SideMenuViewControllerDelegate {
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        if let sideMenu = menu.topViewController as? SideMenuViewController{
            sideMenu.delegate = self
        }
    }
    func menuItemDidSelected(_ item: MenuItem) {
        self.tabBarController?.selectedIndex = item.index
    }
    func searchFirstResponder(){
        if self.tabBarController?.selectedIndex == 2{
            self.searchTextField.becomeFirstResponder()
        }
    }
}
extension ContactListViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchTextField.resignFirstResponder()
        return true
    }
    private func textfieldDidChange(_ sender: UITextField){
        
    }
    @IBAction func didChangeText(_ sender: UITextField){
        self.dataModel.searchText = sender.text
        self.tableView.reloadData()
    }
}
