//
//  ContactFormViewController.swift
//  RealtorCalc
//
//  Created by Bala on 03/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SideMenu
import SkyFloatingLabelTextField

class ContactFormViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showCreateContact"
    
    @IBOutlet weak var firstNameTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var titleTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var companyNameTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var cellNumberTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var faxNumberTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var cityTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var stateTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var zipTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var noteTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var mlsTypeTextField : SkyFloatingLabelTextField!
    @IBOutlet weak var contactImageButton : UIButton!
    @IBOutlet weak var addressTextView : UITextView!
    
    private var contactCompletion: ((Bool)->Void)?
    private var imageData: Data?
    private let addressPlaceholder = "Type your address..."
    lazy var dataModel: ContactFormDataModel = {return ContactFormDataModel()}()
    
    private var mlsList: Array<DropDownInfoModel>?{
        didSet{
            self.selectedValues()
        }
    }
    
    private var selectedMLS: DropDownInfoModel?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultPlaceholder()
        self.title = dataModel.type == .contact ? LocalizableKey.navigationTitle.addContact : "Profile Update"
        self.mlsTypeTextField.isHidden = self.dataModel.type == .contact ? true : false
//        self.defaultValues()
        self.configureUI()
        if dataModel.type == .profile{
            self.mapping()
            self.fetch()
            return
        }
        // Do any additional setup after loading the view.
    }
    private func defaultPlaceholder(){
        self.addressTextView.textColor = UIColor.lightGray
        self.addressTextView.text = addressPlaceholder
    }
    private func configureUI(){
        self.setupLeftBackButton()
        self.contactImageButton.clipsToBounds = true
        self.contactImageButton.layer.cornerRadius = self.contactImageButton.frame.height/2
    }
    private func defaultValues(){
        if dataModel.type == .profile{
            self.mapping()
            return
        }
        self.firstNameTextField.text = "Hello"
        self.lastNameTextField.text = "K"
        self.titleTextField.text = "Beyond"
        self.companyNameTextField.text = "My First Company"
        self.phoneNumberTextField.text = "1234567893"
        self.emailTextField.text = "abd@test.com"
        self.cellNumberTextField.text = "9898989890"
        self.stateTextField.text = "tamilnadu"
        self.cityTextField.text = "chennai"
        self.faxNumberTextField.text = "323938923"
        self.zipTextField.text = "600024"
        self.noteTextField.text = "Neighbour"
        self.addressTextView.text = "No: 17, chennai"

    }
    private func fetch(){
        self.mlsTypeTextField.delegate = self
        CommonRequest.dropDownAPI(type: .mlsBox) { (list) in
            self.mlsList = list
        }
    }
    private func selectedValues(){
        if let mls = self.dataModel.profileDetails?.response?.mlsId{
            if let index = self.mlsList?.index(of: DropDownInfoModel.init(value: mls)){
                self.selectedMLS = self.mlsList?[index]
                self.mlsTypeTextField.text = selectedMLS?.name
            }
        }
    }
    private func isValidationSuccess()->Bool{
        if firstNameTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("first name"))
            return false
        }else if lastNameTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("last name"))
            return false
        }else if titleTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("title"))
            return false
        }else if companyNameTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("company name"))
            return false
        }else if phoneNumberTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("phone number"))
            return false
        }else if emailTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("email Id"))
            return false
        }else if cellNumberTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("Alternate phone number"))
            return false
        }else if stateTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("state"))
            return false
        }else if cityTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("city"))
            return false
        }else if faxNumberTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("fax"))
            return false
        }else if zipTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("zip code"))
            return false
        }else if noteTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("notes"))
            return false
        }else if addressTextView.isEmpty || addressTextView.text == "Type your address..."{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.commonValidation.inValid("address"))
            return false
        }else{
            return true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ContactFormViewController : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type your address..."{
            self.addressTextView.text = ""
            self.addressTextView.textColor = UIColor.darkGray
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if addressTextView.text == ""{
            self.defaultPlaceholder()
        }
    }
}
//MARK:- SideMenu Delegate
extension ContactFormViewController: UISideMenuNavigationControllerDelegate, SideMenuViewControllerDelegate {
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        if let sideMenu = menu.topViewController as? SideMenuViewController{
            sideMenu.delegate = self
        }
    }
    func menuItemDidSelected(_ item: MenuItem) {
        self.tabBarController?.selectedIndex = item.index
    }
}

extension ContactFormViewController{
    @IBAction func submitAction(){
        self.createContact()
    }
    @IBAction func contactImageAction(_ sender: UIButton){
        self.openMediaLibrary()
    }
    private func createContact(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        let param = dataModel.type == .contact ? contactParam : profileParam
        let fileName = dataModel.type == .contact ? Constants.uploadFileName.addContact : Constants.uploadFileName.profilepic
        connection.sendRequest(urlPath: nil, method: .post, param: param, imageData: imageData, imageName: fileName, encoding: .queryString, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.dismiss(animated: true, completion: {
                self.contactCompletion?(true)
            })
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
        
    }
    private var contactParam: Dictionary<String, Any>{
        let key = APIKey.contact.key.self
        var param: Dictionary<String, Any> = [key.userId: UserManager.instance.userID]
        param.updateValue(companyNameTextField.text!, forKey: key.contcmpyNme)
        param.updateValue(emailTextField.text!, forKey: key.contEmail)
        param.updateValue(cellNumberTextField.text!, forKey: key.contcellNo)
        param.updateValue(phoneNumberTextField.text!, forKey: key.contphneNo)
        param.updateValue(faxNumberTextField.text!, forKey: key.conttxtFax)
        param.updateValue(noteTextField.text!, forKey: key.contNotes)
        param.updateValue(addressTextView.text!, forKey: key.conttxtAddress)
        param.updateValue(zipTextField.text!, forKey: key.conttxtZip)
        param.updateValue(cityTextField.text!, forKey: key.conttxtCity)
        param.updateValue(stateTextField.text!, forKey: key.conttxtState)
        param.updateValue(titleTextField.text!, forKey: key.contTitle)
        param.updateValue(Constants.tagValue.contactAdd, forKey: key.tag)
        param.updateValue(firstNameTextField.text!, forKey: key.contfirstNme)
        param.updateValue(lastNameTextField.text!, forKey: key.contlastNme)
        return param
    }
    func didContactCreatedCallback(_ sender: @escaping((Bool)->Void)){
            self.contactCompletion = sender
    }
    func openMediaLibrary(){
        let picker = UIImagePickerController.init()
        picker.delegate = self
        let actionSheet = UIAlertController.init(title: "Image Source", message: "", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }
        let libraryAction = UIAlertAction.init(title: "Photo Library", style: .default) { (action) in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        actionSheet.addAction(cancelAction)
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = self.contactImageButton
            presenter.sourceRect = self.contactImageButton.bounds
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
}
//MARK: - UIImage Picker Delegate
extension ContactFormViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[.originalImage] as? UIImage else { return }
        self.contactImageButton.setImage(pickedImage, for: .normal)
        imageData = pickedImage.jpeg(UIImage.JPEGQuality.medium)
        picker.dismiss(animated: true, completion: nil)
    }
}
//
extension ContactFormViewController{
    func mapping(){
        let profileItem = dataModel.profileDetails?.response
        self.firstNameTextField.text = profileItem?.name
        self.lastNameTextField.text = profileItem?.lastname
        self.titleTextField.text = profileItem?.title
        self.companyNameTextField.text = profileItem?.company_name
        self.phoneNumberTextField.text = profileItem?.phone
        self.emailTextField.text = profileItem?.email
        self.cellNumberTextField.text = profileItem?.cellNo
        self.stateTextField.text = profileItem?.state
        self.cityTextField.text = profileItem?.city
        self.faxNumberTextField.text = profileItem?.fax
        self.zipTextField.text = profileItem?.zip
        self.addressTextView.text = profileItem?.address
        self.contactImageButton.sd_setImage(with: dataModel.profileDetails?.imageURL, for: .normal, completed: nil)
        self.addressTextView.textColor = self.addressTextView.text == addressPlaceholder ? UIColor.lightGray : UIColor.darkGray
        self.selectedValues()
     }
}

extension ContactFormViewController{
    private var profileParam: Dictionary<String, Any>{
        let key = APIKey.profile.key.self
        var param: Dictionary<String, Any> = [APIKey.common.key.userId: UserManager.instance.userID]
        param.updateValue(companyNameTextField.text!, forKey: key.company_name)
        param.updateValue(emailTextField.text!, forKey: key.email)
        param.updateValue(cellNumberTextField.text!, forKey: key.cellNo)
        param.updateValue(phoneNumberTextField.text!, forKey: key.phneNo)
        param.updateValue(faxNumberTextField.text!, forKey: key.txtFax)
        param.updateValue(addressTextView.text!, forKey: key.txtAddress)
        param.updateValue(zipTextField.text!, forKey: key.txtZip)
        param.updateValue(cityTextField.text!, forKey: key.txtCity)
        param.updateValue(stateTextField.text!, forKey: key.txtState)
        param.updateValue(titleTextField.text!, forKey: key.txtTitle)
        param.updateValue(Constants.tagValue.profileUpdate, forKey: APIKey.common.key.tag)
        param.updateValue(firstNameTextField.text!, forKey: key.firstNme)
        param.updateValue(lastNameTextField.text!, forKey: key.lastNme)
        param.updateValue(selectedMLS?.value ?? "", forKey: key.mlsId)
        return param
    }
}
extension ContactFormViewController: UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.selectedMLS = model
            self.mlsTypeTextField.text = model?.name
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mlsTypeTextField{
            self.showBottomPicker(delegate: self, items: mlsList ?? [], tag: 0, title: "MLS Type")
            return false
        }
        return true
    }
    
}
