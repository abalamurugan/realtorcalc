//
//  BuyDownFixedTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 29/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BuyDownFixedTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFixedRate"
    
    @IBOutlet weak var salesPriceTextField: UITextField!
    @IBOutlet weak var downPaymentTextField: UITextField!
    @IBOutlet weak var baseLoanTermTextField: UITextField!
    @IBOutlet weak var paymentPerYearTextField: UITextField!
    @IBOutlet weak var baseInterestRateTextField: UITextField!
    @IBOutlet weak var yearlyIncreaseTextField: UITextField!
    @IBOutlet weak var subsidizedTermTextField: UITextField!
    @IBOutlet weak var subsidyPaymentPerYearTextField: UITextField!
    @IBOutlet weak var subsidyBuyDownTextField: UITextField!

    var buyDownDetails: BuyDownDetailsModel?{
        didSet{
            self.mapping()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    private func defaultValues(){
        salesPriceTextField.text = "400000"
        downPaymentTextField.text = "80000"
        baseInterestRateTextField.text = "4.5"
        baseLoanTermTextField.text = "15"
        paymentPerYearTextField.text = "12"
        subsidizedTermTextField.text = "5"
        subsidyPaymentPerYearTextField.text = "12"
        subsidyBuyDownTextField.text = "3.0"
    }

}
extension BuyDownFixedTableViewController{
    var pmt: Double{
        return Formula.calculatePMT(baseInterestRateTextField.toDouble, paymentPerYearTextField.toDouble, baseLoanTermTextField.toDouble, loanAmount)
    }
    var loanAmount: Double{
        return salesPriceTextField.toDouble - downPaymentTextField.toDouble
    }
    var totalSubsidy: Double{
        return loanAmount * subsidyBuyDownTextField.toDouble/100
    }
    func calculateFixedRate()->Array<BuyDownModel>{
        var fixedRateList: Array<BuyDownModel> = []
        for i in 0...6{
            let item = BuyDownModel()
            if (i + 1) <= subsidizedTermTextField.toInt{
                let yearlySubsidy = totalSubsidy/subsidizedTermTextField.toDouble
                item.yearlySubsidy = yearlySubsidy
                let monthlySubsidy = yearlySubsidy/subsidyPaymentPerYearTextField.toDouble
                item.monthlySubsidy = monthlySubsidy
            }else{
                item.yearlySubsidy = 0
                item.monthlySubsidy = 0
            }
            item.year = Double(i + 1)
            item.totalPayment = pmt
            item.buyerPayment = pmt - (item.monthlySubsidy ?? 0)
            let rate = Formula.rateCalculation(term: baseLoanTermTextField.toDouble, paymentPerYear: paymentPerYearTextField.toDouble, payment: item.buyerPayment ?? 0, loanAmount: loanAmount)
            item.buyerRate = rate
            fixedRateList.append(item)
        }
        return fixedRateList
    }
}
extension BuyDownFixedTableViewController{
    private func mapping(){
        salesPriceTextField.text = buyDownDetails?.salespricefix?.readableFormat
        downPaymentTextField.text = buyDownDetails?.downpaymentfix?.readableFormat
        baseInterestRateTextField.text = buyDownDetails?.baseinterestfix?.toNumber.toPercentage
        baseLoanTermTextField.text = buyDownDetails?.baseloantermfix
        paymentPerYearTextField.text = buyDownDetails?.noofpmtfix
        subsidizedTermTextField.text = buyDownDetails?.subsidytermfix
        subsidyPaymentPerYearTextField.text = buyDownDetails?.subsidypmtfix
        subsidyBuyDownTextField.text = buyDownDetails?.subsidypercentfix?.toNumber.toPercentage
    }
}
extension BuyDownFixedTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.buyDown.key.self
        var dict: Dictionary<String, Any> = [key.salesPriceFix:salesPriceTextField.toDouble]
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPaymentFix)
        dict.updateValue(baseInterestRateTextField.toDouble, forKey: key.baseIntRateFix)
        dict.updateValue(baseLoanTermTextField.toDouble, forKey: key.baseLoanTermFix)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.noOfPMTFix)
        dict.updateValue(subsidizedTermTextField.toDouble, forKey: key.subsidizedTermFix)
        dict.updateValue(subsidyPaymentPerYearTextField.toDouble, forKey: key.subsidyPMTFix)
        dict.updateValue(subsidyBuyDownTextField.toDouble, forKey: key.subBuyDownPerFix)
        dict.updateValue(loanAmount, forKey: key.loanAmountFix)
        dict.updateValue(calculateFixedRate()[0].buyerRate ?? "", forKey: key.initialIntRateFix)
        return dict
    }
}
extension BuyDownFixedTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == baseInterestRateTextField || textField == subsidyBuyDownTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
