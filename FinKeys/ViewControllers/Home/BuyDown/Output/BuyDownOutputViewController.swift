//
//  BuyDownOutputViewController.swift
//  RealtorCalc
//
//  Created by Bala on 02/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BuyDownOutputViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showBuyDownOutput"
    
    var list: Array<BuyDownOutput> = []
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        // Do any additional setup after loading the view.
    }
    
    private func configUI(){
        self.tableView.register(RefinanceHeaderView.nib, forHeaderFooterViewReuseIdentifier: RefinanceHeaderView.reuseIdentifier)
        self.configurePrint()
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.getPDf()
    }
    private func configurePrint(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID, APIKey.common.key.tag : Constants.tagValue.printOption]
        connection.networkRequest(urlPath: nil, method: .get, param: param,encoding: .queryString, { (response: ResponseModel<PrintPaymentModel>) in
            let model = response.item?.response
            let isRightButton = (model?.isPrintAvailable ?? "NO").caseInsensitiveCompare("YES") == .orderedSame ? true : false
            if isRightButton{
                self.setupRightBarButton(buttonTitle: LocalizableKey.reports.print)
            }
        }) { (error) in
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BuyDownOutputViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list[section].list?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BuyDownOutputTableViewCell.reuseIdentifier, for: indexPath) as! BuyDownOutputTableViewCell
        cell.set(list[indexPath.section].list![indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: RefinanceHeaderView.reuseIdentifier) as! RefinanceHeaderView
        headerView.titleLabel.text = list[section].title
        headerView.layer.backgroundColor = UIColor.init(hexString: "f7f8f9").cgColor
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
extension BuyDownOutputViewController{
    var pdfParam: Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict.updateValue(Constants.pdfDetect, forKey: APIKey.pdf.key.detect)
        dict.updateValue(contactModel!.contactId!, forKey: APIKey.pdf.key.contactId)
        dict.updateValue(Constants.reportNames.buydown, forKey: APIKey.pdf.key.name)
        dict.updateValue(Constants.tagValue.reportPdf, forKey: APIKey.pdf.key.tag)
        return dict
    }
    private func getPDf(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: pdfParam, encoding: .queryString, { (response: ResponseModel<PDFModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let pdfURL = response.item?.pdfURL, pdfURL != ""{
                UIApplication.shared.open(URL(string : pdfURL)!, options: [:], completionHandler: { (status) in
                })
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
