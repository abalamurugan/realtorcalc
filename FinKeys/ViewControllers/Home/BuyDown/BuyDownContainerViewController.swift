//
//  BuyDownContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 29/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BuyDownContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showBuyDown"
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var formOneContainer: UIView!
    @IBOutlet weak var formTwoContainer: UIView!
    @IBOutlet weak var calculateButton: UIButton!
    
    var formOneController: BuyDownFixedTableViewController!
    var formTwoController: BuyDownGraduateTableViewController!
    
    var outputList: Array<BuyDownOutput> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        formOneContainer.isHidden = false
        formTwoContainer.isHidden = true
        self.title = LocalizableKey.navigationTitle.buyDown
    }
    @IBAction func segmentedAction(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            formOneContainer.isHidden = false
            formTwoContainer.isHidden = true
        }else if sender.selectedSegmentIndex == 1{
            formOneContainer.isHidden = true
            formTwoContainer.isHidden = false
        }
    }
    @IBAction func calculateAction(_ sender: UIButton){
        self.postFormValues()
    }
    private func showOutput(){
        outputList.removeAll()
        let fixedList = formOneController.calculateFixedRate()
        let graduateList = formTwoController.calculateGraduateRate()
        outputList.append(BuyDownOutput.init(title: "Fixed Rate", list: fixedList))
        outputList.append(BuyDownOutput.init(title: "Graduate Rate", list: graduateList))
        self.performSegue(withIdentifier: BuyDownOutputViewController.segueIdentifier, sender: self)
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == BuyDownFixedTableViewController.segueIdentifier{
            formOneController = segue.destination as? BuyDownFixedTableViewController
        }else if segue.identifier == BuyDownGraduateTableViewController.segueIdentifier{
            formTwoController = segue.destination as? BuyDownGraduateTableViewController
        }else if segue.identifier == BuyDownOutputViewController.segueIdentifier{
            let outputController = segue.destination as! BuyDownOutputViewController
            outputController.list = outputList
            outputController.contactModel = self.contactModel
        }
    }
    

}
extension BuyDownContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getBuyDown()
    }
}
extension BuyDownContainerViewController{
    var paramViewBuyDown: Dictionary<String, Any>{
        
        return Param.viewReportParam(tagReport: Constants.tagValue.buydownView, contactId: self.contactModel!.contactId!)
        
    }
    private func getBuyDown(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewBuyDown, encoding: .queryString, { (response: ResponseModel<BuyDownResponseModel>) in
            self.formOneController.buyDownDetails = response.item?.response
            self.formTwoController.buyDownDetails = response.item?.response
        }) { (error) in
            self.showError(error)
        }
    }
}
extension BuyDownContainerViewController{
    private var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.contactId: self.contactModel!.contactId!]
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(Constants.reportNames.buydown, forKey: APIKey.common.key.action)
        dict.updateValue(Date().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        dict.updateValue(Constants.tagValue.buydownAdd, forKey: APIKey.common.key.tag)
        formOneController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        formTwoController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    
}
