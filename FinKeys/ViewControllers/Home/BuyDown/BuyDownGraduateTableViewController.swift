//
//  BuyDownGraduateTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 29/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BuyDownGraduateTableViewController: UITableViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showGraduate"

    @IBOutlet weak var salesPriceTextField: UITextField!
    @IBOutlet weak var downPaymentTextField: UITextField!
    @IBOutlet weak var baseLoanTermTextField: UITextField!
    @IBOutlet weak var paymentPerYearTextField: UITextField!
    @IBOutlet weak var baseInterestRateTextField: UITextField!
    @IBOutlet weak var yearlyIncreaseTextField: UITextField!
    @IBOutlet weak var subsidizedTermTextField: UITextField!
    @IBOutlet weak var subsidyPaymentPerYearTextField: UITextField!
    @IBOutlet weak var subsidyBuyDownTextField: UITextField!
    @IBOutlet weak var maxLoanIncreaseTextField: UITextField!

    var buyDownDetails: BuyDownDetailsModel?{
        didSet{
            self.mapping()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    private func defaultValues(){
        salesPriceTextField.text = "400000"
        downPaymentTextField.text = "80000"
        baseInterestRateTextField.text = "3.75"
        baseLoanTermTextField.text = "25"
        paymentPerYearTextField.text = "12"
        subsidizedTermTextField.text = "5"
//        subsidyPaymentPerYearTextField.text = "12"
        subsidyBuyDownTextField.text = "3.5"
        yearlyIncreaseTextField.text = "1.0"
        maxLoanIncreaseTextField.text = "2.0"
    }
}
extension BuyDownGraduateTableViewController{
    var pmt: Double{
        return Formula.calculatePMT(baseInterestRateTextField.toDouble, paymentPerYearTextField.toDouble, baseLoanTermTextField.toDouble, loanAmount)
    }
    var loanAmount: Double{
        return salesPriceTextField.toDouble - downPaymentTextField.toDouble
    }
    var totalSubsidy: Double{
        return loanAmount * subsidyBuyDownTextField.toDouble/100
    }
    func calculateGraduateRate()->Array<BuyDownModel>{
        var graduateRateList: Array<BuyDownModel> = []
        var rate: Double = 0
        for i in 0...6{
            let item = BuyDownModel()
            if (i + 1) <= subsidizedTermTextField.toInt{
                let yearlySubsidy = totalSubsidy/subsidizedTermTextField.toDouble
                item.yearlySubsidy = yearlySubsidy
                let monthlySubsidy = yearlySubsidy/paymentPerYearTextField.toDouble
                item.monthlySubsidy = monthlySubsidy
            }else{
                item.yearlySubsidy = 0
                item.monthlySubsidy = 0
            }
            item.year = Double(i + 1)
            if let parent = self.parent as? BuyDownContainerViewController{
                let paymentPerYear = parent.formOneController.paymentPerYearTextField.toDouble
                let term = parent.formOneController.baseLoanTermTextField.toDouble
                let totPayment = Formula.calculatePMT(baseInterestRateTextField.toDouble, paymentPerYear, term, self.loanAmount)
                item.totalPayment = totPayment
            }
            
            if i == 0{
                rate = baseInterestRateTextField.toDouble
            }else{
                let totalInt = baseInterestRateTextField.toDouble + maxLoanIncreaseTextField.toDouble
                let currentInt = rate + yearlyIncreaseTextField.toDouble
                if totalInt >= currentInt{
                    rate = currentInt
                }else{
                    rate = totalInt
                }
                
//                let graduatePercent = baseInterestRateTextField.toDouble + maxLoanIncreaseTextField.toDouble
//                let buyerPercent = rate + yearlyIncreaseTextField.toDouble
//                rate = graduatePercent >= buyerPercent ? buyerPercent : graduatePercent
            }
            let buyerPayment = Formula.calculatePMT(rate, paymentPerYearTextField.toDouble, baseLoanTermTextField.toDouble, self.loanAmount)
            item.buyerPayment = buyerPayment
            item.buyerRate = rate
            graduateRateList.append(item)
        }
        return graduateRateList
    }
}
extension BuyDownGraduateTableViewController{
    private func mapping(){
        salesPriceTextField.text = buyDownDetails?.salespricegrt?.readableFormat
        downPaymentTextField.text = buyDownDetails?.downpaymentgrt?.readableFormat
        baseInterestRateTextField.text = buyDownDetails?.baseinterestgrt?.toNumber.toPercentage
        baseLoanTermTextField.text = buyDownDetails?.baseloanTermgrt
        paymentPerYearTextField.text = buyDownDetails?.noofpmtgrt
        subsidizedTermTextField.text = buyDownDetails?.subsidytermgrt
        subsidyBuyDownTextField.text = buyDownDetails?.subsidypercentadj?.toNumber.toPercentage
        yearlyIncreaseTextField.text = buyDownDetails?.yearlyincreasegrt?.toNumber.toPercentage
        maxLoanIncreaseTextField.text = buyDownDetails?.maxloanIncr?.toNumber.toPercentage

    }
}
extension BuyDownGraduateTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.buyDown.key.self
        var dict: Dictionary<String, Any> = [key.salesPriceGrt:salesPriceTextField.toDouble]
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPaymentGrt)
        dict.updateValue(baseInterestRateTextField.toDouble, forKey: key.baseIntRateGrt)
        dict.updateValue(baseLoanTermTextField.toDouble, forKey: key.baseLoanTermGrt)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.noOfPMTGrt)
        dict.updateValue(subsidizedTermTextField.toDouble, forKey: key.subsidizedTermGrt)
        dict.updateValue(subsidyBuyDownTextField.toDouble, forKey: key.subBuyDownPerGrt)
        dict.updateValue(maxLoanIncreaseTextField.toDouble, forKey: key.maxLoanIncrease)
        dict.updateValue(yearlyIncreaseTextField.toDouble, forKey: key.yearlyIncrGrt)
        dict.updateValue(loanAmount, forKey: key.loanAmountGrt)
        dict.updateValue(calculateGraduateRate()[0].buyerRate ?? "", forKey: key.initialIntRateGrt)
        return dict
    }
}
extension BuyDownGraduateTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == baseInterestRateTextField || textField == subsidyBuyDownTextField || textField == maxLoanIncreaseTextField || textField == yearlyIncreaseTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
