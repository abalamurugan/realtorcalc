//
//  HHomePaymentTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MaxPaymentTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showHomePayment"
    
    @IBOutlet weak var availableFundsTextField : UITextField!
    @IBOutlet weak var variableCloseCostTextField : UITextField!
    @IBOutlet weak var minDownPmtTextField : UITextField!
    @IBOutlet weak var loanOriginationFeeTextField : UITextField!
    @IBOutlet weak var discoundPointsTextField : UITextField!
    @IBOutlet weak var prepaidTextField : UITextField!
    @IBOutlet weak var titleRecordingTextField : UITextField!
    @IBOutlet weak var mortageInsuranceFundingTextField : UITextField!
    @IBOutlet weak var homeWartentyTextField : UITextField!
    @IBOutlet weak var sellerPaidCostTextField : UITextField!

    var maxQualifyDetails: MaxQualificationDetailModel?{
        didSet{
            self.mapping()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        availableFundsTextField.text = "135000"
        variableCloseCostTextField.text = "1.0"
        minDownPmtTextField.text = "20.0"
        loanOriginationFeeTextField.text = "3235"
        discoundPointsTextField.text = "0"
        prepaidTextField.text = "3352"
        titleRecordingTextField.text = "2426"
    }
    func isValidationSuccess()->Bool{
        if loanOriginationFeeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Loan Origination Fees"))
            return false
        } 
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension MaxPaymentTableViewController{
    func approximateClosingCalc()->Double{
        var approximateClosing : Double = 0
        let loanOrigination = loanOriginationFeeTextField.toDouble
        let discount = discoundPointsTextField.toDouble
        let prepaid = prepaidTextField.toDouble
        let title = titleRecordingTextField.toDouble
        let mort = mortageInsuranceFundingTextField.toDouble
        approximateClosing += loanOrigination
        approximateClosing += discount
        approximateClosing += prepaid
        approximateClosing += title
        approximateClosing += mort
        approximateClosing += homeWartentyTextField.toDouble
        approximateClosing += sellerPaidCostTextField.toDouble
        return approximateClosing
    }
    func estimateCashCloseCalc() -> Double{
        var totalClosing : Double = 0
        totalClosing = approximateClosingCalc() + availableFundsTextField.toDouble
        return totalClosing
    }
    var maxHomePrice: Double{
        return (availableFundsTextField.toDouble - approximateClosingCalc())/(variableCloseCostTextField.toDouble + minDownPmtTextField.toDouble) * 100
    }
    var maxPIAmount: Double{
        return -maxHomePrice*(1-minDownPmtTextField.toDouble.toValue)
    }
    var maxPIFunds: Double{
        if let parent = self.parent as? MaximumQualifyContainerViewController{
            let paymentPerYear = parent.financingLimitViewController.paymentPerYearTextField.toDouble
            let term = parent.financingLimitViewController.termYearTextField.toDouble
            let interest = parent.financingLimitViewController.interestRateTextField.toDouble
            return Formula.calculatePMT(interest, paymentPerYear, term, maxPIAmount)
        }
        return 0
    }
    var maxPIPayment: Double{
        var principalInterest: Double = 0
        if let parent = self.parent as? MaximumQualifyContainerViewController{
            principalInterest = parent.buyerDataViewController.princplInterestPortion
        }
        return Double.minimum(maxPIFunds.removeMinus, principalInterest)
    }
}
extension MaxPaymentTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == minDownPmtTextField || textField == variableCloseCostTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    var fundsOutputList: Array<OutputDetailsModel>{
        var fundList: Array<OutputDetailsModel> = []
        fundList.append(OutputDetailsModel.init(key: "Available Funds", value: "",isHeading: true))
        fundList.append(OutputDetailsModel.init(key: "Available Funds:", value: availableFundsTextField.toDouble))
        fundList.append(OutputDetailsModel.init(key: "Fixed Closing Costs", value: approximateClosingCalc()))
        fundList.append(OutputDetailsModel.init(key: "Variable Closing Costs", value: variableCloseCostTextField.toDouble.toPercentage))
        fundList.append(OutputDetailsModel.init(key: "Minimum Down Pmt.", value: minDownPmtTextField.toDouble.toPercentage))
        fundList.append(OutputDetailsModel.init(key: "Maximum Home Price (Using Available Funds)", value: maxHomePrice))
        fundList.append(OutputDetailsModel.init(key: "Max P&I Based on Funds:", value: maxPIFunds.removeMinus))
        fundList.append(OutputDetailsModel.init(key: "Max P&I Pmt.(calculated).", value: maxPIPayment))
        return fundList
    }
    var cashClosingOutputList: Array<OutputDetailsModel>{
        var list: Array<OutputDetailsModel> = []
        list.append(OutputDetailsModel.init(key: "Estimated Cash at Closing", value: "",isHeading: true))
        list.append(OutputDetailsModel.init(key: "Loan Origination Fee", value: loanOriginationFeeTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Discount Points", value: discoundPointsTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Pre-paids (Tax/Int/Ins)", value: prepaidTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Title/Recording/Misc.", value: titleRecordingTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Mtg. Insur. Funding Fee", value: mortageInsuranceFundingTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Home Warranty", value: homeWartentyTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Seller Paid Costs", value: sellerPaidCostTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "Approximate Closing Costs", value: approximateClosingCalc()))
        list.append(OutputDetailsModel.init(key: "  + Down Payment", value: availableFundsTextField.toDouble))

        list.append(OutputDetailsModel.init(key: "Estimated Cash at Closing", value: estimateCashCloseCalc()))

        return list
    }
}
extension MaxPaymentTableViewController{
    private func mapping(){
        availableFundsTextField.text = maxQualifyDetails?.availableFunds?.readableFormat
        variableCloseCostTextField.text = maxQualifyDetails?.varaiableClosing?.toNumber.toPercentage
        minDownPmtTextField.text = maxQualifyDetails?.minDownPmt?.toNumber.toPercentage
        loanOriginationFeeTextField.text = maxQualifyDetails?.loanOriginFees?.readableFormat
        discoundPointsTextField.text = maxQualifyDetails?.discountPoints?.readableFormat
        prepaidTextField.text = maxQualifyDetails?.prePaids?.readableFormat
        titleRecordingTextField.text = maxQualifyDetails?.titleRecordMisc?.readableFormat
        homeWartentyTextField.text = maxQualifyDetails?.homeWarranty?.readableFormat
        sellerPaidCostTextField.text = maxQualifyDetails?.sellerPaid?.readableFormat
        mortageInsuranceFundingTextField.text = maxQualifyDetails?.mtgInsFunding?.readableFormat

    }
    var updateParm: Dictionary<String, Any>{
        let key = APIKey.maximumQualification.key.self
        var dict: Dictionary<String, Any> = [key.availableFunds : availableFundsTextField.toDouble]
        dict.updateValue(variableCloseCostTextField.toDouble, forKey: key.varaiableClosing)
        dict.updateValue(minDownPmtTextField.toDouble, forKey: key.minDownPmt)
        dict.updateValue(loanOriginationFeeTextField.toDouble, forKey: key.loanOriginFees)
        dict.updateValue(discoundPointsTextField.toDouble, forKey: key.discountPoints)
        dict.updateValue(prepaidTextField.toDouble, forKey: key.prePaids)
        dict.updateValue(titleRecordingTextField.toDouble, forKey: key.titleRecordMisc)
        dict.updateValue(mortageInsuranceFundingTextField.toDouble, forKey: key.mtgInsFunding)
        dict.updateValue(homeWartentyTextField.toDouble, forKey: key.homeWarranty)
        dict.updateValue(sellerPaidCostTextField.toDouble, forKey: key.sellerPaid)
        dict.updateValue(availableFundsTextField.toDouble, forKey: key.downPayment)
        return dict
    }
}
