//
//  QualificationViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MaxQualificationTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showQualification"

    @IBOutlet weak var planTypeTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestRateTextField : UITextField!
    @IBOutlet weak var termYearTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var appreciationTextField : UITextField!


    var maxQualifyDetails: MaxQualificationDetailModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    private var paymentTypeList: Array<DropDownInfoModel>?
    private var loanPlanList: Array<DropDownInfoModel>?
    private var selectedPaymentType: DropDownInfoModel?
    private var selectedLoanPlan: DropDownInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    func defaultValues(){
        interestRateTextField.text = "5.00"
        termYearTextField.text = "30"
        paymentPerYearTextField.text = "12"
    }
    private func setup(){
        self.fetch()
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        CommonRequest.dropDownAPI(type: .paymentType) { (paymentList) in
            self.paymentTypeList = paymentList
                CommonRequest.dropDownAPI(type: .loanPlan) { (loanList) in
                LoadingView.instance.hideActivityIndicator(uiView: self.view)
                self.loanPlanList = loanList
            }
        }
        
    }
    private func selectedValues(){
        if let loanPlan = self.maxQualifyDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.planTypeTextField.text = selectedLoanPlan?.name
            }
        }
        if let paymentType = self.maxQualifyDetails?.paymenttype{
            if let index = self.paymentTypeList?.index(of: DropDownInfoModel.init(value: paymentType)){
                self.selectedPaymentType = self.paymentTypeList?[index]
                self.paymentTypeTextField.text = selectedPaymentType?.name
            }
        }
    }
    func isValidationSuccess()->Bool{
        if planTypeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Plan Type"))
            return false
        }else if paymentTypeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Payment type"))
            return false
        }else if interestRateTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Interest Rate"))
            return false
        }else if termYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Term Year"))
            return false
        }else if paymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Payments Per Year"))
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MaxQualificationTableViewController{
    func pmt()->Double{
        if let parent = self.parent as? HomeQualifyContainerViewController{
            let interest = interestRateTextField.text?.toDouble ?? 0
            let paymentYear = paymentPerYearTextField.text?.toDouble ?? 0
            let term = termYearTextField.text?.toDouble ?? 0
            let mtgAmount = parent.homePaymentViewController.calculateFirstMortgageAmount()
            let pmt = Formula.calculatePMT(interest, paymentYear, term, mtgAmount)
            return pmt
         }
        return 0
    }
    var totalPayment: Double{
        let loanTerm = termYearTextField.toDouble
        let paymentPerYear = paymentPerYearTextField.toDouble
        return loanTerm * paymentPerYear
    }
    var firstRatio : Double{
        var totHousingPayment : Double = 0
        var totalMonthlyIncome : Double = 0
        if let parent = self.parent as? HomeQualifyContainerViewController{
            totHousingPayment = parent.qualificationViewController.calculateHousingPayment()
            totalMonthlyIncome = parent.buyerDataViewController.calculateTotalMonthlyIncome()
            totHousingPayment += parent.buyerDataViewController.buyerMonthlyDebtTextField.toDouble
        }
        return (totHousingPayment/totalMonthlyIncome) * 100
    }
    var secondRatio : Double{
        var totHousingPayment : Double = 0
        var totalMonthlyIncome : Double = 0
        if let parent = self.parent as? HomeQualifyContainerViewController{
            totHousingPayment = parent.qualificationViewController.calculateHousingPayment()
            totalMonthlyIncome = parent.buyerDataViewController.calculateTotalMonthlyIncome()
        }
        return (totHousingPayment/totalMonthlyIncome) * 100
    }
    var maxLoanAmount: Double{
        //Need to calculate PV
        let interest = interestRateTextField.toDouble.toValue/paymentPerYearTextField.toDouble
        let nper = (termYearTextField.toDouble * paymentPerYearTextField.toDouble)
        var pmt: Double = 0
        if let _maxPI = self.parent as? MaximumQualifyContainerViewController{
            pmt = _maxPI.maxPaymentViewController.maxPIPayment
        }
        let pv = Formula.PVCalculation(rate: interest, nper: nper, pmt: pmt)
        return pv.removeMinus
    }
    var downPMTAvail: Double{
        if let parent = self.parent as? MaximumQualifyContainerViewController{
            let availableFunds = parent.maxPaymentViewController.availableFundsTextField.toDouble
            let fixedClosingCost = parent.maxPaymentViewController.approximateClosingCalc()
            let variableClosingCost = parent.maxPaymentViewController.variableCloseCostTextField.toDouble
            let exp1 = ((availableFunds - fixedClosingCost)-variableClosingCost * maxLoanAmount / 100)
            return exp1/(1+variableClosingCost/100)
        }
        return 0
    }
    var maxHomePrice: Double{
        return maxLoanAmount + downPMTAvail
    }
    var totalClosingCost: Double{
        return 1/100 * (maxLoanAmount + downPMTAvail) + 9013
    }
}
extension MaxQualificationTableViewController{
    func buyerQualification() -> [OutputDetailsModel]{
        var item : [OutputDetailsModel] = [OutputDetailsModel.init(key: "Loan Plan", value: planTypeTextField.text ?? "")]
        item.append(OutputDetailsModel.init(key: "Payment Type:", value: paymentTypeTextField.text ?? ""))
        item.append(OutputDetailsModel.init(key: "Interest Rate:", value: interestRateTextField.toDouble.toPercentage))
        item.append(OutputDetailsModel.init(key: "Total Payments:", value: totalPayment))
        item.append(OutputDetailsModel.init(key: "Payments Per Year", value: paymentPerYearTextField.toDouble))
        item.append(OutputDetailsModel.init(key: "1st Ratio (Max 28%)", value: firstRatio.toPercentage))
        item.append(OutputDetailsModel.init(key: "2nd Ratio (Max 36%)", value: secondRatio.toPercentage))
        return item
    }
    var financingLimitFactorList: Array<OutputDetailsModel>{
        var list: Array<OutputDetailsModel> = []
        list.append(OutputDetailsModel.init(key: "Financing Limiting Factors", value: "",isHeading: true))
        list.append(OutputDetailsModel.init(key: "Plan Type", value: planTypeTextField.text ?? ""))
        list.append(OutputDetailsModel.init(key: "Interest Rate", value: interestRateTextField.toDouble.toPercentage))
        list.append(OutputDetailsModel.init(key: "Term (Years)", value: (termYearTextField.text ?? "")))
        list.append(OutputDetailsModel.init(key: "Payments Per Year", value: paymentPerYearTextField.text ?? ""))
        list.append(OutputDetailsModel.init(key: "Max Loan Amt.", value: maxLoanAmount))
        list.append(OutputDetailsModel.init(key: "Down Pmt. Avail.", value: downPMTAvail))
        list.append(OutputDetailsModel.init(key: "Tot. Closing Costs:", value: totalClosingCost))
        list.append(OutputDetailsModel.init(key: "Tot. Closing Costs:", value: totalClosingCost))
        list.append(OutputDetailsModel.init(key: "Maximum Home Price (Using Qualifying Ratios)", value: maxHomePrice))
        return list
    }
}
extension MaxQualificationTableViewController : UITextFieldDelegate, BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            planTypeTextField.text = model?.name
            selectedLoanPlan = model
        }else if viewController.dataModel.tag == 1{
            paymentTypeTextField.text = model?.name
            selectedPaymentType = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == planTypeTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 0)
            return false
        }else if textField == paymentTypeTextField{
            self.showBottomPicker(delegate: self, items: paymentTypeList ?? [], tag: 1)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == appreciationTextField || textField == interestRateTextField  {
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
}
extension MaxQualificationTableViewController{
    private func mapping(){
        interestRateTextField.text = maxQualifyDetails?.interestRate?.toNumber.toPercentage
        termYearTextField.text = maxQualifyDetails?.loanTerm
        paymentPerYearTextField.text = maxQualifyDetails?.payPerYr
        appreciationTextField.text = maxQualifyDetails?.annualAppreciation?.toNumber.toPercentage
//        planTypeTextField.text = maxQualifyDetails?.loanPlan
//        paymentTypeTextField.text = maxQualifyDetails?.paymenttype
    }
    var updateParm: Dictionary<String, Any>{
        let key = APIKey.maximumQualification.key.self
        var dict: Dictionary<String, Any> = [key.interestRate : interestRateTextField.toDouble]
        dict.updateValue(termYearTextField.toDouble, forKey: key.termsYears)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYr)
        dict.updateValue(self.selectedLoanPlan?.value ?? "", forKey: key.loanPlan)
        dict.updateValue(self.selectedPaymentType?.value ?? "", forKey: key.paymenttype)
        dict.updateValue(appreciationTextField.toDouble, forKey: key.annualAppreciation)
        return dict
    }
}

