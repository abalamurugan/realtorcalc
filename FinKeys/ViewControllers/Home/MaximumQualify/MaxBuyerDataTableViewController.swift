//
//  BuyerDataTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MaxBuyerDataTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showBuyerData"
    
    
    @IBOutlet weak var buyerOneIncomeTextField : UITextField!
    @IBOutlet weak var buyerTwoIncomeTextField : UITextField!
    @IBOutlet weak var otherIncomeTextField : UITextField!
    @IBOutlet weak var nonTaxableIncomeTextField : UITextField!
    @IBOutlet weak var buyerMonthlyDebtTextField : UITextField!
    @IBOutlet weak var buyerEmployStatusTextField : UITextField!
    @IBOutlet weak var spouseEmployStatusTextField : UITextField!
    @IBOutlet weak var noExcemptionTextField : UITextField!
    @IBOutlet weak var firstRatioTextField: UITextField!
    @IBOutlet weak var secondRatioTextField: UITextField!
    @IBOutlet weak var realEstateTextField: UITextField!
    @IBOutlet weak var mortgageInsuranceTextField: UITextField!
    @IBOutlet weak var hoaFeesTextField: UITextField!
    @IBOutlet weak var hazardInsuranceTextField: UITextField!

    var buyerEmployStatus: BottomPickerModel?
    var spouseEmployStatus: BottomPickerModel?
    var maritalStatus: BottomPickerModel?
    var employStatusList: Array<DropDownInfoModel>?
    private var selectedEmployStatus: DropDownInfoModel?
    private var selectedSpouseStatus: DropDownInfoModel?

    var maxQualifyDetails: MaxQualificationDetailModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetch()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        buyerOneIncomeTextField.text = "6100"
        buyerTwoIncomeTextField.text = "3800"
        otherIncomeTextField.text = "750"
        buyerMonthlyDebtTextField.text = "200"
//        noExcemptionTextField.text = "3"
        firstRatioTextField.text = "28"
        secondRatioTextField.text = "36"
        realEstateTextField.text = "269.61"
        hazardInsuranceTextField.text = "94.36"
        mortgageInsuranceTextField.text = "145.59"
    }
    private func fetch(){
        CommonRequest.dropDownAPI(type: .employmentBox, model: { (buyerStatusList) in
            self.employStatusList = buyerStatusList
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
        })
 
    }
    private func selectedValues(){
        if let employType = self.maxQualifyDetails?.buyerEmpStatus{
            if let index = self.employStatusList?.index(of: DropDownInfoModel.init(value: employType)){
                self.selectedEmployStatus = self.employStatusList?[index]
                self.buyerEmployStatusTextField.text = selectedEmployStatus?.name
            }
        }
        if let employType = self.maxQualifyDetails?.spouseEmpStatus{
            if let index = self.employStatusList?.index(of: DropDownInfoModel.init(value: employType)){
                self.selectedSpouseStatus = self.employStatusList?[index]
                self.spouseEmployStatusTextField.text = selectedSpouseStatus?.name
            }
        }
        
    }
    func isValidationSuccess()->Bool{
        if buyerOneIncomeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer One Income"))
            return false
        }else if buyerTwoIncomeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer Two Income"))
            return false
        }else if buyerMonthlyDebtTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer Monthly Debit"))
            return false
        }else if buyerEmployStatusTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer Employ Status"))
            return false
        }else if spouseEmployStatusTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Spouse Employ Status"))
            return false
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension MaxBuyerDataTableViewController{
    func calculateTotalMonthlyIncome() -> Double{
        var income : Double = 0.0
        income += buyerOneIncomeTextField.toDouble
        income += buyerTwoIncomeTextField.toDouble
        income += otherIncomeTextField.toDouble
        income += nonTaxableIncomeTextField.toDouble
        return income
    }
    func buyerData() -> [OutputDetailsModel]{
        var item : [OutputDetailsModel] = [OutputDetailsModel.init(key: "Buyer One Income", value: buyerOneIncomeTextField.toDouble)]
        item.append(OutputDetailsModel.init(key: "Buyer Two Income", value: buyerTwoIncomeTextField.toDouble))
         item.append(OutputDetailsModel.init(key: "Other Income", value: otherIncomeTextField.toDouble))
         item.append(OutputDetailsModel.init(key: "Non Taxable Income", value: nonTaxableIncomeTextField.toDouble))
         item.append(OutputDetailsModel.init(key: "Buyer Monthly Debit", value: buyerMonthlyDebtTextField.toDouble))
        item.append(OutputDetailsModel.init(key: "Buyer Employ Status", value: buyerEmployStatusTextField.text ?? ""))
        item.append(OutputDetailsModel.init(key: "Buyer Monthly Debit", value: buyerMonthlyDebtTextField.text ?? ""))
//        item.append(OutputDetailsModel.init(key: "Number of Excemption", value: noExcemptionTextField.toDouble))
        return item
    }
    
    var firstRatioIncome: Double{
        return calculateTotalMonthlyIncome() * firstRatioTextField.toDouble.toValue
    }
    var secondRatioIncome: Double{
        return (calculateTotalMonthlyIncome() * secondRatioTextField.toDouble.toValue) - buyerMonthlyDebtTextField.toDouble
    }
    var maxHousingPayment: Double{
        if secondRatioIncome < firstRatioIncome{
            return secondRatioIncome
        }
        return firstRatioIncome
    }
    var princplInterestPortion: Double{
        var total = maxHousingPayment
        total -= realEstateTextField.toDouble
        total -= hazardInsuranceTextField.toDouble
        total -= mortgageInsuranceTextField.toDouble
        total -= hoaFeesTextField.toDouble
        return total
    }
    
}
extension MaxBuyerDataTableViewController {
 
    var buyerOutputList: Array<OutputDetailsModel>{
        var buyerDataList: [OutputDetailsModel] = []
        buyerDataList.append(OutputDetailsModel.init(key: "Buyer Data Input", value: "",isHeading: true))
        buyerDataList.append(OutputDetailsModel.init(key: "Buyer 1 Income", value: buyerOneIncomeTextField.toDouble))
        buyerDataList.append(OutputDetailsModel.init(key: "Buyer 2 Income", value: buyerTwoIncomeTextField.toDouble))
        buyerDataList.append(OutputDetailsModel.init(key: "Other Income", value: otherIncomeTextField.toDouble))
        buyerDataList.append(OutputDetailsModel.init(key: "Non-Taxable Income", value: nonTaxableIncomeTextField.toDouble))
        buyerDataList.append(OutputDetailsModel.init(key: "Total Monthly Income:", value: calculateTotalMonthlyIncome()))
        buyerDataList.append(OutputDetailsModel.init(key: "Buyer's Monthly Debt:", value: buyerMonthlyDebtTextField.toDouble))

        buyerDataList.append(OutputDetailsModel.init(key: "Buyer Employ Status:", value: buyerEmployStatusTextField.text ?? ""))
        buyerDataList.append(OutputDetailsModel.init(key: "Spouse Employ Status:", value: spouseEmployStatusTextField.text ?? ""))
//        buyerDataList.append(OutputDetailsModel.init(key: "Number of Exemptions:", value: noExcemptionTextField.text ?? ""))
        return buyerDataList
    }
    var buyerQualificationOutputList: Array<OutputDetailsModel>{
        var buyerQualificationList: [OutputDetailsModel] = []
        buyerQualificationList.append(OutputDetailsModel.init(key: "Buyer Qualification:", value: "", isHeading: true))
        buyerQualificationList.append(OutputDetailsModel.init(key: "1st Ratio:", value: firstRatioTextField.toDouble.toPercentage))
        buyerQualificationList.append(OutputDetailsModel.init(key: "2nd Ratio:", value: secondRatioTextField.toDouble.toPercentage))
        buyerQualificationList.append(OutputDetailsModel.init(key: "1st Ratio Income:", value: firstRatioIncome))
        buyerQualificationList.append(OutputDetailsModel.init(key: "2nd Ratio Income:", value: secondRatioIncome))
        buyerQualificationList.append(OutputDetailsModel.init(key: "Buyer's Monthly Debt:", value: buyerMonthlyDebtTextField.toDouble))
        buyerQualificationList.append(OutputDetailsModel.init(key: "Maximum Housing Payment:", value: maxHousingPayment))
        buyerQualificationList.append(OutputDetailsModel.init(key: "- Real Estate Taxes", value: realEstateTextField.toDouble))
        buyerQualificationList.append(OutputDetailsModel.init(key: "- Hazard Insurance", value: hazardInsuranceTextField.toDouble))
        buyerQualificationList.append(OutputDetailsModel.init(key: "- Mortgage Insurance", value: mortgageInsuranceTextField.toDouble))
        buyerQualificationList.append(OutputDetailsModel.init(key: "- HOA Fees", value: hoaFeesTextField.toDouble))
        buyerQualificationList.append(OutputDetailsModel.init(key: "Principal & Interest Portion:", value: princplInterestPortion))
        return buyerQualificationList
    }
}
extension MaxBuyerDataTableViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == buyerEmployStatusTextField{
            self.showBottomPicker(delegate: self, items: employStatusList ?? [], tag: 0, title: "Employ Status")
            return false
        }else if textField == spouseEmployStatusTextField{
            self.showBottomPicker(delegate: self, items: employStatusList ?? [], tag: 1, title: "Employ Status")
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == firstRatioTextField || textField == secondRatioTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
    
}
extension MaxBuyerDataTableViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.buyerEmployStatusTextField.text = model?.name
            self.selectedEmployStatus = model
        }else if viewController.dataModel.tag == 1{
            self.spouseEmployStatusTextField.text = model?.name
            self.selectedSpouseStatus = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
    }
}
extension MaxBuyerDataTableViewController{
    private func mapping(){
        buyerOneIncomeTextField.text = maxQualifyDetails?.buyer1Income?.readableFormat
        buyerTwoIncomeTextField.text = maxQualifyDetails?.buyer2Income?.readableFormat
        otherIncomeTextField.text = maxQualifyDetails?.otherIncome?.readableFormat
        buyerMonthlyDebtTextField.text = maxQualifyDetails?.buyerMonthDebt?.readableFormat
//        noExcemptionTextField.text = "3"
        firstRatioTextField.text = maxQualifyDetails?.stRatio?.toNumber.toPercentage
        secondRatioTextField.text = maxQualifyDetails?.ndRatio?.toNumber.toPercentage
        realEstateTextField.text = maxQualifyDetails?.realEstateTaxes?.readableFormat
        hazardInsuranceTextField.text = maxQualifyDetails?.hazardInsurance?.readableFormat
        mortgageInsuranceTextField.text = maxQualifyDetails?.mortInsurance?.readableFormat
        nonTaxableIncomeTextField.text = maxQualifyDetails?.nonTaxableIncome?.readableFormat
        hoaFeesTextField.text = maxQualifyDetails?.hoaFees?.readableFormat
//        buyerEmployStatusTextField.text = maxQualifyDetails?.buyerEmpStatus
//        spouseEmployStatusTextField.text = maxQualifyDetails?.spouseEmpStatus
    }
    var updateParm: Dictionary<String, Any>{
        let key = APIKey.maximumQualification.key.self
        var dict: Dictionary<String, Any> = [key.buyer1Income : buyerOneIncomeTextField.toDouble]
        dict.updateValue(buyerTwoIncomeTextField.toDouble, forKey: key.buyer2Income)
        dict.updateValue(otherIncomeTextField.toDouble, forKey: key.otherIncome)
        dict.updateValue(buyerMonthlyDebtTextField.toDouble, forKey: key.buyerMonthDebt)
        dict.updateValue(nonTaxableIncomeTextField.toDouble, forKey: key.nonTaxableIncome)
        dict.updateValue(firstRatioTextField.toDouble, forKey: key.stRatio)
        dict.updateValue(secondRatioTextField.toDouble, forKey: key.ndRatio)
        dict.updateValue(realEstateTextField.toDouble, forKey: key.realEstateTaxes)
        dict.updateValue(hazardInsuranceTextField.toDouble, forKey: key.hazardInsurance)
        dict.updateValue(mortgageInsuranceTextField.toDouble, forKey: key.mortInsurance)
        dict.updateValue(hoaFeesTextField.toDouble, forKey: key.hoaFees)
        dict.updateValue(selectedEmployStatus?.value ?? "", forKey: key.buyerEmpStatus)
        dict.updateValue(selectedSpouseStatus?.value ?? "", forKey: key.spouseEmpStatus)
        return dict
    }
}
