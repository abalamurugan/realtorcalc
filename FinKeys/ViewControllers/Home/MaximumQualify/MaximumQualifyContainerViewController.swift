//
//  HomeQualifyContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class MaximumQualifyContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showMaximumQualify"
    
    @IBOutlet weak var segmentedControl : UISegmentedControl!
    @IBOutlet weak var buyerDataContainer : UIView!
    @IBOutlet weak var homePaymentContainer : UIView!
    @IBOutlet weak var qualificationContainer : UIView!
    
    var buyerDataViewController : MaxBuyerDataTableViewController!
    var maxPaymentViewController : MaxPaymentTableViewController!
    var financingLimitViewController : MaxQualificationTableViewController!
    
    var equityBuildUpList: Array<EquityModel> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.maxQualify
        buyerDataContainer.isHidden = false
        homePaymentContainer.isHidden = true
        qualificationContainer.isHidden = true
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    
    @IBAction func segmentedAction(_ sender : UISegmentedControl){
        buyerDataContainer.isHidden = true
        homePaymentContainer.isHidden = true
        qualificationContainer.isHidden = true
        if sender.selectedSegmentIndex == 0{
//            if buyerDataViewController.isValidationSuccess(){
                buyerDataContainer.isHidden = false
//            }
        }else if sender.selectedSegmentIndex == 1{
//            if homePaymentViewController.isValidationSuccess(){
                homePaymentContainer.isHidden = false
//            }
        }else if sender.selectedSegmentIndex == 2{
//            if qualificationViewController.isValidationSuccess(){
                qualificationContainer.isHidden = false
//            }
        }
    }

    @IBAction func calculateAction(_ sender : UIButton){
        self.postFormValues()
    }
    var isIncome: Bool{
        if (maxPaymentViewController.maxPIPayment == buyerDataViewController.princplInterestPortion) && (buyerDataViewController.maxHousingPayment == buyerDataViewController.firstRatioIncome){
            return true
        }
        return false
    }
    var monthlyDebtCushion: Double{
        let exp1 = buyerDataViewController.calculateTotalMonthlyIncome() * buyerDataViewController.secondRatioTextField.toDouble.toValue
        let exp2 = buyerDataViewController.calculateTotalMonthlyIncome() * buyerDataViewController.firstRatioTextField.toDouble.toValue
        return exp1 - exp2 - buyerDataViewController.buyerMonthlyDebtTextField.toDouble
    }
    
    var lowerMonthlyDebt: Double{
        let exp1 = (buyerDataViewController.firstRatioIncome - (buyerDataViewController.secondRatioIncome - buyerDataViewController.buyerMonthlyDebtTextField.toDouble))
        if exp1 < 0{
            return 0
        }
        return exp1
    }
    private func showOutput(){
        print("Total Monthly Income=======>\(buyerDataViewController.calculateTotalMonthlyIncome())")
        self.calcEquityBuildUp()
        let equityController = EquityViewController.showEquity()
        var outputList: Array<OutputDetailsModel> = []
        outputList.append(contentsOf: buyerDataViewController.buyerOutputList)
        outputList.append(contentsOf: buyerDataViewController.buyerQualificationOutputList)
        outputList.append(contentsOf: maxPaymentViewController.fundsOutputList)
        var financingLimit = financingLimitViewController.financingLimitFactorList
        financingLimit.append(OutputDetailsModel.init(key: "The Limiting Factor to Qualify is:", value: isIncome ? "Income" : "Debt"))
        financingLimit.append(OutputDetailsModel.init(key: "Monthly Debt Cushion is:", value: isIncome ? monthlyDebtCushion : 0))
//        financingLimit.append(OutputDetailsModel.init(key: "If Limiting Factor is \(isIncome ? "Income" : "Debt"),Lower Monthly \(isIncome ? "Income" : "Debt") by more than", value: ""))
        financingLimit.append(OutputDetailsModel.init(key: "If Limiting Factor is Debt, Lower Monthly Debt by more than", value: lowerMonthlyDebt))

        outputList.append(contentsOf: financingLimit)
        outputList.append(contentsOf: maxPaymentViewController.cashClosingOutputList)
        equityController.dataModel.list = outputList
        equityController.dataModel.equityList = self.equityBuildUpList
        equityController.dataModel.title = LocalizableKey.navigationTitle.maxQualify
        equityController.isHideTotalView = true
        equityController.isHideEquityButton = false
        equityController.dataModel.type = .maximumQualification
        equityController.contactModel = self.contactModel
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: equityController)
        self.present(navigationController, animated: true, completion: nil)
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == MaxBuyerDataTableViewController.segueIdentifier{
            buyerDataViewController = segue.destination as? MaxBuyerDataTableViewController
        }else if segue.identifier == MaxPaymentTableViewController.segueIdentifier{
            maxPaymentViewController = segue.destination as? MaxPaymentTableViewController
        }else if segue.identifier == MaxQualificationTableViewController.segueIdentifier{
            financingLimitViewController = segue.destination as? MaxQualificationTableViewController
        } 
    }
}
extension MaximumQualifyContainerViewController{
    private func calcEquityBuildUp(){
        let value = financingLimitViewController.maxHomePrice
        let loanBalance = financingLimitViewController.maxLoanAmount
        let equity = financingLimitViewController.downPMTAvail
        equityBuildUpList.append(EquityModel.init(value: value, balance: loanBalance, equity: equity))
        for i in 1...10{
            let interestRate = financingLimitViewController.interestRateTextField.toDouble
            let paymentperyear = financingLimitViewController.paymentPerYearTextField.toDouble
//            let term = financingLimitViewController.termYearTextField.toDouble
            let principleInterest = buyerDataViewController.princplInterestPortion
            let nper = (Double(i) * paymentperyear)
            let loanBalanceFV = Formula.FVCalc(rate: interestRate.toValue/paymentperyear, nper: nper, pmt: -principleInterest,fv: loanBalance).removeMinus
            let valueFV = Formula.FVCalc(rate: Double(3).toValue, nper: Double(i), pmt: 0,fv: value).removeMinus
            let _equity = (valueFV - loanBalanceFV).removeMinus
            equityBuildUpList.append(EquityModel.init(value: valueFV, balance: loanBalanceFV, equity: _equity))
        }
    }
}
extension MaximumQualifyContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getMaxQualify()
    }
}
extension MaximumQualifyContainerViewController{
    var paramViewMaxQualify: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.maximumView, contactId: self.contactModel!.contactId!)
    }
    private func getMaxQualify(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewMaxQualify, encoding: .queryString, { (response: ResponseModel<MaxQualificationModel>) in
            self.buyerDataViewController.maxQualifyDetails = response.item?.response
            self.maxPaymentViewController.maxQualifyDetails = response.item?.response
            self.financingLimitViewController.maxQualifyDetails = response.item?.response
        }) { (error) in
            self.showError(error)
        }
    }
}
extension MaximumQualifyContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.contactId: self.contactModel!.contactId!]
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(Constants.reportNames.maxQualification, forKey: APIKey.common.key.action)
        dict.updateValue(Date().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.currentDate)
         dict.updateValue(Constants.tagValue.maximumAdd, forKey: APIKey.common.key.tag)
        buyerDataViewController.updateParm.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        maxPaymentViewController.updateParm.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        financingLimitViewController.updateParm.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
