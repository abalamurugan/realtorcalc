//
//  LenderOutputViewController.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderOutputViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showLenderOutput"
    
    @IBOutlet weak var tableView: UITableView!
    
    var list: Array<LenderModel> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LenderOutputViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LenderOutputTableViewCell.reuseIdentifier, for: indexPath) as! LenderOutputTableViewCell
        cell.set(list[indexPath.row])
        return cell
    }
}
