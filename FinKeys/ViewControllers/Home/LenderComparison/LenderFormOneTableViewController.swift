//
//  LenderFormOneTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderFormOneTableViewController: UITableViewController,StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFormOne"
    
    @IBOutlet weak var loanTypeTextField: UITextField!
    @IBOutlet weak var paymentTypeTextField: UITextField!
    @IBOutlet weak var loanAmountTextField: UITextField!
    @IBOutlet weak var interestRateTextField: UITextField!
    @IBOutlet weak var termTextField: UITextField!
    @IBOutlet weak var paymentPerYearTextField: UITextField!
    @IBOutlet weak var maxLoanValueTextField: UITextField!
    @IBOutlet weak var discountPointsTextField: UITextField!
    @IBOutlet weak var applicationFeesTextField: UITextField!
    @IBOutlet weak var ownerTitleTextField: UITextField!
    @IBOutlet weak var lendersTitleTextField: UITextField!
    @IBOutlet weak var originationFeeTextField: UITextField!
    @IBOutlet weak var commitmentFeeTextField: UITextField!
    @IBOutlet weak var settlementFeeTextField: UITextField!
    @IBOutlet weak var processingFeeTextField: UITextField!
    @IBOutlet weak var underwritingFeeTextField: UITextField!
    @IBOutlet weak var appraisalFeeTextField: UITextField!
    @IBOutlet weak var miscellaneousFeeTextField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultValues()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    func defaultValues(_ amount: String = "460000"){
        loanAmountTextField.text = amount
        interestRateTextField.text = "5"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "95"
        discountPointsTextField.text = "1.0"
        applicationFeesTextField.text = "300"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "3000"
        settlementFeeTextField.text = "400"
        processingFeeTextField.text = "395"
        underwritingFeeTextField.text = "425"
        appraisalFeeTextField.text = "450"
        miscellaneousFeeTextField.text = "500"
    }

     
}
extension LenderFormOneTableViewController{
    var pmtMonthly: Double{
        let pmt = Formula.calculatePMT(interestRateTextField.toDouble, paymentPerYearTextField.toDouble, termTextField.toDouble, loanAmountTextField.toDouble)
        if pmt < 0{
            return pmt.magnitude
        }
        return pmt
    }
    var interestCost: Double{
        let interest = interestRateTextField.toDouble
        return loanAmountTextField.toDouble * interest/100
    }
    var loanCost: Double{
        var cost: Double = 0
        let discount = loanAmountTextField.toDouble * discountPointsTextField.toDouble/100
        let commitment = commitmentFeeTextField.toDouble * loanAmountTextField.toDouble/100
        cost += discount
        cost += applicationFeesTextField.toDouble
        cost += lendersTitleTextField.toDouble
        cost += originationFeeTextField.toDouble
        cost += commitment
        cost += settlementFeeTextField.toDouble
        cost += processingFeeTextField.toDouble
        cost += underwritingFeeTextField.toDouble
        cost += appraisalFeeTextField.toDouble
        cost += miscellaneousFeeTextField.toDouble
        return cost

    }
    var apr: Double{
        let amount = loanAmountTextField.toDouble - loanCost
        return Formula.rateCalculation(term: termTextField.toDouble, paymentPerYear: paymentPerYearTextField.toDouble, payment: pmtMonthly, loanAmount: amount)
    }
    func clear(){
        loanTypeTextField.text = nil
        paymentTypeTextField.text = nil
        loanAmountTextField.text = nil
        interestRateTextField.text = nil
        termTextField.text = nil
        paymentPerYearTextField.text = nil
        maxLoanValueTextField.text = nil
        discountPointsTextField.text = nil
        applicationFeesTextField.text = nil
        ownerTitleTextField.text = nil
        lendersTitleTextField.text = nil
        originationFeeTextField.text = nil
        commitmentFeeTextField.text = nil
        settlementFeeTextField.text = nil
        processingFeeTextField.text = nil
        underwritingFeeTextField.text = nil
        appraisalFeeTextField.text = nil
        miscellaneousFeeTextField.text = nil
    }
}
