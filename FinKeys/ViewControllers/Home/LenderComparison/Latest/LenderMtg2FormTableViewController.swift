//
//  LenderMtg2FormTableViewController.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 26/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderMtg2FormTableViewController: UITableViewController,StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showMtg1"
    
    @IBOutlet weak var loanTypeTextField: UITextField!
    @IBOutlet weak var paymentTypeTextField: UITextField!
    @IBOutlet weak var loanAmountTextField: UITextField!
    @IBOutlet weak var interestRateTextField: UITextField!
    @IBOutlet weak var termTextField: UITextField!
    @IBOutlet weak var paymentPerYearTextField: UITextField!
    @IBOutlet weak var maxLoanValueTextField: UITextField!
    @IBOutlet weak var discountPointsTextField: UITextField!
    @IBOutlet weak var applicationFeesTextField: UITextField!
    @IBOutlet weak var ownerTitleTextField: UITextField!
    @IBOutlet weak var lendersTitleTextField: UITextField!
    @IBOutlet weak var originationFeeTextField: UITextField!
    @IBOutlet weak var commitmentFeeTextField: UITextField!
    @IBOutlet weak var settlementFeeTextField: UITextField!
    @IBOutlet weak var processingFeeTextField: UITextField!
    @IBOutlet weak var underwritingFeeTextField: UITextField!
    @IBOutlet weak var appraisalFeeTextField: UITextField!
    @IBOutlet weak var miscellaneousFeeTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    

}
