//
//  LenderMtg1FormTableViewController.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 26/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
enum LenderFormType {
    case mtg1
    case mtg2
    case mtg3
}
class LenderMtg1FormTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showMtg1"
    static let segueIdentifier1: String = "showMtg2"
    static let segueIdentifier2: String = "showMtg3"
    
    var type: LenderFormType = .mtg1
    var lenderType: Int?
    
    @IBOutlet weak var loanTypeTextField: UITextField!
    @IBOutlet weak var paymentTypeTextField: UITextField!
    @IBOutlet weak var loanAmountTextField: UITextField!
    @IBOutlet weak var interestRateTextField: UITextField!
    @IBOutlet weak var termTextField: UITextField!
    @IBOutlet weak var paymentPerYearTextField: UITextField!
    @IBOutlet weak var maxLoanValueTextField: UITextField!
    @IBOutlet weak var discountPointsTextField: UITextField!
    @IBOutlet weak var applicationFeesTextField: UITextField!
    @IBOutlet weak var ownerTitleTextField: UITextField!
    @IBOutlet weak var lendersTitleTextField: UITextField!
    @IBOutlet weak var originationFeeTextField: UITextField!
    @IBOutlet weak var commitmentFeeTextField: UITextField!
    @IBOutlet weak var settlementFeeTextField: UITextField!
    @IBOutlet weak var processingFeeTextField: UITextField!
    @IBOutlet weak var underwritingFeeTextField: UITextField!
    @IBOutlet weak var appraisalFeeTextField: UITextField!
    @IBOutlet weak var miscellaneousFeeTextField: UITextField!
    @IBOutlet weak var withQualifySwitch: UISwitch!
    @IBOutlet weak var withoutQualifySwitch: UISwitch!
    @IBOutlet weak var mtgInsSwitch: UISwitch!
    @IBOutlet weak var adjustPeriodTextField: UITextField!
    @IBOutlet weak var periodicIntGapTextField: UITextField!
    @IBOutlet weak var lifeTimeIntGapTextField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    private func configUI(){
        self.tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 70, right: 0)
        self.loanTypeTextField.delegate = self
        self.paymentTypeTextField.delegate = self
    }
    func defaultMtg1Values(){
        loanAmountTextField.text = "460000"
        interestRateTextField.text = "5"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "95"
        discountPointsTextField.text = "1.0"
        applicationFeesTextField.text = "300"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "3000"
        settlementFeeTextField.text = "400"
        processingFeeTextField.text = "395"
        underwritingFeeTextField.text = "425"
        appraisalFeeTextField.text = "450"
        miscellaneousFeeTextField.text = "500"
    }
    func defaultMtg2Values(){
        loanAmountTextField.text = "460000"
        interestRateTextField.text = "4.875"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "95"
        discountPointsTextField.text = "1.0"
        applicationFeesTextField.text = "300"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "3000"
        settlementFeeTextField.text = "400"
        processingFeeTextField.text = "350"
        underwritingFeeTextField.text = "425"
        appraisalFeeTextField.text = "450"
        miscellaneousFeeTextField.text = "400"
    }
    func defaultMtg3Values(){
        loanAmountTextField.text = "460000"
        interestRateTextField.text = "3.75"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "95"
        discountPointsTextField.text = "1.0"
        applicationFeesTextField.text = "300"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "3000"
        settlementFeeTextField.text = "400"
        processingFeeTextField.text = "350"
        underwritingFeeTextField.text = "425"
        appraisalFeeTextField.text = "450"
        miscellaneousFeeTextField.text = "400"
    }
    func defaultL2Mtg1Values(){
        loanAmountTextField.text = "460000"
        interestRateTextField.text = "4.5"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "100"
        discountPointsTextField.text = "0.0"
        applicationFeesTextField.text = "400"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "2000"
        settlementFeeTextField.text = "400"
        processingFeeTextField.text = "395"
        underwritingFeeTextField.text = "425"
        appraisalFeeTextField.text = "450"
        miscellaneousFeeTextField.text = "500"
    }
    func defaultL2Mtg2Values(){
        loanAmountTextField.text = "460000"
        interestRateTextField.text = "4.875"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "95"
        discountPointsTextField.text = "1.0"
        applicationFeesTextField.text = "300"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "2000"
        settlementFeeTextField.text = "400"
        processingFeeTextField.text = "350"
        underwritingFeeTextField.text = "425"
        appraisalFeeTextField.text = "450"
        miscellaneousFeeTextField.text = "400"
    }
    func defaultL2Mtg3Values(){
        loanAmountTextField.text = "460000"
        interestRateTextField.text = "5.25"
        termTextField.text = "30"
        paymentPerYearTextField.text = "12"
        maxLoanValueTextField.text = "95"
        discountPointsTextField.text = "1.5"
        applicationFeesTextField.text = "300"
        lendersTitleTextField.text = "1120"
        originationFeeTextField.text = "3500"
        settlementFeeTextField.text = "500"
        processingFeeTextField.text = "450"
        underwritingFeeTextField.text = "550"
        appraisalFeeTextField.text = "650"
        miscellaneousFeeTextField.text = "650"
    }
}
extension LenderMtg1FormTableViewController{
    func mapping(model: LenderMtgModel?){
        guard let item = model else {
            return
        }
        loanAmountTextField.text = item.loanAmount?.readableFormat
        interestRateTextField.text = item.interestRate?.toNumber.toPercentage
        termTextField.text = item.amortTerm
        paymentPerYearTextField.text = item.payPerYear
        maxLoanValueTextField.text = item.maxLoanToValue?.toNumber.toPercentage
        discountPointsTextField.text = item.discountPoints?.toNumber.toPercentage
        applicationFeesTextField.text = item.applicationFee?.readableFormat
        lendersTitleTextField.text = item.lenderTitlePolicy?.readableFormat
        originationFeeTextField.text = item.originFee?.readableFormat
        settlementFeeTextField.text = item.settleFee?.readableFormat
        processingFeeTextField.text = item.processFee?.readableFormat
        underwritingFeeTextField.text = item.underWriteFee?.readableFormat
        appraisalFeeTextField.text = item.apprFee?.readableFormat
        miscellaneousFeeTextField.text = item.miscelFee?.readableFormat
        adjustPeriodTextField.text = item.adjustPeriod
        periodicIntGapTextField.text = item.periodicIntCap?.toNumber.toPercentage
        lifeTimeIntGapTextField.text = item.lifeTimeIntCap?.toNumber.toPercentage
        loanTypeTextField.text = item.loanType
        paymentTypeTextField.text = item.paymentType
    }
}
extension LenderMtg1FormTableViewController{
    var monthlyPayment: Double{
        let pmt = Formula.calculatePMT(interestRateTextField.toDouble, paymentPerYearTextField.toDouble, termTextField.toDouble, loanAmountTextField.toDouble)
        if pmt < 0{
            return pmt.magnitude
        }
        return pmt
    }
    var firstYearIntCost: Double{
        return (loanAmountTextField.toDouble * interestRateTextField.toDouble).toValue
    }
    var loanCostCharges: Double{
        var cost: Double = 0
        let discount = loanAmountTextField.toDouble * discountPointsTextField.toDouble/100
        let commitment = commitmentFeeTextField.toDouble * loanAmountTextField.toDouble/100
        cost += discount
        cost += applicationFeesTextField.toDouble
        cost += lendersTitleTextField.toDouble
        cost += originationFeeTextField.toDouble
        cost += commitment
        cost += settlementFeeTextField.toDouble
        cost += processingFeeTextField.toDouble
        cost += underwritingFeeTextField.toDouble
        cost += appraisalFeeTextField.toDouble
        cost += miscellaneousFeeTextField.toDouble
        return cost
    }
    var apr: Double{
        let amount = loanAmountTextField.toDouble - loanCostCharges
        return Formula.rateCalculation(term: termTextField.toDouble, paymentPerYear: paymentPerYearTextField.toDouble, payment: monthlyPayment, loanAmount: amount)
    }
}
extension LenderMtg1FormTableViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanTypeTextField{
            self.showBottomPicker(delegate: self, items: Constants.loanPlan, tag: 0, title: "Loan Type")
            return false
        }else if textField == paymentTypeTextField{
            self.showBottomPicker(delegate: self, items: Constants.paymentType, tag: 1, title: "Payment Type")
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestRateTextField || textField == discountPointsTextField || textField == maxLoanValueTextField || textField == periodicIntGapTextField || textField == lifeTimeIntGapTextField || textField == commitmentFeeTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension LenderMtg1FormTableViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? BottomPickerModel
        if viewController.dataModel.tag == 0{
            loanTypeTextField.text = model?.value
        }else if viewController.dataModel.tag == 1{
            paymentTypeTextField.text = model?.value
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension LenderMtg1FormTableViewController{
    var updateL1Mtg1Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(loanAmountTextField.toDouble, forKey: key.loanAmount1L1)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate1L1)
        dict.updateValue(termTextField.toDouble, forKey: key.amortTerm1L1)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear1L1)
        dict.updateValue(maxLoanValueTextField.toDouble, forKey: key.maxLoanToValue1L1)
        dict.updateValue(discountPointsTextField.toDouble, forKey: key.discountPoint1L1)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee1L1)
        dict.updateValue(lendersTitleTextField.toDouble, forKey: key.lenderTitlePolicy1L1)
        dict.updateValue(originationFeeTextField.toDouble, forKey: key.originFee1L1)
        dict.updateValue(settlementFeeTextField.toDouble, forKey: key.settleFee1L1)
        dict.updateValue(processingFeeTextField.toDouble, forKey: key.processFee1L1)
        dict.updateValue(underwritingFeeTextField.toDouble, forKey: key.underFee1L1)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee1L1)
        dict.updateValue(miscellaneousFeeTextField.toDouble, forKey: key.misceFee1L1)
        dict.updateValue(adjustPeriodTextField.toDouble, forKey: key.adjustPeriod1L1)
        dict.updateValue(loanTypeTextField.text ?? "", forKey: key.loanType1L1)
        dict.updateValue(withQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWQualify1L1)
        dict.updateValue(withoutQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWOQualify1L1)
        dict.updateValue(mtgInsSwitch.isOn ? "1" : "2", forKey: key.mtgInsOver1L1)
        dict.updateValue(periodicIntGapTextField.toDouble, forKey: key.periodicIntCap1)
        dict.updateValue(lifeTimeIntGapTextField.toDouble, forKey: key.lifeTimeIntCap1)
        return dict
    }
    var updateL1Mtg2Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(loanAmountTextField.toDouble, forKey: key.loanAmount2L1)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate2L1)
        dict.updateValue(termTextField.toDouble, forKey: key.amortTerm2L1)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear2L1)
        dict.updateValue(maxLoanValueTextField.toDouble, forKey: key.maxLoanToValue2L1)
        dict.updateValue(discountPointsTextField.toDouble, forKey: key.discountPoint2L1)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee2L1)
        dict.updateValue(lendersTitleTextField.toDouble, forKey: key.lenderTitlePolicy2L1)
        dict.updateValue(originationFeeTextField.toDouble, forKey: key.originFee2L1)
        dict.updateValue(settlementFeeTextField.toDouble, forKey: key.settleFee2L1)
        dict.updateValue(processingFeeTextField.toDouble, forKey: key.processFee2L1)
        dict.updateValue(underwritingFeeTextField.toDouble, forKey: key.underFee2L1)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee2L1)
        dict.updateValue(miscellaneousFeeTextField.toDouble, forKey: key.misceFee2L1)
        dict.updateValue(adjustPeriodTextField.toDouble, forKey: key.adjustPeriod2L1)
        dict.updateValue(loanTypeTextField.text ?? "", forKey: key.loanType2L1)
        dict.updateValue(withQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWQualify2L1)
        dict.updateValue(withoutQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWOQualify2L1)
        dict.updateValue(mtgInsSwitch.isOn ? "1" : "2", forKey: key.mtgInsOver2L1)
        dict.updateValue(periodicIntGapTextField.toDouble, forKey: key.periodicIntCap1)
        dict.updateValue(lifeTimeIntGapTextField.toDouble, forKey: key.lifeTimeIntCap1)
        return dict
    }
    var updateL1Mtg3Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(loanAmountTextField.toDouble, forKey: key.loanAmount3L1)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate3L1)
        dict.updateValue(termTextField.toDouble, forKey: key.amortTerm3L1)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear3L1)
        dict.updateValue(maxLoanValueTextField.toDouble, forKey: key.maxLoanToValue3L1)
        dict.updateValue(discountPointsTextField.toDouble, forKey: key.discountPoint3L1)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee3L1)
        dict.updateValue(lendersTitleTextField.toDouble, forKey: key.lenderTitlePolicy3L1)
        dict.updateValue(originationFeeTextField.toDouble, forKey: key.originFee3L1)
        dict.updateValue(settlementFeeTextField.toDouble, forKey: key.settleFee3L1)
        dict.updateValue(processingFeeTextField.toDouble, forKey: key.processFee3L1)
        dict.updateValue(underwritingFeeTextField.toDouble, forKey: key.underFee3L1)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee3L1)
        dict.updateValue(miscellaneousFeeTextField.toDouble, forKey: key.misceFee3L1)
        dict.updateValue(adjustPeriodTextField.toDouble, forKey: key.adjustPeriod3L1)
         dict.updateValue(loanTypeTextField.text ?? "", forKey: key.loanType3L1)
        dict.updateValue(withQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWQualify3L1)
        dict.updateValue(withoutQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWOQualify3L1)
        dict.updateValue(mtgInsSwitch.isOn ? "1" : "2", forKey: key.mtgInsOver3L1)
//        dict.updateValue(periodicIntGapTextField.toDouble, forKey: key.periodicIntCap1)
//        dict.updateValue(lifeTimeIntGapTextField.toDouble, forKey: key.lifeTimeIntCap1)
        return dict
    }
    var updateL2Mtg1Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(loanAmountTextField.toDouble, forKey: key.loanAmount1L2)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate1L2)
        dict.updateValue(termTextField.toDouble, forKey: key.amortTerm1L2)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear1L2)
        dict.updateValue(maxLoanValueTextField.toDouble, forKey: key.maxLoanToValue1L2)
        dict.updateValue(discountPointsTextField.toDouble, forKey: key.discountPoint1L2)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee1L2)
        dict.updateValue(lendersTitleTextField.toDouble, forKey: key.lenderTitlePolicy1L2)
        dict.updateValue(originationFeeTextField.toDouble, forKey: key.originFee1L2)
        dict.updateValue(settlementFeeTextField.toDouble, forKey: key.settleFee1L2)
        dict.updateValue(processingFeeTextField.toDouble, forKey: key.processFee1L2)
        dict.updateValue(underwritingFeeTextField.toDouble, forKey: key.underFee1L2)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee1L2)
        dict.updateValue(miscellaneousFeeTextField.toDouble, forKey: key.misceFee1L2)
        dict.updateValue(adjustPeriodTextField.toDouble, forKey: key.adjustPeriod1L2)
        dict.updateValue(loanTypeTextField.text ?? "", forKey: key.loanType1L2)
        dict.updateValue(withQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWQualify1L2)
        dict.updateValue(withoutQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWOQualify1L2)
        dict.updateValue(mtgInsSwitch.isOn ? "1" : "2", forKey: key.mtgInsOver1L2)
        dict.updateValue(periodicIntGapTextField.toDouble, forKey: key.periodicIntCap2)
        dict.updateValue(lifeTimeIntGapTextField.toDouble, forKey: key.lifeTimeIntCap2)
        return dict
    }
    var updateL2Mtg2Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(loanAmountTextField.toDouble, forKey: key.loanAmount2L2)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate2L2)
        dict.updateValue(termTextField.toDouble, forKey: key.amortTerm2L2)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear2L2)
        dict.updateValue(maxLoanValueTextField.toDouble, forKey: key.maxLoanToValue2L2)
        dict.updateValue(discountPointsTextField.toDouble, forKey: key.discountPoint2L2)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee2L2)
        dict.updateValue(lendersTitleTextField.toDouble, forKey: key.lenderTitlePolicy2L2)
        dict.updateValue(originationFeeTextField.toDouble, forKey: key.originFee2L2)
        dict.updateValue(settlementFeeTextField.toDouble, forKey: key.settleFee2L2)
        dict.updateValue(processingFeeTextField.toDouble, forKey: key.processFee2L2)
        dict.updateValue(underwritingFeeTextField.toDouble, forKey: key.underFee2L2)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee2L2)
        dict.updateValue(miscellaneousFeeTextField.toDouble, forKey: key.misceFee2L2)
        dict.updateValue(adjustPeriodTextField.toDouble, forKey: key.adjustPeriod2L2)
        dict.updateValue(loanTypeTextField.text ?? "", forKey: key.loanType2L2)
        dict.updateValue(withQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWQualify2L2)
        dict.updateValue(withoutQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWOQualify2L2)
        dict.updateValue(mtgInsSwitch.isOn ? "1" : "2", forKey: key.mtgInsOver2L2)
        dict.updateValue(periodicIntGapTextField.toDouble, forKey: key.periodicIntCap2)
        dict.updateValue(lifeTimeIntGapTextField.toDouble, forKey: key.lifeTimeIntCap2)
        return dict
    }
    var updateL2Mtg3Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(loanAmountTextField.toDouble, forKey: key.loanAmount3L2)
        dict.updateValue(interestRateTextField.toDouble, forKey: key.interestRate3L2)
        dict.updateValue(termTextField.toDouble, forKey: key.amortTerm3L2)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear3L2)
        dict.updateValue(maxLoanValueTextField.toDouble, forKey: key.maxLoanToValue3L2)
        dict.updateValue(discountPointsTextField.toDouble, forKey: key.discountPoint3L2)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee3L2)
        dict.updateValue(lendersTitleTextField.toDouble, forKey: key.lenderTitlePolicy3L2)
        dict.updateValue(originationFeeTextField.toDouble, forKey: key.originFee3L2)
        dict.updateValue(settlementFeeTextField.toDouble, forKey: key.settleFee3L2)
        dict.updateValue(processingFeeTextField.toDouble, forKey: key.processFee3L2)
        dict.updateValue(underwritingFeeTextField.toDouble, forKey: key.underFee3L2)
        dict.updateValue(appraisalFeeTextField.toDouble, forKey: key.apprFee3L2)
        dict.updateValue(miscellaneousFeeTextField.toDouble, forKey: key.misceFee3L2)
        dict.updateValue(adjustPeriodTextField.toDouble, forKey: key.adjustPeriod3L2)
        dict.updateValue(loanTypeTextField.text ?? "", forKey: key.loanType3L2)
        dict.updateValue(withQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWQualify3L2)
        dict.updateValue(withoutQualifySwitch.isOn ? "1" : "2", forKey: key.assumeWOQualify3L2)
        dict.updateValue(mtgInsSwitch.isOn ? "1" : "2", forKey: key.mtgInsOver3L2)
//        dict.updateValue(periodicIntGapTextField.toDouble, forKey: key.periodicIntCap2)
//        dict.updateValue(lifeTimeIntGapTextField.toDouble, forKey: key.lifeTimeIntCap2)
        return dict
    }
}
