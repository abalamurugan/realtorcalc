//
//  LenderMtgContainerViewController.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 26/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderMtgContainerViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showLender1"
    static let segueIdentifier1: String = "showLender2"

    @IBOutlet weak var mtgForm1ContainerView: UIView!
    @IBOutlet weak var mtgForm2ContainerView: UIView!
    @IBOutlet weak var mtgForm3ContainerView: UIView!
    @IBOutlet weak var lenderTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var officerTextField: UITextField!

    
    var mtg1ViewController: LenderMtg1FormTableViewController!
    var mtg2ViewController: LenderMtg1FormTableViewController!
    var mtg3ViewController: LenderMtg1FormTableViewController!
    
    var lenderInfoModel: LenderInfoModel?{
        didSet{
            self.mapping()
        }
    }
    var lenderType: Int?
//    {
//        didSet{
//            if !mtgForm1ContainerView.isHidden{
//                if lenderType ?? 0 == 0{
//                    self.mtg1ViewController.mapping(model: self.lenderInfoModel?.lender1Mtg1)
//                }else{
//                    self.mtg1ViewController.mapping(model: self.lenderInfoModel?.lender2Mtg1)
//                }
//            }else if !mtgForm2ContainerView.isHidden{
//                if lenderType ?? 0 == 0{
//                    self.mtg2ViewController.mapping(model: self.lenderInfoModel?.lender1Mtg2)
//                }else{
//                    self.mtg2ViewController.mapping(model: self.lenderInfoModel?.lender2Mtg2)
//                }
//            }else if !mtgForm3ContainerView.isHidden{
//                if lenderType ?? 0 == 0{
//                    self.mtg3ViewController.mapping(model: self.lenderInfoModel?.lender1Mtg3)
////                    self.mtg3ViewController.defaultMtg3Values()
//                }else{
////                    self.mtg3ViewController.defaultL2Mtg3Values()
//                    self.mtg3ViewController.mapping(model: self.lenderInfoModel?.lender2Mtg3)
//                }
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
//        if lenderType ?? 0 == 0{
//            self.mtg1ViewController.defaultMtg1Values()
//        }else{
//            self.mtg1ViewController.defaultL2Mtg1Values()
//        }
        // Do any additional setup after loading the view.
    }
    private func configUI(){
        self.mtgForm1ContainerView.isHidden = false
        self.mtgForm2ContainerView.isHidden = true
        self.mtgForm3ContainerView.isHidden = true
    }
    @IBAction func segmentAction(_ sender: UISegmentedControl){
        self.mtgForm1ContainerView.isHidden = true
        self.mtgForm2ContainerView.isHidden = true
        self.mtgForm3ContainerView.isHidden = true
        if sender.selectedSegmentIndex == 0{
            mtg1ViewController.type = .mtg1
            self.mtgForm1ContainerView.isHidden = false
//            if lenderType ?? 0 == 0{
//                self.mtg1ViewController.mapping(model: lenderInfoModel?.lender1Mtg1)
////                self.mtg1ViewController.defaultMtg1Values()
//            }else{
//                self.mtg1ViewController.mapping(model: lenderInfoModel?.lender2Mtg1)
////                self.mtg1ViewController.defaultL2Mtg1Values()
//            }
        }else if sender.selectedSegmentIndex == 1{
            mtg2ViewController.type = .mtg2
            self.mtgForm2ContainerView.isHidden = false
//            if lenderType ?? 0 == 0{
//                self.mtg2ViewController.mapping(model: lenderInfoModel?.lender1Mtg2)
////                self.mtg2ViewController.defaultMtg2Values()
//            }else{
//                self.mtg2ViewController.mapping(model: lenderInfoModel?.lender2Mtg2)
////                self.mtg2ViewController.defaultL2Mtg2Values()
//            }
        }else if sender.selectedSegmentIndex == 2{
            mtg3ViewController.type = .mtg3
            self.mtgForm3ContainerView.isHidden = false
//            if lenderType ?? 0 == 0{
//                self.mtg3ViewController.mapping(model: lenderInfoModel?.lender1Mtg3)
////                self.mtg3ViewController.defaultMtg3Values()
//            }else{
//                self.mtg3ViewController.mapping(model: lenderInfoModel?.lender2Mtg3)
////                self.mtg3ViewController.defaultL2Mtg3Values()
//            }
        }
    }
    

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == LenderMtg1FormTableViewController.segueIdentifier{
            mtg1ViewController = segue.destination as? LenderMtg1FormTableViewController
        }else if segue.identifier == LenderMtg1FormTableViewController.segueIdentifier1{
            mtg2ViewController = segue.destination as? LenderMtg1FormTableViewController
        }else if segue.identifier == LenderMtg1FormTableViewController.segueIdentifier2{
            mtg3ViewController = segue.destination as? LenderMtg1FormTableViewController
        }
    }
}
extension LenderMtgContainerViewController{
    var lenderOneMtgOne: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        list.append(OutputDetailsModel.init(key: "Monthly Payment", value: mtg1ViewController.monthlyPayment))
        list.append(OutputDetailsModel.init(key: "1st Year Interest Cost", value: mtg1ViewController.firstYearIntCost))
        list.append(OutputDetailsModel.init(key: "Loan Costs/Charges", value: mtg1ViewController.loanCostCharges))
        list.append(OutputDetailsModel.init(key: "APR", value: mtg1ViewController.apr.toPercentage))
        return list
    }
    var lenderOneMtgTwo: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        list.append(OutputDetailsModel.init(key: "Monthly Payment", value: mtg2ViewController.monthlyPayment))
        list.append(OutputDetailsModel.init(key: "1st Year Interest Cost", value: mtg2ViewController.firstYearIntCost))
        list.append(OutputDetailsModel.init(key: "Loan Costs/Charges", value: mtg2ViewController.loanCostCharges))
        list.append(OutputDetailsModel.init(key: "APR", value: mtg2ViewController.apr.toPercentage))
        return list
    }
    var lenderOneMtgThree: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        list.append(OutputDetailsModel.init(key: "Monthly Payment", value: mtg3ViewController.monthlyPayment))
        list.append(OutputDetailsModel.init(key: "1st Year Interest Cost", value: mtg3ViewController.firstYearIntCost))
        list.append(OutputDetailsModel.init(key: "Loan Costs/Charges", value: mtg3ViewController.loanCostCharges))
        list.append(OutputDetailsModel.init(key: "APR", value: mtg3ViewController.apr.toPercentage))
        return list
    }
    
}
extension LenderMtgContainerViewController{
    private func mapping(){
        let item = lenderInfoModel?.lender1
        lenderTextField.text = item?.lender
        phoneTextField.text = item?.phone
        officerTextField.text = item?.officer
    }
    var updateL1Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(lenderTextField.text ?? "", forKey: key.lender1)
        dict.updateValue(phoneTextField.text ?? "", forKey: key.phone1)
        dict.updateValue(officerTextField.text ?? "", forKey: key.officer1)
        return dict
    }
    var updateL2Param: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        let key = APIKey.lender.key.self
        dict.updateValue(lenderTextField.text ?? "", forKey: key.lender2)
        dict.updateValue(phoneTextField.text ?? "", forKey: key.phone2)
        dict.updateValue(officerTextField.text ?? "", forKey: key.phone2)
        return dict
    }
    
}
