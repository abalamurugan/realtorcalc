//
//  LenderCompContainerViewController.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 26/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderCompContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showLenderComp"
    
    @IBOutlet weak var lenderOneContainerView: UIView!
    @IBOutlet weak var lenderTwoContainerView: UIView!
    
    var lenderOneViewController: LenderMtgContainerViewController!
    var lenderTwoViewController: LenderMtgContainerViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    private func configUI(){
        self.lenderTwoContainerView.isHidden = true
        self.lenderOneContainerView.isHidden = false
        self.title = LocalizableKey.navigationTitle.lenderComparison
    }
    @IBAction func segmentAction(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            lenderTwoContainerView.isHidden = true
            lenderOneContainerView.isHidden = false
            lenderOneViewController.lenderType = 0
        }else if sender.selectedSegmentIndex == 1{
            lenderTwoContainerView.isHidden = false
            lenderOneContainerView.isHidden = true
            lenderTwoViewController.lenderType = 1
        }
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    @IBAction func calculateAction(_ sender: UIButton){
        self.postFormValues()
//        self.performSegue(withIdentifier: InvestmentOutputViewController.segueIdentifier, sender: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == LenderMtgContainerViewController.segueIdentifier{
            lenderOneViewController = segue.destination as? LenderMtgContainerViewController
        }else if segue.identifier == LenderMtgContainerViewController.segueIdentifier1{
            lenderTwoViewController = segue.destination as? LenderMtgContainerViewController
        }else if segue.identifier == InvestmentOutputViewController.segueIdentifier{
            let outputController = segue.destination as! InvestmentOutputViewController
            outputController.dataModel.type = .lender
            outputController.dataModel.outputList = outputList
            outputController.dataModel.contactModel = self.contactModel
        }
    }

}

extension LenderCompContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.lenderOneViewController.mtg1ViewController.defaultMtg1Values()
        self.lenderOneViewController.mtg2ViewController.defaultMtg2Values()
        self.lenderOneViewController.mtg3ViewController.defaultMtg3Values()
        self.lenderTwoViewController.mtg1ViewController.defaultL2Mtg1Values()
        self.lenderTwoViewController.mtg2ViewController.defaultL2Mtg2Values()
        self.lenderTwoViewController.mtg3ViewController.defaultL2Mtg3Values()

        self.getLenderDetails()
    }
}
extension LenderCompContainerViewController{
    var paramViewLender: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.lenderview, contactId: self.contactModel!.contactId!)
    }
    private func getLenderDetails(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewLender, encoding: .queryString, { (response: ResponseModel<LenderCompModel>) in
            let item = response.item?.response
           
            self.lenderOneViewController.mtg1ViewController.mapping(model: item?.lender1Mtg1)
            self.lenderOneViewController.mtg2ViewController.mapping(model: item?.lender1Mtg2)
            self.lenderOneViewController.mtg3ViewController.mapping(model: item?.lender1Mtg3)
            
            self.lenderTwoViewController.mtg1ViewController.mapping(model: item?.lender2Mtg1)
            self.lenderTwoViewController.mtg2ViewController.mapping(model: item?.lender2Mtg2)
            self.lenderTwoViewController.mtg3ViewController.mapping(model: item?.lender2Mtg3)

            self.lenderOneViewController.lenderInfoModel = response.item?.response
            self.lenderTwoViewController.lenderInfoModel = response.item?.response
        }) { (error) in
            self.showError(error)
        }
}
}
extension LenderCompContainerViewController{
    var outputList: Array<RefinanceOutputModel>{
        var list = Array<RefinanceOutputModel>()
        list.append(RefinanceOutputModel.init(title: "Lender 1 Mtg 1", list: lenderOneViewController.lenderOneMtgOne))
        list.append(RefinanceOutputModel.init(title: "Lender 1 Mtg 2", list: lenderOneViewController.lenderOneMtgTwo))
        list.append(RefinanceOutputModel.init(title: "Lender 1 Mtg 3", list: lenderOneViewController.lenderOneMtgThree))

        list.append(RefinanceOutputModel.init(title: "Lender 2 Mtg 1", list: lenderTwoViewController.lenderOneMtgOne))
        list.append(RefinanceOutputModel.init(title: "Lender 2 Mtg 2", list: lenderTwoViewController.lenderOneMtgTwo))
        list.append(RefinanceOutputModel.init(title: "Lender 2 Mtg 3", list: lenderTwoViewController.lenderOneMtgThree))

        return list
    }
}
extension LenderCompContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.userId: UserManager.instance.userID]
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.reportNames.lender, forKey: APIKey.common.key.action)
        dict.updateValue(Constants.tagValue.lenderAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(APIKey.lender.key.mtg1, forKey: APIKey.lender.key.mtg1)
        dict.updateValue(APIKey.lender.key.mtg2, forKey: APIKey.lender.key.mtg2)
        dict.updateValue(APIKey.lender.key.mtg3, forKey: APIKey.lender.key.mtg3)

        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        lenderOneViewController.updateL1Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        lenderTwoViewController.updateL2Param.forEach { (key,value) in
            dict.updateValue(value, forKey: key)
        }
        lenderOneViewController.mtg1ViewController.updateL1Mtg1Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        lenderOneViewController.mtg2ViewController.updateL1Mtg2Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        lenderOneViewController.mtg3ViewController.updateL1Mtg3Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        lenderTwoViewController.mtg1ViewController.updateL2Mtg1Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        lenderTwoViewController.mtg2ViewController.updateL2Mtg2Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        lenderTwoViewController.mtg3ViewController.updateL2Mtg3Param.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.performSegue(withIdentifier: InvestmentOutputViewController.segueIdentifier, sender: self)
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
