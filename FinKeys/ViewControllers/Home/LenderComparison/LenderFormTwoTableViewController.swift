//
//  LenderFormTwoTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderFormTwoTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFormTwo"
    
    @IBOutlet weak var adjustPeriodTextField: UITextField!
    @IBOutlet weak var periodicInterestTextField: UITextField!
    @IBOutlet weak var lifeTimeInterestTextField: UITextField!
    @IBOutlet weak var negAmortAllowedTextField: UITextField!
    @IBOutlet weak var conversionOptionTextField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

}
