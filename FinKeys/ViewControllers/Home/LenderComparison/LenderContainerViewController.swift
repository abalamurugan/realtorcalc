//
//  LenderContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 26/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class LenderContainerViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showLender"
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var formOneContainer: UIView!
    @IBOutlet weak var formTwoContainer: UIView!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var addMoreButton: UIButton!

    
    var formOneController: LenderFormOneTableViewController!
    var formTwoController: LenderFormTwoTableViewController!
    
    var outputList: Array<LenderModel> = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        // Do any additional setup after loading the view.
    }
    private func configureUI(){
        addMoreButton.layer.cornerRadius = 5
        addMoreButton.layer.borderWidth = 1
        addMoreButton.layer.borderColor = UIColor.primaryColor.cgColor
        formOneContainer.isHidden = false
        formTwoContainer.isHidden = true
        self.title = LocalizableKey.navigationTitle.lenderComparison
    }
    @IBAction func segmentedAction(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            formOneContainer.isHidden = false
            formTwoContainer.isHidden = true
        }else if sender.selectedSegmentIndex == 1{
            formOneContainer.isHidden = true
            formTwoContainer.isHidden = false
        }
    }
    @IBAction func calculateAction(_ sender: UIButton){
        outputList.append(LenderModel.init(monthlyPayment: formOneController.pmtMonthly, interestCost: formOneController.interestCost, loanCost: formOneController.loanCost, apr: formOneController.apr))
        self.performSegue(withIdentifier: LenderOutputViewController.segueIdentifier, sender: self)
    }
    @IBAction func addMoreAction(_ sender: UIButton){
        outputList.append(LenderModel.init(monthlyPayment: formOneController.pmtMonthly, interestCost: formOneController.interestCost, loanCost: formOneController.loanCost, apr: formOneController.apr))
        formOneController.clear()
        formOneController.defaultValues("450000")
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == LenderFormOneTableViewController.segueIdentifier{
           formOneController = segue.destination as? LenderFormOneTableViewController
        }else if segue.identifier == LenderFormTwoTableViewController.segueIdentifier{
            formTwoController = segue.destination as? LenderFormTwoTableViewController
        }else if segue.identifier == LenderOutputViewController.segueIdentifier{
            let outputController = segue.destination as! LenderOutputViewController
            outputController.list = outputList
        }
    }

}
