//
//  BuyerDataTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class BuyerDataTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showBuyerData"
    
    @IBOutlet weak var buyerOneIncomeTextField : UITextField!
    @IBOutlet weak var buyerTwoIncomeTextField : UITextField!
    @IBOutlet weak var otherIncomeTextField : UITextField!
    @IBOutlet weak var nonTaxableIncomeTextField : UITextField!
    @IBOutlet weak var buyerMonthlyDebtTextField : UITextField!
    @IBOutlet weak var maritalStatusTextField : UITextField!
    @IBOutlet weak var buyerEmployStatusTextField : UITextField!
    @IBOutlet weak var spouseEmployStatusTextField : UITextField!
    @IBOutlet weak var noExcemptionTextField : UITextField!
    
    var buyerEmployStatus: DropDownInfoModel?
    var spousEmployStatus: DropDownInfoModel?
//    var maritalStatus: BottomPickerModel?
    var empList: Array<DropDownInfoModel>?

    var homeDetails: NewHomeDetailsModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.connectDelegate()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        buyerOneIncomeTextField.text = "4100"
        buyerTwoIncomeTextField.text = "2500"
        otherIncomeTextField.text = "750"
        buyerMonthlyDebtTextField.text = "1100"
//        noExcemptionTextField.text = "3"
    }
    private func connectDelegate(){
        self.spouseEmployStatusTextField.delegate = self
        self.buyerEmployStatusTextField.delegate = self
        self.fetch()
    }
    private func fetch(){
        CommonRequest.dropDownAPI(type: .employmentBox) { (empList) in
            self.empList = empList
        }
    }
    private func selectedValues(){
        if let buyerEmpStatus = self.homeDetails?.buyer_emp_status{
            if let index = self.empList?.index(of: DropDownInfoModel.init(value: buyerEmpStatus)){
                self.buyerEmployStatus = self.empList?[index]
                self.buyerEmployStatusTextField.text = buyerEmployStatus?.name
            }
        }
        if let spouseEmpStatus = self.homeDetails?.spouse_emp_status{
            if let index = self.empList?.index(of: DropDownInfoModel.init(value: spouseEmpStatus)){
                self.spousEmployStatus = self.empList?[index]
                self.spouseEmployStatusTextField.text = spousEmployStatus?.name
            }
        }
    }
    func isValidationSuccess()->Bool{
        if buyerOneIncomeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer One Income"))
            return false
        }else if buyerTwoIncomeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer Two Income"))
            return false
        }else if buyerMonthlyDebtTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer Monthly Debit"))
            return false
        }else if buyerEmployStatusTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Buyer Employ Status"))
            return false
        }else if spouseEmployStatusTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Spouse Employ Status"))
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension BuyerDataTableViewController{
    func calculateTotalMonthlyIncome() -> Double{
        var income : Double = 0.0
        income += buyerOneIncomeTextField.text?.toDouble ?? 0
        income += buyerTwoIncomeTextField.text?.toDouble ?? 0
        income += otherIncomeTextField.text?.toDouble ?? 0
        income += nonTaxableIncomeTextField.text?.toDouble ?? 0
        return income
    }
    func buyerData() -> [OutputDetailsModel]{
        var item : [OutputDetailsModel] = [OutputDetailsModel.init(key: "Buyer One Income", value: buyerOneIncomeTextField.toDouble)]
        item.append(OutputDetailsModel.init(key: "Buyer Two Income", value: buyerTwoIncomeTextField.toDouble))
         item.append(OutputDetailsModel.init(key: "Other Income", value: otherIncomeTextField.toDouble))
         item.append(OutputDetailsModel.init(key: "Non Taxable Income", value: nonTaxableIncomeTextField.toDouble))
         item.append(OutputDetailsModel.init(key: "Buyer Monthly Debt", value: buyerMonthlyDebtTextField.toDouble))
//        item.append(OutputDetailsModel.init(key: "Marital Status", value: maritalStatusTextField.text ?? ""))
        item.append(OutputDetailsModel.init(key: "Buyer Employ Status", value: buyerEmployStatusTextField.text ?? ""))
        item.append(OutputDetailsModel.init(key: "Buyer Monthly Debt", value: buyerMonthlyDebtTextField.text ?? ""))
//        item.append(OutputDetailsModel.init(key: "Number of Excemption", value: noExcemptionTextField.toDouble))
        return item
    }
   
}
extension BuyerDataTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == spouseEmployStatusTextField{
            self.showBottomPicker(delegate: self, items: empList ?? [], tag: 1, title: "Employ Status")
            return false
        }else if textField == buyerEmployStatusTextField{
            self.showBottomPicker(delegate: self, items: empList ?? [], tag: 0, title: "Employ Status")
            return false
        }
        return true
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension BuyerDataTableViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.buyerEmployStatusTextField.text = model?.name
            self.buyerEmployStatus = model
        }else if viewController.dataModel.tag == 1{
            self.spouseEmployStatusTextField.text = model?.name
            self.spousEmployStatus = model
        }else if viewController.dataModel.tag == 2{
//            self.maritalStatusTextField.text = model.key
//            self.maritalStatus = model
        }
     }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension BuyerDataTableViewController{
    private func mapping(){
        buyerOneIncomeTextField.text = homeDetails?.buyer1_income?.readableFormat
        buyerTwoIncomeTextField.text = homeDetails?.buyer2_income?.readableFormat
        otherIncomeTextField.text = homeDetails?.other_income?.readableFormat
        buyerMonthlyDebtTextField.text = homeDetails?.buyer_month_debt?.readableFormat
        nonTaxableIncomeTextField.text = homeDetails?.non_taxable_income?.readableFormat
        
//        noExcemptionTextField.text = "3"
    }
}
extension BuyerDataTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.newHome.key.self
        var dict: Dictionary<String, Any> = [key.buyer1Income: buyerOneIncomeTextField.toDouble]
        dict.updateValue(buyerTwoIncomeTextField.toDouble, forKey: key.buyer2Income)
        dict.updateValue(otherIncomeTextField.toDouble, forKey: key.otherIncome)
        dict.updateValue(buyerMonthlyDebtTextField.toDouble, forKey: key.buyerMonthlyDebt)
        dict.updateValue(nonTaxableIncomeTextField.toDouble, forKey: key.nonTaxableIncome)
        dict.updateValue(buyerEmployStatus?.value ?? "", forKey: key.SelBuyerEmployment)
        dict.updateValue(spousEmployStatus?.value ?? "", forKey: key.SelSpouseEmployment)
//        dict.updateValue(noExcemptionTextField.toDouble, forKey: key.noOfExemption)
        return dict
    }
}
