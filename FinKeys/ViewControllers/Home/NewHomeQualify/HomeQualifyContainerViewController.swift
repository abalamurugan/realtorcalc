//
//  HomeQualifyContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class HomeQualifyContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showHomeQualify"
    
    @IBOutlet weak var segmentedControl : UISegmentedControl!
    @IBOutlet weak var buyerDataContainer : UIView!
    @IBOutlet weak var homePaymentContainer : UIView!
    @IBOutlet weak var qualificationContainer : UIView!
    
    var buyerDataViewController : BuyerDataTableViewController!
    var homePaymentViewController : HomePaymentTableViewController!
    var qualificationViewController : QualificationTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.newHome
        buyerDataContainer.isHidden = false
        homePaymentContainer.isHidden = true
        qualificationContainer.isHidden = true
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    func defaultValues(){
        
    }
    @IBAction func segmentedAction(_ sender : UISegmentedControl){
        buyerDataContainer.isHidden = true
        homePaymentContainer.isHidden = true
        qualificationContainer.isHidden = true
        if sender.selectedSegmentIndex == 0{
//            if buyerDataViewController.isValidationSuccess(){
                buyerDataContainer.isHidden = false
//            }
        }else if sender.selectedSegmentIndex == 1{
//            if homePaymentViewController.isValidationSuccess(){
                homePaymentContainer.isHidden = false
//            }
        }else if sender.selectedSegmentIndex == 2{
//            if qualificationViewController.isValidationSuccess(){
                qualificationContainer.isHidden = false
//            }
        }
    }

    @IBAction func calculateAction(_ sender : UIButton){
        self.postFormValues()
    }
    private func showOutput(){
        print("Total Monthly Income=======>\(buyerDataViewController.calculateTotalMonthlyIncome())")
        print("1st Mortgage Amount  =======>\(homePaymentViewController.calculateFirstMortgageAmount())")
        print("Total Housing Payment =======>\(qualificationViewController.calculateHousingPayment())")
        print("Closing cost =======>\(homePaymentViewController.estimateCashCloseCalc())")
        //        if buyerDataViewController.isValidationSuccess() && homePaymentViewController.isValidationSuccess() && qualificationViewController.isValidationSuccess(){
        
        var list : [HomeQualifyModel] = []
        list.append(buyerDate())
        list.append(estimateClosingData())
        list.append(newHomePaymentData())
        list.append(buyerQualification())
        list.append(actionToQualify)
        self.performSegue(withIdentifier: HomeQualifyOutputViewController.segueIdentifier, sender: list)
        //        }
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == BuyerDataTableViewController.segueIdentifier{
            buyerDataViewController = segue.destination as? BuyerDataTableViewController
        }else if segue.identifier == HomePaymentTableViewController.segueIdentifier{
            homePaymentViewController = segue.destination as? HomePaymentTableViewController
        }else if segue.identifier == QualificationTableViewController.segueIdentifier{
            qualificationViewController = segue.destination as? QualificationTableViewController
        }else if segue.identifier == HomeQualifyOutputViewController.segueIdentifier{
            let outputController = segue.destination as! HomeQualifyOutputViewController
            outputController.outputList = sender as! [HomeQualifyModel]
            outputController.equityList = self.equityBuildUpYearList()
            outputController.contactModel = self.contactModel
        }
    }
}
extension HomeQualifyContainerViewController {
    private func buyerDate()->HomeQualifyModel{
        let buyer = HomeQualifyModel()
        buyer.title = "Buyer Data"
        buyer.priceDescription = "Total Monthly Income"
        buyer.price = buyerDataViewController.calculateTotalMonthlyIncome()
        let buyerDetails = buyerDataViewController.buyerData()
        buyer.detailList = buyerDetails
        return buyer
    }
    private func estimateClosingData()->HomeQualifyModel{
        let estimateClosing = HomeQualifyModel()
        estimateClosing.title = "Estimate cash at closing"
        estimateClosing.priceDescription = "Cash at closing"
        estimateClosing.price = homePaymentViewController.estimateCashCloseCalc()
        let closingDetails = homePaymentViewController.estimateClosingList()
        estimateClosing.detailList = closingDetails
        return estimateClosing
    }
    private func newHomePaymentData()->HomeQualifyModel{
        let newHome = HomeQualifyModel()
        newHome.title = "New Home Payment"
        newHome.priceDescription = "Total Housing Payment"
        newHome.price = qualificationViewController.calculateHousingPayment()
        var homeDetails = homePaymentViewController.newHomePaymentList()
        homeDetails.append(.init(key: "Principal & Interest Portion", value: qualificationViewController.pmt()))
        homeDetails.append(.init(key: "  + Real Estate Taxes", value: qualificationViewController.realEstate))
        homeDetails.append(.init(key: "  + Hazard Insurance", value: qualificationViewController.hazardInsurance))
        homeDetails.append(.init(key: "  + Mortgage Insurance", value: qualificationViewController.mortgageInsurance))
        homeDetails.append(.init(key: "  + Condo/HOA Fees:", value: qualificationViewController.hoa))
        homeDetails.append(.init(key: "  + 2nd Mortgage Payment:", value: qualificationViewController.secondMtgPayment))
//        homeDetails.append(.init(key: "To Lower 1st Ratio: Raise Monthly Income:", value: raiseMonthlyStIncome))
//        homeDetails.append(.init(key: "To Lower 1st Ratio: Lower Monthly Payment:", value: lowerMonthlyStPayment))
//        homeDetails.append(.init(key: "To Lower 2nd Ratio: Raise Monthly Income:", value: raiseMonthlyNdIncome))
//        homeDetails.append(.init(key: "To Lower 2nd Ratio: Lower Monthly Payment:", value: lowerMonthlyNdPayment))
        newHome.detailList = homeDetails
        return newHome
    }
    private func buyerQualification()->HomeQualifyModel{
        let qualification = HomeQualifyModel()
        qualification.title = "Buyer Qualification"
        qualification.priceDescription = "Max Total Housing Pmt."
        qualification.price = qualificationViewController.maxTotalHousingPmt
        let qualificationDetails = qualificationViewController.buyerQualification()
        qualification.detailList = qualificationDetails
        return qualification
    }
    private var actionToQualify: HomeQualifyModel{
        let model = HomeQualifyModel.init()
        model.actionToQualify = self.actionQualify
        return model
    }
    private var actionQualify: ActionQualifyModel{
        let model = ActionQualifyModel.init()
        model.firstRaiseMonthlyIncome = raiseMonthlyStIncome.readableFormat
        model.firstLowerMonthlyPayment = lowerMonthlyStPayment.readableFormat
        model.secondRaiseMonthlyIncome = raiseMonthlyNdIncome.readableFormat
        model.secondLowerMonthlyPayment = lowerMonthlyNdPayment.readableFormat
        return model
    }
    private func calcEquityBuildUp()->[EquityBuildUpModel]{
//        var equityBuildList : Array<EquityBuildUpModel> = []
        var equityBuildYearList : Array<EquityBuildUpModel> = []

        let interest = qualificationViewController.interestRateTextField.toDouble
        let loanTerm = qualificationViewController.termYearTextField.toDouble

        var balance = homePaymentViewController.calculateFirstMortgageAmount()
        let firstMortgAmont = homePaymentViewController.calculateFirstMortgageAmount()

//        equityBuildList.append(EquityBuildUpModel.init(balance: balance, interest: 0, principal: 0))
        let paymentPerYear = qualificationViewController.paymentPerYearTextField.toDouble
        for index in 1...Int(paymentPerYear) * 10{//10 Loan year
           let ipmt = -(Formula.calculateIPMT(interest, paymentPerYear, loanTerm
            , balance,Double(index)))
            let pmt = Formula.calculatePMT(interest, paymentPerYear, loanTerm, firstMortgAmont, Double(index))
            let ppmt = pmt - ipmt
            balance = balance - ppmt
            if index % 12 == 0{
                equityBuildYearList.append(EquityBuildUpModel.init(balance: balance, interest: ipmt, principal: ppmt))
            }
//            equityBuildList.append(EquityBuildUpModel.init(balance: balance, interest: ipmt, principal: ppmt))
        }
        return equityBuildYearList
    }
    private func equityBuildUpYearList() -> Array<EquityModel>{
        var yearList : Array<EquityModel> = []
        let loanTerm = qualificationViewController.termYearTextField.toDouble
        let interest = qualificationViewController.interestRateTextField.toDouble
        let paymentPerYear = qualificationViewController.paymentPerYearTextField.toDouble

        var purchasePrice = homePaymentViewController.purchasePriceTextField.toDouble
        let mortgageAmount = homePaymentViewController.calculateFirstMortgageAmount()
        let equity = purchasePrice - mortgageAmount
        yearList.append(EquityModel.init(value: purchasePrice, balance: mortgageAmount, equity: equity))
        
        for item in calcEquityBuildUp(){
            purchasePrice = Formula.FVCalc(rate: Double(3).toValue, nper: 1, pmt: 0, fv: purchasePrice).removeMinus
//           purchasePrice = -(Formula.FVCalculation(3, paymentPerYear, loanTerm, purchasePrice))
            let equity = purchasePrice - item.balance!
            yearList.append(EquityModel.init(value: purchasePrice, balance: item.balance!, equity: equity))
        }
        return yearList
    }
}
extension HomeQualifyContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getNewHome()
    }
}
extension HomeQualifyContainerViewController{
    var paramViewHome: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.newhomeview, contactId: self.contactModel!.contactId!)
        
    }
    private func getNewHome(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewHome, encoding: .queryString, { (response: ResponseModel<NewHomeModel>) in
            self.buyerDataViewController.homeDetails = response.item?.response
            self.qualificationViewController.homeDetails = response.item?.response
            self.homePaymentViewController.homeDetails = response.item?.response
        }) { (error) in
            self.showError(error)
        }
    }
}
extension HomeQualifyContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID]
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.reportNames.newHome, forKey: APIKey.common.key.action)
        dict.updateValue(Constants.tagValue.newhomeAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        buyerDataViewController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        homePaymentViewController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        qualificationViewController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showOutput()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
extension HomeQualifyContainerViewController{
    var raiseMonthlyStIncome: Double{
        let exp1 = qualificationViewController.calculateHousingPayment()/qualificationViewController.incomeRatioTextField.toDouble.toValue
        let totalMonthlyIncome = buyerDataViewController.calculateTotalMonthlyIncome()
        if exp1 < totalMonthlyIncome{
            return 0
        }
        return (totalMonthlyIncome - exp1).removeMinus
    }
    var lowerMonthlyStPayment: Double{
        let exp1 = buyerDataViewController.calculateTotalMonthlyIncome() * qualificationViewController.incomeRatioTextField.toDouble.toValue
        if exp1 > qualificationViewController.calculateHousingPayment(){
            return 0
        }
        return (exp1 - qualificationViewController.calculateHousingPayment()).removeMinus
    }
    var raiseMonthlyNdIncome: Double{
        let exp1 = (qualificationViewController.calculateHousingPayment() + buyerDataViewController.buyerMonthlyDebtTextField.toDouble)/qualificationViewController.debitRatioTextField.toDouble.toValue
        let totalMonthlyIncome = buyerDataViewController.calculateTotalMonthlyIncome()
        if exp1 < totalMonthlyIncome{
            return 0
        }
        return (totalMonthlyIncome - exp1).removeMinus
    }
    var lowerMonthlyNdPayment: Double{
        let exp1 = qualificationViewController.secondRatio
        let exp2 = (buyerDataViewController.calculateTotalMonthlyIncome() * qualificationViewController.debitRatioTextField.toDouble.toValue) - buyerDataViewController.buyerMonthlyDebtTextField.toDouble
        if exp1 > qualificationViewController.debitRatioTextField.toDouble{
            return qualificationViewController.calculateHousingPayment() - exp2
        }
        return 0
    }
}
