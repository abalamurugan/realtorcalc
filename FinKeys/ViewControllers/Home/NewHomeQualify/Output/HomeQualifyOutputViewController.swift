//
//  HomeQualifyOutputViewController.swift
//  RealtorCalc
//
//  Created by Bala on 28/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class HomeQualifyOutputViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showHomeOutput"
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var equityButton : UIButton!

    
    var outputList = Array<HomeQualifyModel>()
    var equityList = Array<EquityModel>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        // Do any additional setup after loading the view.
    }
    private func configUI(){
        self.configurePrint()
        self.tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.getPDf()
    }
    
    @IBAction func equityAction(_ sender: UIButton){
        let equityController = EquityViewController.showEquity()
        equityController.dataModel.isEquity = true
        equityController.isHideTotalView = true
        equityController.dataModel.equityList = self.equityList
        equityController.dataModel.title = "Equity Build Up"
        equityController.contactModel = self.contactModel
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: equityController)
        self.present(navigationController, animated: true, completion: nil)
    }
    private func configurePrint(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID, APIKey.common.key.tag : Constants.tagValue.printOption]
        connection.networkRequest(urlPath: nil, method: .get, param: param,encoding: .queryString, { (response: ResponseModel<PrintPaymentModel>) in
            let model = response.item?.response
            let isRightButton = (model?.isPrintAvailable ?? "NO").caseInsensitiveCompare("YES") == .orderedSame ? true : false
            if isRightButton{
                self.setupRightBarButton(buttonTitle: LocalizableKey.reports.print)
            }
        }) { (error) in
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeQualifyOutputViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return outputList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = outputList[indexPath.row]
        if model.title == nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: ActionQualifyTableViewCell.reuseIdentifier, for: indexPath) as! ActionQualifyTableViewCell
            cell.set(item: model.actionToQualify)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeQualifyOutputTableViewCell.reuseIdentifier, for: indexPath) as! HomeQualifyOutputTableViewCell
        cell.set(model)
        cell.detailButton.tag = indexPath.row
        cell.detailButton.addTarget(self, action: #selector(viewDetailAction(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return outputList[indexPath.row].title == nil ? 230 : 100
    }
}
extension HomeQualifyOutputViewController{
    @objc func viewDetailAction(_ sender : UIButton){
        let equityController = EquityViewController.showEquity()
        equityController.dataModel.list = self.outputList[sender.tag].detailList
        equityController.dataModel.model = self.outputList[sender.tag]
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: equityController)
        self.present(navigationController, animated: true, completion: nil)
    }
}
extension HomeQualifyOutputViewController{
    var pdfParam: Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict.updateValue(Constants.pdfDetect, forKey: APIKey.pdf.key.detect)
        dict.updateValue(contactModel!.contactId!, forKey: APIKey.pdf.key.contactId)
        dict.updateValue(Constants.reportNames.newHome, forKey: APIKey.pdf.key.name)
        dict.updateValue(Constants.tagValue.reportPdf, forKey: APIKey.pdf.key.tag)
        return dict
    }
    private func getPDf(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: pdfParam, encoding: .queryString, { (response: ResponseModel<PDFModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let pdfURL = response.item?.pdfURL, pdfURL != ""{
                UIApplication.shared.open(URL(string : pdfURL)!, options: [:], completionHandler: { (status) in
                    
                })
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
