//
//  QualificationViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class QualificationTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showQualification"

    @IBOutlet weak var planTypeTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestRateTextField : UITextField!
    @IBOutlet weak var termYearTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var incomeRatioTextField : UITextField!
    @IBOutlet weak var debitRatioTextField : UITextField!
    @IBOutlet weak var realEstateTaxesTextField : UITextField!
    @IBOutlet weak var hazardInsuranceTextField : UITextField!
    @IBOutlet weak var mortgageInsuranceTextField : UITextField!
    @IBOutlet weak var hoaFeesTextField : UITextField!
    @IBOutlet weak var secondMortgagePaymentTextField : UITextField!
    @IBOutlet weak var appreciationTextField : UITextField!

    var homeDetails: NewHomeDetailsModel?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    private var paymentTypeList: Array<DropDownInfoModel>?
    private var loanPlanList: Array<DropDownInfoModel>?
    private var selectedPaymentType: DropDownInfoModel?
    private var selectedLoanPlan: DropDownInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.setup()
        // Do any additional setup after loading the view.
    }
    func defaultValues(){
        interestRateTextField.text = "5.00"
        termYearTextField.text = "30"
        paymentPerYearTextField.text = "12"
        incomeRatioTextField.text = "28.0"
        debitRatioTextField.text = "36.0"
        realEstateTaxesTextField.text = "375"
        hazardInsuranceTextField.text = "105"
 
    }
    private func fetch(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        CommonRequest.dropDownAPI(type: .paymentType) { (paymentList) in
            self.paymentTypeList = paymentList
            CommonRequest.dropDownAPI(type: .loanPlan) { (loanList) in
                LoadingView.instance.hideActivityIndicator(uiView: self.view)
                self.loanPlanList = loanList
            }
        }
    }
    private func setup(){
        self.fetch()
    }
    private func selectedValues(){
        if let loanPlan = self.homeDetails?.loan_plan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.planTypeTextField.text = selectedLoanPlan?.name
            }
        }
        if let paymentType = self.homeDetails?.payment_type{
            if let index = self.paymentTypeList?.index(of: DropDownInfoModel.init(value: paymentType)){
                self.selectedPaymentType = self.paymentTypeList?[index]
                self.paymentTypeTextField.text = selectedPaymentType?.name
            }
        }
    }
    func isValidationSuccess()->Bool{
        if planTypeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Plan Type"))
            return false
        }else if paymentTypeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Payment type"))
            return false
        }else if interestRateTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Interest Rate"))
            return false
        }else if termYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Term Year"))
            return false
        }else if paymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Payments Per Year"))
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension QualificationTableViewController{
    func calculateHousingPayment()-> Double{
        var totalHousingPayment : Double = 0.0
        let realEstate = realEstateTaxesTextField.text?.toDouble ?? 0
        let hazardInsurance = hazardInsuranceTextField.text?.toDouble ?? 0
        let mortInsurance = mortgageInsuranceTextField.text?.toDouble ?? 0
        let hoa = hoaFeesTextField.text?.toDouble ?? 0
        let secondMtgAmount = secondMortgagePaymentTextField.text?.toDouble ?? 0
        totalHousingPayment += realEstate
        totalHousingPayment += hazardInsurance
        totalHousingPayment += mortInsurance
        totalHousingPayment += hoa
        totalHousingPayment += secondMtgAmount
        totalHousingPayment += pmt()
        return totalHousingPayment
    }
    func pmt()->Double{
        if let parent = self.parent as? HomeQualifyContainerViewController{
            let interest = interestRateTextField.text?.toDouble ?? 0
            let paymentYear = paymentPerYearTextField.text?.toDouble ?? 0
            let term = termYearTextField.text?.toDouble ?? 0
            let mtgAmount = parent.homePaymentViewController.calculateFirstMortgageAmount()
            let pmt = Formula.calculatePMT(interest, paymentYear, term, mtgAmount)
            return pmt
         }
        return 0
    }
    var realEstate : Double{
        return realEstateTaxesTextField.toDouble
    }
    var hazardInsurance : Double{
        return hazardInsuranceTextField.toDouble
    }
    var mortgageInsurance : Double{
        return mortgageInsuranceTextField.toDouble
    }
    var hoa : Double{
        return hoaFeesTextField.toDouble
    }
    var secondMtgPayment : Double{
        return secondMortgagePaymentTextField.toDouble
    }
    
    var totalPayment: Double{
        let loanTerm = termYearTextField.toDouble
        let paymentPerYear = paymentPerYearTextField.toDouble
        return loanTerm * paymentPerYear
    }
    
    var maxPmtIncome: Double{
        let interest = incomeRatioTextField.toDouble
        
        var monthlyIncome: Double = 0
        if let parent = self.parent as? HomeQualifyContainerViewController{
            monthlyIncome = parent.buyerDataViewController.calculateTotalMonthlyIncome()
        }
        return interest * monthlyIncome/100
    }
    var maxPmtDebit: Double{
        let interest = debitRatioTextField.toDouble
        
        var monthlyIncome: Double = 0
        var monthlyDebit: Double = 0
        if let parent = self.parent as? HomeQualifyContainerViewController{
            monthlyIncome = parent.buyerDataViewController.calculateTotalMonthlyIncome()
            monthlyDebit = parent.buyerDataViewController.buyerMonthlyDebtTextField.toDouble
        }
        return (interest * monthlyIncome/100) - (monthlyDebit)
    }
    var maxTotalHousingPmt : Double {
        return maxPmtIncome < maxPmtDebit ? maxPmtIncome : maxPmtDebit
    }
    
    var secondRatio : Double{
        var totHousingPayment : Double = 0
        var totalMonthlyIncome : Double = 0
        if let parent = self.parent as? HomeQualifyContainerViewController{
            totHousingPayment = parent.qualificationViewController.calculateHousingPayment()
            totalMonthlyIncome = parent.buyerDataViewController.calculateTotalMonthlyIncome()
            totHousingPayment += parent.buyerDataViewController.buyerMonthlyDebtTextField.toDouble
        }
        return (totHousingPayment/totalMonthlyIncome) * 100
    }
    var firstRatio : Double{
        var totHousingPayment : Double = 0
        var totalMonthlyIncome : Double = 0
        if let parent = self.parent as? HomeQualifyContainerViewController{
            totHousingPayment = parent.qualificationViewController.calculateHousingPayment()
            totalMonthlyIncome = parent.buyerDataViewController.calculateTotalMonthlyIncome()
        }
        return (totHousingPayment/totalMonthlyIncome) * 100
    }
    
}
extension QualificationTableViewController{
    func buyerQualification() -> [OutputDetailsModel]{
        var item : [OutputDetailsModel] = [OutputDetailsModel.init(key: "Loan Plan", value: planTypeTextField.text ?? "")]
        item.append(OutputDetailsModel.init(key: "Payment Type:", value: paymentTypeTextField.text ?? ""))
        item.append(OutputDetailsModel.init(key: "Interest Rate:", value: interestRateTextField.toDouble.toPercentage))
        item.append(OutputDetailsModel.init(key: "Total Payments:", value: (totalPayment/12).clean))
        item.append(OutputDetailsModel.init(key: "Payments Per Year", value: paymentPerYearTextField.text ?? ""))
        item.append(OutputDetailsModel.init(key: "Max. Pmt. Using Income", value: maxPmtIncome))
        item.append(OutputDetailsModel.init(key: "Max. Pmt. Include. Debt", value: maxPmtDebit))
        item.append(OutputDetailsModel.init(key: "Max Total Housing Pmt.", value: maxTotalHousingPmt))
        item.append(OutputDetailsModel.init(key: "1st Ratio (Max 28%)", value: firstRatio.toPercentage))
        item.append(OutputDetailsModel.init(key: "2nd Ratio (Max 36%)", value: secondRatio.toPercentage))
        return item
    }
}
extension QualificationTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == planTypeTextField{
            self.showBottomPicker(delegate: self, items: loanPlanList ?? [], tag: 0)
            return false
        }else if textField == paymentTypeTextField{
            self.showBottomPicker(delegate: self, items: paymentTypeList ?? [], tag: 1)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestRateTextField || textField == debitRatioTextField || textField == incomeRatioTextField || textField == appreciationTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension QualificationTableViewController{
    private func mapping(){
        interestRateTextField.text = homeDetails?.interest_rate?.toNumber.toPercentage
        termYearTextField.text = "30"
        paymentPerYearTextField.text = homeDetails?.pay_per_year
        incomeRatioTextField.text = homeDetails?.income_ratio?.toNumber.toPercentage
        debitRatioTextField.text = homeDetails?.debt_ratio?.toNumber.toPercentage
        realEstateTaxesTextField.text = homeDetails?.real_estate_tax?.readableFormat
        hazardInsuranceTextField.text = homeDetails?.hazard_Ins?.readableFormat
        appreciationTextField.text = homeDetails?.assAppr?.toNumber.toPercentage
        hoaFeesTextField.text = homeDetails?.conda_hoa_fee?.readableFormat
        mortgageInsuranceTextField.text = homeDetails?.mort_ins?.readableFormat
        secondMortgagePaymentTextField.text = homeDetails?.nd_mort_pay?.readableFormat
    }
}
extension QualificationTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.newHome.key.self
        var dict: Dictionary<String, Any> = [key.interestRate: interestRateTextField.toDouble]
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear)
        dict.updateValue(incomeRatioTextField.toDouble, forKey: key.incomeRatio)
        dict.updateValue(debitRatioTextField.toDouble, forKey: key.debtRatio)
        dict.updateValue(realEstateTaxesTextField.toDouble, forKey: key.realEstateTaxes)
        dict.updateValue(hoaFeesTextField.toDouble, forKey: key.condoHOAFees)
        dict.updateValue(hazardInsuranceTextField.toDouble, forKey: key.hazardInsurance)
        dict.updateValue(secondMortgagePaymentTextField.toDouble, forKey: key.ndMortgagePayment)
        dict.updateValue(termYearTextField.toDouble, forKey: key.totalPayment)
        dict.updateValue(mortgageInsuranceTextField.toDouble, forKey: key.mortgageInsurance)
        dict.updateValue(appreciationTextField.toDouble, forKey: key.assumesAppr)
        dict.updateValue(self.selectedLoanPlan?.value ?? "", forKey: key.loanPlan1)
        dict.updateValue(self.selectedPaymentType?.value ?? "", forKey: key.payType1)
        return dict
    }
}
extension QualificationTableViewController : BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            planTypeTextField.text = model?.name
            selectedLoanPlan = model
        }else if viewController.dataModel.tag == 1{
            paymentTypeTextField.text = model?.name
            selectedPaymentType = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }

}
