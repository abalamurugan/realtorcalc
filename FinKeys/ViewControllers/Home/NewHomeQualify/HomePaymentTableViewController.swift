//
//  HHomePaymentTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 21/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class HomePaymentTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showHomePayment"
    
    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var downPaymentTextField : UITextField!
    @IBOutlet weak var secondMortageTextField : UITextField!
    @IBOutlet weak var mtgInsuranceTextField : UITextField!
    
    @IBOutlet weak var loanOriginationFeeTextField : UITextField!
    @IBOutlet weak var discoundPointsTextField : UITextField!
    @IBOutlet weak var prepaidTextField : UITextField!
    @IBOutlet weak var titleRecordingTextField : UITextField!
    @IBOutlet weak var mortageInsuranceFundingTextField : UITextField!
    
    var homeDetails: NewHomeDetailsModel?{
        didSet{
            self.mapping()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        purchasePriceTextField.text = "450000"
        downPaymentTextField.text = "135000"
        loanOriginationFeeTextField.text = "3150"
        discoundPointsTextField.text = "4725"
        prepaidTextField.text = "3501"
        titleRecordingTextField.text = "3375"
        
    }
    func isValidationSuccess()->Bool{
        if purchasePriceTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Purchase price"))
            return false
        }else if downPaymentTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Down payment"))
            return false
        }else if loanOriginationFeeTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.error.errorTitle, LocalizableKey.commonValidation.inValid("Loan Origination Fees"))
            return false
        } 
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomePaymentTableViewController{
    func calculateFirstMortgageAmount()->Double{
        var mortgageAmount : Double = purchasePriceTextField.text?.toDouble ?? 0
        mortgageAmount -= downPaymentTextField.text?.toDouble ?? 0
        mortgageAmount -= secondMortageTextField.text?.toDouble ?? 0
        mortgageAmount += mortageInsuranceFundingTextField.text?.toDouble ?? 0
        return mortgageAmount
        
    }
    func approximateClosingCalc()->Double{
        var approximateClosing : Double = 0
        let loanOrigination = loanOriginationFeeTextField.toDouble
        let discount = discoundPointsTextField.toDouble
        let prepaid = prepaidTextField.toDouble
        let title = titleRecordingTextField.toDouble
        let mort = mortageInsuranceFundingTextField.toDouble
        approximateClosing += loanOrigination
        approximateClosing += discount
        approximateClosing += prepaid
        approximateClosing += title
        approximateClosing += mort
        return approximateClosing
    }
    func estimateCashCloseCalc() -> Double{
        var totalClosing : Double = 0
        totalClosing = approximateClosingCalc() + downPaymentTextField.toDouble
        return totalClosing
    }
    func estimateClosingList()->[OutputDetailsModel]{
        var list : [OutputDetailsModel] = []
        list.append(OutputDetailsModel.init(key: "Loan Origination Fee", value: loanOriginationFeeTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  + Discount Points", value: discoundPointsTextField.toDouble))

        list.append(OutputDetailsModel.init(key: "  + Pre-Paids (Int/Tax/Ins)", value: prepaidTextField.toDouble))

        list.append(OutputDetailsModel.init(key: "  + Title/Recording/Misc.", value: titleRecordingTextField.toDouble))

        list.append(OutputDetailsModel.init(key: "Approximate Closing Costs", value: approximateClosingCalc()))
        list.append(OutputDetailsModel.init(key: "    + Down Payment", value: downPaymentTextField.toDouble))
        return list
    }
    
    func newHomePaymentList()->[OutputDetailsModel]{
        var list : [OutputDetailsModel] = []
        list.append(OutputDetailsModel.init(key: "Purchase Price", value: purchasePriceTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "  - Down Payment", value: downPaymentTextField.toDouble))
        
        list.append(OutputDetailsModel.init(key: "  - 2nd Mortgage Amount", value: secondMortageTextField.toDouble))
        
        list.append(OutputDetailsModel.init(key: "  + Mtg. Insurance/Funding Fee", value: mtgInsuranceTextField.toDouble))
        
        list.append(OutputDetailsModel.init(key: " = 1st Mortgage Amount", value: calculateFirstMortgageAmount()))
        return list
    }
}
extension HomePaymentTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
extension HomePaymentTableViewController{
    private func mapping(){
        purchasePriceTextField.text = homeDetails?.purchase_price?.readableFormat
        downPaymentTextField.text = homeDetails?.down_pay?.readableFormat
        loanOriginationFeeTextField.text = homeDetails?.loan_org_fee?.readableFormat
        discoundPointsTextField.text = homeDetails?.discount_point?.readableFormat
        prepaidTextField.text = homeDetails?.pre_paids?.readableFormat
        titleRecordingTextField.text = homeDetails?.title_record_misc?.readableFormat
        mortageInsuranceFundingTextField.text = homeDetails?.mort_ins_fund_fee?.readableFormat
        secondMortageTextField.text = homeDetails?.nd_mort_amt?.readableFormat
        mtgInsuranceTextField.text = homeDetails?.ins_fund_fee?.readableFormat
     }
}
extension HomePaymentTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.newHome.key.self
        var dict: Dictionary<String, Any> = [key.purchasePrice: purchasePriceTextField.toDouble]
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPayment)
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPayment1)
        dict.updateValue(loanOriginationFeeTextField.toDouble, forKey: key.loanOrigination)
        dict.updateValue(discoundPointsTextField.toDouble, forKey: key.discountPoint)
        dict.updateValue(prepaidTextField.toDouble, forKey: key.prePaids)
        dict.updateValue(titleRecordingTextField.toDouble, forKey: key.titleRecordMisc)
        dict.updateValue(secondMortageTextField.toDouble, forKey: key.ndMortgageAmt)
        dict.updateValue(mtgInsuranceTextField.toDouble, forKey: key.mtgInsurance)
        dict.updateValue(mortageInsuranceFundingTextField.toDouble, forKey: key.mortInsuFundFee)
        return dict
    }
}
