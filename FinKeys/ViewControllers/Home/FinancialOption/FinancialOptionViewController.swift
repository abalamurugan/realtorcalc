//
//  FinancialOptionViewController.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit


class FinancialOptionViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFinancePayment"
    
    @IBOutlet weak var mtgAmountTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var nameTextField : UITextField!
    @IBOutlet weak var taxesEscrowTextField : UITextField!
    @IBOutlet weak var insuranceEscrowTextField : UITextField!
    @IBOutlet weak var mtgInsuranceEscrowTextField : UITextField!
    @IBOutlet weak var hoaComboFeesTextField : UITextField!
    @IBOutlet weak var calculateButton : UIButton!
    @IBOutlet weak var addMoreButton : UIButton!
    @IBOutlet weak var editFormView : EditFormView!
    @IBOutlet weak var editViewHeightConstrainst : NSLayoutConstraint!
    

    lazy var dataModel : FinancialOptionDataModel = {return FinancialOptionDataModel()}()
    
    var outputList = Array<FinancialModel>()
    
    private var editSaveCompletion: ((FinancialModel, Int)->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        if dataModel.formDetails != nil{//to edit form
            self.mapValues(self.dataModel.formDetails!)
            self.setupLeftBackButton()
        }else{
            self.defaultValues()
        }
        self.connectDelegate()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        mtgAmountTextField.text = "315000"
        interestTextField.text = "4"
        paymentPerYearTextField.text = "12"
        loanTermTextField.text = "30"
        taxesEscrowTextField.text = "240"
        insuranceEscrowTextField.text = "240"
        mtgInsuranceEscrowTextField.text = "0"
        hoaComboFeesTextField.text = "0"
    }
    private func setupUI(){
        self.addMoreButton.layer.borderColor = UIColor.primaryColor.cgColor
        self.addMoreButton.layer.borderWidth = 1.0
        self.addMoreButton.isHidden = self.dataModel.formDetails != nil ? true : false
        self.editFormView.isHidden = true
        self.editViewHeightConstrainst.constant = 0
        let buttonTitle = self.dataModel.formDetails != nil ? "Save" : "Calculate"
        self.calculateButton.setTitle(buttonTitle, for: .normal)
    }
    private func connectDelegate(){
        self.editFormView.delegate = self
        self.nameTextField.delegate = self
        self.loanPlanTextField.delegate = self
        self.paymentTypeTextField.delegate = self
    }
    /// Calculate Finance Option
    ///
    /// - Parameter sender: UIButton
    @IBAction func calculateAction(_ sender :UIButton){
        if dataModel.formDetails != nil{
            let model = updateToModel()
            self.dismiss(animated: true) {
                self.editSaveCompletion?(model, self.dataModel.selectedIndex!)
            }
            return
        }
        if isValidationSuccess(){
            self.saveData()
            self.performSegue(withIdentifier: FinancialOutputViewController.segueIdentifier, sender: self)
            outputList.removeAll()
        }
    }
    private func saveData(){
      
        let mtgAmount = mtgAmountTextField.text?.toDouble ?? 0
        let interest = interestTextField.text?.toDouble ?? 0
        let paymentPerYear = paymentPerYearTextField.text?.toDouble ?? 0
        let loanterm = loanTermTextField.text?.toDouble ?? 0
        let pmt = Formula.calculatePMT(interest, paymentPerYear, loanterm, mtgAmount)
        let financialModel = updateToModel()
        financialModel.pmtPrincipal = pmt
        
        outputList.append(financialModel)
        dataModel.financialOutputList = outputList
        self.editFormView.isHidden = dataModel.count == 0 ? true : false
        self.editViewHeightConstrainst.constant = dataModel.count == 0 ? 0 : 120
        self.editFormView.items = dataModel.financialOutputList
    }
    func updateToModel()->FinancialModel{
        let financialModel = FinancialModel()
        financialModel.mtgInsuranceEscrow = mtgInsuranceEscrowTextField.text?.toDouble
        financialModel.taxesEscrow = taxesEscrowTextField.text?.toDouble
        financialModel.insuranceEscrow = insuranceEscrowTextField.text?.toDouble
        financialModel.hoaComboFee = hoaComboFeesTextField.text?.toDouble
        
        financialModel.loanPlan = loanPlanTextField.text
        financialModel.paymentType = paymentTypeTextField.text
        financialModel.interestRate = interestTextField.toDouble
        financialModel.loanTerm = loanTermTextField.toDouble
        financialModel.paymentPerYear = paymentPerYearTextField.toDouble
        financialModel.contactModel = dataModel.contactModel
        financialModel.mtgAmount = mtgAmountTextField.toDouble
        financialModel.fullName = nameTextField.text
        return financialModel
    }
    /// Clear and calculate another finance option
    ///
    /// - Parameter sender: UIButton
    @IBAction func addMoreAction(_ sender : UIButton){
        if self.isValidationSuccess(){
            self.saveData()
            self.clearValues()
        }
    }
    /// reset all
    private func clearValues(){
        mtgAmountTextField.text = nil
        interestTextField.text = nil
        paymentPerYearTextField.text = nil
        loanTermTextField.text = nil
        interestTextField.text = nil
        mtgInsuranceEscrowTextField.text = nil
        taxesEscrowTextField.text = nil
        insuranceEscrowTextField.text = nil
        hoaComboFeesTextField.text = nil
    }
    
    /// Validation for the mandatory fields
    ///
    /// - Returns: true-> Validation success , false -> Validation failed
    func isValidationSuccess()->Bool{
        if loanPlanTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.errorTitle, LocalizableKey.commonValidation.inValid("loan plan"))
            return false
        }else if paymentTypeTextField.isEmpty{
            self.showAlert(LocalizableKey.errorMessage.errorTitle, LocalizableKey.commonValidation.inValid("payment type"))
            return false
        }else if mtgAmountTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.mtgInvalid)
            return false
        }else if interestTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.interestInvalid)
            return false
        }else if paymentPerYearTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.paymentPerYearInvalid)
            return false
        }else if loanTermTextField.text?.isEmpty ?? false{
            self.showAlert(LocalizableKey.errorMessage.validationFailed, LocalizableKey.amortizationValidation.loanTermInvalid)
            return false
        }
        return true
    }
    
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FinancialOutputViewController.segueIdentifier{
            let outputController = segue.destination as! FinancialOutputViewController
            outputController.dataModel.financialOutputList = dataModel.financialOutputList
            outputController.dataModel.type = .payment
        }
    }
  

}
//MARK:- Edit form
extension FinancialOptionViewController: EditFormViewDelegate{
    func didFormSelected(_ index: Int) {
        let selectedFormDetails = dataModel.financialOutputList?[index]
        let financeViewController = Storyboard.financeOption.storyboard.instantiateViewController(withIdentifier: "FinancialOptionViewController") as! FinancialOptionViewController
        financeViewController.dataModel.formDetails = selectedFormDetails
        financeViewController.dataModel.selectedIndex = index
        financeViewController.title = "Edit Option \(index)"
        financeViewController.didEditCompletion { (financeUpdated, index) in
            self.dataModel.financialOutputList![index] = financeUpdated
            self.dataModel.formDetails = nil
            self.editFormView.items = self.dataModel.financialOutputList
        }
        self.presentWithNavigationController(financeViewController)
    }
}
//MARK:- TextField Delegate
extension FinancialOptionViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == nameTextField{
            let contactPicker = ContactListViewController.showContactPicker()
//            contactPicker.delegate = self
            contactPicker.dataModel.isPicker = true
            self.presentWithNavigationController(contactPicker)
            return false
        }else if textField == loanPlanTextField{
            let items = [BottomPickerModel.init(key: "Conventional", value: "Conventional"), BottomPickerModel.init(key: "VA", value: "VA"), BottomPickerModel.init(key: "FHA", value: "FHA")]
            self.showBottomPicker(delegate: self, items: items, tag: loanPlanTextField.tag, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }else if textField == paymentTypeTextField{
            let items = [BottomPickerModel.init(key: "Fixed", value: "Fixed"), BottomPickerModel.init(key: "ARM", value: "ARM")]
            self.showBottomPicker(delegate: self, items: items, tag: paymentTypeTextField.tag, title: LocalizableKey.financingTitle.paymentType)
            return false
        }
        return true
    }
}
//MARK:- ContactListViewControllerDelegate
//extension FinancialOptionViewController: ContactListViewControllerDelegate{
//    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
//        self.dataModel.contactModel = item
//        self.nameTextField.text = item.fullName
//    }
//}
extension FinancialOptionViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as! BottomPickerModel
        if viewController.dataModel.tag == FinanceOptionType.loanPlan.rawValue{
            self.loanPlanTextField.text = model.key
            self.dataModel.paymentTypeModel = model
        }else if viewController.dataModel.tag == FinanceOptionType.paymentType.rawValue{
            self.paymentTypeTextField.text = model.key
            self.dataModel.loanPlanModel = model
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension FinancialOptionViewController{
    func mapValues(_ item: FinancialModel){
        self.loanPlanTextField.text = item.loanPlan
        self.paymentTypeTextField.text = item.paymentType
        self.interestTextField.text = item.interestRate?.toString
        self.loanTermTextField.text = item.loanTerm?.toString
        self.paymentPerYearTextField.text = item.paymentPerYear?.toString
        self.taxesEscrowTextField.text = item.taxesEscrow?.toString
        self.insuranceEscrowTextField.text = item.insuranceEscrow?.toString
        self.mtgInsuranceEscrowTextField.text = item.mtgInsuranceEscrow?.toString
        self.hoaComboFeesTextField.text = item.hoaComboFee?.toString
        self.nameTextField.text = item.contactModel?.fullName
        self.mtgAmountTextField.text = item.mtgAmount?.toString
    }
    func didEditCompletion(_ sender: @escaping((FinancialModel, Int)->Void)){
        self.editSaveCompletion = sender
    }
}


