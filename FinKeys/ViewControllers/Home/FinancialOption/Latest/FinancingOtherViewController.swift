//
//  FinancingOtherViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 10/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancingOtherViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showOther"
    
    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var mtgOneTextField : UITextField!
    @IBOutlet weak var mtgTwoTextField : UITextField!
    @IBOutlet weak var loanOrigTextField : UITextField!
    @IBOutlet weak var prepaidTextField : UITextField!
    @IBOutlet weak var titleOrRecordTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var taxesEscrowTextField : UITextField!
    @IBOutlet weak var insuranceEscrowTextField : UITextField!
    @IBOutlet weak var mtgInsuranceEscrowTextField : UITextField!
    @IBOutlet weak var hoaComboFeesTextField : UITextField!
    @IBOutlet weak var downPaymentTextField : UITextField!
    @IBOutlet weak var minimumDownSwitch: UISwitch!
    @IBOutlet weak var scrollView : UIScrollView!

    
    var paymentTypeModel: BottomPickerModel?
    var loanPlanModel: BottomPickerModel?
    var loanPlanList: Array<DropDownInfoModel>?
    var selectedLoanPlan: DropDownInfoModel?
    var othersDetails: FinancingOptionsDetails?{
        didSet{
            mapping()
            selectedValues()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.configUI()
        self.connectDelegte()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        interestTextField.text = "3.5"
        loanTermTextField.text = "25"
        paymentPerYearTextField.text = "12"
        taxesEscrowTextField.text = "400"
        insuranceEscrowTextField.text = "240"
        hoaComboFeesTextField.text = "0"
        purchasePriceTextField.text = "450000"
//        mtgOneTextField.text = "405000"
        mtgTwoTextField.text = "0"
        loanOrigTextField.text = "4875"
        prepaidTextField.text = "3501"
        titleOrRecordTextField.text = "3375"
    }
    private func selectedValues(){
        if let loanPlan = self.othersDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.loanPlanTextField.text = selectedLoanPlan?.name
            }
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
    private func configUI(){
//        self.mtgOneTextField.isEnabled = minimumDownSwitch.isOn ? false : true
        self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
    }
    private func connectDelegte(){
        loanPlanTextField.delegate = self
        paymentTypeTextField.delegate = self
    }
    @IBAction func switchValueChanged(_ sender: UISwitch){
//        self.mtgOneTextField.isEnabled = sender.isOn ? false : true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FinancingOtherViewController{
    
    var pmt: Double{
        return Formula.calculatePMT(interestTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, firstMtgAmount)
    }
    var mtgInsPmt: Double{
        return firstMtgAmount * 0.0085/12
    }
    var firstMtgAmount: Double{
        return purchasePriceTextField.toDouble - downPaymentTextField.toDouble
    }
    var totalMonthlyPayment: Double{
        var total: Double = 0
        total += pmt
        total += taxesEscrowTextField.toDouble
        total += insuranceEscrowTextField.toDouble
//        total += mtgInsPmt
        total += hoaComboFeesTextField.toDouble
        return total
    }
    var approxiClosingCost: Double{
        var cost: Double = 0
        cost += loanOrigTextField.toDouble
        cost += prepaidTextField.toDouble
        cost += titleOrRecordTextField.toDouble
        return cost
    }
    var downPayment: Double{
        var payment = purchasePriceTextField.toDouble
//        payment -= mtgOneTextField.toDouble
        payment -= mtgTwoTextField.toDouble
        return payment
    }
    var totalCashClosing: Double{
        return approxiClosingCost + downPaymentTextField.toDouble
    }
}
extension FinancingOtherViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanPlanTextField{
            self.showBottomPicker(delegate: self, items: self.loanPlanList ?? [], tag: 0, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
}
extension FinancingOtherViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.selectedLoanPlan = model
            self.loanPlanTextField.text = model?.name
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension FinancingOtherViewController{
    private func mapping(){
        let item = othersDetails
        interestTextField.text = item?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = item?.loanTerm
        paymentPerYearTextField.text = item?.payPerYr
        taxesEscrowTextField.text = item?.taxEscrow?.readableFormat
        insuranceEscrowTextField.text = item?.insEscrow?.readableFormat
        hoaComboFeesTextField.text = item?.hoaCondoFees?.readableFormat
        purchasePriceTextField.text = item?.purchasePrice?.readableFormat
//        mtgOneTextField.text = item?.stMtgAmt?.readableFormat
        mtgTwoTextField.text = item?.ndMtgAmt?.readableFormat
        loanOrigTextField.text = item?.loanOrigDisc?.readableFormat
        prepaidTextField.text = item?.prePaids?.readableFormat
        titleOrRecordTextField.text = item?.titleRecordMisc?.readableFormat
        downPaymentTextField.text = item?.downPayment?.readableFormat
    }
}
extension FinancingOtherViewController{
    var othersParam: Dictionary<String, Any>{
        let key = APIKey.financing.key.self
        var dict: Dictionary<String, Any> = [:]
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPayment3)
        dict.updateValue(interestTextField.toDouble, forKey: key.interestRate3)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTerm3)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear3)
        dict.updateValue(taxesEscrowTextField.toDouble, forKey: key.taxesEscrow3)
        dict.updateValue(insuranceEscrowTextField.toDouble, forKey: key.insuranceEscrow3)
        dict.updateValue(mtgInsPmt, forKey: key.mtgInsurancePmt3)
        dict.updateValue(hoaComboFeesTextField.toDouble, forKey: key.hoaCondaFees3)
        dict.updateValue(purchasePriceTextField.toDouble, forKey: key.purchasePrice3)
        dict.updateValue(firstMtgAmount, forKey: key.stMtgAmt3)
        dict.updateValue(mtgTwoTextField.toDouble, forKey: key.ndMtgAmt3)
        dict.updateValue(loanOrigTextField.toDouble, forKey: key.loanOrigDisc3)
        dict.updateValue(prepaidTextField.toDouble, forKey: key.prePaids3)
        dict.updateValue(titleOrRecordTextField.toDouble, forKey: key.titleRecordsMisc3)
//        dict.updateValue(mtgInsuranceEscrowTextField.toDouble, forKey: key.mortageInsurance3)
        dict.updateValue(Date().toString(format: "dd-MM-yyyy")!, forKey: key.currentDate)
        dict.updateValue(self.selectedLoanPlan?.value ?? "", forKey: key.loanPlan3)
        return dict
    }
}
