//
//  FinancingConventionalViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 10/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancingConventionalViewController: UIViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showConventional"
    
    
    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var mtgOneTextField : UITextField!
    @IBOutlet weak var mtgTwoTextField : UITextField!
    @IBOutlet weak var loanOrigTextField : UITextField!
    @IBOutlet weak var prepaidTextField : UITextField!
    @IBOutlet weak var titleOrRecordTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var taxesEscrowTextField : UITextField!
    @IBOutlet weak var insuranceEscrowTextField : UITextField!
    @IBOutlet weak var mtgInsuranceEscrowTextField : UITextField!
    @IBOutlet weak var hoaComboFeesTextField : UITextField!
    @IBOutlet weak var minimumDownSwitch: UISwitch!
    @IBOutlet weak var downPaymentTextField : UITextField!
    @IBOutlet weak var downpaymentHeightConstraints : NSLayoutConstraint!
    @IBOutlet weak var scrollView : UIScrollView!
    let percentageDownPayment: Double = 3

    var paymentTypeModel: BottomPickerModel?
    var loanPlanList: Array<DropDownInfoModel>?
    var selectedLoanPlan: DropDownInfoModel?
    var conventionalDetails: FinancingOptionsDetails?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.connectDelegte()
        self.configUI()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        interestTextField.text = "3.875"
        loanTermTextField.text = "30"
        paymentPerYearTextField.text = "12"
        taxesEscrowTextField.text = "410"
        insuranceEscrowTextField.text = "400"
        hoaComboFeesTextField.text = "0"
        purchasePriceTextField.text = "485000"
//        mtgOneTextField.text = "315000"
        mtgTwoTextField.text = "0"
        loanOrigTextField.text = "4800"
        prepaidTextField.text = "3800"
        titleOrRecordTextField.text = "3300"
    }
    private func selectedValues(){
        if let loanPlan = self.conventionalDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.loanPlanTextField.text = selectedLoanPlan?.name
            }
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
    @IBAction func didChangeTextFieldPercentage(_ sender: UITextField){
        sender.text = sender.text?.toNumber.toPercentage
    }
    private func configUI(){
//        self.mtgOneTextField.isEnabled = minimumDownSwitch.isOn ? false : true
        self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
    }
    private func connectDelegte(){
        loanPlanTextField.delegate = self
        paymentTypeTextField.delegate = self
        downPaymentTextField.delegate = self
    }
    @IBAction func switchValueChanged(_ sender: UISwitch){
//        self.mtgOneTextField.isEnabled = sender.isOn ? false : true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FinancingConventionalViewController{

    var pmt: Double{
        return Formula.calculatePMT(interestTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, firstMtgAmount)
    }
    var mtgInsPmt: Double{
        if isDownPaymentGreater{
            return 0
        }
        return firstMtgAmount * 0.005/12
    }
    var totalMonthlyPayment: Double{
        var total: Double = 0
        total += pmt
        total += taxesEscrowTextField.toDouble
        total += insuranceEscrowTextField.toDouble
        total += mtgInsPmt
        total += hoaComboFeesTextField.toDouble
        return total
    }
    var approxiClosingCost: Double{
        var cost: Double = 0
        cost += loanOrigTextField.toDouble
        cost += prepaidTextField.toDouble
        cost += titleOrRecordTextField.toDouble
        return cost
    }
    var downPayment: Double{
        if !minimumDownSwitch.isOn{
            return downPaymentTextField.toDouble
        }
        var payment = purchasePriceTextField.toDouble
//        payment -= mtgOneTextField.toDouble
        payment -= mtgTwoTextField.toDouble
        return payment
    }
    var updatedDownpayment: Double{
        return minimumDownSwitch.isOn ? 0 : downPaymentTextField.toDouble
    }
    var totalCashClosing: Double{
        return approxiClosingCost + downPaymentTextField.toDouble
    }
    var validDownpayment: Double{
        return (purchasePriceTextField.toDouble * percentageDownPayment).toValue
    }
    var isValidDownPayment: Bool{
        if minimumDownSwitch.isOn{
            return true
        }
        return downPaymentTextField.toDouble >= validDownpayment
    }
    private func hideDownpayment(){
        self.downPaymentTextField.isHidden = minimumDownSwitch.isOn ? true : false
        self.downpaymentHeightConstraints.constant = minimumDownSwitch.isOn ? 0 : 40
    }
    var firstMtgAmount: Double{
        if minimumDownSwitch.isOn{
            return purchasePriceTextField.toDouble * 0.97
        }
        return purchasePriceTextField.toDouble - downPaymentTextField.toDouble
    }
    var isDownPaymentGreater: Bool{
        let maxLimit = purchasePriceTextField.toDouble * 20/100//Calc 20% of purchase price
        return maxLimit <= downPayment ? true : false
    }
}
extension FinancingConventionalViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanPlanTextField{
            self.showBottomPicker(delegate: self, items: self.loanPlanList ?? [], tag: 0, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }
        if textField == downPaymentTextField{
            if purchasePriceTextField.isEmpty{
                self.showAlert("", "Please enter valid purchase purchase to enter downpayment")
                return false
            }
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
        if textField == downPaymentTextField && !minimumDownSwitch.isOn{
            if !isValidDownPayment{
                self.showAlert("", "Downpayment should be greater than \(percentageDownPayment)% of purchase price. For example, your minimum downpayment is \(validDownpayment)")
            }
        }
    }
}
extension FinancingConventionalViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.selectedLoanPlan = model
            self.loanPlanTextField.text = model?.name
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension FinancingConventionalViewController{
    private func mapping(){
        let item = conventionalDetails
        interestTextField.text = item?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = item?.loanTerm
        paymentPerYearTextField.text = item?.payPerYr
        taxesEscrowTextField.text = item?.taxEscrow?.readableFormat
        insuranceEscrowTextField.text = item?.insEscrow?.readableFormat
        hoaComboFeesTextField.text = item?.hoaCondoFees?.readableFormat
        purchasePriceTextField.text = item?.purchasePrice?.readableFormat
//        mtgOneTextField.text = item?.stMtgAmt?.readableFormat
        mtgTwoTextField.text = item?.ndMtgAmt?.readableFormat
        loanOrigTextField.text = item?.loanOrigDisc?.readableFormat
        prepaidTextField.text = item?.prePaids?.readableFormat
        titleOrRecordTextField.text = item?.titleRecordMisc?.readableFormat
        minimumDownSwitch.isOn = (item?.minimumDown ?? "0") == "0" ? false : true
        downPaymentTextField.text = item?.downPayment?.readableFormat
//        self.hideDownpayment()
    }
}
extension FinancingConventionalViewController{
    var conventionalParam: Dictionary<String, Any>{
        let key = APIKey.financing.key.self
        var dict: Dictionary<String, Any> = [:]
//        dict.updateValue(downPayment, forKey: key.minimumDown2)
        dict.updateValue(minimumDownSwitch.isOn ? "1" : "0", forKey: key.minimumDown2)
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPayment2)
        dict.updateValue(interestTextField.toDouble, forKey: key.interestRate2)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTerm2)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear2)
        dict.updateValue(taxesEscrowTextField.toDouble, forKey: key.taxesEscrow2)
        dict.updateValue(insuranceEscrowTextField.toDouble, forKey: key.insuranceEscrow2)
        dict.updateValue(mtgInsPmt, forKey: key.mtgInsurancePmt2)
        dict.updateValue(hoaComboFeesTextField.toDouble, forKey: key.hoaCondaFees2)
        dict.updateValue(purchasePriceTextField.toDouble, forKey: key.purchasePrice2)
        dict.updateValue(firstMtgAmount, forKey: key.stMtgAmt2)
        dict.updateValue(mtgTwoTextField.toDouble, forKey: key.ndMtgAmt2)
        dict.updateValue(loanOrigTextField.toDouble, forKey: key.loanOrigDisc2)
        dict.updateValue(prepaidTextField.toDouble, forKey: key.prePaids2)
        dict.updateValue(titleOrRecordTextField.toDouble, forKey: key.titleRecordsMisc2)
//        dict.updateValue(mtgInsuranceEscrowTextField.toDouble, forKey: key.mortageInsurance2)
        dict.updateValue(Date().toString(format: "dd-MM-yyyy")!, forKey: key.currentDate)
        dict.updateValue(self.selectedLoanPlan?.value ?? "", forKey: key.loanPlan2)
        return dict
    }
}
