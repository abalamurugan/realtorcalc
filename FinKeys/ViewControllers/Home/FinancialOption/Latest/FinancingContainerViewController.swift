//
//  FinancingContainerViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 10/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancingContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFinancing"
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var financingFhaContainerView: UIView!
    @IBOutlet weak var financingConventionalContainerView: UIView!
    @IBOutlet weak var financingOtherContainerView: UIView!

    private var fhaController: FinancingFHAViewController!
    private var conventionalController: FinancingConventionalViewController!
    private var othersController: FinancingOtherViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    private func configUI(){
        financingFhaContainerView.isHidden = false
        financingConventionalContainerView.isHidden = true
        financingOtherContainerView.isHidden = true
        self.title = LocalizableKey.navigationTitle.financeOption
    }
    private func setup(){
        self.configUI()
        self.fetch()
    }
    private func fetch(){
        CommonRequest.dropDownAPI(type: .loanPlan) { (list) in
            self.fhaController.loanPlanList = list
            self.conventionalController.loanPlanList = list
            self.othersController.loanPlanList = list
        }
    }
    @IBAction func segmentedAction(_ sender: UISegmentedControl){
        financingFhaContainerView.isHidden = true
        financingConventionalContainerView.isHidden = true
        financingOtherContainerView.isHidden = true
        if sender.selectedSegmentIndex == 0{
            financingFhaContainerView.isHidden = false
        }else if sender.selectedSegmentIndex == 1{
            financingConventionalContainerView.isHidden = false
        }else if sender.selectedSegmentIndex == 2{
            financingOtherContainerView.isHidden = false
        }
    }
    @IBAction func calculateAction(_ sender: UIButton){
        if !fhaController.isValidDownPayment{
            self.showAlert("", "FHA Downpayment should be greater than \(fhaController.percentageDownPayment)% of purchase price. For example, your minimum downpayment is \(fhaController.validDownpayment)")
            return
        }else if !conventionalController.isValidDownPayment{
            self.showAlert("", "Conventional Downpayment should be greater than \(conventionalController.percentageDownPayment)% of purchase price. For example, your minimum downpayment is \(conventionalController.validDownpayment)")
            return
        }
        self.postFormValues()
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FinancingFHAViewController.segueIdentifier{
            fhaController = segue.destination as? FinancingFHAViewController
        }else if segue.identifier == FinancingConventionalViewController.segueIdentifier{
            conventionalController = segue.destination as? FinancingConventionalViewController
        }else if segue.identifier == FinancingOtherViewController.segueIdentifier{
            othersController = segue.destination as? FinancingOtherViewController
        }else if segue.identifier == InvestmentOutputViewController.segueIdentifier{
            let outputController = segue.destination as! InvestmentOutputViewController
            outputController.dataModel.outputList = self.outputList
            outputController.dataModel.contactModel = self.contactModel
            outputController.dataModel.type = .financing
        }
    }
}
extension FinancingContainerViewController{
    var fhaDetails: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        let _controller = fhaController!
//        list.append(OutputDetailsModel.init(key: "Loan Plan:", value: _controller.loanPlanTextField.text!))
        list.append(OutputDetailsModel.init(key: "Minimum Down?", value: _controller.minimumDownSwitch.isOn ? "YES" : "NO"))
        list.append(OutputDetailsModel.init(key: "Interest Rate:", value: _controller.interestTextField.text!))
        list.append(OutputDetailsModel.init(key: "Loan Term:", value: _controller.loanTermTextField.text!))
        list.append(OutputDetailsModel.init(key: "Payments Per Year", value: _controller.paymentPerYearTextField.text!))
        list.append(OutputDetailsModel.init(key: "Principal & Interest Pmt.", value: _controller.pmt))
        list.append(OutputDetailsModel.init(key: "+ Taxes Escrow (Monthly)", value: _controller.taxesEscrowTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "+ Insurance Escrow  (Monthly)", value: _controller.insuranceEscrowTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "Mtg.Ins.Pmt.(580+FICO)(Monthly)", value: _controller.mtgInsPmt))
        list.append(OutputDetailsModel.init(key: "+ HOA/Condo Fees (Monthly)", value: _controller.hoaComboFeesTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "Total Monthly Payment", value: _controller.totalMonthlyPayment))
        list.append(OutputDetailsModel.init(key: "Approximate Closing Costs:", value: _controller.approxiClosingCost))
        list.append(OutputDetailsModel.init(key: "Down Payment", value: _controller.downPayment))
        list.append(OutputDetailsModel.init(key: "Loan Amount", value: _controller.loanAmount))
        list.append(OutputDetailsModel.init(key: "Up Front Mortgage Insurance", value: _controller.upFrontMtgInsurance))
        list.append(OutputDetailsModel.init(key: "-Total 1st Mtg. Amount", value: _controller.fhaMtgOneAmount))
        list.append(OutputDetailsModel.init(key: "Total Cash at Closing", value: _controller.totalCashClosing))
        return list
    }
    var conventionalDetails: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        let _controller = conventionalController!
//        list.append(OutputDetailsModel.init(key: "Loan Plan:", value: _controller.loanPlanTextField.text!))
        list.append(OutputDetailsModel.init(key: "Minimum Down?", value: _controller.minimumDownSwitch.isOn ? "YES" : "NO"))
        list.append(OutputDetailsModel.init(key: "Interest Rate:", value: _controller.interestTextField.text!))
        list.append(OutputDetailsModel.init(key: "Loan Term:", value: _controller.loanTermTextField.text!))
        list.append(OutputDetailsModel.init(key: "Payments Per Year", value: _controller.paymentPerYearTextField.text!))
        list.append(OutputDetailsModel.init(key: "Principal & Interest Pmt.", value: _controller.pmt))
        list.append(OutputDetailsModel.init(key: "+ Taxes Escrow (Monthly)", value: _controller.taxesEscrowTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "+ Insurance Escrow  (Monthly)", value: _controller.insuranceEscrowTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "Mtg.Ins.Pmt.(580+FICO)(Monthly)", value: _controller.mtgInsPmt))
        list.append(OutputDetailsModel.init(key: "+ HOA/Condo Fees (Monthly)", value: _controller.hoaComboFeesTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "Total Monthly Payment", value: _controller.totalMonthlyPayment))
        list.append(OutputDetailsModel.init(key: "Approximate Closing Costs:", value: _controller.approxiClosingCost))
        list.append(OutputDetailsModel.init(key: "Down Payment", value: _controller.updatedDownpayment))
        list.append(OutputDetailsModel.init(key: "- 1st Mtg. Amount", value: _controller.firstMtgAmount))
        list.append(OutputDetailsModel.init(key: "Total Cash at Closing", value: _controller.totalCashClosing))
        return list
    }
    var othersDetails: Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        let _controller = othersController!
//        list.append(OutputDetailsModel.init(key: "Loan Plan:", value: _controller.loanPlanTextField.text!))
//        list.append(OutputDetailsModel.init(key: "Minimum Down?", value: _controller.minimumDownSwitch.isOn ? "YES" : "NO"))
        list.append(OutputDetailsModel.init(key: "Interest Rate:", value: _controller.interestTextField.text!))
        list.append(OutputDetailsModel.init(key: "Loan Term:", value: _controller.loanTermTextField.text!))
        list.append(OutputDetailsModel.init(key: "Payments Per Year", value: _controller.paymentPerYearTextField.text!))
        list.append(OutputDetailsModel.init(key: "Principal & Interest Pmt.", value: _controller.pmt))
        list.append(OutputDetailsModel.init(key: "+ Taxes Escrow (Monthly)", value: _controller.taxesEscrowTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "+ Insurance Escrow  (Monthly)", value: _controller.insuranceEscrowTextField.text ?? 0))
//        list.append(OutputDetailsModel.init(key: "Mtg.Ins.Pmt.(700+FICO)(Monthly)", value: _controller.mtgInsPmt))
        list.append(OutputDetailsModel.init(key: "+ HOA/Condo Fees (Monthly)", value: _controller.hoaComboFeesTextField.text ?? 0))
        list.append(OutputDetailsModel.init(key: "Total Monthly Payment", value: _controller.totalMonthlyPayment))
        list.append(OutputDetailsModel.init(key: "Approximate Closing Costs:", value: _controller.approxiClosingCost))
        list.append(OutputDetailsModel.init(key: "Down Payment", value: _controller.downPayment))
        list.append(OutputDetailsModel.init(key: "- 1st Mtg. Amount", value: _controller.firstMtgAmount))
        list.append(OutputDetailsModel.init(key: "Total Cash at Closing", value: _controller.totalCashClosing))

        return list
    }
    var outputList: Array<RefinanceOutputModel>{
        var list = Array<RefinanceOutputModel>()
        list.append(RefinanceOutputModel.init(title: "FHA Payment & Cash Needed", list: fhaDetails))
        list.append(RefinanceOutputModel.init(title: "Conventional Payment & Cash Needed", list: conventionalDetails))
        list.append(RefinanceOutputModel.init(title: "Other Payment & Cash Needed", list: othersDetails))
        return list
    }
}
extension FinancingContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getFinancingOption()
    }
}
extension FinancingContainerViewController{
    var paramViewFinancing: Dictionary<String, Any>{
        var parm = Param.viewReportParam(tagReport: Constants.tagValue.financingView, contactId: self.contactModel!.contactId!)
        parm.updateValue("option1", forKey: APIKey.financing.key.option1)
        parm.updateValue("option2", forKey: APIKey.financing.key.option2)
        parm.updateValue("option3", forKey: APIKey.financing.key.option3)
        return parm

    }
    private func getFinancingOption(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewFinancing, encoding: .queryString, { (response: ResponseModel<FinancingOption>) in
            let _item = response.item?.response
            self.fhaController.fhaDetails = _item?.option1
            self.conventionalController.conventionalDetails = _item?.option2
            self.othersController.othersDetails = _item?.option3
        }) { (error) in
            self.showError(error)
        }
    }
    var paramUpdateFinancing: Dictionary<String, Any>{
        let key = APIKey.financing.key.self
        var param: Dictionary<String, Any> = [APIKey.common.key.contactId:self.contactModel!.contactId!]
        param.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        param.updateValue("option1", forKey: key.option1)
        param.updateValue("option2", forKey: key.option2)
        param.updateValue("option3", forKey: key.option3)
        param.updateValue(Constants.tagValue.financingAdd, forKey: APIKey.common.key.tag)
        param.updateValue(Constants.reportNames.financeOption, forKey: APIKey.common.key.action)
        fhaController.fhaParam.forEach { (_key, value) in
            param.updateValue(value, forKey: _key)
        }
        conventionalController.conventionalParam.forEach { (_key, value) in
            param.updateValue(value, forKey: _key)
        }
        othersController.othersParam.forEach { (_key, value) in
            param.updateValue(value, forKey: _key)
        }
        return param
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: paramUpdateFinancing, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.performSegue(withIdentifier: InvestmentOutputViewController.segueIdentifier, sender: self)
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
