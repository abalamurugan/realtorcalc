//
//  FinancingFHAViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 10/08/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancingFHAViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFHA"

    
    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var mtgOneTextField : UITextField!
    @IBOutlet weak var mtgTwoTextField : UITextField!
    @IBOutlet weak var loanOrigTextField : UITextField!
    @IBOutlet weak var prepaidTextField : UITextField!
    @IBOutlet weak var titleOrRecordTextField : UITextField!
    @IBOutlet weak var loanPlanTextField : UITextField!
    @IBOutlet weak var paymentTypeTextField : UITextField!
    @IBOutlet weak var interestTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var taxesEscrowTextField : UITextField!
    @IBOutlet weak var insuranceEscrowTextField : UITextField!
    @IBOutlet weak var mtgInsuranceEscrowTextField : UITextField!
    @IBOutlet weak var hoaComboFeesTextField : UITextField!
    @IBOutlet weak var downPaymentTextField : UITextField!
    @IBOutlet weak var downpaymentHeightConstraints : NSLayoutConstraint!
    @IBOutlet weak var minimumDownSwitch: UISwitch!
    @IBOutlet weak var scrollView : UIScrollView!

    
    var paymentTypeModel: BottomPickerModel?
    var loanPlanModel: BottomPickerModel?
    var loanPlanList: Array<DropDownInfoModel>?
    var selectedLoanPlan: DropDownInfoModel?
    let percentageDownPayment: Double = 3.5
    var fhaDetails: FinancingOptionsDetails?{
        didSet{
            self.mapping()
            self.selectedValues()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        self.configUI()
        self.connectDelegte()
        // Do any additional setup after loading the view.
    }
    private func defaultValues(){
        interestTextField.text = "4.250"
        loanTermTextField.text = "30"
        paymentPerYearTextField.text = "12"
        taxesEscrowTextField.text = "382"
        insuranceEscrowTextField.text = "302"
        hoaComboFeesTextField.text = "250"
        purchasePriceTextField.text = "485000"
        mtgTwoTextField.text = "0"
        loanOrigTextField.text = "3875"
        prepaidTextField.text = "3178"
        titleOrRecordTextField.text = "3375"
    }
    private func configUI(){
//        self.mtgOneTextField.isEnabled = minimumDownSwitch.isOn ? false : true
        self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
    }
    private func connectDelegte(){
        loanPlanTextField.delegate = self
    }
    @IBAction func switchValueChanged(_ sender: UISwitch){
//        self.mtgOneTextField.isEnabled = sender.isOn ? false : true
        self.hideDownpayment()
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
    @IBAction func didChangeTextFieldPercentage(_ sender: UITextField){
        sender.text = sender.text?.toNumber.toPercentage
    }
    private func selectedValues(){
        if let loanPlan = self.fhaDetails?.loanPlan{
            if let index = self.loanPlanList?.index(of: DropDownInfoModel.init(value: loanPlan)){
                self.selectedLoanPlan = self.loanPlanList?[index]
                self.loanPlanTextField.text = selectedLoanPlan?.name
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FinancingFHAViewController{
    var fhaMtgOneAmount: Double{
        if minimumDownSwitch.isOn{
            let exp1 = purchasePriceTextField.toDouble - (purchasePriceTextField.toDouble * 0.035)
            return (exp1 + ((exp1) * 0.0175))
        }
        let upFrontMtgIns = !isDownPaymentGreater ? upFrontMtgInsurance : 0
        return loanAmount + upFrontMtgIns
    }
    var pmt: Double{
        print("MTG One amount===>\(fhaMtgOneAmount)")
        print("Loan amount===>\(loanAmount)")
        let amount = minimumDownSwitch.isOn ? fhaMtgOneAmount : loanAmount
        return Formula.calculatePMT(interestTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, amount)
    }
    var mtgInsPmt: Double{
        if isDownPaymentGreater{
            return 0
        }
        let amount = minimumDownSwitch.isOn ? fhaMtgOneAmount : loanAmount
        return amount * 0.0085/12
    }
    var totalMonthlyPayment: Double{
        var total: Double = 0
        total += pmt
        total += taxesEscrowTextField.toDouble
        total += insuranceEscrowTextField.toDouble
        total += mtgInsPmt
        total += hoaComboFeesTextField.toDouble
        return total
    }
    var approxiClosingCost: Double{
        var cost: Double = 0
        cost += loanOrigTextField.toDouble
        cost += prepaidTextField.toDouble
        cost += titleOrRecordTextField.toDouble
        return cost
    }
    var downPayment: Double{
        if !minimumDownSwitch.isOn{
            return downPaymentTextField.toDouble
        }
        var payment = purchasePriceTextField.toDouble
        payment -= fhaMtgOneAmount
        payment -= mtgTwoTextField.toDouble
        print("downpayment===>\(payment)")
        return payment
    }
    var totalCashClosing: Double{
        return approxiClosingCost + downPayment
    }
    var validDownpayment: Double{
        return (purchasePriceTextField.toDouble * percentageDownPayment).toValue
    }
    var isValidDownPayment: Bool{
        if minimumDownSwitch.isOn{
            return true
        }
        return downPaymentTextField.toDouble >= validDownpayment
    }
    var upFrontMtgInsurance: Double{
        if isDownPaymentGreater{
            return 0
        }
        return loanAmount * 0.0175
    }
    var loanAmount: Double{
        if minimumDownSwitch.isOn{
            return purchasePriceTextField.toDouble * (1-0.035)
        }
        let amount = purchasePriceTextField.toDouble - downPaymentTextField.toDouble
        print("new loan amount===>\(amount)")
        return amount
    }
    var updatedDownpayment: Double{
        return minimumDownSwitch.isOn ? 0 : downPaymentTextField.toDouble
    }
    private func hideDownpayment(){
        self.downPaymentTextField.isHidden = minimumDownSwitch.isOn ? true : false
        self.downpaymentHeightConstraints.constant = minimumDownSwitch.isOn ? 0 : 40
    }
    var isDownPaymentGreater: Bool{
        let maxLimit = purchasePriceTextField.toDouble * 20/100//Calc 20% of purchase price
        return maxLimit <= downPayment ? true : false
    }
}
extension FinancingFHAViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loanPlanTextField{
            self.showBottomPicker(delegate: self, items: self.loanPlanList ?? [], tag: 0, title: LocalizableKey.financingTitle.loanPlan)
            return false
        }
        if textField == downPaymentTextField{
            if purchasePriceTextField.isEmpty{
                self.showAlert("", "Please enter valid purchase purchase to enter downpayment")
                return false
            }
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
        if textField == downPaymentTextField && !minimumDownSwitch.isOn{
            if !isValidDownPayment{
                self.showAlert("", "Downpayment should be greater than \(percentageDownPayment)% of purchase price. For example, your minimum downpayment is \(validDownpayment)")
            }
        }
    }
}
extension FinancingFHAViewController: BottomPickerViewControllerDelegate{
    func didPickedItem(viewController: BottomPickerViewController, item: Any) {
        let model = item as? DropDownInfoModel
        if viewController.dataModel.tag == 0{
            self.selectedLoanPlan = model
            self.loanPlanTextField.text = model?.name
        }
    }
    
    func didPickerCancelled(viewController: BottomPickerViewController) {
        
    }
}
extension FinancingFHAViewController{
    private func mapping(){
        let item = fhaDetails
        interestTextField.text = item?.interestRate?.toNumber.toPercentage
        loanTermTextField.text = item?.loanTerm
        paymentPerYearTextField.text = item?.payPerYr
        taxesEscrowTextField.text = item?.taxEscrow?.readableFormat
        insuranceEscrowTextField.text = item?.insEscrow?.readableFormat
        hoaComboFeesTextField.text = item?.hoaCondoFees?.readableFormat
        purchasePriceTextField.text = item?.purchasePrice?.readableFormat
//        mtgOneTextField.text = item?.stMtgAmt?.readableFormat
        mtgTwoTextField.text = item?.ndMtgAmt?.readableFormat
        loanOrigTextField.text = item?.loanOrigDisc?.readableFormat
        prepaidTextField.text = item?.prePaids?.readableFormat
        titleOrRecordTextField.text = item?.titleRecordMisc?.readableFormat
        minimumDownSwitch.isOn = (item?.minimumDown ?? "0") == "0" ? false : true
        downPaymentTextField.text = item?.downPayment?.readableFormat
        self.hideDownpayment()
    }
}
extension FinancingFHAViewController{
    var fhaParam: Dictionary<String, Any>{
        let key = APIKey.financing.key.self
        var dict: Dictionary<String, Any> = [:]
//        dict.updateValue(downPayment, forKey: key.minimumDown1)
        dict.updateValue(minimumDownSwitch.isOn ? "1" : "0", forKey: key.minimumDown1)
        dict.updateValue(downPaymentTextField.toDouble, forKey: key.downPayment1)
        dict.updateValue(interestTextField.toDouble, forKey: key.interestRate1)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTerm1)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear1)
        dict.updateValue(taxesEscrowTextField.toDouble, forKey: key.taxesEscrow1)
        dict.updateValue(insuranceEscrowTextField.toDouble, forKey: key.insuranceEscrow1)
        dict.updateValue(mtgInsPmt, forKey: key.mtgInsurancePmt1)
        dict.updateValue(hoaComboFeesTextField.toDouble, forKey: key.hoaCondaFees1)
        dict.updateValue(purchasePriceTextField.toDouble, forKey: key.purchasePrice1)
        dict.updateValue(fhaMtgOneAmount, forKey: key.stMtgAmt1)
        dict.updateValue(mtgTwoTextField.toDouble, forKey: key.ndMtgAmt1)
        dict.updateValue(loanOrigTextField.toDouble, forKey: key.loanOrigDisc1)
        dict.updateValue(prepaidTextField.toDouble, forKey: key.prePaids1)
        dict.updateValue(titleOrRecordTextField.toDouble, forKey: key.titleRecordsMisc1)
//        dict.updateValue(mtgInsuranceEscrowTextField.toDouble, forKey: key.mortageInsurance1)
        dict.updateValue(Date().toString(format: "dd-MM-yyyy")!, forKey: key.currentDate)
        dict.updateValue(self.selectedLoanPlan?.value ?? "", forKey: key.loanPlan1)
        return dict
    }
}
extension FinancingFHAViewController{

}
