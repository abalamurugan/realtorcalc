//
//  FinancialOutputViewController.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancialOutputViewController: UIViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showFinanceOutput"
    
    
    @IBOutlet weak var tableView : UITableView!
    
    lazy var dataModel : FinancialOptionDataModel = {return FinancialOptionDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.output
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension FinancialOutputViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataModel.type == .payment{
            let cell = tableView.dequeueReusableCell(withIdentifier: FinancialOutputTableViewCell.reuseIdentifier, for: indexPath) as! FinancialOutputTableViewCell
            cell.set(dataModel[indexPath])
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: FinancialCashOutputTableViewCell.reuseIdentifier, for: indexPath) as! FinancialCashOutputTableViewCell
            cell.set(dataModel[indexPath])
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height : CGFloat = dataModel.type == .payment ? 170 : 120
        return height
    }
}
