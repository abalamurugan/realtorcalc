//
//  FinancialOptionContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class FinancialOptionContainerViewController: UIViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showFinanceOption"
    
    @IBOutlet weak var segmentedControl : UISegmentedControl!
    @IBOutlet weak var financePaymentContainer : UIView!
    @IBOutlet weak var financeCashContainer : UIView!

    var financePaymentController : FinancialOptionViewController!
    var financeCashController : FinancialCashViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.financeOption
        financeCashContainer.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    @IBAction func segmentChanged(_ sender : UISegmentedControl){
        let type = FinanceOptionType.init(rawValue: sender.selectedSegmentIndex)
        if type! == .payment{
            financePaymentContainer.isHidden = false
            financeCashContainer.isHidden = true
        }else{
            financePaymentContainer.isHidden = true
            financeCashContainer.isHidden = false
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FinancialOptionViewController.segueIdentifier{
            financePaymentController = segue.destination as? FinancialOptionViewController
        }else if segue.identifier == FinancialCashViewController.segueIdentifier{
            financeCashController = segue.destination as? FinancialCashViewController
        }
    }
    

}
