//
//  FinancialCashViewController.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import Alamofire

class FinancialCashViewController: UIViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showFinanceCash"
    
    @IBOutlet weak var purchasePriceTextField : UITextField!
    @IBOutlet weak var mtgOneTextField : UITextField!
    @IBOutlet weak var mtgTwoTextField : UITextField!
    @IBOutlet weak var loanOrigTextField : UITextField!
    @IBOutlet weak var prepaidTextField : UITextField!
    @IBOutlet weak var titleOrRecordTextField : UITextField!
    @IBOutlet weak var mortageInsuranceTextField : UITextField!
    @IBOutlet weak var calculateButton : UIButton!
    @IBOutlet weak var addMoreButton : UIButton!

    lazy var dataModel : FinancialOptionDataModel = {return FinancialOptionDataModel()}()
    
    var outputList = Array<FinancialModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addMoreButton.layer.borderColor = UIColor.primaryColor.cgColor
        self.addMoreButton.layer.borderWidth = 1.0
        self.defaultValues()
        // Do any additional setup after loading the view.
    }
    func defaultValues(){
        purchasePriceTextField.text = "450000"
        mtgOneTextField.text = "315000"
        mtgTwoTextField.text = "0"
        loanOrigTextField.text = "7875"
        titleOrRecordTextField.text = "3375"
        prepaidTextField.text = "3178"
        mortageInsuranceTextField.text = "1733"
    }
    /// Mandatory field validations
    ///
    /// - Returns: true-> Validation success, false -> validation failed
    func isValidationSuccess()-> Bool{
        if purchasePriceTextField.text?.isEmpty ?? false{
            showAlert(LocalizableKey.error.errorTitle, LocalizableKey.financialValidation.purchaseInvalid)
            return false
        }else if mtgOneTextField.text?.isEmpty ?? false{
            showAlert(LocalizableKey.errorMessage.errorTitle, LocalizableKey.financialValidation.mtgInvalid)
            return false
        }
        return true
    }
    
    /// Save data Before calc
    private func saveData(){
        let purchase = self.purchasePriceTextField.text?.toDouble ?? 0
        let mtg1 = self.mtgOneTextField.text?.toDouble ?? 0
        let mtg2 = self.mtgTwoTextField.text?.toDouble ?? 0
        
        let financeModel = FinancialModel()
        financeModel.downPayment = dataModel.getDownPayment(purchase, mtg1, mtg2)
        financeModel.loanOrgiExtra = self.loanOrigTextField.text?.toDouble ?? 0
        financeModel.prepaidExtra = self.prepaidTextField.text?.toDouble ?? 0
        financeModel.titleRecordExtra = self.titleOrRecordTextField.text?.toDouble ?? 0
        financeModel.mortageExtra = self.mortageInsuranceTextField.text?.toDouble ?? 0
        outputList.append(financeModel)

    }
    /// Calculate Finance Option
    ///
    /// - Parameter sender: UIButton
    @IBAction func calculateAction(_ sender :UIButton){
        if isValidationSuccess(){
            self.saveData()
            self.performSegue(withIdentifier: FinancialOutputViewController.segueIdentifier, sender: self)
        }
        
    }
    /// Clear and calculate another finance option
    ///
    /// - Parameter sender: UIButton
    @IBAction func addMoreAction(_ sender : UIButton){
        if self.isValidationSuccess(){
            self.saveData()
            self.clearValues()
        }
    }
    private func clearValues(){
        purchasePriceTextField.text = nil
        mtgTwoTextField.text = nil
        mtgOneTextField.text = nil
        mortageInsuranceTextField.text = nil
        loanOrigTextField.text = nil
        titleOrRecordTextField.text = nil
        prepaidTextField.text = nil

    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FinancialOutputViewController.segueIdentifier{
            let outputController = segue.destination as! FinancialOutputViewController
            outputController.dataModel.financialOutputList = outputList
            outputController.dataModel.type = .cashNeeded
        }
    }
   
}
