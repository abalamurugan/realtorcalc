//
//  RefinanceContainerViewController.swift
//  RealtorCalc
//
//  Created by Bala on 05/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RefinanceContainerViewController: BaseViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showRefinance"
    
    var refinanceFormController : RefinanceTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = LocalizableKey.navigationTitle.reFinance
        self.contactPickerAction(UIButton())
        // Do any additional setup after loading the view.
    }
    
    @IBAction func calculateAction(_ sender : UIButton){
        self.postFormValues()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == RefinanceTableViewController.segueIdentifier{
            refinanceFormController = segue.destination as? RefinanceTableViewController
        }
    }
    override func contactPickerAction(_ sender: UIButton) {
        self.showContactPicker(delegate: self)
    }
    
}
extension RefinanceContainerViewController: ContactListViewControllerDelegate{
    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
        self.contactModel = item
        self.contactNameTextField.text = item.fullName
        self.getRefinance()
    }
}
extension RefinanceContainerViewController{
    var paramViewRefinance: Dictionary<String, Any>{
        return Param.viewReportParam(tagReport: Constants.tagValue.refianceView, contactId: self.contactModel!.contactId!)
        
    }
    private func getRefinance(){
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: paramViewRefinance, encoding: .queryString, { (response: ResponseModel<RefinanceModel>) in
            self.refinanceFormController.refinanceDetails = response.item?.response
        }) { (error) in
            self.showError(error)
        }
    }
}

extension RefinanceContainerViewController{
    var postParam: Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID]
        dict.updateValue(self.contactModel!.contactId!, forKey: APIKey.common.key.contactId)
        dict.updateValue(Constants.reportNames.refinance, forKey: APIKey.common.key.action)
        dict.updateValue(Constants.tagValue.refinanceAdd, forKey: APIKey.common.key.tag)
        dict.updateValue(Date.init().toString(format: Constants.dateFormat) ?? "", forKey: APIKey.common.key.createdDate)
        refinanceFormController.updateParam.forEach { (key, value) in
            dict.updateValue(value, forKey: key)
        }
        return dict
    }
    private func postFormValues(){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.sendRequest(urlPath: nil, method: .post, param: postParam, encoding: nil, { (json, data) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.refinanceFormController.submitAction()
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
}
