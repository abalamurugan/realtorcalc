//
//  RefinanceTableViewController.swift
//  RealtorCalc
//
//  Created by Bala on 05/05/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class RefinanceTableViewController: UITableViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showRefinanceForm"
    
    @IBOutlet weak var existOriBalanceTextField : UITextField!
    @IBOutlet weak var interestRateExistTextField : UITextField!
    @IBOutlet weak var loanTermTextField : UITextField!
    @IBOutlet weak var paymentPerYearTextField : UITextField!
    @IBOutlet weak var monthPaidExistTextField : UITextField!
    @IBOutlet weak var holdingPeriodTextField : UITextField!

    @IBOutlet weak var currentBalanceTextField : UITextField!
    @IBOutlet weak var intArrearTextField : UITextField!
    @IBOutlet weak var interestRateNewTextField : UITextField!
    @IBOutlet weak var newMortTermTextField : UITextField!
    @IBOutlet weak var closingFinanceTextField : UITextField!
    @IBOutlet weak var costUpFrontTextField : UITextField!
    @IBOutlet weak var fundRefinancedTextField : UITextField!
    @IBOutlet weak var paymentPerYearNewTextField : UITextField!

    
    @IBOutlet weak var intEarnedAccummulativeTextField : UITextField!
    @IBOutlet weak var intLostPrepaidClosingCostTextField : UITextField!
    @IBOutlet weak var futureValueExcessFundsTextField : UITextField!

    var closingCostNew : Double{
        return closingFinanceTextField.toDouble + costUpFrontTextField.toDouble
    }
    var currentPmt : Double{
        return Formula.calculatePMT(interestRateExistTextField.toDouble, paymentPerYearTextField.toDouble, loanTermTextField.toDouble, existOriBalanceTextField.toDouble)
    }
    var newMortageAmount : Double{
        var amount = existingMtgPayoff
        amount += closingFinanceTextField.toDouble
        amount += costUpFrontTextField.toDouble
        amount += fundRefinancedTextField.toDouble
        return amount
    }
    var currentBalance: Double{
        let interest = interestRateExistTextField.toDouble.toValue/paymentPerYearTextField.toDouble
        return Formula.FVCalc(rate: interest, nper: monthPaidExistTextField.toDouble, pmt: -currentPmt, fv: existOriBalanceTextField.toDouble).removeMinus
    }
    var existingMtgPayoff: Double{
        return currentBalance + intArrearTextField.toDouble
    }
    var newPmt : Double{
        return Formula.calculatePMT(interestRateNewTextField.toDouble, 12, newMortTermTextField.toDouble, newMortageAmount)
    }
    var monthlySavings : Double{
        return currentPmt - newPmt
    }
    var recapturePeriod : Double{
        return closingCostNew/monthlySavings
    }
    
    var refinanceDetails: RefinanceDetailModel?{
        didSet{
            self.mapping()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.defaultValues()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == RefinanceOutputViewController.segueIdentifier{
            let refinanceOutputController = segue.destination as! RefinanceOutputViewController
            refinanceOutputController.dataModel.outputList = sender as? [RefinanceOutputModel]
            if let parent = self.parent as? RefinanceContainerViewController{
                refinanceOutputController.contactModel = parent.contactModel
            }
        }
    
    }
    private func defaultValues(){
        existOriBalanceTextField.text = "620000"
        interestRateExistTextField.text = "4.625"
        loanTermTextField.text = "25"
        paymentPerYearTextField.text = "12"
        monthPaidExistTextField.text = "24"
        holdingPeriodTextField.text = "24"
        
        currentBalanceTextField.text = "592378"
        intArrearTextField.text = "2150"
        interestRateNewTextField.text = "4.125"
        newMortTermTextField.text = "30"
        closingFinanceTextField.text = "1500"
        costUpFrontTextField.text = "1450"
        fundRefinancedTextField.text = "15000"
        
        intEarnedAccummulativeTextField.text = "2.125"
        intLostPrepaidClosingCostTextField.text = "2.500"
        futureValueExcessFundsTextField.text = "6.785"
        
    }
    func submitAction(){
        var refinanceOutputList = Array<RefinanceOutputModel>()
        refinanceOutputList.append(RefinanceOutputModel.init(title: "Recapture Period", list: recaptureOutputList))
        refinanceOutputList.append(RefinanceOutputModel.init(title: "Saving Summary", list: savingSummaryList))
        refinanceOutputList.append(RefinanceOutputModel.init(title: "Detailed Refinance Information", list: detailedRefinanceList))


        self.performSegue(withIdentifier: RefinanceOutputViewController.segueIdentifier, sender: refinanceOutputList)
    }
}
extension RefinanceTableViewController{
    var recaptureOutputList : Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        list.append(OutputDetailsModel.init(key: "New Mortgage Amount:", value: newMortageAmount))
        list.append(OutputDetailsModel.init(key: "Closing Costs on New Mortgage:", value: closingCostNew))
        list.append(OutputDetailsModel.init(key: "Current Mtg. Payment (P&I)", value: currentPmt))
        list.append(OutputDetailsModel.init(key: "  - New Payment (P&I)", value: newPmt))
        list.append(OutputDetailsModel.init(key: "  Monthly Savings/(Cost)", value: monthlySavings))
        list.append(OutputDetailsModel.init(key: "Recapture Period (Closing Costs/Monthly Savings)", value: recapturePeriod.roundUp))
        return list
    }
    var savingSummaryList : Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        print("FV====>\(futureBalanceExisting)")
        print("FV 2====>\(futureBalanceNew)")
        list.append(OutputDetailsModel.init(key: "Future Balance in 30 Months on Existing Mortgage:", value: futureBalanceExisting))
        list.append(OutputDetailsModel.init(key: "Future Balance in 30 Months on New Mortgage:", value: futureBalanceNew))
        list.append(OutputDetailsModel.init(key: "  + Prepaid Closing Costs", value: costUpFrontTextField.toDouble))
        list.append(OutputDetailsModel.init(key: "Less Accum. Savings (Monthly Savings X Holding Period)", value: lessAccumSavings))
        list.append(OutputDetailsModel.init(key: "  Adjusted Unpaid Balance:", value: adjUnpaidBalance))
        list.append(OutputDetailsModel.init(key: "Savings/(Cost) of Refinancing over 30 Months", value: savingCostOverMonth))
        return list
    }
    var detailedRefinanceList : Array<OutputDetailsModel>{
        var list = Array<OutputDetailsModel>()
        print("futureValueFundsInvested ====>\(futureValueFundsInvested)")
        list.append(OutputDetailsModel.init(key: "Interest Earned on Accumulated Savings:", value: interestEarned))
        list.append(OutputDetailsModel.init(key: "Interest Lost on Prepaid Closing Costs:", value: interestLost))
        list.append(OutputDetailsModel.init(key: "Future Value of Excess Funds Invested:", value: futureValueFundsInvested))
        list.append(OutputDetailsModel.init(key: "Detailed Refinance Savings/(Cost):", value: detailedRefinanceCost))
        list.append(OutputDetailsModel.init(key: "Total Refinance Savings/(Cost):", value: totalRefinanceSaving))

        return list
    }
    var futureBalanceExisting: Double{
        let interest = interestRateExistTextField.toDouble.toValue
        let nper = monthPaidExistTextField.toDouble + holdingPeriodTextField.toDouble
        let fv = Formula.FVCalc(rate: interest/paymentPerYearTextField.toDouble, nper: nper, pmt: -currentPmt, fv: existOriBalanceTextField.toDouble)
        return fv.removeMinus
    }
    var futureBalanceNew: Double{
        let interest = interestRateNewTextField.toDouble.toValue
        let fv = Formula.FVCalc(rate: interest/paymentPerYearTextField.toDouble, nper: newMortTermTextField.toDouble, pmt: -newPmt, fv: newMortageAmount)
        return fv.removeMinus
    }
    var lessAccumSavings: Double{
        return monthlySavings * holdingPeriodTextField.toDouble
    }
    var adjUnpaidBalance: Double{
        var unpaidBalance = futureBalanceNew
        unpaidBalance += costUpFrontTextField.toDouble
        unpaidBalance -= lessAccumSavings
        return unpaidBalance
    }
    var savingCostOverMonth: Double{
        return futureBalanceExisting - adjUnpaidBalance
    }
}
extension RefinanceTableViewController{
    var interestEarned: Double{
        return intEarnedAccummulativeTextField.toDouble.toValue * lessAccumSavings
    }
    var interestLost: Double{
        return intLostPrepaidClosingCostTextField.toDouble.toValue * costUpFrontTextField.toDouble
    }
    var futureValueFundsInvested: Double{
        let interest = futureValueExcessFundsTextField.toDouble.toValue/12
        return Formula.FVCalc(rate: interest, nper: holdingPeriodTextField.toDouble, pmt: 0, fv: fundRefinancedTextField.toDouble).removeMinus
    }
    var detailedRefinanceCost: Double{
        var refinanceCost = interestEarned
        refinanceCost -= interestLost
        refinanceCost += futureValueFundsInvested
        return refinanceCost
    }
    var totalRefinanceSaving: Double{
        return detailedRefinanceCost + savingCostOverMonth
    }
}
extension RefinanceTableViewController{
    private func mapping(){
        existOriBalanceTextField.text = refinanceDetails?.originalBalance?.readableFormat
        interestRateExistTextField.text = refinanceDetails?.interestRateE?.toNumber.toPercentage
        loanTermTextField.text = refinanceDetails?.loanTerm
        paymentPerYearTextField.text = refinanceDetails?.payPerYear
        monthPaidExistTextField.text = refinanceDetails?.monthsPaidMortgage
        holdingPeriodTextField.text = refinanceDetails?.holdingPeriod
        
        currentBalanceTextField.text = refinanceDetails?.currentBalance?.readableFormat
        intArrearTextField.text = refinanceDetails?.interestArrears?.readableFormat
        interestRateNewTextField.text = refinanceDetails?.interestRateN?.toNumber.toPercentage
        newMortTermTextField.text = refinanceDetails?.mortgageTerm
        closingFinanceTextField.text = refinanceDetails?.closingCostFinanced?.readableFormat
        costUpFrontTextField.text = refinanceDetails?.closingCostUpFront?.readableFormat
        fundRefinancedTextField.text = refinanceDetails?.excessFunds?.readableFormat
        paymentPerYearNewTextField.text = refinanceDetails?.payPerYearN

        intEarnedAccummulativeTextField.text = refinanceDetails?.interestAccumulated?.toNumber.toPercentage
        intLostPrepaidClosingCostTextField.text = refinanceDetails?.interestLost?.toNumber.toPercentage
        futureValueExcessFundsTextField.text = refinanceDetails?.fundsInvested?.toNumber.toPercentage
    }
}
extension RefinanceTableViewController{
    var updateParam: Dictionary<String, Any>{
        let key = APIKey.refinance.key.self
        var dict: Dictionary<String, Any> = [key.originalBalance : existOriBalanceTextField.toDouble]
        dict.updateValue(interestRateExistTextField.toDouble, forKey: key.interestRateE)
        dict.updateValue(loanTermTextField.toDouble, forKey: key.loanTerm)
        dict.updateValue(paymentPerYearTextField.toDouble, forKey: key.payPerYear)
        
        dict.updateValue(monthPaidExistTextField.toDouble, forKey: key.monthsPaidMortgage)
        dict.updateValue(holdingPeriodTextField.toDouble, forKey: key.holdingPeriod)
        dict.updateValue(currentBalanceTextField.toDouble, forKey: key.currentBalanceE)
        dict.updateValue(intArrearTextField.toDouble, forKey: key.interestArrears)
        dict.updateValue(interestRateNewTextField.toDouble, forKey: key.interestRateN)
        dict.updateValue(newMortTermTextField.toDouble, forKey: key.mortgageTerm)
        dict.updateValue(closingFinanceTextField.toDouble, forKey: key.closingCostFinanced)
        dict.updateValue(costUpFrontTextField.toDouble, forKey: key.closingCostUpFront)
 
        dict.updateValue(fundRefinancedTextField.toDouble, forKey: key.excessFunds)
        dict.updateValue(intEarnedAccummulativeTextField.toDouble, forKey: key.interestAccumulated)
        dict.updateValue(intLostPrepaidClosingCostTextField.toDouble, forKey: key.interestLost)
        dict.updateValue(futureValueExcessFundsTextField.toDouble, forKey: key.fundsInvested)
        dict.updateValue(paymentPerYearNewTextField.toDouble, forKey: key.payPerYearN)
        return dict
    }
}
extension RefinanceTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text?.replacingOccurrences(of: "%", with: "")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == interestRateNewTextField || textField == interestRateExistTextField || textField == intEarnedAccummulativeTextField || textField == intLostPrepaidClosingCostTextField || textField == futureValueExcessFundsTextField{
            textField.text = textField.text?.toNumber.toPercentage
        }
    }
    @IBAction func didChangeTextField(_ sender: UITextField){
        sender.text = sender.text?.toNumber.readableFormat
    }
}
