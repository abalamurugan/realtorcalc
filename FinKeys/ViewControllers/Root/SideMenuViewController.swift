//
//  SideMenuViewController.swift
//  RealtorCalc
//
//  Created by Bala on 14/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SideMenu

protocol SideMenuViewControllerDelegate {
    func menuItemDidSelected(_ item : MenuItem)
}

class SideMenuViewController: UIViewController, StoryboardSegueIdentifier {
    static var segueIdentifier: String = "showSideMenu"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    lazy var dataModel : SideMenuDataModel = {return SideMenuDataModel()}()
    
    var delegate : SideMenuViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataMapping()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    private func dataMapping(){
        self.nameLabel.text = UserManager.instance.userModel?.name
        self.emailLabel.text = UserManager.instance.userModel?.email
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SideMenuViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.reuseIdentifier, for: indexPath) as! SideMenuTableViewCell
        cell.titleLabel.text = dataModel[indexPath].title
        cell.iconImageView.image = dataModel[indexPath].icon
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      /*  if let type = dataModel[indexPath].type{
            if type == .financialKey{
                let root = Storyboard.main.storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                root.selectedIndex  = 2
                if let _menu = self.parent as? UISideMenuNavigationController{
//                    print("ddsds")
//                    if let navRoot = _menu.tabBarController?.viewControllers!.first as? UINavigationController{
//                        print("nav root ")
//                    }
                }
            }
        }*/
         
        self.dismiss(animated: true, completion: {
            self.delegate?.menuItemDidSelected(self.dataModel[indexPath])
        })
    }
}
