//
//  BaseViewController.swift
//  RealtorCalc
//
//  Created by balamurugan on 23/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit
import SideMenu
enum DropDownAPIType: String {
    case paymentType = "paymentBox"
    case loanPlan = "loanTypeBox"
    case acBox = "acBox"
    case statusBox = "statusBox"
    case employmentBox = "employmentBox"
    case yesnoBox = "yesnoBox"
    case mlsBox = "mlsBox"

}

class BaseViewController: UIViewController {
    
    @IBOutlet weak var contactNameTextField: UITextField!
    
    var contactModel: ContactInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    func activityIndicator(_ title: String = "") {
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView.init(style: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        effectView.addSubview(activityIndicator)
        effectView.addSubview(strLabel)
        view.addSubview(effectView)
    }
    var loadingIndicator: Bool = false{
        didSet{
            if loadingIndicator{
                self.activityIndicator()
            }else{
                self.effectView.removeFromSuperview()
            }
        }
    }
    func titleLogo(){
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    func setupLeftMenu(){
        let leftButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "icn_menu"), style: .plain, target: self, action: #selector(openMenu(_ :)))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
    }
    func setupLeftBackButton(){
        let leftButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "icn_close"), style: .plain, target: self, action: #selector(backAction(_ :)))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
    }
    func setupRightBarButton(buttonTitle: String){
        let leftButton : UIBarButtonItem = UIBarButtonItem.init(title: buttonTitle, style: .plain, target: self, action: #selector(rightBarButtonAction(_:)))
        leftButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = leftButton
    }
    func rightBarButtonWithIcon(image: UIImage){
        let rightButton: UIBarButtonItem = UIBarButtonItem.init(image: image, style: .plain, target: self, action: #selector(rightBarButtonAction(_:)))
        rightButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = rightButton
    }
    @objc func backAction(_ sender : UIBarButtonItem){
        self.dismiss(animated: true, completion: nil)
    }
    @objc func openMenu(_ sender : UIBarButtonItem){
         self.present(SideMenuManager.defaultManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    func showError(_ error: Any?, title: String = ""){
        let heading = title == "" ? "Error" : title
        if let _error = error as? String{
            self.showAlert("", _error)
        }else if let _err = error as? Error{
            self.showAlert(heading, _err.localizedDescription)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BaseViewController{
    @objc func rightBarButtonAction(_ sender: UIBarButtonItem){
        
    }
    @IBAction func contactPickerAction(_ sender: UIButton){
       
    }
   
    
}
//
//// MARK: - ContactListViewControllerDelegate
//extension BaseViewController: ContactListViewControllerDelegate{
//    func didPickedContact(viewController: UIViewController, item: ContactInfoModel) {
//        self.contactModel = item
//        self.contactNameTextField.text = item.fullName
//    }
//}
