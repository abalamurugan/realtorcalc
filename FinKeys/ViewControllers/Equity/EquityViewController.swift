//
//  EquityViewController.swift
//  RealtorCalc
//
//  Created by Bala on 28/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import UIKit

class EquityViewController: BaseViewController, StoryboardSegueIdentifier {
    
    static var segueIdentifier: String = "showEquityView"
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var totalAmountLabel : UILabel!
    @IBOutlet weak var totalView : UIView!
    @IBOutlet weak var equityButton : UIButton!

    var isHideTotalView: Bool = false
    var isHideEquityButton: Bool = true
    class func showEquity()->EquityViewController{
        return Storyboard.homeQualify.storyboard.instantiateViewController(withIdentifier: "EquityViewController") as! EquityViewController
    }
    lazy var dataModel : EquityDataModel = {return EquityDataModel()}()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.dataMapping()
    
        // Do any additional setup after loading the view.
    }
    override func rightBarButtonAction(_ sender: UIBarButtonItem) {
        self.showPDF()
    }
    private func showPDF(){
        if dataModel.type == .rentBuy{
            self.getPDf(reportName: Constants.reportNames.rentBuy)
        }else if dataModel.type == .maximumQualification{
            self.getPDf(reportName: Constants.reportNames.maxQualification)
        }else if dataModel.type == .transitionEquity{
            self.getPDf(reportName: Constants.reportNames.transitionEquity)
        }
    }
    func setupUI(){
        if dataModel.isEquity{
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.estimatedRowHeight = 55
        }
        self.setupLeftBackButton()
        tableView.register(EquityBuildupTableViewCell.nib, forCellReuseIdentifier: EquityBuildupTableViewCell.reuseIdentifier)//Register reusable cell
        tableView.register(EquityBuildUpThreeTableViewCell.nib, forCellReuseIdentifier: EquityBuildUpThreeTableViewCell.reuseIdentifier)
        if self.dataModel.equityList != nil{
            self.totalView.isHidden = true
        }
        totalView.isHidden = isHideTotalView
        equityButton.isHidden = isHideEquityButton
        self.configurePrint()
        self.tableView.tableFooterView = UIView()
    }
    private func configurePrint(){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.userId : UserManager.instance.userID, APIKey.common.key.tag : Constants.tagValue.printOption]
        connection.networkRequest(urlPath: nil, method: .get, param: param,encoding: .queryString, { (response: ResponseModel<PrintPaymentModel>) in
            let model = response.item?.response
            let isRightButton = (model?.isPrintAvailable ?? "NO").caseInsensitiveCompare("YES") == .orderedSame ? true : false
            if isRightButton{
                self.setupRightBarButton(buttonTitle: LocalizableKey.reports.print)
            }
        }) { (error) in
            
        }
    }
    func dataMapping(){
        self.title = dataModel.title
        if let newHome = dataModel.model as? HomeQualifyModel{
            self.title = newHome.title
            self.totalAmountLabel.text = newHome.price?.readableFormat
        }
    }
    @IBAction func equityAction(_ sender: UIButton){
        let equityController = EquityViewController.showEquity()
        equityController.dataModel.equityList = self.dataModel.equityList
        equityController.dataModel.title = LocalizableKey.navigationTitle.equityBuildUp
        equityController.isHideTotalView = true
        equityController.dataModel.isEquity = true
        equityController.contactModel = self.contactModel
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: equityController)
        self.present(navigationController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EquityViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if dataModel.isEquity{
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataModel.isEquity{
            if section == 0{
                return 1
            }
            return dataModel.equityList?.count ?? 0
        }
        return dataModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataModel.isEquity{
            let cell = tableView.dequeueReusableCell(withIdentifier: EquityBuildUpThreeTableViewCell.reuseIdentifier, for: indexPath) as! EquityBuildUpThreeTableViewCell
            if indexPath.section == 0{
                cell.equityBackgroundView.layer.backgroundColor = UIColor.primaryColor.cgColor
                cell.setTitle()
                cell.configUI(indexPath: indexPath)
                return cell
            }
            cell.configUI(indexPath: indexPath)
            cell.set(dataModel.equityList![indexPath.row])
            let bgColor = indexPath.row % 2 == 0 ? UIColor.init(hexString: "F8F9F9") : UIColor.white
            cell.equityBackgroundView.layer.backgroundColor = bgColor.cgColor
            cell.countLabel.text = "\(indexPath.row)"
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: EquityBuildupTableViewCell.reuseIdentifier, for: indexPath) as! EquityBuildupTableViewCell
        let item = dataModel[indexPath]
        cell.set(item)
        if item.isHeading ?? false{
            cell.equityBackgroundView.layer.backgroundColor = UIColor.white.cgColor
            cell.titleLabel.font = UIFont.helveticaNeue(size: 14, weight: .bold)
        }else{
            let bgColor = indexPath.row % 2 == 0 ? UIColor.init(hexString: "F8F9F9") : UIColor.white
            cell.equityBackgroundView.layer.backgroundColor = bgColor.cgColor
            cell.titleLabel.font = UIFont.init(name: "HelveticaNeue", size: 12)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dataModel.isEquity ? 35 : UITableView.automaticDimension
    }
}
extension EquityViewController{
    func pdfParm(reportName: String)->Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict.updateValue(Constants.pdfDetect, forKey: APIKey.pdf.key.detect)
        dict.updateValue(contactModel!.contactId!, forKey: APIKey.pdf.key.contactId)
        dict.updateValue(reportName, forKey: APIKey.pdf.key.name)
        dict.updateValue(Constants.tagValue.reportPdf, forKey: APIKey.pdf.key.tag)
        return dict
    }
    private func getPDf(reportName: String){
        LoadingView.instance.showActivityIndicator(uiView: self.view)
        let connection = NetworkManager.init()
        connection.networkRequest(urlPath: nil, method: .get, param: pdfParm(reportName: reportName), encoding: .queryString, { (response: ResponseModel<PDFModel>) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            if let pdfURL = response.item?.pdfURL, pdfURL != ""{
                UIApplication.shared.open(URL(string : pdfURL)!, options: [:], completionHandler: { (status) in
                })
            }
        }) { (error) in
            LoadingView.instance.hideActivityIndicator(uiView: self.view)
            self.showError(error)
        }
    }
    
}
