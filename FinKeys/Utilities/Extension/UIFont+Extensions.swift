//
//  UIFont+Extensions.swift
//  RealtorCalc
//
//  Created by balamurugan on 14/06/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit
enum HelveticaNeueFontWeight: String{
    case regular = "Regular"
    case black = "Black"
    case light = "Light"
    case boldItalic = "BoldItalic"
    case lightItalic = "LightItalic"
    case thin = "Thin"
    case mediumItalic = "MediumItalic"
    case medium = "Medium"
    case bold = "Bold"
    case blackItalic = "BlackItalic"
    case italic = "Italic"
    case thinItalic = "ThinItalic"
}

extension UIFont{
    static func helveticaNeue(size: CGFloat,weight: HelveticaNeueFontWeight) -> UIFont?{
        return UIFont.init(name: "HelveticaNeue-\(weight.rawValue)", size: size)
    }
}
