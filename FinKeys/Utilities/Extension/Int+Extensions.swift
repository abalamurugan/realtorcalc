
//
//  Int+Extension.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

extension Double{
    
    var toString : String{
        return "\(self)"
    }
    var readableFormat: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.groupingSize = 3
        numberFormatter.secondaryGroupingSize = 3
        return "$\(numberFormatter.string(from: NSNumber.init(value: self)) ?? "\(self)")"
    }
    var readableWithoutCurrency: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.groupingSize = 3
        numberFormatter.secondaryGroupingSize = 3
        return "\(numberFormatter.string(from: NSNumber.init(value: self)) ?? "\(self)")"
    }
    var toPercentage : String{
        return "\(self.roundUpPercentage)%"
    }
    var removeMinus: Double{
        if self < 0{
            return -(self)
        }
        return self
    }
    var toInt: Int{
        return Int(self)
    }
}
extension String{
    var readableFormat: String{
        return self.toDouble.readableFormat
    }
    var readableWithoutCurrency: String{
        return self.toDouble.readableWithoutCurrency
    }
    var toDouble : Double{
        let charSet = CharacterSet.init(charactersIn: "0123456789.").inverted
        let value = self.removeCharacters(from: charSet)
        return Double(value) ?? 0
    }
    var toString : String{
        let charSet = CharacterSet.init(charactersIn: "0123456789.").inverted
        let value = self.removeCharacters(from: charSet)
        return value
    }
    var toNumber: Double{
        let charSet = CharacterSet.init(charactersIn: "0123456789.").inverted
        let value = self.removeCharacters(from: charSet)
        return value.toDouble
    }
    var toInt: Int{
        return Int(self) ?? 0
    }
}
extension Double{
    var roundUp: String{
        return String.init(format: "%.2f", self)
    }
    var roundUpPercentage: String{
        return String.init(format: "%.3f", self)
    }
    var clean: String{
        return String.init(format: "%.0f", self)
    }
    var toValue: Double{
        return self/100
    }
}
extension Int{
    var numberReadable: String{
        if self == 1{
            return "1st"
        }else if self == 2{
            return "2nd"
        }else if self == 3{
            return "3rd"
        }else if self == 4{
            return "4th"
        }else if self == 5{
            return "5th"
        }
        return "\(self)"
    }
}
