//
//  String+Extensions.swift
//  RealtorCalc
//
//  Created by balamurugan on 05/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation


extension String{
    var toJson: Dictionary<String, Any>?{
        let data = self.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            {
                return jsonArray
            } else {
                print("bad json")
                return nil
            }
        } catch let error as NSError {
            print("error===>\(error)")
            return nil
        }
    }
}
extension String {
    
    func removeCharacters(from forbiddenChars: CharacterSet) -> String {
        let passed = self.unicodeScalars.filter { !forbiddenChars.contains($0) }
        return String(String.UnicodeScalarView(passed))
    }
    
    func removeCharacters(from: String) -> String {
        return removeCharacters(from: CharacterSet(charactersIn: from))
    }
}
