//
//  Date+Extensions.swift
//  RealtorCalc
//
//  Created by Bala on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
// MARK: - Date
extension Date{
    
    /// convert date format to string
    ///
    /// - Parameter format: any date format
    /// - Returns: string type date
    func toString( format:String)->String?{
        let df = DateFormatter.init()
        df.dateFormat = format
        return df.string(from: self)
    }
    
}


// MARK: - String
extension String{
    /// convert string format to date
    ///
    /// - Parameter format: format: any date format
    /// - Returns: date
    func date( format:String)->Date?{
        let df = DateFormatter.init()
        df.dateFormat = format
        df.locale = Locale(identifier: "en_US_POSIX")
        //        df.dateFormat = "yyyy-MM-dd"
        //        df.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        //        df.timeZone = TimeZone.autoupdatingCurrent
        return df.date(from: self)
    }
    func toDateFormat( format:String)->Date?{
        let df = DateFormatter.init()
        df.dateFormat = format
        df.dateFormat = "yyyy-MM-dd"
        return df.date(from: self)
    }
    func floatValue() -> Float? {
        if let floatval = Float(self) {
            return floatval
        }
        return nil
    }
    var daysBetween: Int{
        let toDate = Date()
        var fromDate = self.date(format: "dd-MM-yyyy")
        if fromDate == nil{
            fromDate = self.date(format: "MM/dd/yyyy")
        }
        if let date = fromDate{
            return Calendar.current.dateComponents([.day], from: date, to: toDate).day ?? 0
        }
        return 0
    }
    
}
