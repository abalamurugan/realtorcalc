//
//  Dictionary+Extensions.swift
//  RealtorCalc
//
//  Created by balamurugan on 01/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

extension Dictionary{
    var isSuccess: Bool{
        if let json = self as? Dictionary<String, Any>{
            if let success = json["success"] as? Int,success == 200{
                return true
            }
        }
        return false
    }
    var error: String?{
        if let json = self as? Dictionary<String, Any>{
            if let success = json["success"] as? Int,success != 200{
                return json["error_message"] as? String
            }
        }
        return nil
    }
    var jsonToString: String?{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: .utf8) // the data will be converted to the string
            return convertedString
        } catch let myJSONError {
            print(myJSONError)
            return nil
        }
        
    }
}
extension Encodable {
    var toJson: Dictionary<String, Any>? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)?.toJson
        } catch {
            return nil
        }
    }
    var toJsonString: String? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}
