//
//  UIViewController+Extensions.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

extension UIViewController{
    
    func showAlert(_ title: String, _ subTitle : String, _ completionAction : (()->Void)?=nil){
        let alertController = UIAlertController.init(title: title, message: subTitle, preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
            completionAction?()
        }))
        present(alertController, animated: true, completion: nil)
        
        //        let alert = UIAlertController(title: title, message: subTitle, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        //        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, _ subTitle : String, actionOneTitle: String, actionTwoTitle: String, actionOnecompletion : @escaping (()->Void) , actionTwocompletion : @escaping (()->Void)){
        let alertController = UIAlertController.init(title: title, message: subTitle, preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: actionOneTitle, style: .default, handler: { (action) in
            actionOnecompletion()
        }))
        alertController.addAction(UIAlertAction.init(title: actionTwoTitle, style: .default, handler: { (action) in
            actionTwocompletion()
        }))
        present(alertController, animated: true, completion: nil)
        
        //        let alert = UIAlertController(title: title, message: subTitle, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        //        present(alert, animated: true, completion: nil)
    }
    
    func presentWithNavigationController(_ controller : UIViewController){
        let navigationController = UINavigationController.init(rootViewController: controller)
        self.present(navigationController, animated: true, completion: nil)
    }
    func sideMenuSetup(){
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        SideMenuManager.default.menuLeftNavigationController = storyboard.instantiateViewController(withIdentifier: "UISideMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        SideMenuManager.default.menuFadeStatusBar = true
        SideMenuManager.default.menuWidth = self.view.frame.width - 70
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        //        SideMenuManager.default.menuShadowRadius = 100
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.clear
        //        SideMenuManager.default.menuShadowOpacity = 1
        SideMenuManager.default.menuAnimationFadeStrength = 0.6
        //        SideMenuManager.default.menuShadowColor = UIColor.bla
        
    }
    func logout(){
        UserManager.instance.userModel = nil
        UserStorage.clearAll()
        let login = Storyboard.login.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(login, animated: true, completion: nil)
    }
    func showLoading(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    func hideLoading(){
        self.dismiss(animated: true, completion: nil)
    }
    func showBottomPicker(delegate: BottomPickerViewControllerDelegate, items: [Any], tag: Int? = nil, title: String = "Pick your choice"){
        let bottomPicker = BottomPickerViewController.showBottomPicker()
        bottomPicker.delegate = delegate
        bottomPicker.dataModel.tag = tag
        bottomPicker.dataModel.items = items
        bottomPicker.dataModel.pickerTitle = title
        self.present(bottomPicker, animated: true, completion: nil)
    }
    func showContactPicker(delegate: ContactListViewControllerDelegate){
        let contactPicker = ContactListViewController.showContactPicker()
        contactPicker.delegate = delegate
        contactPicker.dataModel.isPicker = true
        self.presentWithNavigationController(contactPicker)
    }
    func openMediaLibrary(delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate), button: UIButton){
        let picker = UIImagePickerController.init()
        picker.delegate = delegate
        let actionSheet = UIAlertController.init(title: "Image Source", message: "", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }
        let libraryAction = UIAlertAction.init(title: "Photo Library", style: .default) { (action) in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        actionSheet.addAction(cancelAction)
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = button
            presenter.sourceRect = button.bounds
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
}
