//
//  Storyboard.swift
//  RealtorCalc
//
//  Created by balamurugan on 29/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit

enum Storyboard : String{
    case main = "Main"
    case login = "Login"
    case output = "Output"
    case homeQualify = "HomeQualify"
    case loanCompare = "LoanComparison"
    case reusable = "Reusable"
    case contact = "Contact"
    case financeOption = "FinancialOption"
    case MarketAnalysis = "MarketAnalysis"
    var storyboard : UIStoryboard{
        let story = UIStoryboard.init(name: self.rawValue, bundle: Bundle.main)
        return story
    }
}



class NavigationPresentModel:UIStoryboardSegue{
    override func perform() {
        let navi = UINavigationController.init(rootViewController: self.destination)
        self.source.present(navi, animated: true, completion: nil)
    }
}
