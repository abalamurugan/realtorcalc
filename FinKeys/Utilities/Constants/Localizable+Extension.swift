//
//  Localizable+Extension.swift
//  RealtorCalc
//
//  Created by balamurugan on 29/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

extension LocalizableKey{
    static let navigationTitle = LocalizableKey.titles.navigation.self
    static let errorMessage = LocalizableKey.error.self
    static let amortizationValidation = LocalizableKey.validation.amortization.self
    static let financialValidation = LocalizableKey.validation.financialOption.self
    static let loginValidation = LocalizableKey.validation.login.self
    static let commonValidation = LocalizableKey.validation.common.self
    static let commonMessages = LocalizableKey.validation.message.self
    static let commonTitle = LocalizableKey.validation.title.self
    static let financingTitle = LocalizableKey.titles.financingOption.self

}
