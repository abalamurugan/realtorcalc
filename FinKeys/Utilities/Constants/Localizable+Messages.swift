//
//  Localizable+Messages.swift
//  RealtorCalc
//
//  Created by balamurugan on 30/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
extension LocalizableKey{
    struct error {
        static let errorTitle = "Error"
        static let validationFailed = "Validation Failed"
    }
}
