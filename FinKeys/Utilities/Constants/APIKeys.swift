//
//  APIKeys.swift
//  RealtorCalc
//
//  Created by Bala on 07/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

struct APIKey {
    struct authenticate {
        struct key {
            static let email = "email"
            static let password = "password"
            static let tag = "tag"
            static let enterEmail = "enterEmail"
            static let newPass = "newPass"
            static let confirmPass = "confirmPass"
            static let enterOtp = "enterOtp"
            static let phone = "phone"
            static let firstname = "firstname"
            static let txtTitle = "txtTitle"
            static let cellNo = "cellNo"
            static let telephone = "telephone"
            static let txtFax = "txtFax"
            static let txtAddress = "txtAddress"
            static let txtCity = "txtCity"
            static let txtState = "txtState"
            static let txtZip = "txtZip"
            static let mlsId = "mlsId"
            static let lastname = "lastname"
        }
    }
    struct contact {
        struct key {
            static let userId = "userId"
            static let tag = "tag"
            static let contTitle = "contTitle"
            static let contfirstNme = "contfirstNme"
            static let contlastNme = "contlastNme"
            static let contcmpyNme = "contcmpyNme"
            static let contcellNo = "contcellNo"
            static let contphneNo = "contphneNo"
            static let conttxtAddress = "conttxtAddress"
            static let conttxtFax = "conttxtFax"
            static let conttxtCity = "conttxtCity"
            static let conttxtState = "conttxtState"
            static let conttxtZip = "conttxtZip"
            static let contNotes = "contNotes"
            static let contEmail = "contEmail"
        }
    }
    struct profile {
        struct key {
            static let email = "email"
            static let userId = "userId"
            static let txtTitle = "txtTitle"
            static let firstNme = "firstNme"
            static let lastNme = "lastNme"
            static let company_name = "company_name"
            static let cellNo = "cellNo"
            static let phneNo = "phneNo"
            static let txtFax = "txtFax"
            static let txtAddress = "txtAddress"
            static let txtCity = "txtCity"
            static let txtState = "txtState"
            static let txtZip = "txtZip"
            static let paymentStatus = "paymentStatus"
            static let mlsId = "mlsId"
        }
    }
    struct amortization{
        struct key {
            static let mtgAmount = "mtgAmount"
            static let loanType = "loanType"
            static let interestRate = "interestRate"
            static let periodicInterest = "periodicInterest"
            static let lifeTimeRate = "lifeTimeRate"
            static let loanTerm = "loanTerm"
            static let payPerYr = "payPerYr"
            static let paymentMade = "paymentMade"
            static let datepicker = "datepicker"
            static let extraPrincipalAmt = "extraPrincipalAmt"
            static let currentDate = "currentDate"
            static let paymentType = "paymentType"
        }
    }
    struct common {
        struct key {
            static let userId = "userId"
            static let contactId = "contactId"
            static let tag = "tag"
            static let name = "name"
            static let action = "action"
            static let createdDate = "currentDate"
            static let currentDate = "currentDate"
        }
    }

}
