//
//  Config.swift
//  RealtorCalc
//
//  Created by Bala on 07/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class Config{
//    static let base_url = "http://appbox.website/rc/webservice/common_recv.php"
//    static let basePath = "http://appbox.website/rc/"
    static let base_url = "http://myfinancialkeys.com/FK/webservice/common_recv.php"
    static let basePath = "http://myfinancialkeys.com/FK/"
    
}

struct Constants {
    struct tagValue {
        static let login = "login"
        static let contact = "contactList"
        static let contactAdd = "contactAdd"
        static let reportPdf = "reportPdf"
        static let getProfile = "profileview"
        static let profileUpdate = "profileUpdate"
        static let otpGenerate = "OTPGenerate"
        static let otpVerify = "OTPVerify"
        static let passwordChange = "passwordChange"
        static let register = "register"
        //Get report records from DB using this tag
        static let amortizationView = "amortizationView"
        static let financingView = "financingView"
        static let loancomparsionView = "loancomparsionView"
        static let rentvsbuyView = "rentvsbuyView"
        static let transitionEquityView = "transitionEquityView"
        static let armvsfixedView = "armvsfixedView"
        static let refianceView = "refianceView"
        static let buydownView = "buydownView"
        static let newhomeview = "newhomeview"
        static let investmentView = "investmentView"
        static let maximumView = "maximumView"
        static let CMAView = "CMAView"
        static let lenderview = "lenderview"
        static let lenderAdd = "lenderAdd"
        //Update report values to db
        static let amortizationAdd = "amortizationAdd"
        static let financingAdd = "financingAdd"
        static let transitionEquityAdd = "transitionEquityAdd"
        static let buydownAdd = "buydownAdd"
        static let maximumAdd = "maximumAdd"
        static let armvsfixedAdd = "armvsfixedAdd"
        static let CMAAdd = "CMAAdd"
        static let rentvsbuyAdd = "rentvsbuyAdd"
        static let loancompasionAdd = "loancompasionAdd"
        static let refinanceAdd = "refinanceAdd"
        static let investmentAdd = "investmentAdd"
        static let newhomeAdd = "newhomeAdd"
        static let acBox = "acBox"
        static let MLSData = "MLSData"
        static let printOption = "printOption"

    }
    struct reportNames {
        static let amortization = "amortization"
        static let financeOption = "financing_option"
        static let transitionEquity = "transition_Equity_to_new_Home"
        static let buydown = "buy_down_strategy"
        static let maxQualification = "maximum_Qualifying"
        static let armFixed = "arm_vs_fixed"
        static let newHome = "new_home_qualifying"
        static let CMA = "comparative_market"
        static let lender = "lender_comparsion"
        static let rentBuy = "rent_vs_buy"
        static let loanComparison = "loan_comparsion"
        static let refinance = "refinance"
        static let investment = "investment_analysis"
        
    }
    struct uploadFileName {
        static let addContact = "contact_file"
        static let profilepic = "profilepic"

    }
    static var loanPlan: [BottomPickerModel]{
        return  [BottomPickerModel.init(key: "Conventional", value: "Conventional"), BottomPickerModel.init(key: "VA", value: "VA"), BottomPickerModel.init(key: "FHA", value: "FHA")]
    }
    static var paymentType: [BottomPickerModel]{
        return  [BottomPickerModel.init(key: "Fixed", value: "Fixed"), BottomPickerModel.init(key: "ARM", value: "ARM")]
    }
    static var employStatus: [BottomPickerModel]{
        return  [BottomPickerModel.init(key: "Employed", value: "Employed"), BottomPickerModel.init(key: "Self-Employ", value: "Self-Employ")]
    }
    static var maritalStatus: [BottomPickerModel]{
        return  [BottomPickerModel.init(key: "Single", value: "Single"), BottomPickerModel.init(key: "Married", value: "Married")]
    }
    static let pdfDetect = "mobile"
    static let dateFormat = "dd-MM-YYYY"
}
struct Param {
    static func viewReportParam(tagReport: String, contactId: String)->Dictionary<String, Any>{
        var dict: Dictionary<String, Any> = [:]
        dict.updateValue(contactId, forKey: APIKey.common.key.contactId)
        dict.updateValue(UserManager.instance.userID, forKey: APIKey.common.key.userId)
        dict.updateValue(tagReport, forKey: APIKey.common.key.tag)
        return dict
    }
}
