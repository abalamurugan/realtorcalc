//
//  LocalizableKey.swift
//  RealtorCalc
//
//  Created by balamurugan on 29/03/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

struct LocalizableKey {
    struct reports{
        static let print = "Print"
    }
    struct titles {
        struct navigation {
            static let realtorCalc = "Realtor Calc"
            static let amortization = "Amortization"
            static let output = "Output"
            static let addContact = "Add Contact"
            static let financeOption = "Finance Option"
            static let arm = "ARM vs Fixed"
            static let profile = "Edit Profile"
            static let newHome = "New Home Qualify"
            static let reFinance = "Refinance"
            static let rentBuy = "Rent vs Buy"
            static let repurchase = "Transition Equity"
            static let loanComparison = "Loan Comparison"
            static let lenderComparison = "Lender Comparison"
            static let buyDown = "Buydown Strategy Report"
            static let maxQualify = "Maximum Qualification"
            static let marketAnalysis = "Market Analysis"
            static let investment = "Investments"
            static let equityBuildUp = "Equity Buildup"
            static let contact = "Contacts"
            static let account = "Account"
            static let forgotPassword = "Forgot Password"
            static let changePassword = "Change Password"
            static let adjustable = "Adjustable"
        }
        struct financingOption {
            static let loanPlan = "Choose your loan plan"
            static let paymentType = "Choose your payment type"
        }
        
    }
    struct cma {
        static let acBox = "AC Box"
        static let statusType = "Status Type"
        static let hotTub = "Hot Tub"
        static let secSys = "Sec Sys"
        static let pool = "Pool"
        static let address = "Address"
        static let proximity = "Proximity"
        static let status = "Status"
        static let dateSold = "Date Sold"
        static let style = "Style"
        static let yearBuilt = "Year Built"
        static let acres = "Acres"
        static let landscape = "Landscape"
        static let lotValue = "Lot Value"
        static let garage = "Garage"
        static let carport = "Carport"
        static let sqTotal = "Sq. Ft. Total"
        static let sqMain = "Sq Ft Main"
        static let sqUp = "Sq Ft Up"
        static let sqDown = "Sq Ft Down"
        static let finishDown = "% Finish Dn"
        static let bedsTotal = "Beds Total"
        static let bedsMain = "Beds Main"
        static let bedsUp = "Beds Up"
        static let bedsDn = "Beds Dn"
        static let f1FullBath = "F1: Full Baths"
        static let f1HalfBath = "F1: 1/2 Baths"
        static let f1TFBath = "F1: 3/4 Baths"
        static let f2FullBath = "F2: Full Baths"
        static let f2HalfBath = "F2: 1/2 Baths"
        static let f2TFBath = "F2: 3/4 Baths"
        static let bFullBath = "B: Full Baths"
        static let bHalfBath = "B: 1/2 Baths"
        static let bTFBath = "B: 3/4 Baths"
        static let bathsTotal = "Bath Total"
        static let fireplaces = "Fireplaces"
        static let ac = "A/C"
        static let condition = "Condition."
        static let view = "View"
        static let concession = "Concession"
        static let listedPrice = "Listed Price"
        static let sold = "Sold"
        static let adjusted = "Adjusted"
        static let comparableOne = "Comparable 1 of 3"
        static let comparableTwo = "Comparable 2 of 3"
        static let comparableThree = "Comparable 3 of 3"
        static let subjectProperty = "Subject Property"

    }
    struct validation {
        struct amortization {
            static let mtgInvalid = "Please enter Mtg. amount to continue"
            static let interestInvalid = "Please enter interest percentage to continue"
            static let loanTermInvalid = "Please enter loan term to continue"
            static let lifeTimeInvalid = "Please enter interest percentage to continue"
            static let paymentMadeInvalid = "Please enter No. payment made to continue"
            static let paymentPerYearInvalid = "Please enter payment per year to continue"
            static let periodicInterestInvalid = "Please enter periodic interest made to continue"
            static let dateInvalid = "Please enter payment starts on date"

        }
        struct financialOption {
            static let purchaseInvalid = "Please enter purchase amount to continue"
            static let mtgInvalid = "Please enter atlease one Mtg. amount to continue"
        }
        struct login {
            static let emailInvalid = "Please enter valid email to continue"
            static let passwordInvalid = "Please enter valid password to continue"
            static let passwordNotMatch = "Confirm password doesn't match with new password"

        }
        struct homeQualify {
            static func inValid(_ fieldName : String)-> String{
                return "Please enter valid \(fieldName) to continue"
            }
        }
        struct common {
            static func inValid(_ fieldName : String)-> String{
                return "Please enter valid \(fieldName) to continue"
            }
        }
        struct message {
            static let logout = "Are you sure you want to logout?"
        }
        struct title {
            static let logout = "logout"
        }
        
    }
}

