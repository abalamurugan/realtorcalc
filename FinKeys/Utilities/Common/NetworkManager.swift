//
//  NetworkManager.swift
//  RealtorCalc
//
//  Created by Bala on 07/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
 
//enum HTTPMethod : String{
//    case get = "GET"
//    case post = "POST"
//    case put = "PUT"
//    case update = "UPDATE"
//}

class NetworkManager{
    
    func sendRequest(urlPath : String? = nil, method: HTTPMethod = .get, param : Dictionary<String, Any>? = nil, imageData: Data? = nil, imageName: String? = nil, encoding : URLEncoding? = nil, _ success : @escaping((_ json:Dictionary<String, Any>, _ data: Data?) -> Void),_ failure : @escaping(Any?) -> Void){
        var url : String = Config.base_url
        if let path = urlPath{
            url += path
        }
        print("URLPATH======>",url)
        print("PARAM======>",param ?? [:])
        if imageData != nil{
            self.uploadFile(url: url, param: param, imageData: imageData!, imageName: imageName, { (json, data) in
                success(json, data)
            }) { (error) in
                failure(error)
            }
            return
        }
//        Alamofire.request(url, method: method, parameters: param, encoding: encoding ?? URLEncoding.queryString, headers: Header.getHeader()).responseString { (respnse) in
//
//            print(respnse)
//        }
        Alamofire.request(url, method: method, parameters: param, encoding: encoding ?? URLEncoding.queryString, headers: Header.getHeader()).validate().responseJSON { (response) in
            if let _response = response.result.value{
                if let json = _response as? Dictionary<String,Any>{
                    if json.isSuccess{
                        success(json, response.data)
                    }else{
                        failure(json.error)
                    }
                }
            }else{
                failure(response.result.error)
            }
        }
    }
    private func uploadFile(url: String, param: Dictionary<String, Any>? = nil, imageData: Data, imageName: String? = nil, _ success : @escaping((_ json:Dictionary<String, Any>, _ data: Data?) -> Void),_ failure : @escaping(Any?) -> Void){
        Alamofire.upload(multipartFormData: { (multipart) in
            if let _param = param{
                for (key, value) in _param {
                    multipart.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
             }
            
            multipart.append(imageData, withName: imageName ?? "image", fileName: "image.png", mimeType: "image/png")
        }, usingThreshold: .init(), to: url, method: .post, headers: Header.getHeader()) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    if let _response = response.result.value{
                        if let json = _response as? Dictionary<String,Any>{
                            if json.isSuccess{
                                success(json, response.data)
                            }else{
                                failure(json.error)
                            }
                        }
                    }else{
                        failure(response.result.error)
                    }
                })
                break
            case .failure(let error):
                failure(error)
                break
            }
        }
    }
    func networkRequest<T>(urlPath : String? = nil, method: HTTPMethod = .get, param : Dictionary<String, Any>? = nil,encoding : URLEncoding? = nil, _ model: @escaping((ResponseModel<T>)->Void),_ failure : @escaping(Any?) -> Void){
        var url : String = Config.base_url
        if let path = urlPath{
            url += path
        }
        print("URLPATH======>",url)
        print("PARAM======>",param ?? [:])
        Alamofire.request(url, method: method, parameters: param, encoding: encoding ?? JSONEncoding.default, headers: Header.getHeader()).validate().responseJSON { (response) in
            do{
                if let _response = response.result.value{
                    if let json = _response as? Dictionary<String,Any>{
                        if json.isSuccess{
                            let decoder = JSONDecoder()
                            var modelClass = ResponseModel<T>.init()
                            modelClass.item = try decoder.decode(T.self, from: response.data!)
                            modelClass.json = json
                            model(modelClass)
                        }else{
                            failure(json.error)
                        }
                    }
                }else{
                    failure(response.result.error)
                }
            }catch let error{
                print("error===>",error)
                failure(error.localizedDescription)
            }
            
        }
    }
    
}
class Header{
    static func getHeader()->HTTPHeaders{
        return ["appid":"f23d38aa0a15699a63091862f3473994", "Content-Type": "application/json"]
    }
}
struct ResponseModel<T: Codable> {
    var item: T?
    var json: Dictionary<String, Any>?
}
