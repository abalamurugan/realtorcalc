//
//  CommonRequest.swift
//  FINANCIALKeys
//
//  Created by balamurugan on 06/09/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class CommonRequest {
    class func dropDownAPI(type: DropDownAPIType, model: @escaping((Array<DropDownInfoModel>?)->Void)){
        let connection = NetworkManager.init()
        let param: Dictionary<String, Any> = [APIKey.common.key.tag:type.rawValue]
        connection.networkRequest(urlPath: nil, method: .get, param: param, encoding: .queryString, { (response: ResponseModel<DropdownModel>) in
            model(response.item?.response?.data)
        }) { (error) in
            model(nil)
            print("error in drop down list api")
        }
    }
}
