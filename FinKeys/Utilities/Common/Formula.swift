//
//  Formula.swift
//  RealtorCalc
//
//  Created by Bala on 06/04/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

class Formula {
    static let FINANCIAL_MAX_ITERATIONS = 100
    static let FINANCIAL_ACCURACY = 1.0e-6
    static func calculatePMT(_ interestRate: Double, _ paymentPerYear: Double, _ loanTerm : Double, _ loanMtgAmount : Double, _ type : Double = 1)-> Double{
        let interest = interestRate / 100
        let powA = (1 + interest/paymentPerYear)
        let powB = -1 * loanTerm * paymentPerYear
        let exp3 = (1 - (pow(powA, Double(powB))))
        let expressionFirst = (loanMtgAmount * (interest / paymentPerYear))
        let expA = (expressionFirst / exp3)
        return expA
    }
    static func calculateIPMT(_ interestRate: Double, _ paymentPerYear: Double, _ loanTerm : Double, _ loanMtgAmount : Double,_ type : Double = 1)-> Double{
        let rate = interestRate/100
        let pmt = self.calculatePMT(interestRate, paymentPerYear, loanTerm, loanMtgAmount)
        let exp = -(loanMtgAmount * pow(1 + rate/paymentPerYear, type - type) * rate/paymentPerYear + pmt * (pow(1 + rate/paymentPerYear, type - type) - 1))
        return exp
    }
    static func PPMTCalculation(_ interestRate: Double, _ paymentPerYear: Double, _ loanTerm : Double, _ loanMtgAmount : Double)-> Double{
        let rate = interestRate/100
        let pmt = self.calculatePMT(interestRate, paymentPerYear, loanTerm, loanMtgAmount)
        let ipmt = -(loanMtgAmount * pow(1 + rate/paymentPerYear, 1 - 1) * rate/paymentPerYear + pmt * (pow(1 + rate/paymentPerYear, 1 - 1) - 1))
        return pmt - ipmt
    }
    static func FVCalculation(_ interestRate: Double, _ paymentPerYear: Double, _ loanTerm : Double, _ loanMtgAmount : Double, _ type: Double = 0)-> Double{
        let rate = interestRate/100
        let pvif = (pow(1 + rate/paymentPerYear, paymentPerYear));
        let fvif = (pow(1 + rate/paymentPerYear, paymentPerYear) - 1) / rate/paymentPerYear
        let pmt = self.calculatePMT(interestRate, paymentPerYear, loanTerm, loanMtgAmount)
        let fv = (-((loanMtgAmount * pvif) + pmt *
            (1 + rate/paymentPerYear * type) * fvif));
        
        return fv
    }
    static func rateCalculation(term: Double, paymentPerYear: Double, payment: Double, loanAmount: Double)->Double{
        let _term = term * paymentPerYear
//        function rate($month, $payment, $amount)
//        {
            // make an initial guess
            let error = 0.0000001
            var high = 1.00
            var low = 0.00
            var rate = (2.0 * (_term * payment - loanAmount)) / (loanAmount * _term);
            
            while(true) {
                // check for error margin
                var calc = pow(1 + rate, _term);
                calc = (rate * calc) / (calc - 1.0);
                calc -= payment / loanAmount;
                
                if (calc > error) {
                    // guess too high, lower the guess
                    high = rate;
                    rate = (high + low) / 2;
                } else if (calc < -error) {
                    // guess too low, higher the guess
                    low = rate;
                    rate = (high + low) / 2;
                }else{
                    break
                }
            }
            
            return rate * 12 * 100 ;
//        }
    }
   
    static private func pvif(rate: Double, nper: Double)->Double{
        return (pow(1 + rate, nper));
    }
    static private func fvif(rate: Double, nper: Double)->Double{
        // Removable singularity at rate == 0
        if rate == 0{
            return nper
        }
        // FIXME: this sucks for very small rates
        return (pow(1 + rate, nper) - 1) / rate;
    }
    static func PVCalculation(rate: Double, nper: Double, pmt: Double, fv: Double = 0.0, type: Double = 0)->Double{
        let pvif = self.pvif(rate: rate, nper: nper)
        let fvif = self.fvif(rate: rate, nper: nper)
        if pvif == 0{
            return 0
        }
        let pv = ((-fv - pmt * (1.0 + rate * type) * fvif) / pvif)
        return pv.isFinite ? pv : 0
    }
    
    /// FVReturns the future value of an investment based on periodic, constant payments and a constant interest rate. For a more complete description of the arguments in FV, see the PV function.
    ///
    /// - Parameters:
    ///   - rate: is the interest rate per period.
    ///   - nper: is the total number of payment periods in an annuity.
    ///   - pmt: is the payment made each period; it cannot change over the life of the annuity. Typically, pmt contains principal and interest but no other fees or taxes. If pmt is omitted, you must include the pv argument.
    ///   - fv: is the present value, or the lump-sum amount that a series of future payments is worth right now. If pv is omitted, it is assumed to be 0 (zero), and you must include the pmt argument.
    ///   - type: is the number 0 or 1 and indicates when payments are due. If type is omitted, it is assumed to be 0. 0 or omitted, At the end of the period 1, At the beginning of the period
    /// - Returns: Future Value
    static func FVCalc(rate: Double, nper: Double, pmt: Double, fv: Double = 0.0, type: Double = 0)->Double{
        let pvif = self.pvif(rate: rate, nper: nper)
        let fvif = self.fvif(rate: rate, nper: nper)
        let futureValue = (-((fv * pvif) + pmt *
            (1.0 + rate * type) * fvif))
        return futureValue.isFinite ? futureValue : 0
    }
    static func IRRCalc(values: Array<Double>, guess: Double = 0.1)->Double?{
        if values.count < 1{
            return nil
        }
        
        // create an initial bracket, with a root somewhere between bot and top
        var x1 = 0.0
        var x2 = guess
        var f1 = self.NPV(rate: x1, values: values) ?? 0
        var f2 = self.NPV(rate: x2, values: values) ?? 0
        for _ in 0...FINANCIAL_MAX_ITERATIONS{
            if ((f1 * f2) < 0.0){
                break
            }
            if (abs(f1) < abs(f2)) {
                x1 += 1.6 * (x1 - x2)
                x2 += 1.6 * (x2 - x1)
                f1 = self.NPV(rate: x1, values: values) ?? 0
            } else {
                f2 = self.NPV(rate: x2, values: values) ?? 0
            }
        }
       
//        if ((f1 * f2) > 0.0){
//            return nil
//        }
        
        let f = self.NPV(rate: x1, values: values)
        var rtb = 0.0
        var dx = 0.0
        var x_mid = 0.0
        var f_mid = 0.0
        
        if ((f ?? 0) < 0.0) {
            rtb = x1;
            dx = x2 - x1;
        } else {
            rtb = x2;
            dx = x1 - x2;
        }
        for _ in 0...FINANCIAL_MAX_ITERATIONS{
            dx *= 0.5;
            x_mid = rtb + dx;
            f_mid = self.NPV(rate: x_mid, values: values) ?? 0
            if (f_mid <= 0.0){
                rtb = x_mid
            }
            if ((abs(f_mid) < FINANCIAL_ACCURACY) || (abs(dx) < FINANCIAL_ACCURACY)){
                return x_mid
            }
        }
        
        return nil;
    }
    
    /**
     * NPV
     * Calculates the net present value of an investment by using a
     * discount rate and a series of future payments (negative values)
     * and income (positive values).
     *
     *        n   /   values(i)  \
     * NPV = SUM | -------------- |
     *       i=1 |            i   |
     *            \  (1 + rate)  /
     *
     */
    static private func NPV(rate: Double, values: Array<Double>)->Double?{
        if values.count < 1{
            return nil
        }
        var npv = 0.0;
        for i in 0...values.count - 1{
            print("i value===>\(i)")
            let exp1 = values[i] / pow(1 + rate, Double(i + 1))
            npv += values[i] / (pow(1 + rate, Double(i + 1)));
        }
        return npv.isFinite ? npv: nil
    }
}
