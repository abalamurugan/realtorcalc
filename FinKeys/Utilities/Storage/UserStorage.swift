//
//  UserStorage.swift
//  RealtorCalc
//
//  Created by balamurugan on 01/07/19.
//  Copyright © 2019 BESS. All rights reserved.
//

import Foundation

enum StorageKey: String{
    case userDetails = "userDetails"
}

class UserStorage {
    
    static func clearAll(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
    
    static func setValues(value: Any, key: String){
        UserDefaults.standard.set(value, forKey: key)
    }
    static func setBool(bool: Bool, key: String){
        UserDefaults.standard.set(bool, forKey: key)
    }
    static func getValues(key: String)->Any?{
        return UserDefaults.standard.value(forKey: key)
    }
    static func getDictValues(key: String)->Dictionary<String, Any>?{
        let value = UserDefaults.standard.value(forKey: key)
        if let _value = value as? Dictionary<String,Any> {
            return _value
        }
        return nil
    }
    static func getUserDetails(key: String)->UserDetailsModel?{
        let value = UserDefaults.standard.value(forKey: key)
        if let _value = value as? Dictionary<String,Any> {
           return UserDetailsModel.init(dict: _value)
        }
        return nil
    }
}

